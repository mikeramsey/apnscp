<?php
require_once dirname(__DIR__, 1) . '/TestFramework.php';
	require_once(__DIR__ . '/SoapTestClient.php');

class SoapTest extends TestFramework {
    const COMMAND_MISSING = 'does not exist';

	public function setUp()
	{
		parent::setUp();

		if (!SOAP_ENABLED) {
			return $this->markTestSkipped('API support disabled');
		}

		if (APNSCPD_HEADLESS) {
			return $this->markTestSkipped('Panel running in headless mode');
		}
	}

	public function testMessageHandling()
	{
		if (!is_debug()) {
			return $this->markTestSkipped('Panel not in debug mode');
		}
		$key = $this->createKey();

		$client = SoapTestClient::create($key);
		$this->assertTrue($client->test_message_class('info', 'Hello'));
		$response = $client->__getLastResponse();
		$this->assertContains('Hello', simplexml_load_string($response)->xpath('SOAP-ENV:Header/ns2:Messages')[0]->asXml());
	}

	public function testErrorToleranceHeader()
	{
		if (!is_debug()) {
			return $this->markTestSkipped('Panel not in debug mode');
		}

		$key = $this->createKey();

		$client = SoapTestClient::create($key, [
			'stream_context' => stream_context_create([
				'http' => [
					'header' => 'Abort-On: info'
				]
			])
		]);
		try {
			$this->assertTrue($client->test_message_class('info', 'Hello'));
			$this->fail('info must generate exception');
		} catch (\SoapFault $e) {
			$this->assertTrue(true, 'info generates exception');
		} catch (Throwable $e) {
			$this->fail($e->getMessage());
		}
	}

	private function createKey(): string
	{
		$afi = \apnscpFunctionInterceptor::factory(\Auth::context(TestHelpers::getAdmin()));
		$keys = $afi->auth_get_api_keys();
		if (!$keys) {
			$key = $afi->auth_create_api_key();
			register_shutdown_function(function () use ($key, $afi) {
				$afi->auth_delete_api_key($key);
			});
		} else {
			$key = $keys[0]['key'];
		}
		$this->assertNotEmpty($key, "admin api key generation");
		return $key;
	}

	public function testImpersonation()
	{
		$key = $this->createKey();

		$client = SoapTestClient::create($key);

		$account = \Opcenter\Account\Ephemeral::create();
		$context = $account->getContext();
		$id = $client->admin_hijack($context->site, $context->username, 'UI');
		$this->assertNotEmpty($id, 'ID generated');
		$uri = 'http://localhost:' . \Auth_Redirect::CP_PORT . '/apps/dashboard?' . \apnscpSession::SESSION_ID . '=' . $id;
		$data = file_get_contents($uri);
		$this->assertContains($context->domain, $data, 'Dashboard contains impersonated domain');
	}

	public function testExceptionHandling()
	{
		if (!platform_is('8')) {
			return $this->markTestSkipped('Older platform missing API call');
		}

		if (!is_debug()) {
			return $this->markTestSkipped('Panel not in debug mode');
		}

		$key = $this->createKey();

		$client = SoapTestClient::create($key);
		try {
			$this->assertTrue($client->test_message_class('fatal', 'Crap!'));
		} catch (\SoapFault $e) {
			return $this->assertContains('Crap!', $e->getMessage());
		}
		$this->fail('Unexpected condition');
	}

    public function testAdminAccess()
	{
        $key = $this->createKey();

        $client = SoapTestClient::create($key);
        $this->assertInternalType('array', $client->admin_get_domains());
        $e = null;
        try {
            $this->assertFalse($client->site_get_admin_email() || true, "throw exception on admin accessing site");
        } catch (\SoapFault $e) {
            $this->assertRegExp('/' . static::COMMAND_MISSING . '/', $e->getMessage());
        }
        $this->assertNotNull($e, 'site access throws exception');
    }
}