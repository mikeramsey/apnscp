<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */
require_once dirname(__DIR__, 1) . '/TestFramework.php';

class SelfReferentialContext extends TestFramework
{
	const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

	public function testAccountHairpin() {
		$oldctx = \Auth::context(\Auth::get_admin_login());
		$this->assertEquals(PRIVILEGE_ADMIN, $oldctx->level);
		$ctx = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
		\Opcenter\Http\Apache::buildConfig('now', true);
		$new = \Auth::autoload()->setID($oldctx->id)->authInfo(true);

		$this->assertEquals(PRIVILEGE_ADMIN, $new->level);
		$this->assertEquals(PRIVILEGE_ADMIN, \Auth::profile()->level);
		$this->assertEquals(PRIVILEGE_SITE, $ctx->level);

		$path = null;
		for ($i = 0; $i < 30; $i++) {
			try {
				$http = \HTTP\SelfReferential::instantiateContexted($ctx, [$ctx->domain]);
				$file = 'unit-test.test.' . random_int(0, PHP_INT_MAX);
				$path = $ctx->domain_fs_path(\Web_Module::MAIN_DOC_ROOT . DIRECTORY_SEPARATOR . $file);
				touch($path);

				$http->get(basename($path));
			} catch (\apnscpException $e) {
				$this->fail("Exception occurred: " . $e->getMessage());
			} catch (\GuzzleHttp\Exception\ClientException $e) {
				sleep(2);
				continue;
			} finally {
				if ($path && file_exists($path)) {
					unlink($path);
				}
			}

			return true;
		}

		$this->fail("Unable to validate self-referential test");
	}
}

