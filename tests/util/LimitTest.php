<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	require_once dirname(__FILE__) . '/../TestFramework.php';

	class LimitTest extends TestFramework
	{
		public function testAddressLimitSuccess()
		{
			$proc = new Util_Process_Fork;
			$proc->setOption('leader', false);
			$proc->setOption('resource', ['data' => 6 * 1024 * 1024]);
			$pid = $proc->run('/bin/bash -c "</dev/zero head -c $((1024**2*4)) | tail"');
			$status = null;
			self::assertGreaterThan(1, $pid['return']);
			self::assertEquals($pid['return'], pcntl_waitpid($pid['return'], $status));
			self::assertEquals(0, $status);
		}

		public function testAddressLimitFailure()
		{
			$proc = new Util_Process_Fork;
			$proc->setOption('leader', false);
			$proc->setOption('resource', ['data' => 4 * 1024 * 1024]);
			$pid = $proc->run('/bin/bash -c "</dev/zero head -c $((1024**2*4)) | tail"');
			$status = null;
			self::assertGreaterThan(1, $pid['return']);
			self::assertEquals($pid['return'], pcntl_waitpid($pid['return'], $status));
			self::assertNotEquals(0, $status);
		}
	}