<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class AddonDomainAliasesUpdate extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testAliasesUpdate() {
			/**
			 * via Discord:
			 *
			 * Think I just found a bug
			 * If you create a user in Nexus, then login as that user and create an addon domain,
			 * then logout as that user, go back to Nexus, change their aliases, log back in is that user,
			 * they'll be unable to delete the existing addon domain as the alias changed.
			 */
			$acct1 = \Opcenter\Account\Ephemeral::create([
				'aliases.max'  => null
			]);

			$afi = $acct1->getApnscpFunctionInterceptor();
			$testDomain = \Opcenter\Auth\Password::generate(16, 'a-z0-9')  . '.test';
			$afi->aliases_add_domain($testDomain, '/var/www/somepath');
			$afi->aliases_synchronize_changes();
			$this->assertContains($testDomain, $acct1->getContext()->getAccount()->conf['aliases']['aliases'], 'New domain in aliases');
			$this->assertArrayHasKey($testDomain, $afi->aliases_list_shared_domains());
			$editor = new Util_Account_Editor($acct1->getContext()->getAccount(), $acct1->getContext());
			$editor->setConfig('aliases', 'aliases', []);
			$this->assertTrue($editor->edit());
			$this->assertNotContains($testDomain, $acct1->getContext()->getAccount()->conf['aliases']['aliases'],
				'New domain removed from aliases');
			$this->assertArrayNotHasKey($testDomain, $afi->aliases_list_shared_domains());
		}
	}

