<?php
require_once dirname(__DIR__, 1) . '/TestFramework.php';

class GitInit extends TestFramework
{
	protected $filename;

	protected $auth;

	public function setUp()
	{
		parent::setUp(); // TODO: Change the autogenerated stub
		$ctx = TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
		$this->auth = $ctx;
		$this->site = $this->auth->site;
		$afi = apnscpFunctionInterceptor::factory($this->auth);
		$this->setApnscpFunctionInterceptor($afi);
	}

	public function tearDown()
	{
		if ($this->filename && is_dir($path = $this->auth->domain_fs_path($this->filename))) {
			\Opcenter\Filesystem::rmdir($path);
		}
	}

	public function testGitCommit()
	{
		$this->filename = '/tmp/git-test.' . uniqid();
		$prefix = $this->domain_fs_path();
		$path = $prefix . $this->filename;
		$this->assertFileNotExists($path);
		$this->assertTrue($this->getApnscpFunctionInterceptor()->file_create_directory($this->filename, 0755, true));
		$this->assertTrue($this->getApnscpFunctionInterceptor()->git_init($this->filename, false));
		$this->assertTrue($this->file_touch($this->filename . '/foo'));
		$this->assertTrue($this->getApnscpFunctionInterceptor()->git_add($this->filename, null));
		$this->assertIsString($hash = $this->getApnscpFunctionInterceptor()->git_commit($this->filename, 'Unit test'));
		$this->assertFileExists($path . '/foo');
		unlink($path . '/foo');
		$this->assertFileNotExists($path . '/foo');

		$this->assertTrue($this->getApnscpFunctionInterceptor()->git_checkout($this->filename, $hash, ['foo']));
		$this->assertFileExists($path . '/foo');
	}
}