<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\packman;

	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->init_js('prism');
			$this->add_css('packman.css');
			$this->add_javascript('packman.js');
			$this->add_javascript('/js/ajax_unbuffered.js');
			$this->add_javascript('var ajax_url = \'/apps/packman/packman-ajax.php\';', 'internal', false);
		}

		public function on_postback($params)
		{
			if (!$this->errors_exist()) {
			}

		}

		public function subdomain_enabled($mUser)
		{
			return $this->getApnscpFunctionInterceptor()->web_user_service_enabled($mUser, 'subdomain');
		}

		public function toggle_ror()
		{

		}

		public function list_installed_modules($mType)
		{
			if ($mType == "ruby") {
				return ($this->getApnscpFunctionInterceptor()->web_list_installed_gems());
			} else if ($mType == "php") {
				return array();
			} else if ($mType == "perl") {
				return array();
			}
		}

		public function list_remote_modules($mType)
		{
			if ($mType == "ruby") {
				return ($this->getApnscpFunctionInterceptor()->web_list_remote_gems());
			} else if ($mType == "php") {
				return array();
			} else if ($mType == "perl") {
				return array();
			}
		}

		public function get_gem_description($mName)
		{
			return $this->getApnscpFunctionInterceptor()->web_get_gem_description($mName);
		}

		public function rubyOnRailsEnabled()
		{
			return $this->getApnscpFunctionInterceptor()->web_ruby_on_rails_enabled();
		}

	}

?>
