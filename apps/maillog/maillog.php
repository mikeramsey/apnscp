<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Page extends Page_Container
	{

		public function __construct()
		{
			parent::__construct();
		}

		public function on_postback($params)
		{
			if (!$this->errors_exist()) {
			}

		}
	}

?>