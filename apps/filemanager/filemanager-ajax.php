<?php
	define('INCLUDE_PATH', realpath(dirname($_SERVER['SCRIPT_FILENAME']) . '/../../../../'));
	include(INCLUDE_PATH . '/lib/html/ajax_cmd.php');

	function get_directory_contents($mDir, $mLimit = 10, $mFilter = null)
	{
		global $function;
		$files = $function->file_get_directory_contents($mDir);
		if (!$files) {
			return '';
		}
		$file_list = array();
		foreach ($files as $file) {
			if ($mFilter && $mFilter != $file['file_type']) {
				continue;
			}
			$file_list[] = $file['file_name'] . "|" . basename($file['file_name']) . ($file['file_type'] == 'dir' ? '/' : '');
		}

		return join("\n", $file_list);
	}

	function upload_progress($id)
	{
		$ret = array(
			'success' => false
		);

		if (!$id) {
			return json_encode($ret);
		}

		$progresskey = ini_get("session.upload_progress.prefix") . $_POST[ini_get("session.upload_progress.name")];
		$progress = \Session::get($progresskey);
		if (!$progress) {
			return json_encode($ret);
		}

		$ret = array(
			'success'      => true,
			'pct'          => round($progress['bytes_processed'] / $progress['content_length'] * 100, 1),
			'size'         => $progress['content_length'],
			'time_elapsed' => microtime(true) - $progress['start_time'],
		);
		// @todo rate is better sampled in a smaller window
		$ret['rate'] = $progress['current'] / $progress['time_elapsed'];
		$ret['time_remaining'] = ($progress['content_length'] - $progress['bytes_processed']) / $progress['rate'];

		return json_encode($ret);
	}

	function handle_upload($cwd, $id)
	{
		$ret = handle_upload_raw($cwd, $id);
		$msg = \Error_Reporter::flush_buffer(\Error_Reporter::E_ERROR | \Error_Reporter::E_WARNING);

		return json_encode(
			array(
				'success' => (bool)$ret,
				'msg'     => array_pop($msg),
				'file'    => $ret
			)
		);
	}

	function handle_upload_raw($cwd, $id)
	{
		$Page = new Page();

		return $Page->handleFileUpload();
	}

?>
