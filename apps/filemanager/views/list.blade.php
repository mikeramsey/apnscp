@php
namespace apps\filemanager;
$parent = null;
@endphp
<tr id="dir-hdr">
	<td class="head1 center px-2" width="26" align="center">
		@if ($Page->getState() == 'list')
			<label class="custom-checkbox custom-control my-1 p-0 align-items-center w-100 d-flex" for="select_all">
				<input type="checkbox" class="custom-control-input" id="select_all"/>
				<span class="custom-control-indicator"></span>
			</label>
		@endif
	</td>
	<td class="head1 name">Name</td>
	<td class="head1 right hidden-md-down">Date modified</td>
	<td class="head1 right hidden-xs-down">Size</td>
	<td class="head1 left hidden-sm-down">Owner</td>
	<td class="head1 center actions">Actions</td>
</tr>

@if ($Page->getCurrentPath() != '/')

	@php $parent = $Page->statParent(); @endphp
	<tr id="file-root">
		<td class="" align="center">
			&nbsp;
		</td>
		<td class=" name" align="left">
			<a class="node node-parent-dir"
			   href="filemanager?cwd={{ $Page->getParentDirectory() }}">
				Parent Directory
			</a>
		</td>
		<td class="right date hidden-md-down">
			{{--
				Locale isn't threadsafe
			--}}
			{{
				(new \DateTime())->setTimestamp($parent['mtime'])->format(\UCard::init()->getDateTimeLocale())
			}}
		</td>
		<td class="right hidden-xs-down"></td>
		<td class="left owner hidden-sm-down">{{ $parent['owner'] ?? "" }}</td>
		<td class="action center"></td>
	</tr>
@endif

@php
	$contents = $Page->getDirectoryContents();
	if (!$contents) return;
	$parent_stat = $Page->statParent();
	$size = $i = 0;
	$reported = false;
@endphp

@foreach ($contents as $content)
	@php
		$filename = basename($content['file_name']);
		if (strlen($filename) >= Page::SHADOW_MIN_LEN) {
			$filename = '<span class="filename expanded">' . e($filename) . '</span>' .
				'<span class="filename_truncated">' . e(substr_replace($filename,
					' ... ',
					Page::SHADOW_MIN_LEN / 2 - 2,
					strlen($filename) - (Page::SHADOW_MIN_LEN / 2 - 2) * 2)) . '</span>';
		} else {
			$filename = '<span class="filename">' . e($filename) . '</span>';
		}
		$is_compressed = $Page->isCompressedFile($content['file_name']);
	@endphp
	<tr class="entry ftype-{{ $content['file_type'] }}">
	<td class="" valign="bottom" align="center">
		@if ($parent_stat['can_write'] && $parent_stat['can_execute'])
			<label class="custom-checkbox custom-control my-1 p-0 align-items-center d-flex">
				<input type="checkbox" name="file[]" class="custom-control-input" value="{{ $content['file_name'] }}"/>
				<span class="custom-control-indicator"></span>
			</label>
		@endif
	</td>
	<td class="name " align="left">
		@php
			if (!isset($content['can_read']) && !$reported) {
				$reported = true;
				$data = "WANTED: " . var_export($content, true) . "\r\n" . var_export($contents, true) . "\r\n";
				Error_Reporter::report("MISSING!!!! " . $data);
			}
		@endphp

			@if ($content['can_read'] || (($content['link'] === 1 || $content['file_type'] == 'dir') && $content['can_execute']))

				@php
					$icon_type = $Page->getIconType(
						$content['file_type'],
						$content['link'],
						$content['can_read'],
						$content['can_execute'],
						$content['referent'],
						$content['portable']
					);
				@endphp

				<img src="{{ $icon_type }}" alt="" align="middle" />

				@if ($content['file_type'] != 'dir' && $content['link'] != 2)
					@if ($content['can_read'])
						<span class="name">
							<a href="{{ \HTML_Kit::page_url_params(['f' => $content['file_name']]) }}">
								{!! $filename !!}
							</a>
						</span>
					@else
						{!! $filename !!}
					@endif
				@else
					<span class="name">
				        @if ($content['can_read'] && $content['can_execute'])
							<a href="filemanager?cwd={{ (!$content['link'] ? $content['file_name'] : $content['referent']) }}">
						@endif
			            {!! $filename !!}
						@if ($content['can_read'] && $content['can_execute'])
			                </a>
						@endif
		            </span>
				@endif

		@else
			<img src="{{
				$Page->getIconType($content['file_type'],
					$content['link'],
					$content['can_read'],
					$content['can_execute'],
					$content['referent'],
					$content['portable'])
 			}}" alt="" align="middle" />
			&nbsp; {!! $filename !!}
		@endif
	</td>
	<td class="right date hidden-md-down">
		{{
			(new \DateTime())->setTimestamp($content['mtime'])->format(\UCard::init()->getDateTimeLocale())
		}}
	</td>
	<td class="right size hidden-xs-down">
		@if ($content['file_type'] == 'file' || $content['file_type'] == 'link' && $content['link'] == 1)
			{{ \Formatter::reduceBytes($content['size']) }}
		@endif
	</td>
	<td class="left owner  hidden-sm-down">
		{{ $content['owner'] }}
	</td>
	<td class=" actions center">
		<div class="input-group justify-content-center">
			<div class="btn-group">
				<button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
				        aria-expanded="false"></button>

				<div class="dropdown-menu dropdown-menu-right">
					@if ($is_compressed && $content['can_read'])
						<a title="Open" class="ui-action ui-action-label btn btn-block ui-action-open-archive dropdown-item"
					        href="filemanager?co={{ $content['file_name'] }}">
							Open
						</a>
					@elseif ($content['file_type'] != 'dir' && $content['can_write'] && $content['can_read'] && !$is_compressed && !$content['link'] )
						<a title="Edit" class="ui-action-label btn btn-block ui-action ui-action-edit dropdown-item"
					        href="{{ \HTML_Kit::page_url_params(array('f' => $content['file_name'])) }}">
							Edit
						</a>
					@endif

					@if ($content['file_type'] != 'dir' && $content['link'] != 2 && $content['can_read'])
						<a title="Download" class="btn-block ui-action ui-action-label btn ui-action-download dropdown-item"
					        href="?download={{ $content['file_name'] }}">
							Download
						</a>
					@endif

					@if ($content['can_chown'])
						<a title="Properties" href="{{ \HTML_Kit::page_url() }}?ep={{ $content['file_name'] }}" class="ui-action ui-action-label btn btn-block ui-action-properties dropdown-item">
							Properties
						</a>
					@endif

					@if ($parent_stat['can_write'])
						<a href="#" title="Rename"
					        class="ui-action btn ui-action-label btn-block ui-action-rename dropdown-item">
							Rename
						</a>
					@endif

					@if ($parent_stat['can_write'] && $parent_stat['can_execute'])
						<a title="Delete"
					        class="ui-action ui-action-label btn btn-block warn ui-action-delete dropdown-item"
					        href="?d={{ base64_encode($content['file_name']) }}"
					        onClick="return confirm('Are you sure you want to delete {{ $content['file_name'] }}?');">
							Delete
						</a>
					@endif

				</div>
			</div>
		</div>


	</td>
</tr>
@php
	$size += $content['size'];
	$i++;
@endphp
@endforeach
<tfoot>
	<tr>
		<td class=" right disk-folder" colspan="2">
	        <span id="folder-selected">
	        Selected Files: <span id="selected-size"></span> MB (<span id="selected-count"></span> items)
	        </span>

			<div id="folder-unselected">
		        Current Folder:
		        <span id="folder-size">
			        {{ \Formatter::reduceBytes($size) }}
		        </span> (<span id="folder-count">{{ $i }}</span> items)
	        </div>
		</td>
		<td class="right disk-total" colspan="3">
			<span id='disk-free'>
				{{ ($account = $Page->diskUsage())['free'] }}
			</span> Free ::
			<span id='disk-total'>
				{{ $account['total'] }}
			</span> Total
		</td>
	</tr>
	</tfoot>

