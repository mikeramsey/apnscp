<?php
$mode = $Page->get_mode();

$action = 'filemanager?' . $mode . '=';
switch ($Page->view_state()) {
	case 'list_archive':
		$action .= $Page->getParentDirectory();
		break;
	case 'view_file':
		$editor_mode = isset($_GET['editor']) && $_GET['editor'] == 'rich' ? 'rich' : 'raw';
		$action .= $Page->getCurrentPath() . '&editor=' . $editor_mode;
		break;
	default:
		$action .= $Page->getCurrentPath();
}
?>
<!-- main object data goes here... -->
<form method="post" enctype="multipart/form-data" action="<?=$action?>">
	<div class="row">
		<div class="col-12">
			<fieldset class="form-group form-inline">
				<div class="btn-group">
					<button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown"
					        aria-haspopup="true" aria-expanded="false">
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<div class="dropdown-menu">
						<?php $home = \Util_Conf::home_directory(); ?>
						<a class="dropdown-item"
						   href="<?=\HTML_Kit::page_url_params(array('cwd' => $home, 'co' => null)); ?>">
							Home Folder
						</a>
						<a class="dropdown-item"
						   href="<?=\HTML_Kit::page_url_params(array('cwd' => '/var/www/html', 'co' => null)); ?>">
							Website Files
						</a>
						<a class="dropdown-item"
						   href="<?=\HTML_Kit::page_url_params(array('cwd' => '/var/www', 'co' => null)); ?>">
							Addon Domains
						</a>
						<a class="dropdown-item"
						   href="<?=\HTML_Kit::page_url_params(array('cwd' => '/var/log/httpd', 'co' => null)); ?>">
							Website Logs
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#" class="ui-action-label ui-action-folder" id="folderJump">
							Browse...
						</a>
					</div>
					<h4 class="mb-0 mt-1 ml-1"><?php print($Page->getCurrentPath()); ?></h4>
				</div>

			</fieldset>
		</div>
	</div>

	@if ($Page->getState() == "list")
		<div class="row">
			<div class="col-12" id="fm-commands">
				@include('partials.commands')
			</div>
		</div>
	@endif

	<table width="100%" id="fm_table"
	       class="tbl-filemanager table table-condensed <?=$Page->getState() == 'list' ? 'table-hover' : ''?> table-sm">
		@if ($Page->getState() == 'list')
			@includeWhen($protected = $Page->isProtected(), 'partials.protected-resource')
			@includeWhen(!$protected, 'list')
		@elseif ($Page->getState() == 'view_file')
			@include('view-file')
		@elseif ($Page->getState() == 'edit_perms')
			@include('edit-perms')
		@elseif ($Page->getState() == 'list_archive')
			@include('list-archive')
		@endif
	</table>
</form>
