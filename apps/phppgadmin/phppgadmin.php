<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\phppgadmin;

	use Error_Reporter;
	use Page_Container;

	class Page extends Page_Container
	{
		/**
		 * @var mixed
		 */
		protected $database;
		private $phpPgAdminURL;
		private $pgsqlUsername;
		private $pgsqlPassword;
		private $failed = false;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('phppgadmin.css');
			Error_Reporter::suppress_php_error('pg_connect');
			\Page_Renderer::display_on_error();
			$this->pgsqlUsername = $this->get_pgsql_username();
			$this->pgsqlPassword = $this->get_pgsql_password();
			$this->phpPgAdminURL = PHPPGADMIN_LOCATION;

			if (!\count($_POST) && !$this->check_pgsql_login()) {
				$this->pgsqlPassword = $this->get_pgsql_password();
				if (!$this->check_pgsql_login()) {
					warn("Unable to login to phpPgAdmin, please update your password.");
					$this->pgsqlPassword = null;

					return;

				}
			}
			if (is_null($this->pgsqlPassword)) {
				return;
			}

			if (isset($_GET['db']) && preg_match(\Regex::SQL_DATABASE, $_GET['db'])) {
				$this->database = $_GET['db'];
			}
			$this->setup_phppgadmin_session();
		}

		public function get_pgsql_username()
		{
			$user = $this->sql_get_pgsql_username();

			return $user;
		}

		public function set_pgsql_username($user)
		{
			$olduser = $this->sql_get_pgsql_username();
			if ($olduser != $user) {
				warn("changed default PostgreSQL user from `%s' to `%s'",
					$olduser, $user);
			}
			$this->pgsqlUsername = $user;

			return $this->sql_set_pgsql_username($user);
		}

		public function get_pgsql_password()
		{
			return $this->sql_get_pgsql_password();
		}

		public function set_pgsql_password($pass)
		{
			return $this->pgsql_set_password($pass);
		}

		public function check_pgsql_login($username = null, $password = null)
		{
			if (!$username) {
				$username = $this->get_pgsql_username();
			}
			if (!$password) {
				$password = $this->get_pgsql_password();
			}
			$conn = 'host=127.0.0.1 user=%s password=%s dbname=template1';
			return pg_connect(sprintf($conn, $username, $password)) && pg_close() or false;
		}

		public function setup_phppgadmin_session()
		{
			$user = $this->pgsqlUsername;
			$password = $this->pgsqlPassword;
			$curl = new \CurlLayer();
			$curl->set_form_method('GET');
			$routed = \Opcenter\Net\Iface::routed() ?? '127.0.0.1';
			$curl->change_option(CURLOPT_RESOLVE, [SERVER_NAME . ':443:' . $routed, SERVER_NAME . ':80:' . $routed]);
			$curl->trustServerCertificate();
			$curl->toggle_body(true);
			$flag = platform_is('7.5') ? 'disable' : 'allow';
			$url = $this->phpPgAdminURL() . '/redirect.php?subject=server&server=127.0.0.1%3A5432%3A' . $flag . '&';
			$data = $curl->try_request($url);
			if (false === strpos($data, "\r\n\r\n")) {
				if (curl_errno($curl->ch) & (CURLE_SSL_CACERT | CURLE_SSL_PEER_CERTIFICATE)) {
					return error("SSL is not configured correctly yet on this server. See https://docs.apiscp.com/admin/SSL - error message: %s",
						curl_error($curl->ch));
				}

				return error("Internal subrequest failed. Check server setup. Error: %s", curl_error($curl->ch));
			}
			//https://192.168.0.6/phpPgAdmin/redirect.php?subject=server&server=127.0.0.1%3A5432%3Aallow&
			$dom = new \DOMDocument();
			silence(function () use ($dom, $data) {
				$dom->loadHTML($data);
			});
			$post = [];
			foreach ($dom->getElementsByTagName('input') as $e) {
				$name = $e->getAttribute("name");
				$value = $e->getAttribute("value");
				if (0 === strpos($name, "loginUsername")) {
					$value = $user;
				} else if (0 === strpos($name, "loginPassword")) {
					$value = $password;
				}
				$post[$name] = $value;
			}
			$curl->reset();
			// loginPassword is now randomized
			$curl->set_form_method('POST');
			$curl->change_option(CURLOPT_POSTFIELDS, $post);
			//$curl->change_option(CURLOPT_RETURNTRANSFER,0);
			$curl->change_option(CURLOPT_BINARYTRANSFER, 0);
			$curl->change_option(CURLOPT_FOLLOWLOCATION, 1);
			$curl->change_option(CURLOPT_HEADER, 1);
			$curl->change_option(CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			$data = $curl->try_request($this->phpPgAdminURL() . '/index.php');
			//var_export($data);
			$ppa = null;
			//die(var_export($key));
			foreach (explode("\n", $data) as $line) {
				if (preg_match('/PPA_ID=([^;]+);/', $line, $match)) {
					$ppa = $match[1];
				}
			}
			if (!$ppa) {
				return;
			}
			$return = 'index.php';
			if ($this->database) {
				$return = 'redirect.php?server=127.0.0.1:5432:disable&subject=database&database=' . $this->database;
			}
			$this->phpPgAdminURL = $this->phpPgAdminURL() . '/dummyset.php?p=' . $ppa . '&return=' . urlencode($return);
			\Util_HTTP::forwardNoProxy();
			header('Location: ' . $this->phpPgAdminURL, true, 302);
		}

		public function phpPgAdminURL()
		{
			return rtrim($this->phpPgAdminURL, '/');
		}

		public function on_postback($params)
		{
			if (isset($params['username']) && $params['username']) {
				$this->set_pgsql_username($params['username']);
			}
			if (!empty($params['reset-password'])) {
				$myuser = $this->get_pgsql_username();
				foreach ($this->pgsql_list_users() as $user => $hosts) {
					if ($user !== $myuser) {
						continue;
					}
					$this->pgsql_edit_user($user, $params['password']);
				}
			}

			if (empty($params['password'])) {
				return;
			}

			if (!$this->check_pgsql_login($this->pgsqlUsername, $params['password'])) {
				error("PostgreSQL password submitted does not match record in system for login " . $this->pgsqlUsername);
				$this->pgsqlPassword = null;

				return;
			}

			$this->set_pgsql_password($params['password']);
			$this->pgsqlPassword = $params['password'];
			$this->setup_phppgadmin_session();
		}

		public function loginSucceeded()
		{
			return !$this->failed;
		}

		public function password_exists()
		{
			return !is_null($this->pgsqlPassword);
		}

	}