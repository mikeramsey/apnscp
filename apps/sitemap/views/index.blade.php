@php
	$buf = array();
	$i = 0;
@endphp
@while (($catObj = $Page->getCategory()))

<div class="app-category">
	<h3> {{ $catObj->getName() ?: 'Main' }} </h3>

	<dl class="app-list">
		@while ($moduleObj = $Page->getLink($catObj->getInternal()))
			<dt class="application">
				<a href="{{ $moduleObj['href'] }}">{{ $moduleObj['name'] }}</a>
			</dt>

			<dd class="description">
				@php eval('?>' . $Page->getHelpTagline($moduleObj['internal'])); @endphp
			</dd>
		@endwhile
	</dl>
</div>
@endwhile