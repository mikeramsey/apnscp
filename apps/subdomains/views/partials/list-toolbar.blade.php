<div class="d-flex align-items-end form-group mt-3" id="toolbar">
	@include('partials.search')
	@includeWhen(\count($Page->list_domains()) > 1, 'partials.toolbox')
</div>
