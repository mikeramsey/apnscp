<h3>List Subdomains</h3>
<table id="subdomains" class="multirow tablesorter table">
	<thead>
		<th class="nosort" align="center" width="20"></th>
		<th class="" align="center" width="200">
			Subdomain
		</th>
		<th class="" width="" align="center">
			Type
		</th>
		<th class="" width="" align="center">
			Owner
		</th>
		<th class="">
			Document Root
		</th>
		<th class="center nosort" width="150">
			Actions
		</th>
	</thead>
	<tbody>
		@foreach ($Page->list_subdomains() as $subdomain => $fs_location)
			@php
				$info = $Page->subdomain_info($subdomain);
				if ($fs_location && $info['user']) {
					$class = '';
				} else {
					$class = 'text-disabled'; // disabled subdomain, file doesn't exist
				}
				$url = 'http://' . $subdomain;
				if ($info['type'] == 'global') {
					$url .= '.' . \Util_Conf::login_domain();
				}
			@endphp

			<tr class="entry">
				<td align="center" data-immutable>
					<label class="custom-control custom-checkbox p-2 m-0 d-flex align-self-center" for="subdomain{{ $loop->iteration }}">
						<input type="checkbox" name="subdomains[]" class="custom-control-input"
						       value="{{ $subdomain }}" id="subdomain{{ $loop->iteration }}"/>
						<span class="custom-control-indicator" data-immutable="true"></span>
					</label>
				</td>
				<td class="subdomain_parent {{ $class }}">
					<span class="subdomain" data-source="subdomain">
						{{ $subdomain }}
					</span>
				</td>
				<td class="subdomain_type {{$class}}" @if (!$fs_location) colspan="2" @endif>
					@if (!$fs_location)
						docroot not found
					@else
						{{ $info['type'] }}
					@endif
				</td>
				@if ($fs_location)
					<td class="subdomain_owner  {{ $class }}" data-source="user">
						<span data-source="user" class="user">
							{{ $info['user'] }}
						</span>
					</td>
				@endif
				<td class="subdomain_fs_parent {{ $class }}">
					<span class="subdomain_fs" data-source="file">
						{{ $fs_location ? $fs_location : ($info['path'] ? $info['path'] : "") }}
					</span>
				</td>
				<td class="nosort center actions">
					<div class="btn-group">
						<a href="{{ $url }}"
						   class="btn btn-secondary hidden-md-down ui-action ui-action-visit-site ui-action-label">
							Visit
						</a>
						<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
						        aria-haspopup="true" aria-expanded="false">
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a href="{{ $url }}"
							   class="btn-block dropdown-item hidden-lg-up ui-action ui-action-visit-site ui-action-label">
								Visit
							</a>
							<a href="{{ \HTML_kit::new_page_url_params('/apps/webapps', ['hostname' => $subdomain]) }}"
							   class="dropdown-item  btn-block ui-action-label ui-action">
								<i class="fa fa-magic"></i>
								App Manager
							</a>

							<a href="{{ \HTML_kit::new_page_url_params('/apps/personalities', ['edit' => $subdomain]) }}"
							   class="dropdown-item  btn-block ui-action-label ui-action">
								<i class="fa fa-apache"></i>
								.htaccess Editor
							</a>

							<button title="open file manager" type="submit"
							        class="dropdown-item btn-block ui-action ui-action-manage-files ui-action-label"
							        name="file-manager[{{ $info['path'] }}]" value="{{ $info['path'] }}">
								Browse Files
							</button>

							<button class="dropdown-item  btn-block ui-action-label ui-action ui-action-edit"
							        name="rename" value="{{ $subdomain }}">
								Edit
							</button>
							<div class="dropdown-divider"></div>
							<button class="dropdown-item btn btn-block warn ui-action-label ui-action ui-action-delete"
							        onClick="return confirm('Are you sure you want to delete the subdomain {{ $subdomain }}?');"
							        name="delete" value="{{ $subdomain }}">
								<i class="fa fa-times"></i>
								Delete
							</button>
						</div>

					</div>


				</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
				<p class="text-center">
					Select multiple subdomains from the <i class="fa fa-check-square-o"></i> column and perform an
					action:
					<button type="submit" class="btn btn-secondary warn" name="Delete_Subdomains" id="Delete_Subdomains"
					        value="Delete Subdomains">
						Delete Subdomains
					</button>
				</p>
			</td>
		</tr>
	</tfoot>
</table>