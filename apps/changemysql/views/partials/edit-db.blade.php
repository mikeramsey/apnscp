<div class="col-12 col-sm-6 col-md-7 col-lg-8 float-sm-right">
	<h4>Privilege Mode</h4>
	<div class="btn-group align-items-center" id="privilegeMode" data-toggle="buttons">
		<fieldset class="form-group">
			<button type="button" class="btn btn-secondary active">
				<label class="custom-control custom-radio mb-0 d-flex mt-1 mr-0 align-self-center">
					<input type="radio" class="custom-control-input"
					       value="simple" data-hide="privileges-advanced" name="options" id="optionSimple"
					       aria-pressed="true" checked autocomplete="off"/>
					<span class="custom-control-indicator"></span>
					Simple
				</label>
			</button>
			<button type="button" class="btn btn-secondary">
				<label class="custom-control custom-radio mb-0 d-flex mt-1 mr-0 align-self-center">
					<input type="radio" class="custom-control-input"
					       value="advanced" data-hide="privileges-simple" name="options" id="optionAdvanced"
					       aria-pressed="true" autocomplete="off"/>
					<span class="custom-control-indicator"></span>
					Advanced
				</label>
			</button>
		</fieldset>
	</div>

	<div class="row">
		@foreach($dbUsers as $userinfo)
			@php
				$user = $userinfo['user'];
				$host = $userinfo['host'];
				$perms = $userinfo['privs'];
				if (!$Page->show_hosts() && $host != 'localhost') {
					continue;
				}

				$key = '[' . $user . '][' . $host . ']';
				$advancedRef = md5($key);
				$db_key = 'editdb[' . $mydb . ']';
				$read = (bool)$perms['select'];
				$write = $perms['insert'] && $perms['update'] && $perms['delete'];
			@endphp

		<div class="col-12 mb-1">
			<div class="row">
				<input type="hidden" name="{{ $db_key }}[state][simple]{{ $key }}"
				       value="{{ base64_encode(serialize(array(
						   'READ'  => $read,
						   'WRITE' => $write
					   ))) }}"/>
				<input type="hidden" name="{{ $db_key }}[state][complex]{{ $key }}"
				       value="{{  base64_encode(serialize($perms)) }}"/>

				<div class="d-block privilege-heading btn-group col-12" data-toggle="">
					<fieldset class="form-group">
                                <span class="user">
                                    <i class="fa fa-user"></i>
                                    {{ $user }}
                                </span>

						<span class="host">
                                    <i class="fa fa-laptop"></i>
                                    {{ $host }}
                                </span>
					</fieldset>
					<div class="row">
						<div class="privileges-simple privileges col-12">
							<label class="btn btn-primary-outline @if ($read) bg-success active @endif">
								<div class="custom-control custom-checkbox mb-0 mt-1 mr-0 d-flex align-self-center">
									<input type="checkbox" class="read custom-control-input"
									       name="{{  $db_key }}[simple]{{ $key }}[READ]"
									       value="1" @if ($read) CHECKED='checked' @endif />
									<span class="custom-control-indicator"></span>
									Read
								</div>
							</label>
							<label class="btn btn-primary-outline @if ($write) bg-success active @endif">
								<div class="custom-control custom-checkbox mb-0 mt-1 mr-0 d-flex align-self-center">
									<input type="checkbox" class="read custom-control-input"
									       name="{{ $db_key }}[simple]{{ $key }}[WRITE]"
									       value="1" @if ($write) CHECKED='checked' @endif />
									<span class="custom-control-indicator"></span>
									Write
								</div>
							</label>

						</div>

						<div class="col-12 privileges privileges-advanced">
							<button type="button"
							        class="dropdown-toggle btn btn-secondary ui-action ui-action-label ui-action-advanced"
							        data-toggle="collapse" data-target="#{{ $advancedRef }}"
							        aria-expanded="false" aria-controls="{{ $advancedRef }}">
								Show Privileges
							</button>
						</div>
					</div>

					<div class="full collapse " id="{{ $advancedRef }}" aria-expanded="false">
						<label class="d-block mt-3 custom-control custom-checkbox">
							<input type="checkbox" class="toggle_all custom-control-input"/>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Toggle All</span>
						</label>
					@php
						$priv_list = array();
					@endphp
					@foreach ($perms as $priv => $granted)
						@php
							$priv = strtoupper($priv);
							$priv_list[] = '<label class="mb-0 col-12 mr-0 my-2 col-md-6 col-lg-4 col-xl-3 custom-control custom-checkbox ' . ($granted ? "active" : "") . '">' .
								'<input type="checkbox" class="custom-control-input" name="' . $db_key . '[complex]' . $key . '[' . $priv . ']" value="Y" ' .
								($granted ? "CHECKED='checked'" : "") . ' /> ' .
								'<span class="custom-control-indicator"></span>' .
								'<span class="custom-control-description">' . str_replace('_', ' ',
									$priv) . '</span>' .
								'</label>';
						@endphp
					@endforeach
					{!! implode("", $priv_list) !!}
					</div>
				</div>
			</div>
			<hr/>
		</div>
		@endforeach
	</div>
</div>