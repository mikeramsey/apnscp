/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

var clipboard, globalScopeName, DEFERRAL_ID = 'formDefer';

$(document).ready(function() {
	var combo = $('#scopeIndex').combobox().on('comboboxselect', function (event, item) {
		return loadScope(item.item.value);
	}).on('change', function (e) {
		var val = $(this).val();
		combo.combobox("instance").input.val(val);
		return loadScope(val);
	});

	combo.combobox("instance").input.autocomplete("instance")._renderItem = function (ul, item) {
		return $("<li class='py-0'></li>")
			.data("item.autocomplete", item)
			.append("<span class='dropdown-item py-0 d-flex'>" + item.label + "<span class='align-self-end my-auto ml-auto font-weight-normal text-uppercase badge bg-transparent d-none d-md-inline'>" + item.option.getAttribute('data-help') + "</span></span>")
			.appendTo(ul);
	};

	var val;
	if (!!(val = $('#scopeIndex').val())) {
		loadScope(val);
	}
	$('#scopeScene').on('click', '.scope-related', function (e) {
		e.preventDefault();
		$('#scopeIndex').val(this.getAttribute('data-scope')).triggerHandler('change');
		return false;
	}).on('submit', '#scopeApply', submitScope).click(function (e) {
		if (e.target.hasAttribute('data-scope-link')) {
			loadScope(e.target.value);
			return false;
		}
		return true;
	}).on('click', ':button[name="restart-panel"]', function () {
		apnscp.cmd('scope_set', ['cp.restart', true]).done(function () {
			apnscp.addMessage('Panel restarting now. Service will be briefly interrupted.');
		});
	}).on('click', ':button[name="run-bootstrapper"]', function () {
		apnscp.cmd('scope_set', ['system.integrity-check', $(this).val()]);
		return false;
	});

	$('#scopeHistory').on('click', 'button', function (e) {
		var args, toggled = !!$(this).data('toggled');
		var that = $(this), toggled = !!$(this).data('toggled');

		args = $(this).data(toggled ? 'args' : 'original');
		applyScope($(this).data('scope'), args).done(function () {
			that.data('toggled', toggled ^ 1);
			if (!toggled) {
				that.addClass('redo').text(that.data('redo-title'));
			} else {
				that.removeClass('redo').text(that.data('undo-title'));
			}
		});

	});
});

var sharedTriggerState = {
	handled: false
};
$(window).on('load', function () {
	activateCopyCommand(jQuery);
}).on('popstate', function (e) {
	$('#scopeScene').trigger('scope:popstate', sharedTriggerState);
	if (!sharedTriggerState.handled) {
		var scope = window.location.pathname.match(/apply\/(.*)$/);
		loadScope(scope ? scope[1] : '');
	}
	sharedTriggerState.handled = false;
});

function submitScope(e) {
	if ($('#' + DEFERRAL_ID).length) {
		return;
	}
	e.preventDefault();
	// clean ajaxWait() population
	$('#scopeScene :hidden[name="scope"]').remove();
	var seen = {},
		// remove duplicate arguments as it would be parsed server-side
		args = $("#scopeApply").serializeArray().reverse().filter(function (elem, index, self) {
			if (-1 === elem.name.indexOf('[]')) {
				if (!(elem.name in seen)) {
					seen[elem.name] = 1;
					return true;
				}
				return false;
			} else if (elem.name.indexOf('[') < elem.name.indexOf(']')) {
				// extract element
				return true;
			}
			return true;
		}).reverse();

	var originalArgs = $(':submit', this).data('original');

	applyScope(globalScopeName, args).done(function () {
		$('#scopeApply').effect("transfer", {to: $("#scopeHistory")}, 500, function () {
			$('#scopeHistory .heading').removeClass('hide');
			var $template = $('#scopeHistory ol .template').clone();
			var scopeCommand = args.map(function (e, k) {
				if (e.name.indexOf('[') < e.name.indexOf(']')) {
					var key = e.name.substring(e.name.indexOf('[') + 1, e.name.indexOf(']'));
					if (key !== '') {
						return key + ':' + e.value;
					}
					return e.value;
				}
				return "'" + e.value + "'";
			});
			if (args.length > 1) {
				scopeCommand = "'[" + scopeCommand.join(",") + "]'";
			} else {
				scopeCommand = scopeCommand.join("");
			}
			scopeCommand = globalScopeName + " " + scopeCommand;

			var $btn = $template.removeClass('hide template').children('button').data('scope', globalScopeName).data('args', args).data('original', originalArgs).end().children('span').text(scopeCommand).end();
			$('button', $btn).ajaxWait();
			$('#scopeHistory ol').append($btn);
		});
	});
	return false;
}

/**
 * Copy command
 * @param $
 */
function activateCopyCommand($) {
	var COPY_SUCCESS_MSG = 'copied!';
	clipboard = new Clipboard('#cliCmd .ui-action-copy');
	$('#cliCmd [data-toggle="tooltip"]').tooltip();

	clipboard.on('success', function (e) {
		var originalTitle = $(e.trigger).attr('data-original-title');
		$(e.trigger).attr('data-original-title', COPY_SUCCESS_MSG).tooltip('show').on('hidden.bs.tooltip', function (e) {
			$(e.currentTarget).attr('data-original-title', originalTitle);
		});
		e.clearSelection();
	});

}

function registerEvent(name, data, handler) {
	if (-1 === name.indexOf('.')) {
		name = name + SCOPE_EVENT_NS;
	}
	$('#scopeScene').on(name, data, handler);

	return this;
}
/**
 * Load Scope from backend
 * @param name
 * @param vars optional variables to pass
 */
function loadScope(name, vars) {
	var timeout = setTimeout(function () {
		$('#scopeScene').empty();
	}, 150);
	return apnscp.render(vars || null, "render/" + name).done(function (html) {
		clearTimeout(timeout);
		$('#scopeScene').unbind(name === globalScopeName ? '.INVALID-NAMESPACE' : SCOPE_EVENT_NS).
			html(html);
		$('#scopeScene').trigger('scope:changed', [globalScopeName, name]);
		globalScopeName = name;
	}).done(function() {
		if (clipboard) {
			clipboard.destroy();
		}

		if (name && window.location.pathname !== "/apps/scopes/apply/" + name) {
			window.history.pushState({}, null, "/apps/scopes/apply/" + name);
		}

		$('#submitScope').ajaxWait();
		$(['#scopeIndex', '#scopeIndex ~ input']).val(name);
		activateCopyCommand(jQuery);
	});
}

/**
 * Apply Scope
 *
 * @param name
 * @param val
 */
function applyScope(name, val) {
	return apnscp.post(val, 'apply/' + name).done(function () {
		// update Scope settings
		loadScope(name);
	});
}