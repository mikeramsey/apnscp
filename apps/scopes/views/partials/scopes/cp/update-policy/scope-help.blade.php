<p class="help">
	{{ $scope->getHelp() }}. See <a href="https://docs.apiscp.com/UPGRADING">docs/UPGRADING.md</a>
	for detailed configuration.
</p>