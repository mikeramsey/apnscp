<style type="text/css">
	#bootstrapperActions{
		right:0;
		top: 0;
	}

	#submitScope {
		display: none;
	}
</style>

<div class="align-content-end position-absolute d-flex" id="bootstrapperActions">
	<button type="button" name="restart-panel"
        class="ui-action ui-action-refresh ui-action-label btn btn-secondary ml-3">Restart {{ PANEL_BRAND }}</button>
</div>