<h5>CLI Invocations</h5>
<div class="mb-2">
	<span class="ui-action-copy" data-clipboard-target="#getValueCommand">
		<b class="text-uppercase" id="clipboardCopyGet" data-toggle="tooltip" title="Copy to Clipboard">Getter</b>
		<code id="getValueCommand" aria-describedby="clipboardCopyGet" class="mr-2">
			cpcmd scope:get {{ $scope->getFullyQualifiedScope() }}
		</code>
	</span>
</div>
<div class="mb-2">
	<span class="ui-action-copy" data-clipboard-target="#setValueCommand">
		<b class="text-uppercase" title="Copy to Clipboard" id="clipboardCopySet" data-toggle="tooltip">Setter</b>
		<code id="setValueCommand" aria-describedby="clipboardCopySet" class="mr-2">
			cpcmd scope:set {{ $scope->getFullyQualifiedScope() }} <i class="arg">varname</i> <i
					class="arg">value</i>
		</code>
	</span>
</div>