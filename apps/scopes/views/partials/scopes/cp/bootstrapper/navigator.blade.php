<div class="mb-2 mb-sm-0 position-relative">
	<label class="d-block d-md-none mb-0">Scope Selection</label>
	<select name="role" class="custom-select" data-toggle="combobox">
		<option value="">--- GLOBAL ---</option>
		@foreach ($scope->getRoles() as $role)
			<option value="{{ $role }}" @if ($scope->activeRole() === $role) SELECTED @endif>{{ $role }}</option>
		@endforeach
	</select>
	<hr class="d-md-none"/>
</div>