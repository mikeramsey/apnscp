@include('partials.scope-form-submit')

<div class="flex mt-2">
	<h5 class="hide heading">Pending Changes</h5>
	<ol class="list pl-3" id="pendingList">
		<li class="mb-1 template hide">
			<button class="btn btn-sm btn-outline-secondary ui-action-label warn ui-action-delete ui-action mr-1"
			        type="button"> {{ _("Delete") }}
			</button>
			<span class="font-weight-normal text-uppercase badge badge-default bg-transparent role">apnscp/initialize</span>
			<span class="var"></span>:
			<span class="preaction"></span> => <span class="postaction"></span>
		</li>
	</ol>
</div>
