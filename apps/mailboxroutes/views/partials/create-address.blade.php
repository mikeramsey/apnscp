<div class="row">
	<h3 class="col-12">Create a New Address</h3>
</div>

<form action="/apps/mailboxcreate" method="post">

	<div class="row">
		<div class="col-12">
			<fieldset class="form-group row" id="address-entry">
				<label class="input-label col-12">Enter a new email address:</label>
				<div class="col-12">
					<div class="input-group">
						<div class="btn-group">
							<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
							        aria-haspopup="true" aria-expanded="false">
								<span class="sr-only">Additional Options</span>
							</button>
							<div class="dropdown-menu dropdown-menu-form" id="catchAllOption">
								<div class="dropdown-item">
									<label for="catchallNo" class="mb-0">
										<input type="radio" class="catchall" name="catchall" value="0" checked="CHECKED"
									       id="catchallNo"/>
										Create normal address
									</label>
								</div>
								<div class="dropdown-item" title="Catch-alls capture mail sent to a non-existent e-mail account.
                                    Such accounts are hotbeds for spam and may not forward to a remote address"
								   data-toggle="tooltip" data-placement="bottom">
									<label for="catchall" rel="catchall" class="mb-0 apnscp-tooltip">
										<input type="radio" class="catchall" name="catchall" value="1" id="catchall"/>

										Create catch-all
									</label>
								</div>
							</div>
						</div>
						<span class="input-group-addon">
								<i class='fa fa-user'></i>
							</span>
						<input type="text" maxlength="52" class="form-control col-4" placeholder="user"
						       name="username" id="username" aria-describedby="user name"/>
						<span class="input-group-addon">@</span>
						<select name="domain[]" class="form-control custom-select" id="transport">
							@foreach ($Page->get_transports() as $host)
								<option id="{{ $host }}" value="{{ $host }}">{{ $host }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<p class="missing-domain hidden-sm-down note col-6 push-6 text-muted" id="missing-domain"
				   data-toggle="tooltip" data-placement="left"
				   title="Visit Mail &gt; Mail Routing to enable e-mail for the domain." rel="missing-domain">
					Missing a domain?
				</p>
			</fieldset>

		</div>
	</div>

	@include('partials.add-address')

	<hr class="clearfix"/>
	<p class="text-muted note mt-1">Need help accessing email? Check KB:
		<a class="ui-action ui-action-label ui-action-kb" href="{{ MISC_KB_BASE }}/e-mail/accessing-e-mail/">
			Accessing Email
		</a>
	</p>

	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-primary mr-3 mt-1" value="Add Address" name="add">
				Add Address
			</button>

			<button class="mt-1 btn btn-secondary" id="bulk-add" data-toggle="collapse" href="#bulk-add-form"
	            aria-expanded="false" aria-controls="bulk-add-form">
				Add Multiple Addresses
			</button>
		</div>
	</div>

	<div class="mt-1 row">
		<div id="bulk-add-form" class="collapse">
			<h4 class="col-12">Multiple Addresses</h4>

			<p id="multipleAddressHelp" class="collapse col-12">
				<b>Instructions:</b> each e-mail address should reside on a line by itself. The left-hand
				value is the e-mail address to create. If no domain is provided, the domain is assumed to be
				<em>{{ \Util_Conf::login_domain() }}</em>. Any address after the left-hand value is a
				destination.
				If no domain is present, it is assumed to be a local user
				(User &gt; <a class="ui-action ui-action-switch-app ui-action-label" href="/apps/usermanage">
					Manage Users
				</a>). Any user not present is discarded.
			</p>
			<div class="col-12">
				<div class="row">
					<fieldset class="col-12">
						<label for="bulk-entry" class="hinted">myemail user@gmail.com, user3@hotmail.com, localuser<br/>email{{ '@' . \Util_Conf::login_domain() }} localuser</label>
						<textarea name="bulk-entry" class="w-100 hinted form-control" id="bulk-entry"></textarea>
					</fieldset>
				</div>
				<div class="row">
					<div class="col-12 mt-1 ">
						<input type="submit" class="btn btn-primary" value="Bulk Add Address" name="bulk-add"/>
						<button class="btn ml-3 btn-secondary" data-toggle="collapse" href="#multipleAddressHelp"
						        aria-expanded="false" aria-controls="multipleAddressHelp">
							<i class="fa fa-life-ring"></i>
							Help
						</button>
					</div>

					<div class="col-12 mt-1">
						<fieldset class="form-group checkbox">
							<label class="custom-checkbox custom-control mt-1 align-items-center d-flex">
								<input type="checkbox" class="custom-control-input" name="bulk-overwrite"
								       value="1" id="bulk-overwrite"/>
								<span class="custom-control-indicator"></span>
								Overwrite email address
							</label>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>