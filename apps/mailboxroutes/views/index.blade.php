<!-- end disabled -->
<div class="row">
	<div class="col-12 text-right">
		<label>Change Mode:</label>
		@if ($Page->getMode() === 'add')
			<a class="ui-action ui-action-switch-app ui-action-label" href="{{ \HTML_Kit::new_page_url_params(\Template_Engine::init()->getPathFromApp('mailboxroutes')) }}"
				>View/Edit Mailboxes</a>
		@else
			<a class="ui-action ui-action-switch-app ui-action-label" href="{{ \HTML_Kit::new_page_url_params(\Template_Engine::init()->getPathFromApp('mailboxcreate')) }}"
				>Add Email Address</a>
		@endif
	</div>
</div>
<form method="post" action="{{ \HTML_Kit::page_url_params(['mode' => $Page->getMode()]) }}">
	@includeWhen($Page->disabled_exists(), 'partials.disabled-addresses')
</form>
@includeWhen($Page->getMode() === 'add', 'partials.create-address')
@includeWhen($Page->getMode() !== 'add', 'partials.enabled-addresses')