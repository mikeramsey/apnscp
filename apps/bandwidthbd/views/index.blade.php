<?php
$periods = $Page->getSpans();
$active = $Page->getActiveSpan();
$begin = $active['begin'];
$end = $active['end'];
$bw = $Page->getBandwidth($begin, $end);
?>
<form action="bandwidthbd" method="get">
	<div class="row clearfix mb-1">
		<div class="col-12 col-md-6 period-selector">
			<label>Period</label>
			<select id="bandwidth-span" class="form-control custom-select" name="span" onChange="this.form.submit();">
				<?php
                    foreach ($periods as $period) {
                        ?>
				<option
				<?php if ($period['begin'] == $begin && $period['end'] == $end) {
                            print('selected="selected"');
                        } ?> value="<?php print($period['begin'].",".$period['end']); ?>">
				<?php
                                $str = date('F jS, Y',$period['begin']) . ' - ';
                                if ($period['end']) {
                                    $str .= date('F jS, Y', $period['end']);
                                } else {
                                    $str .= 'Current';
                                }

                                print $str; ?></option>
				<?php
                    }
                ?>
			</select>
		</div>
	</div>
</form>

<h3>Usage Overview</h3>
<div class="row mb-1">
	<div class="col-12 col-lg-6">
		<?php if (!$bw): ?>
		<div class="alert alert-info">
			No bandwidth data available for current cycle.
		</div>
		<?php else: ?>
		<table id="bandwidth-data" class="bandwidth-data table">
			<thead class="heading">
			<tr>
				<th class="heading service"></th>
				<th class="heading ext-info"></th>
				<th class="heading in bandwidth right">
					<i class="fa fa-download"></i>
					In (MB)
				</th>
				<th class="heading out bandwidth right">
					<i class="fa fa-upload"></i>
					Out (MB)
				</th>
				<th class="heading out percentage">% Total</th>
			</tr>
			</thead>
			<tbody>
			<?php
                foreach ($bw as $majname => $majdata):
			if ($majname == "*") continue;
			?>
			<tr class="bandwidth-major">
				<td class="" colspan="2">
					<h4><?php print $majname; ?></h4>
				</td>
				<td class="in right"><?php print \Formatter::commafy(sprintf("%.2f", $majdata['in']/1024/1024)); ?>MB
				</td>
				<td class="out right"><?php print \Formatter::commafy(sprintf("%.2f", $majdata['out']/1024/1024)); ?>MB
				</td>
				<td class="percentage"><?php printf ("%.2f%%", ($majdata['total']/$bw['*']['total'])*100); ?></td>
			</tr>
			<?php
                    foreach ($majdata['items'] as $bwitem): ?>
			<tr class="bandwidth-item">
				<td class="ext-info" colspan="2">
					<span class="key" style="border-color:<?php print $bwitem['color']; ?>;"></span>
					<?php print $bwitem['ext_info']; ?>
				</td>
				<td class="in right"><?php print \Formatter::commafy(sprintf("%.2f", $bwitem['in']/1024/1024)); ?>MB
				</td>
				<td class="out right"><?php print \Formatter::commafy(sprintf("%.2f", $bwitem['out']/1024/1024)); ?>MB
				</td>
				<td class="percentage"><?php printf ("%.2f%%", (($bwitem['in']+$bwitem['out'])/$majdata['total'])*100); ?></td>
			</tr>
			<?php
                        /** bw items */
                    endforeach;
                    /** bw categories */
                endforeach;
            ?>
			</tbody>
			<tfoot>

			<tr>
				<td class=""></td>
				<td class="total right"><b>Total</b></td>
				<td class="in right"><?php print \Formatter::commafy(sprintf("%.2f", $bw['*']['in']/1024/1024)); ?>MB
				</td>
				<td class="out right"><?php print \Formatter::commafy(sprintf("%.2f", $bw['*']['out']/1024/1024)); ?>MB
				</td>
				<td class="percentage"><?php print \Formatter::commafy(sprintf("%.2f", ($bw['*']['in']+$bw['*']['out'])/1024/1024/1024)); ?>
					GB
				</td>
			</tr>
			</tfoot>
		</table>
	</div>
	<div class="col-12 hidden-md-down col-lg-6">
		<div id="bwgraph" class="img-fluid img w-100 h-100"></div>
		<div id="bwlegend"></div>
	</div>
	<?php endif; ?>
</div>