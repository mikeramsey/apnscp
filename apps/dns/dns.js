if (typeof(ns) === 'undefined') {
	var ns = '';
}
var DFD = new $.Deferred(), ACTIVE_EDIT = null, DNS_HINT_MAP = {
	'A': '127.0.0.1',
	'AAAA': '2002:7f00:0001',
	'A6': '2002:7f00:0001',
	'CAA': '128 issue "letsencrypt.org"',
	'CERT': 'IPGP 0 0 14 57446EFDE098E5C934B69C7DC208ADDE26C2B797',
	'CNAME': 'hostname.example.com',
	'DNAME': 'aliased.example.com',
	'DNSKEY': '257 3 5 2018hiZsq1jkCS3osdcAksvcd3oSC0f43OI=',
	'DS': '25924 5 1 0AC4F2E44C582AE809208098F7BE2C44AB947DCC',
	'HINFO': '"MACHINE1" (University Computer)',
	'LOC': '33 46 23.6424 N 84 23 42.59 W 293m 0.00m 10000m 10m',
	'MX': '10 mail.example.com',
	'NAPTR': '100 10 "U" "E2U+sip" "!^.*$!sip:customer-service@example.com!" .',
	'NS': 'ns1.example.com',
	'PTR': 'srv1.example.com',
	'SMIMEA': '3 0 0 3082036E30820256A003020102021500BD0C84427A0D76F6D76249F88E3E94FF7BFC996D300D06092A864886F70D01010B05003020311E301C06035504030C156D61747440617069736E6574776F726B732E636F6D301E170D3139303830323030303131355A170D3230303830313030303131355A30263124302206092A864886F70D01090116156D61747440617069736E6574776F726B732E636F6D30820122300D06092A864886F70D01010105000382010F003082010A0282010100E955AA34AE95B4B3A85316040DFA42DBD6FB77B15EEF872F8851C0E1493BA04D3857EE6BC4AE591CED09F7E0751178C4A8B38CF6B9A036D23839C7F81AB5BCD71B9BE923BC4E29A27906F0DD824B2ED6630AB9F76D27034189BF9744463C575DAC8BA146BF44A99854AAEBCF88AA4A19D6A6D4A2754B6080EE9FF44F0336C179AC226A079DC8BFDDB7CD2FF39438720D6B9FDE325193495F8EBB4B874F1934FA09E83876352274BE8A96A8445DB89357BFD37FEC46DE98ED70FB0F5DEB890D73F8F85D2F71B82C4B2EA2715A9C03A996B089A5C832BD9007227202666097A3C9A01592BEDADC24DCE659C625D2F971B91C9F8365C663B7E64C249965B36C3EBD0203010001A38198308195301F0603551D230418301680143798C95ECF161FDA18D9C29E725348CCF05B0B37301D0603551D0E041604143798C95ECF161FDA18D9C29E725348CCF05B0B37300E0603551D0F0101FF0404030205A0300C0603551D130101FF0402300030130603551D25040C300A06082B0601050507030430200603551D110419301781156D61747440617069736E6574776F726B732E636F6D300D06092A864886F70D01010B050003820101002E8EA47FB20C3630C5D2A8A9E045AF1A099449BF3C86C7942B71E6F46B5D19E590F3D8FA695227544FB0140341144C16819B710275724E35A4378927AAEC24644EB11F51E60E28C7F36CBCAD4D32FFFDEA29BCE22F935B5CD435CA3FCF8714324A088B647974B81B74263F16D1F4BC1CF7D6033711B0A566685F7855DC93A0F9C1FECDB4E94FF10CC05AEE07B2D181FABA861EAA7C287E4EDC024B95AE742088951F118FF26DA4C515BF223512817A7CF382A01274466B9AEFAB51CA78167C35AE51A4BBB24CE3B556FCDC395631F99026CEE1A8D06B53B0C924DF88D7E7DF0CD1F9E861E52A97BBE5E96FA5C0D8048A524FE50E3903C052656634339FBE14DA',
	'SOA': 'master.example.com. hostmater.example.com. 1 3600 1800 86400 600',
	'SPF': 'v=spf1 a mx ip4:69.64.153.131 include:_spf.google.com ~all',
	'SRV': '10 50 5611 my.jabberserver.com',
	'SSHFP': '1 1 0ac4f2e44c582ae809208098f7be2c44ab947dcc',
	'TLSA': '3 1 1 6343fbfe4ab12deb9c829bb85ea3abc803c0f70559b506ddd14467ee0a7ab70d',
	'TXT': 'v=spf1 a mx ip4:69.64.153.131 include:_spf.google.com ~all',
	'URI': '10 1 "ftp://ftp1.example.com/public"'
}, change_rr = function () {
	var rr_type = $('#rr :selected').val();
	var $param = $('#parameter');
	if (rr_type && DNS_HINT_MAP[rr_type] != undefined && (!$param.val() || $param.hasClass('hinted')))
		$('#label-parameter').text(DNS_HINT_MAP[rr_type]).unbind('.hinted').hinted();
}, tbActions = {
	'toolbox-dns-restore': {
		warn: "This will erase all DNS changes made since the account was created.  Do you wish to proceed?",
		action: "restore"
	},
	'toolbox-mx-apnscp': {
		warn: "All custom MX records will be replaced with the default settings.  " +
		"This server will receive e-mail for this domain.  Do you wish to proceed?",
		action: "mxapnscp"
	},
	'toolbox-mx-gmail': {
		warn: "Mail for this account will be handled by Hosted GMail.  " +
		"You must already have a hosted GMail account.  Do you wish to proceed?",
		action: "mxgmail"
	},
	'toolbox-mx-clone': {
		warn: "MX records will be cloned from " + ns + ".  Do you wish to proceed?",
		action: 'mxclone'
	},
	'toolbox-dns-details': {
		warn: null,
		action: function(e) {
			$(e.target.dataset.target).collapse('toggle');
		}
	},
	'toolbox-clone-domain': {
		warn: null,
		action: function () {
			$('#cloneDomain select').append(
				($('#domain-picker option').get() || []).filter(function (el) {
					return el.value != dns_domain;
				}).map(function (el) {
					return '<option value="' + el.value + '">' + el.value + '</option>';
				})
			).on('change', function () {
				$('#cloneDomain .source').text(this.value);
				$('#confirmRecordImport').prop('checked', false);
			}).triggerHandler('change');
			var dialog = apnscp.modal($('#cloneDomain'), {buttons:
			[
				$('<button>').attr({
					'type': 'submit',
					name: 'clone-domain',
					'class': 'btn btn-primary',
					value: 1,
					'id': 'confirmRecordSubmit'
				}).prop({
					'disabled': true
				}).text("Clone DNS").click(function() {
					$('.modal-content form').submit();
				})
			]});

			dialog.on('show.bs.modal', function() {
				var $indicator = $('<i class="ui-ajax-indicator"></i>'),
					$btn = dialog.find('.btn-primary').find('.ui-ajax-indicator').remove().end().prepend($indicator);
				dialog.find('.ui-ajax-error-msg').remove();

				$(':checkbox', dialog).on('change', function () {
					var $btn = $('.modal-footer :submit');
					$btn.prop('disabled', !$(this).prop('checked'));
				});
				$('form', dialog).on('submit', function() {
					$btn.data('old-title', $(this).text()).text('Processing').prepend($indicator.addClass('mr-2 ui-ajax-loading').append([
						$(':input[name=src-domain]').clone(),
						$(':input[name=target-domain]').clone(),
					]));
				});
			}).modal('show');
		}
	},
	'toolbox-export': {
		action: 'export'
	}
};

$(window).on('load', function () {
	// set deferred object as completed
	DFD.resolve();

	$('#rr').change(change_rr);
	change_rr();
	apnscp.hinted();
	$('tr.entry').highlight();
	$('#dns_records').tablesorter({headers: {0: {sorter: false}, 6: {sorter: false}}});
	$('#dns_records thead tr th:eq(6)').addClass('nosort');
	$('#dns_records span.parameter_truncated').parent().mouseover(function () {

		$('span.parameter_full', this).css('display', 'block');
	}).mouseout(function () {
		$('span.parameter_full', this).css('display', 'none');
		$('span.parameter_truncated', this).css('display', 'block');
	});

	$('#dns_records span.hostname_truncated').parent().parent().mouseover(function () {
		$('span.hostname_truncated', this).css('display', 'none');
		$('span.hostname_full', this).css('display', 'block');
	}).mouseout(function () {
		$('span.hostname_full', this).css('display', 'none');
		$('span.hostname_truncated', this).css('display', 'block');
	});

	$('.dns-filter').click(function (e) {
		if (e.target.id == "do_filter") {
			var field = $('#filter_spec').val(),
				pattern = $('#filter').val(),
				re = new RegExp(pattern, "i"),
				selector = 'td.dns_' + field;
			$('tr.entry').filter(function () {
				return re.test($(this).children(selector).text()) == false;
			}).fadeOut('fast', function () {
				$(this).removeClass('ui-highlight').find(':checkbox').prop('checked', false);
			});
			var $el = $('<li class="ui-action-delete filter-' + field + '" />').text(pattern).click(function () {
				return remove_filter.apply(this, [pattern, $(this).data("selector")]);
			});
			$el.data("selector", selector);
			$(this).find('.ui-active-filters').append($el);
			if ($('#reset_filter:hidden').length > 0) {
				$('#reset_filter').fadeIn();
			}
			return false;
		}
		if (e.target.id == "reset_filter") {
			$(this).find('.ui-active-filters').empty();
			$('#reset_filter').hide();
			return $('tr.entry:hidden').fadeIn();
		}
	}).bind('keydown', function (e) {
		if (e.keyCode === $.ui.keyCode.ESCAPE) {
			$('#reset_filter').click();
			$('#filter').val('');
		} else if (e.keyCode === $.ui.keyCode.ENTER) {
			$(this).triggerHandler('click');
		}
	});

	$('select#domain-picker').change(function () {
		var mode = location.search.substring(1).match(/mode=([a-z]+)/);
		if (mode != null && mode[1] == 'edit')
			mode = 'edit';
		else mode = 'add';
		location.assign('/apps/dns.php?domain=' + $(this).val() + '&mode=' + mode);
	});

	$('#toolbox a').click(function (e) {
		if (tbActions[this.id].warn != undefined) {
			if (!confirm(tbActions[this.id].warn)) {
				return false;
			}
		}

		if (typeof (tbActions[this.id].action) === 'function') {
			tbActions[this.id].action(e);
		} else {
			$(this).attr('href', '/apps/dns?mode=edit&domain=' + dns_domain/* from dns.php */ + '&toolbox=' + tbActions[this.id].action);
		}

		return true;
	});
	$("#toolbox [data-toggle=tooltip]").tooltip();
});

String.prototype.escapeHTML = function () {
	return (
		this.replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;').replace(/"/g, '&quot;')
	);
};

$(document).ready(function () {
	$('tr.entry').bind('click keydown', function (e) {
		return handle_row(e);
	});

	$("#domain-picker").combobox().on('comboboxselect', function(event, item) {
		$('select#domain-picker').triggerHandler('change');
	});

	$('#domainDetails').on('shown.bs.collapse', function (e) {
		var container = this;
		apnscp.render({'render-details': dns_domain}, '').done(function (ret) {
			$(container).html(ret);
		});
		return true;
	});
});

/**
 * Listener for actions column
 */
function handle_row(e) {
	var $caller = $(e.target);
	if (e.type == "keydown") {
		switch (e.which) {
			case 27:
				// ESC
				var $row = $(e.currentTarget);
				/* TR class="entry" */
				restore_row($row);
				break;
			case 13:
				// ENTER
				$(e.currentTarget).find(':submit.save').click();
				break;
			case 9:
				// TAB
				return true;
			default:
				return true;
		}
		return false;
	} else if ($caller.hasClass('ui-action-delete')) {
		// delete DNS entry
		var $target = $(e.currentTarget),
			subdomain = $target.find('span.hostname').text(),
			name = [subdomain, dns_domain].join(".").replace(/^\.|\.$|\s+/g, ''),
			rr = $target.find('.dns_rr').text().replace(/\s+/g, ''),
			// don't substitute whitespace, which can be present accidentally
			parameter = $target.find('.dns_parameter .parameter').text();
		// .replace(/^\s+|\s+$/g,'');
		if (!confirm("Delete DNS record " + name + "?")) {
			return false;
		}
		params = {
			zone: dns_domain,
			subdomain: subdomain,
			rr: rr,
			parameter: parameter.trim()
		};

		apnscp.cmd('dns_remove_record', params, function (ret) {
			if (ret.success) {
				$(e.currentTarget).fadeOut('slow', function () {
					$(this).remove();
				});
			}
		}, {contextId: typeof contextId !== 'undefined' ? contextId : session.id});
		return false;
	} else if ($caller.hasClass('ui-action-edit')) {
		// edit DNS entry
		make_editable($caller.attr('rel'));
		return false;
	}
	return true;
}

function remove_filter(pattern, selector) {
	var re = new RegExp(pattern, "i");
	$('tr.entry:hidden').filter(function () {
		return re.test($(this).children(selector).text()) == false;
	}).fadeIn('fast');

	$(this).remove();
	if ($('.ui-active-filters li').length == 0) {
		$('#reset_filter').fadeOut();
	}
	return false;
}

function restore_row($row) {
	if (typeof($row) != "object") {
		$row = $($row.currentTarget);
		/* TR class="entry" */
	}
	var old_data = $row.find(':submit').data('old');
	return set_row($row, old_data);
}

function set_row($parent, data) {
	$('.dns_rr', $parent).text(data.rr);
	$('.dns_parameter', $parent).html(
		$('<span class="parameter">' + data.parameter.escapeHTML() + '</span>')
	);
	$('.dns_hostname', $parent).html(
		$('<span class="hostname">' + data.name + '</span>')
	);
	$('.dns_rr', $parent).text(data.rr);
	$('.dns_ttl', $parent).text(data.ttl);
	set_hash(data.name, data.rr, data.parameter, $parent);
	$('.actions', $parent).replaceWith($parent.data('buttons'));

	// update original state

	return false;
}

/**
 * $row jQuery Object parent row (TR.entry)
 * data_new hash
 * data_old hash
 *
 * Save data
 */
function save_row($row, data_new, data_old) {
	$('#ajax-singleton').remove();
	var $indicator = $('<span class="ui-action-label ui-ajax-loading mx-0" id="ajax-singleton"></span>');
	$row.find('.actions .btn-primary').prepend($indicator);
	return apnscp.cmd('dns_modify_record', [dns_domain, data_old.name, data_old.rr, data_old.parameter.trim(), data_new], function (ret) {
		var klass;

		if (ret.success) {
			set_row($row, data_new);
			// $indicator gets wiped on set_row
			$row.find('.actions .btn:first-child').prepend($indicator);
			klass = 'ui-ajax-success';
		} else {
			klass = 'ui-ajax-error';
			set_row($row, data_old);
		}

		$indicator.removeClass('ui-ajax-loading').addClass(klass);
		setTimeout(function () {
			$indicator.fadeOut('normal', function () {
				$(this).remove();
			})
		}, 1500);
	}, {indicator: $indicator, contextId: typeof contextId !== 'undefined' ? contextId : session.id});
}

function make_editable(location) {
	if (DFD.state() == "pending") {
		$(ACTIVE_EDIT).find(':submit').click();
		// remove row highlight
		// @TODO highlight should be rewritten to fire on :checkbox/div/span
		$(ACTIVE_EDIT).click();
	}
	return DFD.done(function () {
		_make_editable(location);
	});

}

function _make_editable(location) {
	var $row = $('tr.entry:eq(' + location + ')'),
		name_value = $('span.hostname', '#name' + location).text().replace(/^\s*|\s*$/g, ''),
		rr_value = $('#rr' + location).text().replace(/^\s*|\s*$/g, ''),
		class_value = $('#class' + location).text().replace(/^\s*|\s*$/g, ''),
		ttl_value = $('#ttl' + location).text().replace(/^\s*|\s*$/g, ''),
		parameter_value = $('span.parameter', '#parameter' + location).text().replace(/^\s*|\s*$/g, ''),
		$buttons = $('.actions', $row).clone(true);
	$row.data('buttons', $buttons);
	// update active edited row
	ACTIVE_EDIT = $row;
	DFD = new $.Deferred();
	$('#name' + location).html('<input size="30" class="form-control" type="text" style="text-align:left;" ' +
		'name="name[' + location + ']" value="' + name_value + '" />');

	var rr_text = '<select class="rr-select form-control custom-select" name="rr[' + location + ']">';
	var opts = DNS_RRS || Object.keys(DNS_HINT_MAP);
	for (var i = 0; i < opts.length; i++) {
		rr_text += '<option value="' + opts[i] + '" ' +
			(rr_value == opts[i] ? "SELECTED" : "") + '>' +
			opts[i] +
			'</option>';
	}
	rr_text += '</select>';

	$('#rr' + location).html(rr_text);

	var $save = $('<button type="submit" class="btn btn-primary save" value="Save" name="save">Save</button>').click(function () {
		/**
		 * Save DNS modification
		 */
		var data_old = $(this).data('old'),
			hash = set_hash(
				data_old.name,
				data_old.rr,
				data_old.parameter,
				$row
			), data_new = {
				name: $(':input', '#name' + location).val(),
				rr: $(':input', '#rr' + location).val(),
				ttl: $(':input', '#ttl' + location).val(),
				'class': "IN", /** CLASS parameter will always be IN(ternet) */
				parameter: $(':input', '#parameter' + location).val(),
				hash: hash
			};
		$(this).data('new', data_new);
		$.when(save_row($row, data_new, data_old)).done(function () {
			ACTIVE_EDIT = null;
			DFD.resolve();
		});
		return false;

	}), $undo = $('<input type="reset" class="btn btn-secondary reset" value="Undo" name="undo" />').click(function () {
		ACTIVE_EDIT = null;
		DFD.resolve();
		return restore_row($row);
	}), $state = $('<input type="hidden" name="original_state[' + location + ']" value="' +
		name_value + ' ' + rr_value + ' ' + parameter_value.escapeHTML() + '" />');

	$save.data('old', {
		name: name_value,
		rr: rr_value,
		ttl: ttl_value,
		'class': class_value,
		parameter: parameter_value,
	});

	$('tr.entry:eq(' + location + ') .actions').empty().append($save).append($undo).append($state);

	$('#ttl' + location).html('<input type="text" class="form-control" size="6" name="ttl[' + location + ']" value="' + ttl_value + '" />');
	$('#parameter' + location).html('<input type="text" class="form-control" size="30" name="parameter[' + location + "]\" value=\"" + parameter_value.escapeHTML() + "\" />");

	// make subdomain field first focus
	$('#name' + location).find('input').focus();
	return false;
}

/**
 *
 */
function set_hash(subdomain, rr, parameter, $row) {
	apnscp.call_app('dns', 'record_encode', [subdomain, rr, parameter], {
		async: true,
		success: function (hash) {
			$row.find('.actions .ui-action-delete').attr('rel', hash);
			$row.find(':checkbox[name=delete\\[\\]]').val(hash);
		},
		contextId: typeof contextId !== 'undefined' ? contextId : session.id
	});

}

