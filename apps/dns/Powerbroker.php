<?php declare(strict_types=1);

namespace apps\dns;

use Opcenter\Dns\Providers\Powerdns\Module;

class Powerbroker extends Module
{
	public function getExportedFunctions(): array
	{
		if (!($this->permission_level & PRIVILEGE_ADMIN)) {
			return parent::getExportedFunctions();
		}

		return ['*' => PRIVILEGE_ADMIN] + parent::getExportedFunctions();
	}

	public function __sleep()
	{
		return array_keys(get_object_vars($this));
	}

}