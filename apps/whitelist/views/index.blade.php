<h3>Whitelist</h3>
<div class="row">
	<div class="col-12">
		<form action="{{ HTML_Kit::page_url() }}" method="POST">
			<label class="form-control-label d-block mb-0">
				IPv4 or IPv6 Address
			</label>
			<div class="form-inline">
				<div class="input-group mt-2">
					<span class="input-group-addon">
						<i class="fa fa-globe"></i>
					</span>
					<label for="ipInput" class="hinted">{{ \Auth::client_ip() }}</label>
					<input class="form-control mr-3" maxlength="39" size="39" id="ipInput" name="ip" placeholder="" value="" />
				</div>
				<button class=" btn btn-primary ui-action ui-action-add ui-action-label bindable mt-2" name="add" type="submit">
					Add Entry
				</button>
			</div>
			<ul class="text-danger list-unstyled mt-1" id="errors">

			</ul>
		</form>
	</div>
</div>

@include('partials.active')