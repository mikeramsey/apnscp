<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace apps\nexus;

	use Opcenter\Account\Enumerate;
	use Opcenter\Auth\Shadow;
	use Opcenter\CliParser;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Plans;
	use Opcenter\SiteConfiguration;

	class Page extends \Page_Container
	{
		const NOTIFY_CREATE_KEY = 'nexus.notify-create';
		// @var int minimum number of accounts to check state
		const DETAILED_LIST_THRESHOLD = 25;
		const THRESHOLD_DANGER = 95;
		const THRESHOLD_WARNING = 75;
		const SERVICE_CACHE_KEY = 'admin.svccfg';
		const ACCOUNT_CACHE_KEY = 'admin.acctc';
		const IGNORED_SKIPLIST = [
			'siteinfo.domain',
			'siteinfo.email',
			'siteinfo.admin',
			'siteinfo.admin_user',
			'siteinfo.notes',
			'siteinfo.plan',
			'auth.tpasswd',
			'auth.passwd',
			'auth.cpasswd'
		];
		protected $oldServiceCache = [];
		protected $accountCache = [];
		protected $resourceCache = [];
		protected $serviceCache;
		/**
		 * @var string last site created
		 */
		protected $lastCreated;
		private $skipList;

		private $fieldChanges;

		public function __construct(bool $doPostback = true)
		{
			parent::__construct($doPostback);
			$this->add_javascript('nexus.js');
			$this->add_css('nexus.css');
			$this->skipList = array_combine(static::IGNORED_SKIPLIST,
				array_fill(0, \count(static::IGNORED_SKIPLIST), 1));
			if (null === $this->serviceCache) {
				$this->populateServiceCache();
			}
			if ([] === $this->resourceCache) {
				$this->populateResourceCache();
			}
			if ($this->getMode() === 'edit') {
				$this->populateSiteCache();
			}
		}

		private function populateServiceCache(): void
		{
			$plan = $this->getPlanName();
			$this->serviceCache = $this->getServiceCache($plan);
		}


		private function populateResourceCache(): void
		{
			$cache = \Cache_Account::spawn();
			if (false !== ($res = $cache->get('nexus.rescache'))) {
				$this->resourceCache = $res;
				return;
			}
			$this->resourceCache = [
				'bandwidth' => $this->admin_get_usage('bandwidth'),
				'storage'   => $this->admin_get_usage('storage'),
				'cgroup'    => \Preferences::get('nexus.show-resources') ? $this->admin_get_usage('cgroup') : []
			];
			// pulling quota can be computationally expensive; this should
			// ideally be moved to a cron in the future
			$n = count($this->resourceCache['storage']);
			if ($n < 100) {
				$ttl = 300;
			} else if ($n < 250) {
				$ttl = 619;
			} else if ($n < 450) {
				$ttl = 1811;
			} else if ($n < 600) {
				$ttl = 2879;
			} else {
				$ttl = 5441;
			}
			$cache->set('nexus.rescache', $this->resourceCache, $ttl);
		}

		/**
		 * Get notification threshold
		 *
		 * @param string $type
		 * @return int
		 */
		public function getThreshold(string $type): int
		{
			static $thresholds;
			if (!$thresholds) {
				$thresholds = [
					'danger'  => (int)\Preferences::get('nexus.threshold-danger', self::THRESHOLD_DANGER),
					'warning' => (int)\Preferences::get('nexus.threshold-warning', self::THRESHOLD_WARNING),
				];
			}
			return $thresholds[$type] ?? 999;
		}

		/**
		 * Get site resource usage
		 *
		 * @param string $type
		 * @param string $site
		 * @return array|null
		 */
		public function getResourceUsage(string $type, string $site): array
		{
			return $this->resourceCache[$type][$site] ?? [];
		}

		private function getServiceCache(string $plan = null): array {
			$cache = \Cache_Global::spawn();
			$parsed = $cache->get(static::SERVICE_CACHE_KEY);
			$plan = $plan ?? $this->getPlanName();
			if (false !== $parsed && isset($parsed[$plan])) {
				return $parsed[$plan];
			}
			$parsed = (array)$parsed;
			$data = $this->admin_get_service_info(null, $plan);
			$parsed[$plan] = $data;
			$cache->set(static::SERVICE_CACHE_KEY, $parsed, 599);

			return $data;
		}

		protected function getMode(): string
		{
			$uri = strtok($_SERVER['REQUEST_URI'], '?');
			if (0 === strpos($uri, \Template_Engine::BASE_DIR . '/' . $this->getApplicationID() . '/edit/')) {
				return 'edit';
			}

			return 'new';
		}

		private function populateSiteCache()
		{
			$conf = \Auth::context(null, $this->getEditTarget())->getAccount()->conf;
			$plan = array_get($conf, 'siteinfo.plan', Plans::default());
			if ($plan === DefaultNullable::NULLABLE_MARKER) {
				$plan = Plans::default();
			}
			if (!Plans::exists($plan)) {
				warn("Plan %(original)s does not exist - defaulting to %(default)s",
					['original' => $plan, 'default' => Plans::default()]
				);

				$plan = Plans::default();
			}

			$oldPlanCache = $this->getServiceCache($plan);
			// pluck plan from siteinfo.plan
			$newPlan = array_get($_POST, 'config.siteinfo.plan', $plan);
			if ($newPlan !== array_get($oldPlanCache, 'siteinfo.plan', Plans::default())) {
				$this->serviceCache = $this->getServiceCache($newPlan);
				$this->fieldChanges = Plans::diff($newPlan, $plan);
			}

			foreach ($conf as $svc => $svcvars) {
				$forced = false;
				if (null !== ($previous = array_get($this->oldServiceCache, "${svc}.enabled", null))) {
					$forced = !$previous && array_get($conf, "${svc}.enabled", false);
				}
				foreach ($svcvars as $var => $val) {
					if (!isset($this->serviceCache[$svc][$var])) {
						// version or enabled
						continue;
					}
					if ($forced || $val !== $oldPlanCache[$svc][$var]['default']) {
						$this->serviceCache[$svc][$var]['value'] = $val;
					}
				}
			}
		}

		protected function getEditTarget(): ?string
		{
			$target = basename(strtok($_SERVER['REQUEST_URI'], '?'));
			if (0 === strpos($target, 'site')) {
				return $target;
			}

			return null;
		}

		public function getCreateTarget(): ?string
		{
			return $this->lastCreated;
		}

		/**
		 * Get site from edit target
		 *
		 * @return string
		 */
		public function getSite(): ?string
		{
			return $this->getCreateTarget() ?? $this->getEditTarget();
		}

		public function _render()
		{
			if ($this->getMode() === 'edit') {
				$this->populateSiteCache();
				return;
			}

			$json = array_build($this->listAccounts(), function ($k, $v) {

				return [$k, $this->getMetaFromSite($k)['siteinfo']['domain'] ?? null];
			});
			$this->add_javascript('var SITES=' . json_encode($json) . ';', 'internal', false, true);
		}

		public function compose(array $view, array $params = []): \Illuminate\View\View
		{
			return parent::compose($view,
				$params + ['mode' => $this->getMode()]);
		}

		public function on_postback($params)
		{
			$actionMap = [
				'suspend'  => 'deactivate_site',
				'activate' => 'activate_site',
				'delete'   => 'delete_site'
			];
			foreach ($actionMap as $action => $fn) {
				if (!isset($params[$action])) {
					continue;
				}
				$fn = "admin_${fn}";
				$val = $params[$action];
				$args = [];
				if ($fn === 'admin_deactivate_site') {
					if (!is_array($val)) {
						$val = explode(',', trim($val, ','));
					}
					$args = [
						'reason' => $params['custom-message'] ?? null
					];
				}
				$this->forgetCache($val);
				return $this->$fn($val, $args);
			}

			if (isset($params['change-plan'])) {
				$_POST['config'] = $this->filterConfig(
					$params['config'],
					$this->getServiceCache($params['change-plan'])
				);
				array_walk_recursive($_POST['config'], static function (&$v) {
					$v = \Util_Conf::inferType($v);
					if (\is_array($v)) {
						$v = implode("\n", $v);
					}
				});
				array_set($_POST, 'siteinfo.plan', array_get($params, 'siteinfo.plan'));
			}
			if (isset($params['hijack'])) {
				if (!$sid = $this->admin_hijack($params['hijack'])) {
					return false;
				}
				\Auth::get_driver()->login_success();
				exit();
			}
			if (isset($params['resetpw'])) {
				return $this->auth_reset_password(null, $params['resetpw']);
			}
			if (isset($params['add'])) {
				if ($this->addSite($params['config'], $params['opts'] ?? [])) {
					$this->flush();
					$url = \Template_Engine::init()->find_active_page()['link'] ?? null;
					success("%s created successfully", array_get($params['config'], 'siteinfo.domain'));
					if ($url && !is_debug()) {
						header('Location: ' . \HTML_Kit::new_page_url_params($url, ['flashed' => true]), true, 302);
						exit(0);
					}
				}
			} else if (isset($params['edit']) || isset($params['reset'])) {
				$site = basename(strtok($_SERVER['REQUEST_URI'], '?'));
				return $this->editSite(
					$site,
					$params['config'],
					[
						'reset' => isset($params['reset']) ?: null
					]
				) && $this->flush();
			}
			if (isset($params['suspend'])) {
				return $this->admin_suspend_site($params['suspend']) && $this->forgetCache($params['suspend']);
			}

			if (isset($params['suspend-bulk'])) {
				if (empty($params['select'])) {
					return error("No sites selected to suspend");
				}

				$this->forgetCache($params['select']);
				return $this->admin_suspend_site($params['select']);
			}

			if (isset($params['activate-bulk'])) {
				if (empty($params['select'])) {
					return error("No sites selected to activate");
				}
				$this->forgetCache($params['select']);
				return $this->admin_activate_site($params['select']);
			}
		}

		/**
		 * Create a site
		 *
		 * @param array $config
		 * @return mixed
		 */
		protected function addSite(array $config, array $opts = [])
		{
			$domain = array_pull($config, 'siteinfo.domain');
			$admin_user = array_pull($config, 'siteinfo.admin_user');
			$filtered = $this->filterConfig($config);
			if (!empty($opts['crypt']) && !empty($config['auth']['tpasswd'])) {
				$config['auth']['cpasswd'] = Shadow::crypt(array_pull($config, 'auth.tpasswd'));
			}
			$flags = array_map(static function ($v) {
				$val = \Util_Conf::inferType($v);
				if ($val === 1 || $val === 0) {
					return $val ? true : false;
				}

				return $val;
			}, $opts);
			return $this->admin_add_site($domain, $admin_user, $filtered, $flags) && $this->lastCreated = $domain;
		}

		/**
		 * Remove default configuration
		 *
		 * @param array      $config
		 * @param array|null $baseConfiguration plan to compare against
		 * @return array
		 * @throws \ReflectionException
		 */
		private function filterConfig(array $config, array $baseConfiguration = null): array
		{
			$filtered = [];
			if (null === $baseConfiguration) {
				$baseConfiguration = $this->serviceCache;
			}
			foreach ($config as $svc => $svcconfig) {
				$filtered[$svc] = [];
				foreach ($svcconfig as $svcvar => $svcval) {
					if ($this->isNullable($svc, $svcvar) && $svcval === DefaultNullable::NULLABLE_MARKER) {
						// no-op
						$val = $svcval;
					} else if (false !== strpos($svcval, "\n") || \is_array(array_get($baseConfiguration,
							"${svc}.${svcvar}.value")))
					{
						/**
						 * @XXX hacky shit
						 */
						$svcval = \Util_Conf::inferType('[' . implode(
							',',
							preg_split("/[\r\n]{1,2}/", $svcval, -1, PREG_SPLIT_NO_EMPTY)) . ']'
						);

						if ($baseConfiguration[$svc][$svcvar]['value'] === $svcval) {
							continue;
						}

						$tmp = [];
						foreach ($svcval as $k => &$v) {
							// IPv6 parsing, this would fail if the first entry were "0:aaaa:ffff"
							// @TODO
							// it's better to add a special parsing condition or specialized parser for these
							// service values than grope from char presence
							if (!isset($svcval[0]) && false !== strpos($v, ':')) {
								$v = $k . ':' . $v;
								$tmp[] = $v;
							} else {
								$tmp[$k] = $v;
							}
						}
						unset($v);
						$svcval = $tmp;
						$val = CliParser::collapse($svcval);
					} else {
						$val = \Util_Conf::inferType($svcval);
						if ($val === $baseConfiguration[$svc][$svcvar]['value']) {
							continue;
						}
						if (\is_array($val)) {
							// collapse back down to string if array
							$val = CliParser::collapse($val);
						}
					}

					$filtered[$svc][$svcvar] = $val;
				}
			}

			return array_filter($filtered);
		}

		protected function flush(): bool
		{
			$this->accountCache = [];
			$plan = $this->getPlanName();
			$_POST = [];
			// store otherwise it'd be necessary to fetch/merge accountCache again
			// @todo rewrite
			array_set($_POST, 'config.siteinfo.plan', $plan);
			return true;
		}

		/**
		 * Edit a site
		 *
		 * @param string $site
		 * @param array  $config
		 * @return mixed
		 */
		protected function editSite(string $site, array $config, array $flags = [])
		{
			$this->oldServiceCache = \Auth::context(null, $site)->conf->conf;
			$filtered = $this->filterConfig($config);

			return $this->admin_edit_site($site, $filtered, $flags) && $this->forgetCache($site);
		}

		/**
		 * @param array|string $site
		 */
		private function forgetCache($site) {
			// @TODO move to model
			$cache = \Cache_Global::spawn();
			$sites = $cache->get(self::ACCOUNT_CACHE_KEY);
			array_forget($sites, $site);
			$cache->set(self::ACCOUNT_CACHE_KEY, $sites, 900);
			return true;
		}

		public function isIgnored(string $name)
		{
			return isset($this->skipList[$name]);
		}

		public function listDomains()
		{
			static $domains;
			if (isset($domains)) {
				return $domains;
			}

			return $domains = Enumerate::domains();
		}

		public function listAccounts()
		{
			if (empty($this->accountCache)) {
				$cache = \Cache_Global::spawn();
				if (false !== ($tmp = $cache->get(self::ACCOUNT_CACHE_KEY))) {
					$all = array_fill_keys(Enumerate::sites(), null);
					$missing = array_diff_key($all, $tmp);
					if ($missing) {
						$tmp += $this->buildAccountList(array_keys($missing));

					}
					$this->accountCache = $tmp;
				} else {
					$this->accountCache = $this->buildAccountList();
				}
			}
			return $this->accountCache;
		}

		public function getMetaFromSite(string $site): array
		{
			$accounts = $this->listAccounts();
			if (empty($accounts[$site])) {
				$this->accountCache = $this->buildAccountList([$site]);
			}
			return $this->accountCache[$site] ?? [];
		}

		/**
		 * Gather accounts on server
		 *
		 * @param bool|null $detailed include account meta
		 */
		private function buildAccountList(array $domains = []): array {
			$domains = $domains ?: Enumerate::sites();

			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL|\Error_Reporter::E_ERROR);
			/**
			 * @XXX setting a very low IO bandwidth for any site can cause enumeration
			 *      to slow to a crawl
			 */
			$amnestyThreshold = time() - QUOTA_STORAGE_DURATION;
			$build = array_build($domains, static function ($i, $site) use ($amnestyThreshold) {
				try {
					$auth = \Auth::context(null, $site);
				} catch (\apnscpException $e) {
					return [$site, []];
				}
				$account = $auth->getAccount();

				$meta = [
					'active' => $account->active,
					'site_id' => $auth->site_id,
					'siteinfo' => [
						'domain'     => $account->conf['siteinfo']['domain'],
						'admin_user' => $account->conf['siteinfo']['admin_user'],
						'email'      => $account->conf['siteinfo']['email']
					],
					'amnesty' => [
						'diskquota'  => $account->conf['diskquota']['amnesty'] >= $amnestyThreshold ?
							$account->conf['diskquota']['amnesty'] : null
					],
					'aliases' => [
						'aliases'    => implode(', ', (array)($account->conf['aliases']['aliases'] ?? []))
					],
					'billing' => [
						'invoice'    => $account->conf['billing']['invoice'] ?? ($account->conf['billing']['parent_invoice'] ?? null),
						'subordinate'=> (bool)$account->conf['billing']['parent_invoice']
					]
				];
				return [$site, $meta];
			});
			\Error_Reporter::exception_upgrade($oldex);

			$cache = \Cache_Global::spawn();
			if (false !== ($existing = $cache->get(self::ACCOUNT_CACHE_KEY))) {
				$build += $existing;
			}
			uasort($build, static function($a, $b) {
				if (!isset($a['siteinfo'])) {
					return -1;
				} else if (!isset($b['siteinfo'])) {
					return 1;
				}
				return strnatcmp($a['siteinfo']['domain'], $b['siteinfo']['domain']);
			});
			$cache->set(self::ACCOUNT_CACHE_KEY, $this->accountCache, 900);

			return $build;
		}

		public function getServices()
		{
			return $this->serviceCache;
		}

		public function isBool(array $optinfo): bool
		{
			return $optinfo['range'] === '[0,1]';
		}

		public function getInput(string $name, $default = null, string $prefix = 'config.')
		{
			if (array_has($_POST, $prefix . $name)) {
				$val = array_get($_POST, $prefix . $name);
				if ($prefix === 'config.') {
					[$service, $var] = explode('.', $name, 2);
					if (!$this->isNulled($service, $var, $val)) {
						if (false !== strpos($val, "\n")) {
							return preg_split("/[\r\n]{1,2}/", $val);
						}

						if (\is_array(array_get($this->serviceCache, $name . '.value', $default))) {
							// item is still an array
							return (array)$val;
						}
					}
				}

				return $val;
			}

			if (array_has($_GET, $name)) {
				$val = array_get($_GET, $name, $default);
			} else {
				$val = array_get($this->serviceCache, $name . '.value', $default);
			}

			return $val;
		}

		/**
		 * Value should be substituted by DefaultNullable
		 *
		 * @param string $service
		 * @param string $var
		 * @param mixed  $val
		 * @return bool
		 * @throws \ReflectionException
		 */
		protected function isNulled(string $service, string $var, $val): bool
		{
			if (!$this->isNullable($service, $var)) {
				return false;
			}
			return $val === DefaultNullable::NULLABLE_MARKER;

		}

		/**
		 * Configuration can be defaulted
		 *
		 * @param string $name
		 * @param string $var
		 * @return bool
		 * @throws \ReflectionException
		 */
		public function isNullable(string $name, string $var): bool
		{
			return (new \ReflectionClass($this->getValidationClass($name, $var)))->implementsInterface(DefaultNullable::class);
		}

		/**
		 * @param string $name
		 * @param string $var
		 * @return string
		 */
		public function getValidationClass(string $name, string $var): ?string
		{
			return (new ConfigurationContext($name, new SiteConfiguration('')))->getValidatorClass($var);
		}

		/**
		 * Service is instance of
		 * @param string $name
		 * @param string $var
		 * @param string $hasClass
		 * @return bool
		 */
		public function serviceIs(string $name, string $var, string $hasClass): bool
		{
			if (null === ($class = $this->getValidationClass($name, $var))) {
				return false;
			}
			return (new \ReflectionClass($class))->isSubclassOf($hasClass);
		}


		public function getPlans(): array {
			return Plans::list();
		}

		public function planActive(string $name): bool {
			return $name === $this->getPlanName();
		}

		public function getPlanName(): string {
			if (isset($_GET['plan'])) {
				return $_GET['plan'];
			}

			return $_POST['config']['siteinfo']['plan'] ?? ($this->serviceCache['siteinfo']['plan']['value'] ?? Plans::default());
		}


		public function hasChange(string $service, string $parameter): bool
		{
			return isset($this->fieldChanges[$service][$parameter]);
		}
	}
