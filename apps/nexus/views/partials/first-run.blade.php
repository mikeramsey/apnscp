<h3 class="display-4 text-center">Welcome to {{ PANEL_BRAND }}!</h3>
<p class="lead mb-3 text-center">
	<span class="my-2 d-block">
		This is your first time here, so let's make this quick. Create your first site by clicking on
		<b>
			<i class="fa fa-plus"></i> Create Site
		</b>
		above. That's it. Enjoy!
	</span>
	<a class="btn btn-primary mt-2 mr-3" href="https://docs.apiscp.com" target="_blank">
		<i class="ui-action ui-action-label ui-action-kb"></i>
			Documentation
	</a>
	<a class="btn btn-primary mt-2" href="https://hq.apiscp.com" target="_blank">
		<i class="ui-action ui-action-label fa-ghost"></i>
		Blog
	</a>
</p>
<hr />
<h4>Configuring DNS</h4>
<p>
	DNS comes in a variety of flavors and you'll need to pick one to get the most out of {{ PANEL_BRAND }}. We use the
	builtin module to encourage you to select a better alternative. Check out <a href="https://docs.apiscp.com/admin/DNS/#providers">Configuring DNS</a>
	for a detailed guide on all the options available or for the intrepid type, build your own.
</p>
<h4>Customizing services</h4>
<p>
	{{ PANEL_BRAND }} is built with extensibility in mind. Plans can be created from command-line using <code>artisan opcenter:plan --new myplan</code>.
	A walkthrough is provided in Site and Plan management &gt; &quot;<a href="https://docs.apiscp.com/admin/Plans/#creating-plans">Creating plans</a>&quot;. You can
	even change my name to something else with <code>cpcmd scope:set apnscp.config core panel_brand NEWNAME</code>.
</p>
<h4>Backups</h4>
<p>
	{{ PANEL_BRAND }} includes Bacula as a drop-in solution for backup management. Quite simply run <code>yum install -y bacula-console apnscp-bacula</code> to get started.
	Learn more about <a href="https://docs.apiscp.com/admin/Backups/">Backups with Bacula</a>.
</p>
<h4>Monitoring</h4>
<p>
	What good is a downed server? {{ PANEL_BRAND }} includes a passive monitoring framework nicknamed &quot;Argos&quot;. Argos provides a variety of relay channels including Pushover,
	Slack, and Xmpp. It takes a few keystrokes to initialize. Explore monitoring with <a href="https://docs.apiscp.com/admin/Monitoring/">Argos</a>.
</p>
<h4>Easy administration</h4>
<p>
	<a href="https://docs.apiscp.com/admin/Scopes">Scopes</a> are a powerful, structured interface to complex system administration tasks.
	Get started with <a href="/apps/scopes">Scopes</a> app or <code>cpcmd scope:list</code>. You can also use <code>cpcmd scope:info scope.name</code>
	to get details on each one. <code>cpcmd scope:set</code> sets a scope value and <code>cpcmd scope:get</code> fetches the current value.
</p>

<h4>Integrity checks</h4>
@if (\cmd('common_get_email'))
	<p>
		Be on the lookout for a platform integrity check in your inbox in the coming days. Integrity checks run once a month and scour your server
		for changes. {{ PANEL_BRAND }} does its best to correct changes for added peace of mind. We'll send a report to <b>{{ \cmd('common_get_email') }}</b>
		with the results. Email address may be updated in <a href="/apps/changeinfo">Settings</a>.
	</p>
@else
	<div class="ui-postback-msg ui-postback-msg-failed">
		Oops, you haven't set an email yet for the admin! {{ PANEL_BRAND }} will send a report once a month with a platform inspection summary detailing
		all changes repaired on this server. Set your email address in <a href="/apps/changeinfo">Settings</a> to ensure you get this report!
	</div>
@endif