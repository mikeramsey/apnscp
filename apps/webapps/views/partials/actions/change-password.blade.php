<button id="changePW" data-toggle="collapse" aria-expanded="false"
        aria-controls="pwForm"
        data-note-target="#resetNote" data-target="#pwForm" type="button"
        class="btn btn-secondary warn  mb-3" name="detect"
        value="{{ $app->getDocumentMetaPath() }}">
	<i class="fa fa-user-secret"></i>
	Change Admin Password
</button>

<div class="collapse form-inline" id="pwForm">
	<div class="row d-flex">
		<div class="form-group has-feedback col-12 col-md-6 mb-3">
			<div class="row">
				<label class="col-md-12">Password</label>
				<div class="col-md-12 input-group">
					<div class="input-group-addon"><i class="fa fa-lock"></i></div>
					<input id="password" type="password" data-minlength="6"
					       class="form-control"
					       name="password" value="" id="password" required/>
				</div>
				<div class="help-block col-12">
					Six character minimum
				</div>
			</div>
		</div>

		<div class="form-group align-self-start col-12 col-md-6 password-confirm-container mb-3">
			<div class="row">
				<label class="col-md-12">Verify Password</label>
				<div class="col-md-12 input-group">
					<div class="input-group-addon"><i class="fa fa-lock"></i></div>
					<input type="password" data-match-error="Password does not match"
					       class="form-control form-control-error"
					       data-match="#password" required name="password_confirm" value=""
					       id="password_confirm" required/>
				</div>
				<div class="col-12 help-block with-errors text-danger"></div>
			</div>
		</div>

		<div class="btn-group align-self-start col-md-6 col-12 mb-3">
			<button type="submit" id="create-user" name="resetpw" class="btn main"
			        value="Reset Password">
				Change Password
			</button>
		</div>
	</div>
</div>