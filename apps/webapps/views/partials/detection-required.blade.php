<h5 class="text-danger">Uh oh...</h5>
<p class="lead">
	We seem to have hit a snag. You appear to have a Web App installed within here, but
	{{ PANEL_BRAND }} doesn't see it yet. Try <b>Detect</b> below so we can update our records.
	If this doesn't work, the contents in this directory must be removed first to install
	a new Web App. You can remove this directory within <b>Files</b> &gt; <b>File Manager</b>.
</p>
<div class="d-flex align-items-start">
	@include('partials.actions.detect')
	<div class="mr-3"></div>
	@include('partials.actions.uninstall')

	<a class="ml-3 mb-3 btn btn-secondary ui-action ui-action-label ui-action-switch-app" href="/apps/filemanager?cwd={{ $app->getAppRoot() }}">File Manager</a>
</div>