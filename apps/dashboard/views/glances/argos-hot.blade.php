<div id="argos">
	<div class="row">
		@php
			$services = (array)\cmd('argos_status');
			ksort($services);
		@endphp
		@foreach ($services as $service => $status)
			<div class="col-6 col-md-4 col-xl-3 mb-3 @if ($status['failed']) alert-danger text-danger @endif"
			     style="font-size:0.85em; background:#fcfcfc;">
				<div class="pa-2 rounded" style="">
					<div class="row">
						<div class="col-12 d-flex align-content-around bg-light py-1">
							<h6 class="mb-0 text-truncated w-100 d-inline-block position-relative" data-toggle="tooltip"
							    title="{{ $status['status'] }} &middot; {{ $status['type'] }} &middot; {{ $service }} @isset($status['pid']) &middot; PID {{ $status['pid'] }} @endif">
								<i class="fa fa-circle status-indicator mr-1 text @if ($status['failed']) text-danger @else text-success @endif"></i>
								<i class="fa @if ($status['type'] === 'filesystem') fa-hdd-o @elseif ($status['type'] === 'program') fa-terminal-alt @elseif ($status['type'] === 'system') fa-server @else fa-cogs @endif"></i> {{ $service }}
								@if (!$status['monitored'])
									<a href="#" data-monitor="{{ $service }}" style="right:0"
									   class="position-absolute btn p-0 ma-0 ml-auto border-0 ui-action ui-action-label ui-action-restore ui-action-refresh pr-0"></a>
								@elseif ($status['type'] === 'process')
									<a href="#" data-restart="{{ $service }}" style="right:0"
									   class="position-absolute btn p-0 ma-0 ml-auto border-0 ui-action ui-action-label ui-action-restart ui-action-refresh pr-0"></a>
								@endif
							</h6>
						</div>
					</div>
					@if (!empty($status['last_output']))
						<div class="row">
							<div class="col-6 py-1 px-3">
								Last output
							</div>
							<div class="col-6 py-1 px-3 text-right">
								{{ $status['last_output'] }}
							</div>
						</div>
					@endif

					@if (!empty($status['uptime']))
						<div class="row">
							<div class="col-6 py-1 px-3">
								Uptime
							</div>
							<div class="col-6 py-1 px-3 text-right">
								{{ $status['uptime'] }}
							</div>
						</div>
					@endif

					@if (!empty($status['inodes_total']))
						<div class="row">
							<div class="col-6 py-1 px-3">
								inode usage
							</div>
							<div class="col-6 px-3 text-right">
								@include('master::partials.shared.usage-fill', [
								'used' => $status['inodes_total']-$status['inodes_free'],
								'unit' => '',
								'percentage' => 1-($status['inodes_free']/$status['inodes_total'])
								])
							</div>
						</div>
					@endif

					@isset ($status['memory_total_raw'])
						<div class="row">
							<div class="col-6 py-1 px-3">
								Memory
							</div>
							<div class="col-6 px-3">
								@include('master::partials.shared.usage-fill', [
									'used' => $status['memory_total_raw'],
									'unit' => 'MB',
									'srcunit' => 'B',
									'percentage' => $status['memory_total_raw']/(\Opcenter\System\Memory::stats()['memtotal']*1024)
								])
							</div>
						</div>
					@endisset

					@isset($status['cpu_total'])
						<div class="row">
							<div class="col-6 py-1 px-3">
								CPU
							</div>
							<div class="col-6 px-3">
								@include('master::partials.shared.usage-fill', [
									'used' => $status['cpu_total'] * 100,
									'percentage' => $status['cpu_total'],
									'unit'       => '%'
								])
							</div>
						</div>
					@endisset

					@isset($status['disk_write_raw'])
						<div class="row">
							<div class="col-6 py-1 px-3">
								Disk r/w (&Sigma; r/w)
							</div>
							<div class="col-6 py-1 px-3 text-right">
								{{ (int)Formatter::changeBytes($status['disk_read_bw_raw'] ?? 0, 'KB', 'B') }} &middot;
								{{ (int)Formatter::changeBytes($status['disk_write_bw_raw'] ?? 0, 'KB', 'B') }} KB/s
								<br/>
								({{ (int)Formatter::changeBytes($status['disk_read_raw'] ?? 0, 'MB', 'B') }} &middot;
								{{ (int)Formatter::changeBytes($status['disk_write_raw'] ?? 0, 'MB', 'B') }} MB)
							</div>
						</div>
					@endisset

					@if (!empty($status['timing']))
						<div class="row">
							<div class="col-6  py-1 px-3">
								Latency
							</div>
							<div class="col-6  py-1 px-3 text-right">
								{{ $status['timing'] }} ms
							</div>
						</div>
					@endif

					@if($status['type'] === 'system')
						<div class="row">
							<div class="col-6 py-1 px-3">
								CPU
							</div>
							<div class="col-6 py-1 px-3 text-right">
								{!! implode('<br />', explode(' ', $status['cpu'])) !!}
							</div>
						</div>
					@endif
				</div>
			</div>
		@endforeach
	</div>
</div>