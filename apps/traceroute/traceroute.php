<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\traceroute;

	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->init_js('ajax_unbuffered');
			$this->add_javascript('traceroute.js');
			$url = \HTML_Kit::new_page_url_params('/apps/traceroute/traceroute-ajax.php',
				array('fn' => 'perform_traceroute'));
			$this->add_javascript('var clientip = "' . \Auth::client_ip() . '";', 'internal');
		}

		public function on_postback($params)
		{
			if (isset($_POST['destination']) && !preg_match(\Regex::DOMAIN, $_POST['destination']) &&
				ip2long($_POST['destination']) < 0) {
				error("Invalid destination host/IP");
			}
		}

		public function getIP()
		{
			if (!isset($_POST['target'])) {
				return \Auth::client_ip();
			}
			if (preg_match(\Regex::DOMAIN, $_POST['target']) ||
				ip2long($_POST['target']) >= 0) {
				return $_POST['target'];
			}

			return '';
		}

		public function traceroute($ip)
		{
			$d = array(
				0 => array('pipe', 'r'),
				1 => array('pipe', 'w'),
				2 => array('pipe', 'w')
			);
			$pipes = array();
			$outfp = fopen('/tmp/' . session_id(), 'w');
			ftruncate($outfp, 0);
			$flags = LOCK_EX | LOCK_NB;
			$wb = false;
			flock($outfp, $flags, $wb);
			$proc = proc_open("/bin/traceroute " . $ip, $d, $pipes);
			fclose($pipes[0]);
			fclose($pipes[2]);
			$stdout = $pipes[1];
			$w = $e = array();
			$r = array($stdout);
			ignore_user_abort(true);
			while (stream_select($r, $w, $e, null) > 0) {
				stream_set_blocking($stdout, false);
				print "\0";
				ob_flush();
				flush();
				if (connection_aborted()) {
					$pstatus = proc_get_status($proc);
					posix_kill($pstatus['pid'], 9);
					break;
				}
				$bytes = stream_copy_to_stream($stdout, $outfp);
				stream_set_blocking($stdout, true);
				if (!$bytes) {
					break;
				}

			}
			fclose($outfp);
			fclose($stdout);
			proc_close($proc);
		}

		public function reserve_tee()
		{
			return (new \apps\crontab\Page)->reserve_tee();
		}

		public function run($cmd, $tee)
		{
			return (new \apps\crontab\Page)->run($cmd, $tee);
		}
	}
