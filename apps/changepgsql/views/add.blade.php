<form method="post" action="changepgsql" id="addForm">
	<h3>Create Database</h3>
	<div class="mb-1">
		<div class="form-group row">
			<div class="col-12 col-md-6 mt-1">
				<label for="database_new" class="">Database Name</label>
				<div class="input-group">
                    <span class="input-group-addon flex-row">
                        <i class="fa fa-tag mr-1 align-self-center"></i>
	                    {{ $Page->getDBPrefix() }}
                    </span>
					<input class="form-control" type="text" name="database_name" value="" id="database_new"/>
				</div>
			</div>
			<div class="col-12 col-md-6 mt-1 d-lg-flex align-items-end align-self-end">
				<div class="btn-group">
					<button type="submit" id="create_db" class="btn main add" value="Create" name="CreateDB">
						<i class="fa fa-postgres-w"></i>
						Create Postgres Database
					</button>
					<a class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
					   data-toggle="collapse" aria-controls="backupOptions" data-target="#backupOptions"
					   aria-expanded="false">
						<span class="sr-only">Toggle Advanced</span>
					</a>
				</div>
			</div>
		</div>
		<p class="col-12 note">
			All databases are prefixed with {{ $Page->getDBPrefix() }}
		</p>
	</div>

	<div id="backupOptions" class="collapse">
		<div class="row">
			<fieldset class="form-group col-12">
				<label for="backup_db">
					<input type="checkbox" class="checkbox checkbox-inline" name="backup_db" value="1" checked="1"
						id="backup_db"/>
							Backup database (see <a class="ui-action ui-action-switch-app ui-action-label"
		                        href="{{ \HTML_Kit::page_url() }}">PostgreSQL Backups</a>)
				</label>
			</fieldset>

			<fieldset class="form-group col-12 col-md-4 col-lg-2">
				<label>Number of backups to hold</label>
				<input type="text" name="b_hold" size="2" value="{{ DATABASE_BACKUP_PRESERVE }}" class="form-control"/>
			</fieldset>

			<fieldset class="form-group col-12 col-md-4 col-lg-2">
				<label>Compression Method</label>

				<select class="form-control custom-select" name="b_extension">
					@foreach (Sql_Module::BACKUP_EXTENSIONS as $ext)
						<option value="{{ $ext }}" @if ($ext === DATABASE_BACKUP_EXTENSION) selected="selected" @endif>
							{{ $ext }}
						</option>
					@endforeach
				</select>
			</fieldset>

			<fieldset class="form-group  col-12 col-md-4 col-lg-2">
				<label>Backup Frequency</label>
				<div class="input-group">
					<input type="text" name="b_frequency" size="2" value="{{ DATABASE_BACKUP_SPAN }}"
					       class="form-control"/>
					<span class="input-group-addon">days</span>
				</div>
			</fieldset>

			<fieldset class="col-12 form-group form-inline mt-1 show" id="uploadContainer">
				<div class="row">
					<div class="col-12" id="progressBox"></div>
				</div>
				<div class="row">
					<h6 class="col-12">Use SQL Export</h6>
					<div class="pa-1 col-12" style="position: relative;">
						<div id="progressbox" class="">
							<div id="progress" class="progress"></div>
						</div>
						<label class="custom-file">
							<input type="file" id="upload-file" class="custom-file-input">
							<span class="custom-file-control">
                                <i class="fa fa-upload"></i>
                            </span>
						</label>&nbsp;
					</div>


					<div class="col-12">
						<div id="container-upload-list"></div>
					</div>

					<div class="col-12">
						<p class="note">
							Max upload size is {{ ini_get('upload_max_filesize') }} &mdash;
							use <a class="ui-action ui-action-label ui-action-switch-app"
							       href="/apps/webdisk">WebDisk</a> or
							<a class="ui-action ui-action-label ui-action-kb"
							   href="{{ MISC_KB_BASE }}/ftp/accessing-ftp-server/">FTP</a> for larger files.
						</p>
						<p class="note">
							Compressed database backups are supported.
						</p>
					</div>
					<div id="progressbar"></div>

				</div>
			</fieldset>
		</div>
	</div>


</form>

<form method="post" action="changepgsql" data-toggle="validator">
	<h3>Create User</h3>
	@php
		$userLen = \Pgsql_Module::IDENTIFIER_MAXLEN - \strlen($Page->getDBPrefix());
	@endphp

	<div class="mb-1 clearfix row">
		<fieldset class="d-flex form-group col-12 col-md-6 col-xl-3">
			<label class="">User</label>
			<div class="input-group">
                <span class="input-group-addon flex-row">
                    <i class="fa fa-user mr-1 align-self-center"></i>
	                {{ $Page->getDBPrefix() }}
                </span>
				<input type="text" name="new_user_name" tabindex="1"
				       class="form-control" @if ($userLen < 1) DISABLED @endif maxlength="{{ $userLen }}"/>
			</div>
		</fieldset>

		<fieldset class="d-flex form-group has-feedback col-12 col-md-6 col-xl-3">
			<div class="row">
				<label class="col-md-12">Password</label>
				<div class="col-md-12 input-group">
					<div class="input-group-addon"><i class="fa fa-lock"></i></div>
					<input id="password" tabindex="2" type="password" data-minlength="6" class="form-control"
					       name="password" value="" id="password" required/>
				</div>
				<div class="help-block col-12">
					Six character minimum
				</div>
			</div>
		</fieldset>

		<fieldset class="d-flex form-group col-12 col-md-6 col-xl-3 password-confirm-container">
			<div class="row">
				<label class="col-md-12">Verify Password</label>
				<div class="col-md-12 input-group">
					<div class="input-group-addon"><i class="fa fa-lock"></i></div>
					<input type="password" tabindex="3" data-match-error="Password does not match"
					       class="form-control form-control-error"
					       data-match="#password" required name="password_confirm" value="" id="password_confirm"
					       required/>
				</div>
				<div class="col-12 help-block with-errors text-danger"></div>
			</div>
		</fieldset>

		<fieldset class="d-flex col-12 col-md-6 col-xl-3 form-group align-self-top">
			<label class="w-100 hidden-sm-down">&nbsp;</label>
			<div class="btn-group">
				<button type="submit" id="create-user" tabindex="4" name="Create_User" class="btn main add"
				        value="Add User">
					Add User
				</button>
				<a class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
				   data-toggle="collapse" tabindex="5" aria-controls="userAdvanced" data-target="#userAdvanced"
				   aria-expanded="false">
					<span class="sr-only">Toggle Advanced</span>
				</a>
			</div>
		</fieldset>

	</div>

	<div id="userAdvanced" class="mt-1 collapse row">
		<fieldset class="d-flex form-group  col-12 col-md-6">
			<label>Max Connections</label>
			<input type="text" maxlength=2 size=2 name="new_max_connections" value="5" class="form-control"/>
		</fieldset>
	</div>
</form>