<tr>
	<td class="">
		{{ $db_name }}
	</td>
	<td class="hidden-sm-down">
		<span editable="{{ $loop->index }}">
			{{ $Page->backup_program($backup_params['extension']) }}
		</span>
	</td>
	<td class="hidden-sm-down">
		<span editable="{{ $loop->index }}">
			{{ $backup_params['span'] }}
		</span>
	</td>
	<td class="">
		<span editable="{{ $loop->index }}">
			{{ $backup_params['hold'] }}
		</span>
	</td>
	<td class="">
		<span editable="{{ $loop->index }}">
			{{ \UCard::init()->formatDate((new DateTime)->setTimestamp($backup_params['next']), IntlDateFormatter::MEDIUM)  }}
		</span>
	</td>
	<td class="hidden-sm-down">
		<span editable="{{ $loop->index }}">
			@if ($backup_params['email']) Yes @endif
		</span>
	</td>
	<td class="center">
		<!--<a href="#" OnClick="return make_editable(this,<?= $i ?>);"><img src="/images/actions/edit_icon.gif" alt="Edit" border="0" /></a>&nbsp;&nbsp;-->
		<a href="#" class="btn btn-primary ui-action-label btn warn ui-action ui-action-delete"
		   OnClick="return confirm('Are you sure you would like to delete the job on {{ $db_name }}?') && apnscp.assign_url('{!! \HTML_Kit::page_url() !!}?d={{ base64_encode($db_name) }}');">
			Delete
		</a>
	</td>
</tr>