<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\error;

	use Error_Reporter;
	use Page_Container;

	class Page extends Page_Container
	{
		protected $exception;
		private $errorCode = 404;
		private $errorMap = [
			'200' => [
				'name' => '???',
				'status' => '200 OK'
			],
			'403' => [
				'name' => 'Forbidden',
				'status' => '403 Forbidden'
			],
			'404' => [
				'name'   => 'Page Not Found',
				'status' => '404 Not Found'
			],
			// unsupported Laravel route!
			'405' => [
				'name' => 'Unsupported method',
				'status' => '405 Unsupported method'
			],
			'429' => [
				'name' => 'Too Many Requests',
				'status' => '429 Try Again Later'
			],
			'550' => [
				'name'   => 'Internal Server Error',
				'status' => '550 Internal Error'
			]
		];

		public function __construct(int $errorCode = null)
		{
			if (null === $errorCode) {
				$errorCode = http_response_code();
			}

			$this->errorCode = $errorCode;

			$this->add_css('#app-error #ui-app { background: transparent; border: 0 none; }', 'internal');
			\Page_Renderer::hide_all();
			$_SERVER['REQUEST_URI'] = '/apps/error';
			if (isset($_SERVER['HTTP_REFERER']) &&
				strstr($_SERVER['HTTP_REFERER'], "/apps/filemanager")) {
				return;
			} else if (isset($_POST['stack'])) {
				Error_Reporter::report("JS error");
				exit;
			}

			$response = $this->errorMap[$errorCode]['status-code'] ?? $errorCode;
			$this->setTitle($this->errorMap[$errorCode]['name']);
			$this->addBodyClass('e' . $errorCode . ' error');
			if (!headers_sent() || !is_debug()) {
				// not guaranteed in debug mode
				header('HTTP/1.1 ' . $this->errorMap[$errorCode]['status'], true, $response);
			}
		}

		public function _init()
		{
			parent::_init();
			if (!is_debug()) {
				return;
			}
			if ($this->getErrorCode() < 500) {
				echo $this->getErrorTitle();
				return;
			}

			$formatter = static function ($buffer) {
				if (\HTML_Kit::jsonify()) {
					return json_encode($buffer);
				}
				echo '<code><pre>', var_dump($buffer), '</pre></code>';
			};

			if ($ex = $this->getException()) {
				if (!\HTML_Kit::jsonify()) {
					echo $ex->getMessage(), "\n";
				}
				$stack = isset($_GET['FULL_STACK']) ? $ex->getTrace() : $ex->getTraceAsString();
				$formatter($stack);
			}

			$formatter(\Error_Reporter::get_buffer());
		}

		public function getException(): ?\Throwable
		{
			return $this->exception;
		}

		public function setException(\Throwable $ex): void
		{
			$this->exception = $ex;
		}


		public function on_postback($params)
		{
			if (!isset($params['mode'])) {
				return;
			}
			switch ($params['mode']) {
				case 'report':
					//Error_Reporter::report(var_export($params, true));
					header('HTTP/1.1 200 OK', true, 200);
					echo 1;
					exit();
					break;
			}
		}

		public function getErrorTitle()
		{
			return array_get($this->errorMap, $this->errorCode . '.name', 'Gateway Error');
		}

		public function getErrorCode(): int {
			return $this->errorCode;
		}

		public function getErrorDescription()
		{
			switch ($this->errorCode) {
				case 429:
					return 'Try again later';
				case 404:
					return 'No sentient life detected here';
				default:
					return 'No sentient programmers detected' . "<br />" . '¯\_(ツ)_/¯';
			}
		}

		public function errorCSS()
		{
			return 'e' . $this->errorCode;
		}

		public function errorName()
		{
			return $this->errorMap[$this->errorCode]['name'];
		}

		public function render()
		{
			if (!is_debug()) {
				ob_clean();
				parent::render();
			}
		}
	}