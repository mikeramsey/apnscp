<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\login;

	use Auth\Sectoken;
	use Crm_Module;
	use Mail;
	use Mail_Mime;
	use Net_Gethost;
	use Opcenter\Auth\Password;

	class Page extends \Page_Container
	{
		const PASSWORD_MAX_TIME = 7200; // 2 hours
		const SYS_STATUS_URL = MISC_SYS_STATUS;
		// target server to apply login
		private $server;

		public function __construct()
		{
			if (!isset($_SESSION['username'])) {
				// otherwise Page_Container::_init() will throw errors
				$this->_initSession();
			}
			parent::__construct();
		}

		private function _initSession()
		{
			// allow tracking of session state via Anvil
			$_SESSION['valid'] = false;
			$_SESSION['username'] = $_SESSION['domain'] =
			$_SESSION['password'] = $_SESSION['level'] =
			$_SESSION['user_id'] = $_SESSION['group_id'] =
			$_SESSION['site_id'] = null;
		}

		public function validateCsrf(): bool
		{
			return true;
		}

		public function _render()
		{
			\Page_Renderer::hide_all();
			if (isset($_GET['headless'])) {
				print json_encode($this->getMessages());
				die();
			}
			$this->setTitle("Login");
			$this->add_javascript('login.js');
		}

		public function on_postback($params)
		{
			$mode = $this->getMode();
			if (isset($params['logout'])) {

				$_SESSION = [];
				session_regenerate_id(true);
				$this->_initSession();
				if (isset($_GET['server']) && ctype_alnum($_GET['server'])
					&& $_GET['server'] !== SERVER_NAME_SHORT) {
					$hostname = \Auth_Redirect::makeCPFromServer($_GET['server']);
					\Auth_Redirect::send($hostname);
				}

				return info("You have been logged out");
			} else if ($mode === 'login' && isset($params['password'])) {
				$username = $this->getUsername();
				$password = $this->getPassword();
				$domain = $this->getDomain();

				$autologout = (bool)$this->getParam('autologout');

				if ($domain && !preg_match(\Regex::DOMAIN, $domain)) {
					return error("invalid domain");
				}
				if (!preg_match(\Regex::USERNAME, $username)) {
					return error("invalid username");
				}
				if (!$password) {
					return error("missing password");
				}

				if (!isset($params['remember'])) {
					foreach (array('username', 'domain', 'password') as $cookie) {
						$this->_setCookie($cookie, null);
					}
					$this->_setCookie('remember', false);
				} else {
					$this->_setCookie('username', $username);
					$this->_setCookie('domain', $domain);
					$this->_setCookie('remember', true);
					$this->_setCookie('autologout', $autologout);
				}

				$server = $this->getTargetServer();

				if ($server !== SERVER_NAME_SHORT) {
					$hostname = \Auth_Redirect::makeCPFromServer($server);
					return \Auth_Redirect::send($hostname);
				}
				if (!\Auth::get_driver()->login($username, $domain, $password, $autologout)) {
					$this->_initSession();

					return false;
				}
				\apnscpFunctionInterceptor::expire($this->getAuthContext()->id);
				$this->setContext(\Auth::profile());
				$afi = \apnscpFunctionInterceptor::init(true);
				$this->setApnscpFunctionInterceptor($afi);
				$this->_setCookie('proxy', $server, 0);
				$this->checkNotify($username, $domain);


				/**
				 * @XXX bug in Chrome, cannot reproduce elsewhere
				 * initial login in a local subnet (e.g. 192.168.0.175)
				 * indefinitely hangs, need second refresh. Irreproducible
				 * on FQDN, other browsers, or incognito - weird.
				 */
				$location = \Template_Engine::instantiateContexted($this->getAuthContext())->getEntryLocation();
				header("Location: $location", true, 303);
				echo '<html><head>
				  <meta http-equiv="refresh" content="1;' . $location . '">
				</head></html>';
				ob_end_flush();
				die();
			} else if ($mode == 'forgot_info') {
				return $this->_forgot_info($params);
			}

			array_forget($params, ['username', 'domain', 'password', 'autologout', 'remember']);
			// still left behind, garbage?
			if ($params) {
				// Unrecognized parameters. Likely cPanel brute-force attempt, e.g.
				// https://domain.com:2083/apps/login?login_only=1
				\Auth_Anvil::anvil();
			}
		}

		public function getMode()
		{
			if (isset($_GET['forgot_info'])) {
				return 'forgot_info';
			} else {
				return strstr(\HTML_Kit::page_url(), 'login') ? 'login' : 'forgot_info';
			}
		}

		public function getUsername()
		{
			$user = trim($this->getParam('username'));
			$user = filter_var($user, FILTER_SANITIZE_STRING);

			return $this->parseLogin($user);
		}

		public function getParam($param)
		{
			// Ensim throwback
			$prefix = 'ocw_login_';
			$val = null;
			if (isset($_GET[$prefix . $param])) {
				return error("%s prefix no longer supported", $prefix);
			}
			if (isset($_POST[$param])) {
				return $_POST[$param];
			} else if (isset($_GET[$param])) {
				$val = $_GET[$param];
			} else if (isset($_COOKIE[$param])) {
				$val = $_COOKIE[$param];
			}

			return trim($val);
		}

		/**
		 * Get target login server
		 *
		 * @return string
		 */
		private function getTargetServer()
		{
			return $this->server ?: SERVER_NAME_SHORT;
		}

		/**
		 * Parse
		 * @param $login
		 * @return bool|string|void
		 */
		private function parseLogin($login)
		{
			$login = strtolower($login);
			if (false !== $pos = strpos($login, '/')) {
				if (!\Auth_Redirect::CP_LAYOUT) {
					return error('[auth] => server_format must be set for /server redirection');
				}
				$server = substr($login, $pos+1);
				$login = substr($login, 0, $pos);
				if (!ctype_alnum($server)) {
					return error('Short-hand server redirects must be local hostnames');
				}
				$this->server = $server;
			}
			$sep = strpos($login, '@');
			if ($sep === false) {
				$sep = strpos($login, '#');
			}
			if ($sep === false) {
				return $login;
			}
			$username = substr($login, 0, $sep);
			$domain = substr($login, ++$sep);
			// situation when user is really braindead and supplies username
			// as user@domain and completely ignores domain
			if (!trim($_POST['domain'] ?? '')) {
				$_POST['domain'] = $domain;
			}

			return $username;
		}

		public function getPassword()
		{
			return $this->getParam('password');
		}

		public function getDomain()
		{
			$domain = strtolower($this->getParam('domain'));
			$domain = $this->_stripDomain($domain);
			$domain = filter_var($domain, FILTER_SANITIZE_STRING);
			if ($domain === SERVER_NAME) {
				// no domain may duplicate the server name
				return '';
			}

			return $domain;
		}

		private function _stripDomain($domain)
		{
			// what if...
			if (!strncmp($domain, 'http', 4) && false !== strpos($domain, ':')) {
				// they specified http:// for the domain...

				$domain = parse_url($domain, PHP_URL_HOST);
			}
			if (substr($domain, 0, 4) === 'www.') {
				$domain = substr($domain, 4);
			}

			return trim($domain);
		}

		private function _setCookie($param, $val, $ttl = 7776000)
		{
			$time = $_SERVER['REQUEST_TIME'];
			if (is_null($val)) {
				$time -= 90 * 86400;
				unset($_COOKIE[$param]);
			} else if ($ttl > 0) {
				$time += $ttl;
			} else {
				$time = 0;
			}
			setcookie(
				$param,
				$val,
				$time,
				'/',
				null,
				true
			);
		}

		protected function checkNotify($username, $domain)
		{
			$afi = $this->getApnscpFunctionInterceptor();
			if ($this->_browserTrusted($username, $domain)) {
				return true;
			} else if ($afi->auth_is_demo()) {
				return true;
			}
			$notify = array_get(\Preferences::factory($this->getAuthContext()), "notify.login", true);
			if (!$notify) {
				return true;
			}

			// @todo security questions
			$email = array_filter([(new \apps\changeinfo\Page)->getEmail()]);

			if (!$email) {
				return true;
			}
			foreach ($email as $e) {
				$this->_sendEmail($e, "Unrecognized Login Source", "unrecognized", [
					'config'   => ['geoip' => true],
					'browser'  => $_SERVER['HTTP_USER_AGENT'],
					'domain'   => $domain,
					'username' => $username,
					'url'      => \Util_HTTP::makeUrl()
				]);
			}
		}

		private function _browserTrusted($user = null, $domain = null)
		{
			$afi = $this->getApnscpFunctionInterceptor();

			if ($afi->common_get_service_value(\Module\Support\Auth::getAuthService(), \Auth_Module::PWOVERRIDE_KEY)) {
				// temp password by admin
				return true;
			}

			$userhash = md5($user . $domain);
			$browserkeys = array();
			if (null !== ($cookie = array_get($_COOKIE, \Auth_Module::SECURITY_TOKEN, null))) {
				$browserkeys = (array)\Util_PHP::unserialize(base64_decode($cookie));
			}
			$challengekey = array_get($browserkeys, $userhash, '');
			$checker = Sectoken::instantiateContexted(\Auth::profile());
			$success = $checker->check($challengekey);
			$browserkeys[$userhash] = (string)$checker;
			$cookie = base64_encode(serialize($browserkeys));
			$this->_setCookie(\Auth_Module::SECURITY_TOKEN, $cookie);

			return $success;
		}

		private function _sendEmail($email, $subject, $template, array $extrafields = array())
		{
			$ip = \Auth::client_ip();
			$location = (new \Util_Geoip)->locate($ip);
			// cannot use afi as user may not be authenticated...
			$hostname = Net_Gethost::gethostbyaddr_t($ip, 3500);
			$gmap = "https://www.google.com/maps/preview/@" . $location['latitude'] . "," . $location['longitude'] . ",8z";
			$vars = array_merge([
				'title'     => $subject,
				'ip'        => $ip,
				'hostname'  => $hostname,
				'latitude'  => $location['latitude'],
				'longitude' => $location['longitude'],
				'city'      => $location['city'],
				'state'     => $location['state'],
				'country'   => $location['country'],
				'gmapurl'   => $gmap,
				'config'    => array('geoip' => true)
			], $extrafields);
			\Lararia\Bootstrapper::minstrap();
			$mail = \Illuminate\Support\Facades\Mail::to($email);
			$mail->send((new \Lararia\Mail\Simple("email.auth.$template", $vars))->setSubject($subject));
		}

		private function _forgot_info($params)
		{
			$db = \MySQL::initialize();
			if (isset($params['challenge']) && $params['challenge']) {
				// email client whitespace >:|
				$token = trim($params['challenge']);
				if (\strlen($token) !== 40 || !ctype_xdigit($token)) {
					return error("Malformed challenge request. It should be 40 characters long");
				}
				$q = $db->query("SELECT site_id, username, unix_timestamp(timestamp) as timestamp FROM info_requests WHERE challenge = '" . $db->escape_string($token) . "'");
				if ($q->num_rows < 1) {
					return false;
				}
				$rs = $q->fetch_object();
				if ((time() - (int)$rs->timestamp) > self::PASSWORD_MAX_TIME) {
					return error("Password reset expired, only valid for %d hours", self::PASSWORD_MAX_TIME / 3600);
				}

				// call reset program...
				$password = Password::generate(8);
				$context = \Auth::context($rs->username, 'site' . $rs->site_id);
				$ds = \DataStream::get($context);
				if (!$ds->query("auth_change_password", $password)) {
					return ("Error while changing password");
				}

				// check if IP restrictions prevented proper login
				$this->updateIPRestrictions($ds);

				info("Your new password is provided below. <br />" .
					"Change your password after logging in " .
					"via &quot;Account&quot; -&gt; &quot;Settings&quot;. " .
					"<pre class='new-password mt-1 text-center'>" . $password . "</pre>" .
					"<div class='d-block text-center'><a class='btn btn-secondary' href='/apps/login' tabindex='2'><i class='fa fa-undo mr-2'></i>" .
					"Return to login</a></div>");

				return $password;
			} else if (isset($params['pw_req'])) {
				if (isset($_GET['challenge'])) {
					return error("Enter a valid challenge token.");
				}
				if (!parent::validateCsrf()) {
					return error("Failed to validate CSRF request");
				}
				// password reset requested
				$domain = $params['domain'];
				$username = $this->parseLogin(array_get($params, 'username'));
				$email = $this->request_reset($domain, $username);
				$maskemail = $this->_maskEmail($email);
				$info = 'Your request has been sent' .
					($maskemail ? ' to <strong>' . $maskemail . '</strong>' : '') . ' from ' .
					\Crm_Module::FROM_NO_REPLY_ADDRESS .
					'<br /><br />This request is valid for 60 minutes.' .
					'<br /><br /><a href="/login" class="login">Return to login</a>';
				if (!\Error_Reporter::is_error()) {
					info($info);
				}
			} else if (!isset($params['challenge'])) {
				// no form action, display default info
				return info('A password reset request will be issued to the email address ' .
					'on record for the primary username. <br /><br />Contact ' .
					'<a href="mailto:' . \Crm_Module::FROM_ADDRESS . '">support</a> if the e-mail on ' .
					'record is invalid.  Be sure to provide a receipt of the last payment or ' .
					'subscription number on the account to verify identity.');
			} else if (isset($_GET['challenge'])) {
				return info("Enter your challenge token above to reset your account password.");
			}
		}

		/**
		 * Update login IP restrictions
		 *
		 * @param \DataStream $ds
		 * @return bool true if restriction made, false otherwise
		 */
		private function updateIPRestrictions(\DataStream $ds): bool
		{
			if (!AUTH_UPDATE_RESTRICTIONS_ON_RESET) {
				return false;
			}
			if ([] !== ($restrictions = $ds->query('auth_get_ip_restrictions'))) {
				return false;
			}
			if (\in_array(\Auth::client_ip(), $restrictions, true)) {
				return true;
			}
			return $ds->query('auth_restrict_ip', \Auth::client_ip()) &&
				info('IP restrictions are in place. %s not found in restriction list. Added to list.', \Auth::client_ip());
		}

		/**
		 * Request password reset, also
		 *
		 * @param $domain
		 * @return bool
		 */
		public function request_reset($domain = null, $username = null)
		{
			$db = \MySQL::initialize();
			$params = array_merge($_GET, $_POST);
			if ($domain === null && isset($params['domain'])) {
				$domain = $params['domain'];
			}
			if (!$domain) {
				return error("no domain specified!");
			}
			$domain = $this->_stripDomain($domain);
			$domain = strtolower(filter_var($domain, FILTER_SANITIZE_STRING));

			if (!preg_match(\Regex::DOMAIN, $domain)) {
				return error("invalid domain");
			}

			$site_id = $this->_findDomain($domain);

			if (!$site_id) {
				return false;
			}
			$hide = !empty($username);
			if ($username) {
				$ctx = \Auth::context(null, $domain);
				if (!\apnscpFunctionInterceptor::factory($ctx)->user_exists($username)) {
					// non-existent user, don't alert
					return true;
				}
			}
			$profile = \Auth::context($username, $domain);
			$ds = \DataStream::get($profile);
			if (!$username) {
				// admin user
				[$email, $username] = $ds->multi("common_get_email")->multi("common_get_admin_username")->sendMulti();
			} else {
				// mock up a session
				$email = $ds->query('common_get_email');
				if (!$email) {
					// no email set, sorry charlie!
					return true;
				}
			}
			$ip = \Auth::client_ip();
			$input = $domain;
			for ($i = 0; $i < 10; $i++) {
				$input .= \Util_PHP::random_int();
			}
			$challenge = sha1($input);
			$db->query("INSERT INTO info_requests (site_id, username, domain, ip, challenge, class)  " .
				"VALUES (" . $site_id . ",'" . $username . "', '" . $domain . "'," . sprintf("%u", ip2long($ip)) .
				",'" . $challenge . "','password');");
			$cpurl = \HTML_Kit::absolute_url('forgot_info', ['challenge' => $challenge]);

			$this->_sendEmail($email, "Password Reset Request", 'pwreset',
				array(
					'token'     => $challenge,
					'manualurl' => strtok($cpurl, '='),
					'url'       => $cpurl,
					'username'  => $username,
					'domain'    => $domain,
				)
			);

			return $hide ? true : $email;
		}

		private function _findDomain($domain)
		{
			$domain = $this->_stripDomain($domain);

			$id = \Auth::get_site_id_from_domain($domain);
			if (!$id && !\Auth_Redirect::redirect($domain)) {
				return error("Unknown domain - domain `%s' is not hosted here.", $domain);
			}

			return $id;
		}

		private function _maskEmail($email, $html = true)
		{
			if (!is_string($email)) {
				return null;
			}
			$char_pos = strpos($email, '@');
			$email = substr($email, 0, 1) . '<hidden>' .
				substr($email, $char_pos - 1, 1) . substr($email, $char_pos);
			if ($html) {
				$email = str_replace("<hidden>", "<em>&lt;hidden&gt;</em>", $email);
			}

			return $email;
		}

		public function getResponse()
		{
			if ($this->errors_exist()) {
				return $this->getMessages(\Error_Reporter::E_ERROR);
			}

			return $this->getMessages(\Error_Reporter::E_ALL & ~\Error_Reporter::E_ERROR);
		}

		/**
		 * System status configured for panel
		 *
		 * @return bool
		 */
		public function hasSystemStatus(): bool
		{
			return (bool)static::SYS_STATUS_URL;
		}

		public function getSystemStatus()
		{
			if (!static::SYS_STATUS_URL) {
				return null;
			}
			$key = "sys.status";
			$cache = \Cache_Super_Global::spawn();
			if (false !== ($status = $cache->get($key))) {
				return $status;
			}
			$url = $this->getSystemStatusUrl() . '/api/v1/components/groups';
			$adapter = new \HTTP_Request2_Adapter_Curl();
			$req = new \HTTP_Request2($url, \HTTP_Request2::METHOD_GET, ['adapter' => $adapter]);
			try {
				$resp = $req->send();
				if ($resp->getStatus() !== 200) {
					return true;
				}
			} catch (\Throwable $e) {
				return null;
			}
			$body = json_decode($resp->getBody(), true);
			$status = "Operational";
			$marker = null;
			foreach ($body['data'] as $d) {
				foreach ($d['enabled_components'] as $c) {
					$tmp = $c['status_name'];
					if ($tmp !== "Operational") {
						$status = $tmp;
						$marker = $c['description'] ? $c['description'] : $c['name'];
					}
				}
			}
			$status = array('status' => $status, 'marker' => $marker);
			$cache->set($key, $status, 300);

			return $status;
		}

		/**
		 * Get current system status URL
		 *
		 * @return string
		 */
		public function getSystemStatusUrl(): string
		{
			return static::SYS_STATUS_URL;
		}

		public function status2Css($status)
		{
			$status = str_replace(" ", "-", strtolower($status));
			switch ($status) {
				case 'operational':
					return 'text-success';
				case 'major-outage':
					return 'text-danger';
				case 'partial-outage':
					return 'text-warning';
				case 'performance-issues':
					return 'text-info';
				default:
					return 'text-muted';

			}
		}
	}

?>
