<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\changeinfo;

	use DateTimeZone;
	use Frontend\Css\StyleManager;
	use Module\Support\Webapps\App\Type\Discourse\Handler as Discourse;
	use UCard;

	class Page extends \Page_Container
	{

		public function __construct()
		{
			parent::__construct();
			$this->add_css('changeinfo.css');
			$this->add_javascript('changeinfo.js');
		}

		public function _init()
		{
			// call this earlier to ensure style gets posted before postback processes
			$params = $_POST;
			if (isset($params['save']) && StyleManager::allowSelection()) {
				$newtheme = array_get($params, 'theme', null);
				if ($this->themePermitted($newtheme)) {
					// don't like this approach
					if ($newtheme === StyleManager::getTheme()) {
						$newtheme = 'custom/' . $newtheme;
					}
					\Preferences::set(\Page_Renderer::THEME_KEY, $newtheme);
				} else {
					error("Unknown theme `%s'", $newtheme);
				}

			} else if (!empty($params['theme-upload'])) {
				if (empty($_FILES)) {
					return error("no theme uploaded!");
				}
				$this->is_postback = false;
				$this->useCustomTheme($_FILES['themefile']['tmp_name']);
			}

			foreach([\Page_Renderer::THEME_SWAP_BUTTONS, \Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE,
						\Page_Renderer::THEME_EXTERNAL_OPENER] as $var) {
				if (null === ($val = array_pull($params, "pref.${var}"))) {
					continue;
				}
				\Preferences::set($var, (bool)$val);
			}

			return parent::_init(); // TODO: Change the autogenerated stub
		}

		public function themePermitted($theme)
		{
			$themes = $this->getThemes();
			if (StyleManager::exists()) {
				$themes[] = StyleManager::getTheme();
			}

			return \in_array($theme, $themes, true);
		}

		public function getThemes()
		{
			return StyleManager::getThemes();
		}

		public function useCustomTheme($filename)
		{
			register_shutdown_function(function ($file) {
				if (file_exists($file)) {
					unlink($file);
				}
			}, $filename);

			if (!STYLE_ALLOW_CUSTOM) {
				return error("custom themes not enabled");
			}
			if (!$filename) {
				return error("no theme uploaded");
			}
			$style = new StyleManager($filename);
			if (!$style->valid()) {
				return error("stylesheet format is invalid");
			}

			return $style->save();
		}

		public function _render()
		{
			if (UCard::is('site')) {
				$tag = "session.prefix = " . json_encode($this->sql_get_prefix()) . ";";
				$this->add_javascript($tag, 'internal', false);
			}
		}

		public function on_postback($params)
		{
			if (!empty($params['theme-upload'])) {
				if (empty($_FILES)) {
					return error("no theme uploaded!");
				}

				return $this->useCustomTheme($_FILES['themefile']['tmp_name']);
			}
			$notifyOpts = array('login', 'passwordchange', 'domainchange', 'usernamechange');
			foreach ($notifyOpts as $opt) {
				$key = 'notify.' . $opt;
				// backwards compatibility
				\Preferences::forget($key);
				\Preferences::set($key, (bool)array_get($params, $key));
			}
			foreach (dot($params['pref']) as $key => $val) {
				\Preferences::set($key, $val);
			}
			if ($params['password']) {
				if ($params['password'] != $params['password_confirm']) {
					return error("Passwords do not match");
				}
				$status = $this->auth_change_password($params['password']);
				$this->bind($status);
			}
			if (!UCard::is('admin') && $params['gecos-challenge'] != $this->checksum($params['full_name'])) {
				$this->user_change_gecos($this->getAuthContext()->username, $params['full_name']);
			}

			if ($params['email-challenge'] != $this->checksum($params['email'])) {
				$this->setEmail($params['email']);
			}

			if ($params['timezone'] !== date_default_timezone_get()) {
				$this->setTimezone($params['timezone']);
			}

			if (0 !== strpos(\Preferences::get('language', ''), $params['language'])) {
				$this->setLanguage($params['language']);
			}
			if (isset($params['delete-magento-key'])) {
				$this->magento_delete_key();
			}

			if (isset($params['delete-geolite-key'])) {
				\Preferences::forget(Discourse::MAXMIND_KEY_PREF);
				return;
			} else if (isset($params['geolite-key']) && $params['geolite-key']) {
				// @todo validate
				\Preferences::set(Discourse::MAXMIND_KEY_PREF, (string)$params['geolite-key']);
			}

			if (isset($params['magento-pubkey']) && $params['magento-pubkey']) {
				$this->magento_set_key($params['magento-pubkey'], $params['magento-privkey']);
			}

			if (isset($params['hostname']) && $params['hostname'] !== gethostname()) {
				$this->config_set('net.hostname', $params['hostname']);
			}

			if (isset($params['panel-restart'])) {
				$this->scope_set('cp.restart', 1);
			}
			if (isset($params['delete-ga-key'])) {
				$this->deleteApiKey();
			} else if (isset($params['ga-client-id'])) {
				$secret = $params['ga-client-secret'] ?? null;
				$this->setApiKey($params['ga-client-id'], $secret);
			}


			if (isset($params['purgecache'])) {
				return $this->file_purge();
			}
			if (!UCard::is('admin')) {
				$this->user_flush();
			}

			if (isset($params['remove-ip'])) {
				return $this->auth_remove_ip_restriction($params['remove-ip']);
			} else if (isset($params['add-ip'])) {
				return $this->auth_restrict_ip($params['add-ip']);
			}
		}

		public function checksum($val)
		{
			return md5($val);
		}

		public function setEmail($email)
		{
			if (!$this->common_set_email($email)) {
				return false;
			}
			\Session::forget('avatar');
			UCard::reinit();

			return true;
		}

		public function setTimezone($zone)
		{

			return $this->common_set_timezone($zone);
		}

		public function setLanguage($language)
		{
			$card = \UCard::get();
			$key = $card->getLanguageKey();
			if (false === strpos($language, '.UTF-8')) {
				$language .= '.UTF-8';
			}
			if (false === setlocale(LC_ALL, $language)) {
				return error("invalid language `%s'", $language);
			}

			return $card->setPref($key, $language);
		}

		public function deleteApiKey()
		{
			\Util_Conf::set_analytics_api_key(null);
		}

		public function setApiKey($key, $secret)
		{
			return \Util_Conf::set_analytics_api_key($key, $secret);
		}

		public function change_password($passwd)
		{
			return $this->auth_change_password($passwd);

		}

		public function get_full_name()
		{
			$users = $this->user_getpwnam($this->getAuthContext()->username);

			return htmlentities($users['gecos']);

		}

		public function getEmail()
		{
			return $this->common_get_email();
		}

		public function getTimezones()
		{
			return DateTimeZone::listIdentifiers();
		}

		public function getLanguages()
		{
			return array(
				'en_US' => 'United States/English',
				'en_GB' => 'United Kingdom/English',
			);
		}

		public function getMyLanguage()
		{
			$locale = setlocale(LC_ALL, 0);
			if (false !== ($pos = strpos($locale, "."))) {
				return substr($locale, 0, $pos);
			}

			return $locale;
		}

		public function getMyTimezone()
		{
			return date_default_timezone_get();
		}

		public function getAnalyticsUser()
		{
			return \Util_Conf::get_analytics_api_key();
		}

		public function prefGet($name)
		{
			// all options default to on by default..
			return \Preferences::get('notify.' . $name, true);
		}

		public function getActiveTheme()
		{
			return \Preferences::get('theme.name', STYLE_THEME);
		}

		/**
		 * Magento can be installed
		 *
		 * @return bool
		 */
		public function hasMagento(): bool
		{
			// @TODO unify with Magento Webapp
			if (!$this->ssl_permitted() || $this->mysql_version() < 50500) {
				return false;
			}
			return !\Module\Support\Webapps::blacklisted('magento');
		}

		public function getMaxMindKey(): ?string
		{
			return \Preferences::get(Discourse::MAXMIND_KEY_PREF);
		}
	}