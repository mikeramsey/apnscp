<label class="form-control-static">
	Primary domain
</label>

<fieldset class="form-group">
	{{ \UCard::get()->getDomain() }}
	<button type="button" class="btn btn-secondary change" id="change-domain">
		<i class="ui-action ui-action-edit"></i>
		Change
	</button>
</fieldset>

<div class="rules" id="domain-rules">
	<h3>Important Rules</h3>
	<ol class="rules">
		<li>This domain cannot exist elsewhere on any other account.
		<li>Once promoted to the primary domain, all HTTP files for this domain will be served from
			<em>/var/www/html</em>
			(<em>{{ cmd('user_get_home', \Session::get('username')) }}/mainwebsite_html</em> is an alias) &ndash; this
			is a perk to being the primary domain and may not
			be changed.
		<li>Upon changing domain, you will be automatically logged out. Login with your new domain.
	</ol>
	<label class="custom-control custom-checkbox mb-0">
		<input type="checkbox" class="custom-control-input"
		       name="delfiles" value="agree"/>
		<span class="custom-control-indicator"></span>
		I understand the rules!
	</label>
</div>