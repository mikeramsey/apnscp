<h4 class="">
	<i class="ui-action ui-action-label ui-menu-category-{{ Template_Engine::init()->getApplicationFromId('dns')->getCategory() }} d-inline"></i>
	{{ Template_Engine::init()->getApplicationFromId('dns')->getName() }}
</h4>
<fieldset class="form-group">
	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
			       name="pref{{ \HTML_Kit::prefify(apps\dns\Page::SHOW_APEX_NS_PREFERENCE) }}"/>
			<input class="form-check-input custom-control-input"
			       @if (\Preferences::get(apps\dns\Page::SHOW_APEX_NS_PREFERENCE)) checked @endif
			       type="checkbox" value="1" name="pref{{ \HTML_Kit::prefify(apps\dns\Page::SHOW_APEX_NS_PREFERENCE) }}"/>
			<span class="custom-control-indicator"></span>
			Show apex NS records
		</label>
	</div>
</fieldset>
