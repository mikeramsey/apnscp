<h4 class="">
	<i class="ui-action ui-action-label ui-menu-category-{{ Template_Engine::init()->getApplicationFromId('filemanager')->getCategory() }} d-inline"></i>
	{{ Template_Engine::init()->getApplicationFromId('filemanager')->getName() }}
</h4>
<fieldset class="form-group">
	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
			       name="pref{{ \HTML_Kit::prefify(apps\filemanager\Page::PROTECT_DIRS_KEY) }}"/>
			<input class="form-check-input custom-control-input"
			       @if (\Preferences::get(apps\filemanager\Page::PROTECT_DIRS_KEY, true)) checked @endif
			       type="checkbox" value="1"
			       name="pref{{ \HTML_Kit::prefify(apps\filemanager\Page::PROTECT_DIRS_KEY) }}"/>
			<span class="custom-control-indicator"></span>
			Protect system directories from browsing
		</label>
	</div>
</fieldset>
