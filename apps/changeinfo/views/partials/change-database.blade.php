<label class="form-control-static">
	Database Prefix
</label>

<fieldset class="form-group">
	{{ \cmd('sql_get_prefix') }}
	<button type="button" class="btn btn-secondary change" id="change-prefix">
		<i class="ui-action ui-action-edit"></i>
		Change
	</button>
</fieldset>

<div class="rules" id="prefix-rules">
	<h3>Important Rules</h3>
	<ol class="rules">
		<li>A database prefix must be at least {{ \Sql_Module::MIN_PREFIX_LENGTH }} characters long.
		<li>All data sources that reference the old database configuration,
			e.g. {{ (array_get(\cmd('sql_list_mysql_databases'), 0, '<no databases detected>')) }}
			must be changed to
			reflect the new prefix.
		<li>All database users besides the primary user, {{ \Session::get('username') }}, will also be
			renamed.
		<li>You are responsible for making these changes to your applications.
	</ol>
	<label class="custom-control custom-checkbox mb-0">
		<input type="checkbox" class="custom-control-input"
		       name="delfiles" value="agree"/>
		<span class="custom-control-indicator"></span>
		I understand the rules!
	</label>
</div>