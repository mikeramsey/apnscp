superclass:
  PassThrough:
    help: Disable all filters.
    name: Disable all filters.
    filters:
      -
  CoreFilters:
    name: Safe optimizations
    help: Determined safe for most sites.
    filters:
      - add_head
      - inline_import_to_link
      - combine_css
      - rewrite_css
      - fallback_rewrite_css_urls
      - rewrite_style_attributes_with_url
      - flatten_css_imports
      - rewrite_javascript
      - rewrite_javascript_external
      - rewrite_javascript_inline
      - combine_javascript
      - inline_css
      - inline_javascript
      - rewrite_images
      - convert_jpeg_to_progressive
      - convert_png_to_jpeg
      - convert_jpeg_to_webp
      - convert_to_webp_lossless
      - inline_images
      - recompress_images
      - recompress_jpeg
      - recompress_png
      - recompress_webp
      - convert_gif_to_png
      - strip_image_color_profile
      - strip_image_meta_data
      - jpeg_subsampling
      - resize_images
      - resize_rendered_image_dimensions
      - extend_cache
      - extend_cache_css
      - extend_cache_images
      - extend_cache_scripts
      - convert_meta_tags
  OptimizeForBandwidth:
    name: Optimize for bandwidth
    help: Strong guarantee of safety and is suitable as a default setting for use with sites that are not aware of PageSpeed.
    filters:
      - rewrite_css
      - rewrite_javascript
      - rewrite_javascript_external
      - rewrite_javascript_inline
      - convert_jpeg_to_progressive
      - convert_png_to_jpeg
      - convert_jpeg_to_webp
      - recompress_images
      - recompress_jpeg
      - recompress_png
      - recompress_webp
      - convert_gif_to_png
      - strip_image_color_profile
      - strip_image_meta_data
      - jpeg_subsampling
      - in_place_optimize_for_browser
all:
  <<: &optimize
    canonicalize_javascript_libraries:
      name: Use a CDN for common libraries
      help: Redirects JavaScript libraries to a JavaScript hosting service.
      link: canonicalize-js
    extend_cache:
      name: Extends cache of unoptimized static files
      help: Extends cache lifetime of CSS, JS, and image resources that have not otherwise been optimized, by signing URLs with a content hash.
      link: cache-extend
    extend_cache_css:
      name: Extends cache of unoptimized CSS
      help: Implied by extend_cache. Extends cache lifetime of otherwise unoptimized CSS resources by signing URLs with a content hash.
      link: cache-extend
    extend_cache_images:
      name: Extends cache of unoptimized images
      help: Implied by extend_cache. Extends cache lifetime of otherwise unoptimized images by signing URLs with a content hash.
      link: cache-extend
    extend_cache_scripts:
      name: Extends cache of unoptimized JavaScript
      help: Implied by extend_cache. Extends cache lifetime of otherwise unoptimized scripts by signing URLs with a content hash.
      link: cache-extend
    extend_cache_pdfs:
      name: Extends cache of unoptimized PDFs
      help: Extends cache lifetime of PDFs by signing URLs with a content hash.
      link: cache-extend
    local_storage_cache:
      name: Cache inlined resources in browser
      help: Cache inlined resources in HTML5 local storage.
    outline_css:
      name: Externalize CSS
      help: Externalize large blocks of CSS into a cacheable file.
      link: css-outline
    outline_javascript:
      name: Externalize JavaScript
      help: Externalize large blocks of JavaScrit into a cacheable file.
      link: js-outline
  <<: &rtt
    combine_css:
      name: Combine multiple CSS elements
      help: Combines multiple CSS elements into one.
      link: css-combine
    flatten_css_imports:
      name: Flatten @import
      help: Inline CSS by flattening all @import rules.
    inline_css:
      name: Inline CSS
      help: Inlines small CSS files into the HTML document.
      link: css-inline
    inline_google_font_css:
      name: Inline Google Font CSS
      help: Inlines small CSS files used by fonts.googleapis.com into the HTML document.
      link: css-inline-google-fonts
    combine_javascript:
      name: Combine multiple <script> elements
      help: Combines multiple script elements into one.
      link: js-combine
    inline_javascript:
      name: Inline JavaScript
      help: Inlines small JS files into the HTML document.
      link: js-inline
    move_css_above_scripts:
      name: Relocate CSS above <script>
      help: Moves CSS elements above <script> tags.
      link: css-above-scripts
    sprite_images:
      name: Merge background images into a single sprite
      help: Combine background images in CSS files into one sprite.
      link: image-sprite
    insert_dns_prefetch:
      name: Prefetch DNS
      help: Inserts <link rel="dns-prefetch" href="//www.example.com"> tags to reduce DNS resolution time.
  <<: &request
    rewrite_domains:
      name: Additionally rewrite domains
      help: Rewrites the domains of resources not otherwise touched by PageSpeed, based on MapRewriteDomain and ShardDomain settings in the config file.
      link: domain-rewrite
  <<: &payload
    collapse_whitespace:
      name: Minify HTML
      help: Removes excess whitespace in HTML files (avoiding <pre>, <script>, <style>, and <textarea>).
      link: whitespace-collapse
    combine_heads:
      name: Combine multiple <head>
      help: Combines multiple <head> elements found in document into one.
      link: head-combine
    elide_attributes:
      name: Remove insignificant attributes
      help: Removes attributes which are not significant according to the HTML spec.
      link: attribute-elide
    rewrite_javascript:
      name: Minify JavaScript
      help: Rewrites JavaScript files to remove excess whitespace and comments. In OptimizeForBandwidth mode, the minification occurs in-place without changing URLs.
      link: js-minify
    rewrite_javascript_external:
      name: Minify external JavaScript
      help: Implied by rewrite_javascript. Rewrites JavaScript external files to remove excess whitespace and comments. In OptimizeForBandwidth mode, the minification occurs in-place without changing URLs.
      link: js-minify
    rewrite_javascript_inline:
      name: Minify inline JavaScript
      help: Implied by rewrite_javascript. Rewrites inline JavaScript blocks to remove excess whitespace and comments.
      link: js-minify
    rewrite_images:
      name: Optimize images
      help: Optimizes images, re-encoding them, removing excess pixels, and inlining small images. In OptimizeForBandwidth mode, the minification occurs in-place without changing URLs.
      link: /doc/reference-image-optimize#rewrite_images
    convert_jpeg_to_progressive:
      name: Convert JPEG to progressive JPEG
      help: Converts larger jpegs to progressive format. Implied by recompress images.
      link: /doc/reference-image-optimize#convert_jpeg_to_progressive
    convert_png_to_jpeg:
      name: Opportunistically compress GIF and PNG into JPEG
      help: Converts gif and png images into jpegs if they appear to be less sensitive to compression artifacts and lack alpha transparency. Implied by recompress images.
      link: /doc/reference-image-optimize#convert_png_to_jpeg
    convert_jpeg_to_webp:
      name: Convert JPEG to WebP
      help: Producess lossy webp rather than jpeg images for browsers that support webp. Implied by recompress images.
      link: /doc/reference-image-optimize#convert_jpeg_to_webp
    convert_to_webp_animated:
      name: Convert GIF to WebP
      help: Replaces animated gif images with webp images on browsers that support the format.
      link: /doc/reference-image-optimize#convert_to_webp_animated
    convert_to_webp_lossless:
      name: Convert PNG to WebP
      help: Implied by rewrite_images. Replaces png and non-animated gif images with webp images on browsers that support the format.
      link: /doc/reference-image-optimize#convert_to_webp_lossless
    inline_images:
      name: Inline small images
      help: "Implied by rewrite_images. Replaces small images by data: urls."
      link: /doc/reference-image-optimize#inline_images
    recompress_images:
      name: Recompress images
      help: Implied by rewrite_images. Recompresses images, removing excess metadata and transforming gifs into pngs.
      link: /doc/reference-image-optimize#recompress_images
    recompress_jpeg:
      name: Recompress JPEG
      help: Implied by recompress_images. Recompresses jpegs, removing excess metadata.
      link: /doc/reference-image-optimize#recompress_jpeg
    recompress_png:
      name: Recompress PNG
      help: Implied by recompress_images. Recompresses pngs, removing excess metadata.
      link: /doc/reference-image-optimize#recompress_png
    recompress_webp:
      name: Recompress WebP
      help: Implied by recompress_images. Recompresses webps, removing excess metadata.
      link: /doc/reference-image-optimize#recompress_webp
    convert_gif_to_png:
      name: Convert GIF to PNG
      help: Implied by recompress_images. Optimizes gifs to pngs.
      link: /doc/reference-image-optimize#convert_gif_to_png
    strip_image_color_profile:
      name: Strip image color profile
      help: Implied by recompress_images. Strips color profile info from images.
      link: /doc/reference-image-optimize#strip_image_color_profile
    strip_image_meta_data:
      name: Strip image EXIF data
      help: Implied by recompress_images. Strips EXIF meta data from images.
      link: /doc/reference-image-optimize#strip_image_meta_data
    jpeg_subsampling:
      name: Reduce JPEG color sampling
      help: Implied by recompress_images. Reduces the color sampling of jpeg images to 4:2:0.
      link: /doc/reference-image-optimize#jpeg_sampling
    resize_images:
      name: Resize if <IMG> requests smaller version
      help: Implied by rewrite_images. Resizes images when the corresponding <img> tag specifies a smaller width and height.
      link: /doc/reference-image-optimize#resize_images
    resize_rendered_image_dimensions:
      name: Resize images to on-screen size
      help: Implied by rewrite_images. Resizes an image when the rendered dimensions of the image are smaller than the actual image.
      link: /doc/reference-image-optimize#resize_rendered_image_dimensions
    prioritize_critical_css:
      name: Inline CSS on the page
      help: Replace CSS tags with inline versions that include only the CSS used by the page.
    dedup_inlined_images:
      name: Deduplicate inlined images
      help: Replaces repeated inlined images with JavaScript that loads the image from the first occurence of the image.
    remove_comments:
      name: Remove comments
      help: Removes comments in HTML files (but not in inline JavaScript or CSS).
      link: comment-remove
    remove_quotes:
      name: Remove attribute quotes
      help: Removes quotes around HTML attributes that are not lexically required.
      link: quote-remove
    rewrite_css:
      name: Minify CSS
      help: Rewrites CSS files to remove excess whitespace and comments, and, if enabled, rewrite or cache-extend images referenced in CSS files. In OptimizeForBandwidth mode, the minification occurs in-place without changing URLs.
      link: css-rewrite
    fallback_rewrite_css_urls:
      name: Try-rewrite all CSS
      help: Rewrites resources referenced in any CSS file that cannot otherwise be parsed and minified.
      link: css-rewrite
    rewrite_style_attributes:
      name: Rewrite CSS in style attributes
      help: Rewrite the CSS in style attributes by applying the configured rewrite_css filter to it.
    rewrite_style_attributes_with_url:
      name: Rewrite external CSS in style attributes
      help: Rewrite the CSS in style attributes if it contains the text 'url(' by applying the configured rewrite_css filter to it
      link: rewrite-style-attributes
    trim_urls:
      name: Convert URLs to relative links
      help: Shortens URLs by making them relative to the base URL.
  <<: &rendering
    convert_meta_tags:
      name: Send <meta> tags as HTTP headers
      help: Adds a response header for each meta tag with an http-equiv attribute.
    defer_javascript:
      name: Defer JavaScript execution until page load
      help: Defers the execution of JavaScript in HTML until page load complete.
      link: js-defer
    hint_preload_subresources:
      name: Preload related resources
      help: Inserts Link:</example.css>; rel=preload headers to permit earlier fetching of important resources.
    inline_preview_images:
      name: Use low-quality placeholders
      help: Uses inlined low-quality images as placeholders which will be replaced with original images once the web page is loaded.
    resize_mobile_images:
      name: Use low-quality placeholders on mobile
      help: Works just like inline_preview_images, but uses smaller placeholder images and only serves them to mobile browsers.
      link: /doc/filter-inline-preview-images#resize_mobile_images
    insert_image_dimensions:
      name: Always set image width and height attributes
      help: Adds width and height attributes to <img> tags that lack them.
      link: /doc/reference-image-optimize#insert_image_dimensions
    lazyload_images:
      name: Lazyload images
      help: Loads images when they become visible in the client viewport.
    responsive_images:
      name: Responsive images
      help: Makes images responsive by adding srcset with images optimized for various resolutions.
      link: image-responsive
    in_place_optimize_for_browser:
      name: Add browser optimizations
      help: Perform browser-dependent in-place resource optimizations.
      link: /doc/system#in_place_optimize_for_browser
    move_css_to_head:
      name: Relocate CSS into <head>
      help: Moves CSS elements into the <head>.
      link: css-to-head
  <<: &other
    add_head:
      name: Add <head>
      help: Adds a <head> element to the document if not already present.
      link: head-add
    #add_instrumentation:
    #  name: Add latency checks
    #  help: Adds JavaScript to page to measure latency and send back to the server.
    #  link: instrumentation-add
    include_js_source_maps:
      name: Create source maps
      help: Adds source maps to rewritten JavaScript files.
      link: source-maps-include
    inline_import_to_link:
      name: Inline @import to <link>
      help: Inlines <style> tags comprising only CSS @imports by converting them to equivalent <link> tags.
      link: css-inline-import
    insert_ga:
      name: Add Google Analytics
      help: Adds the Google Analytics snippet to each HTML page.
    make_google_analytics_async:
      name: Use Google Analytics asynchronously
      help: Convert synchronous use of Google Analytics API to asynchronous
    make_show_ads_async:
      name: Use Google AdSense asynchronously
      help: Convert synchronous use of Google AdSense API to asynchronous
    pedantic:
      name: Force HTML4 compliance
      help: Add default types for <script> and <style> tags if the type attribute is not present and the page is not HTML5. The purpose of this filter is to help ensure that PageSpeed does not break HTML4 validation.
filters:
  - name: "Optimize Caching"
    items: *optimize
  - name: "Minimize Round Trip Times"
    items: *rtt
  - name: "Minimize Request Overhead"
    items: *request
  - name: "Minimize Payload Size"
    items: *payload
  - name: "Optimize Browser Rendering"
    items: *rendering
  - name: "Other"
    items: *other