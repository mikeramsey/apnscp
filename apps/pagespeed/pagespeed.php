<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */


namespace apps\pagespeed;

use apps\pagespeed\models\Insight;
use Google\Service\PagespeedInsights;
use Google\Service\PagespeedInsights\LighthouseResultV5 as Lighthouse_Result;
use HTTP\Cache;
use Module\Support\Webapps;
use Symfony\Component\Yaml\Yaml;
use Tivie\HtaccessParser\HtaccessContainer;
use Tivie\HtaccessParser\Token\Directive;
use const Tivie\HtaccessParser\Token\TOKEN_DIRECTIVE;

class Page extends \Page_Container {
	const DIRECTIVE_NAME = 'ModPagespeedEnableFilters';
	const SUPERCLASS_NAME = 'ModPagespeedRewriteLevel';

	protected $domain;
	// filter cache
	protected $filters = [];

	public function __construct($doPostback = true)
	{
		$this->domain = \Util_Conf::login_domain();
		parent::__construct($doPostback);
	}


	public function _layout()
	{
		$this->add_javascript('pagespeed.js');
		parent::_layout();
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$show = $this->loadFiltersFromHostname($this->domain);
		return view('index', [
			'filters' => $this->filters,
			'domain' => $this->getDomain(),
			'malformedHtaccess' => $show === null
		]);
	}

	public function getDomain(): string
	{
		return $this->domain;
	}
	/**
	 * Switch domain
	 *
	 * @param string $domain
	 * @return mixed
	 */
	public function loadDomain(string $domain)
	{
		$this->domain = $domain;
		return $this->index();
	}

	public function getAvailableDomains()
	{
		static $domains;
		if (isset($domains)) {
			return $domains;
		}

		$domains = Webapps::getAllHostnames($this->getAuthContext());
		\Util_Conf::sort_domains($domains, 'assoc', true);

		return $domains;
	}

	public function getAvailableFilters() {
		$manifest = $this->loadManifest();
		return $manifest['filters'];
	}

	/**
	 * Read filters from .htaccess
	 *
	 * @param string $hostname
	 * @return array
	 */
	private function loadFiltersFromHostname(string $hostname): ?array {
		// @TODO no inheritance possible
		// ?PageSpeedFilters=+debug will list shorthand filter names
		$docroot = $this->getApnscpFunctionInterceptor()->web_get_docroot($hostname);
		$filters = [];
		if (!$docroot) {
			error("Unknown domain `%s'", $hostname);
			return $filters;
		}
		if (!$this->getApnscpFunctionInterceptor()->file_exists($docroot . '/.htaccess')) {
			return $filters;
		}

		$parser = $this->getApnscpFunctionInterceptor()->personality_scan($docroot . '/.htaccess');
		if (false === $parser) {
			return null;
		}
		$line = $parser->search(self::DIRECTIVE_NAME, \Tivie\HtaccessParser\Token\TOKEN_DIRECTIVE, true);
		if (null !== $line) {
			$filters = array_fill_keys(explode(',', array_get($line->getArguments(), 0)), 1);
		}

		$line = $parser->search(self::SUPERCLASS_NAME, \Tivie\HtaccessParser\Token\TOKEN_DIRECTIVE, true);
		if (null !== $line) {
			// get superclass filter
			$name = array_get($line->getArguments(), 0);
			$filters[$name] = 1;
			$filters += array_fill_keys(array_get($this->getSuperclassFilters(), "${name}.filters", []), 1);
		}

		return array_keys($this->filters = $filters);
	}

	/**
	 * Load app manifest
	 *
	 * @return array
	 */
	private function loadManifest(): array
	{
		$cache = \Cache_Global::spawn();
		if (false !== ($filters = $cache->get('pagespeed.filters'))) {
			return $filters;
		}

		$filters = Yaml::parseFile(__DIR__ . '/filters.yml');
		$cache->set('pagespeed.filters', $filters, 43200);

		return $filters;
	}

	/**
	 * Get superclass filters
	 *
	 * @return array
	 */
	public function getSuperclassFilters(): array
	{
		return array_get($this->loadManifest(), 'superclass', []);
	}

	public function purge(string $domain): bool
	{
		return Cache::instantiateContexted($this->getAuthContext())->purge($domain);
	}

	/**
	 * Apply filters to a domain
	 *
	 * @param string $domain
	 * @return bool
	 */
	public function apply(string $domain, array $filters, string $superclass = ''): bool
	{
		$docroot = $this->getApnscpFunctionInterceptor()->web_get_docroot($domain);
		if (!$docroot) {
			return error("Unknown domain `%s'", $domain);
		}
		/** @var $parser HtaccessContainer */
		$parser = $this->getApnscpFunctionInterceptor()->personality_scan($docroot . '/.htaccess');
		$hash = $this->getApnscpFunctionInterceptor()->personality_hash($parser->txtSerialize());

		$this->setDirective($parser, self::DIRECTIVE_NAME, implode(',', $filters));
		$this->setDirective($parser, self::SUPERCLASS_NAME, $superclass);
		return $this->getApnscpFunctionInterceptor()->personality_commit($domain, $hash, $parser);
	}

	private function setDirective(HtaccessContainer $container, string $name, $val) {
		if (!$val) {
			if (null !== ($idx = $container->getIndex($name, TOKEN_DIRECTIVE))) {
				$container->offsetUnset($idx);
			}
			return;
		}

		$line = $container->search($name, \Tivie\HtaccessParser\Token\TOKEN_DIRECTIVE, true);

		if (null === $line) {
			// append
			return $container->append(new Directive($name, (array)$val));
		}

		$line->removeArgument(implode('', $line->getArguments()));
		$line->setArguments((array)$val);
	}

	/**
	 * Run Lighthouse test
	 *
	 * @param string $domain
	 * @return Lighthouse_Result
	 */
	public function runTest(string $domain): Lighthouse_Result
	{
		$domain = $this->web_normalize_hostname($domain);
		if (!$this->web_get_docroot($domain)) {
			throw new \ArgumentError("Invalid domain");
		}
		$client = new \Google_Client();
		$client->setApplicationName("pagespeed");
		$service = new PagespeedInsights($client);
		$resp = $service->pagespeedapi->runpagespeed($this->getPreferredDomainUri($domain), ['strategy' => 'desktop']);
		return $resp->getLighthouseResult();
	}

	/**
	 * Render Lighthouse results
	 *
	 * @param string            $domain
	 * @param Lighthouse_Result $result
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function renderResults(string $domain, Lighthouse_Result $result)
	{
		if (!preg_match(\Regex::DOMAIN, $domain)) {
			fatal('Invalid domain');
		}
		$this->domain = $domain;
		$insight = new Insight($result);
		return view('partials.test-results', [
			'domain'  => $domain,
			'test'    => $insight,
			'details' => $insight->getDetails(),
			'filters' => $this->loadFiltersFromHostname($domain)
		]);
	}

	/**
	 * Filter is enabled
	 *
	 * @param string $filter
	 * @return bool
	 */
	public function filterEnabled(string $filter): bool
	{
		return isset($this->filters[$filter]);
	}

	/**
	 * Get filters active for domain
	 *
	 * @return array
	 */
	public function getActiveFilters(): array
	{
		return array_keys($this->filters);
	}

	/**
	 * Link to documentation
	 *
	 * @param string $filter
	 * @return string
	 */
	public function docLink(string $filter) {
		static $manifest;
		if (!isset($manifest)) {
			$manifest = $this->loadManifest();
		}

		$slug = $manifest['all'][$filter]['link'] ?? str_replace('_', '-', $filter);
		return 'https://modpagespeed.com' . (strpos($slug, '/') === 0 ? $slug : '/doc/filter-' . $slug);
	}

	public function getPreferredDomainUri(string $hostname) {
		if (!$this->ssl_enabled() || !$this->ssl_cert_exists()) {
			return 'http://' . $hostname;
		}


		if (!$cert = $this->ssl_get_certificate()) {
			return 'http://'. $hostname;
		}

		$sans = $this->ssl_get_alternative_names($cert);
		$wcparent = "*." . substr($hostname, strpos($hostname, '.') + 1);
		foreach ($sans as $san) {
			if ($san === $hostname || ($san[0] === '*' && $wcparent === $san)) {
				return 'https://' . $hostname;
			}
		}
		return 'http://' . $hostname;
	}

	public function formatSpeedIndex(int $index) {
		if ($index < 4399) {
			return 'text-success';
		}

		if ($index < 5899) {
			return 'text-warning';
		}

		return 'text-danger';
	}
}