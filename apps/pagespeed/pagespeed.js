/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

$(document).ready(function () {
	$('.ajax-wait').ajaxWait();
	$('.master-select').change(function () {
		var state = $(this).prop('checked');
		$(this).closest('thead').siblings('tbody').find(':checkbox').each(function (i, e) {
			$(e).prop('checked', !!state);
		}).promise().done(function () {
			var $form = $('#filterForm');

			$.ajax(apnscp.call_app_ajax(null, null, [], {
				url: $form.attr('action'),
				method: 'POST',
				dataType: 'json',
				data: $form.serializeArray()
			}));
		});
	});
	$("#domain-picker").combobox().on('comboboxselect', function (event, item) {
		$('select#domain-picker').triggerHandler('change');
	});

	$('#domain-picker').on('change', function() {
		apnscp.assign_url('/apps/' + session.appId + '/' + $(this).val());
		return true;
	});

	$('#purge').click(function(e) {
		var domain = $(this).val();
		$.ajax(apnscp.call_app_ajax(null, null, [], {
			url: '/apps/' + session.appId + '/purge',
			method: 'POST',
			dataType: 'json',
			data: {domain: domain}
		})).done(function (ret) {
			apnscp.addMessage('Cache Purged');
		});
	});

	$('#filterForm .filter-control').change(function() {
		var $form = $('#filterForm'), $el = $(this),
			post = $form.serializeArray();
		if ($('#superclass :checked').length) {
			var impliedFilters = keyFilters($('#superclass :checked').eq(0).data('filters'));
			post = post.filter(function (e) {
				return !(e.value in impliedFilters);
			});
		}

		$.ajax(apnscp.call_app_ajax(null, null, [], {
			url: $form.attr('action'),
			method: 'POST',
			dataType: 'json',
			data: post
		})).fail(function () {
			$el.prop('checked', $el.prop('checked')^1);
		});
	});

	$('#insightForm').submit(function (e) {
		e.preventDefault();
		var $form = $(this);
		$.ajax(apnscp.call_app_ajax(null, null, [], {
			url: $form.attr('action'),
			method: 'POST',
			dataType: 'html',
			data: $form.serializeArray()
		})).done(function (ret) {
			$('#insightForm :submit').effect("transfer", {to: $('#pagespeedTests')}, function () {
				$(ret).appendTo($('#pagespeedTests'));
			});
		});
	});

	$('#superclass :checkbox').change(function (e) {
		var flipped = flipped = keyFilters($(this).data('filters')),
			that = this;
		var checked = $(this).prop('checked');

		$("#superclass :checkbox").filter(function (i, $el) {
			return $el != that;
		}).prop('checked', false);

		$('#ui-app .filter-control').each(function (i, $el) {
			if (checked) {
				$($el).prop('checked', $el.value in flipped);
			} else if ($el.value in flipped) {
				$($el).prop('checked', false);
			}
		}).promise().done(function () {
			$('#ui-app .filter-control').eq(0).triggerHandler('change');
		});
	});
});

function keyFilters(filters) {
	var filters = filters.split(' ');
	return Object.keys(filters)
		.reduce(function (obj, key) {
			obj[filters[key]] = key;
			return obj;
		}, {});
}