<h4 class="">General Service Configuration</h4>
<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-control-label bg-white">
		<i class="fa fa-hdd-o"></i>
		Storage
	</h5>
	<div class="col-sm-6 form-inline">
		<label class="pl-0 form-inline custom-checkbox custom-control align-items-center mr-0 d-flex">
			<input type="checkbox" class="custom-control-input" name="disk-unlimited"
			       id="disk_unlimited" value="0" @if (!$Page->get_option('disk_quota')) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Unlimited
		</label>

		<span class="text-muted mx-3">or</span>

		<div class="form-inline d-inline-block">
			<div class="d-flex align-items-center">
				<input type="text" id="disk_quota" name="disk_quota" class="d-inline-block form-control mr-1" min="0"
				       size="4" value="{{ $Page->get_option('disk_quota') }}"/> MB
			</div>
			<div id='disk_slider' class='mt-1 ui-slider-1 hidden-sm-down'></div>
		</div>
	</div>
</fieldset>