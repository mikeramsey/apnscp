<?php declare(strict_types=1);
	/*
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
	 */

	namespace apps\setup\models;

	use apnscpFunctionInterceptor;
	use apnscpFunctionInterceptorTrait;
	use Auth;
	use ContextableTrait;
	use Opcenter\Mail\Services\Haproxy;
	use Opcenter\Mail\Services\Rspamd\Dkim;
	use Opcenter\Net\Ip4;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Authentication
	 *
	 * Resolves best hostname for a given domain
	 *
	 * - Prefers service,XXXserver service value if resolves
	 * - Falls back to domain name if IP matches hostname
	 * - Resorts to server name last
	 *
	 * @package apps\setup\models
	 */
	class Authentication
	{
		use apnscpFunctionInterceptorTrait;
		use ContextableTrait;

		/**
		 * @var SiteConfiguration
		 */
		protected SiteConfiguration $svc;
		protected string $domain;

		// @var root domain resolves to server
		protected bool $dnsActive;

		// @var bool IP address has propagated
		private array $propagatedCache = [];

		// SSL certificate contains hostname
		private array $sslContainsCache = [];

		// @var array IPs wanted to confirm resolution
		private array $desiredIps;

		protected function __construct(SiteConfiguration $svc, string $domain = null)
		{
			$this->svc = $svc;
			$this->domain = $domain ?? $this->getAuthContext()->domain;
			$this->desiredIps = array_filter([$this->dns_get_public_ip(), $this->dns_get_public_ip6()]);
			$this->dnsActive = $this->hostnameResolves($this->domain);
		}

		private function hostnameResolves(string $hostname): bool
		{
			if (!isset($this->propagatedCache[$hostname])) {
				$ip = $this->dns_gethostbyname_t($hostname, 2000);
				$this->propagatedCache[$hostname] = in_array(
					$ip,
					$this->desiredIps,
					true
				);
			}

			return $this->propagatedCache[$hostname];
		}

		/**
		 * Login username
		 *
		 * @return string
		 */
		public function username(): string
		{
			return $this->getAuthContext()->username;
		}

		/**
		 * Server IP address
		 *
		 * @return string
		 */
		public function ipAddress(): string
		{
			return $this->desiredIps[0] ?? Ip4::my_ip();
		}

		/**
		 * DKIM feature present
		 *
		 * @return bool
		 */
		public function hasDkim(): bool
		{
			if ($this->getAuthContext()->level & PRIVILEGE_USER) {
				return false;
			}

			return $this->dkim_has();
		}

		/**
		 * DNS hosting enabled
		 *
		 * @return bool
		 */
		public function hasDns(): bool
		{
			if ($this->getAuthContext()->level & PRIVILEGE_USER) {
				return false;
			}

			return $this->dns_configured();
		}

		/**
		 * Email enabled for account
		 *
		 * @return bool
		 */
		public function hasMail(): bool
		{
			return $this->svc->getSiteFunctionInterceptor()->email_user_enabled($this->username());
		}

		/**
		 * Hostname for service
		 *
		 * @param string $service
		 * @param bool   $sslCheck
		 * @return string
		 */
		public function hostname(string $service, bool $sslCheck = false): string
		{
			$service = strtolower($service);
			switch ($service) {
				case 'ssh':
					return $this->loginHostname($this->domain, $sslCheck);
				case 'smtp':
					return $this->loginHostname($this->common_get_service_value('mail', "${service}server"),
						$sslCheck);
				default:
					return $this->loginHostname($this->common_get_service_value($service, "${service}server"),
						$sslCheck);
			}
		}

		/**
		 * Login hostname
		 *
		 * @param null $hostname
		 * @param bool $sslCheck
		 * @return string
		 */
		private function loginHostname($hostname = null, bool $sslCheck = false): string
		{
			$hostname = $hostname ?? $this->domain;
			if ($this->hostnameResolves($hostname) && (!$sslCheck || $this->sslContains($hostname))) {
				return $hostname;
			}

			if ($hostname !== $this->domain && $this->hostnameResolves($this->domain) &&
				(!$sslCheck || $this->sslContains($this->domain))) {
				return $this->domain;
			}

			return $this->serverName();
		}

		public function sslContains(string $hostname): bool
		{
			// ignore in low-memory mode
			if (!Haproxy::present()) {
				return false;
			}

			if (!isset($this->sslContainsCache[$hostname])) {
				$this->sslContainsCache[$hostname] = $this->svc->getSiteFunctionInterceptor()->ssl_contains_cn($hostname);
			}

			if ($this->sslContainsCache[$hostname] && ends_with($hostname, $this->domain)) {
				// don't know but let's assume it is
				return true;
			}

			return $this->sslContainsCache[$hostname];
		}

		public function serverName(): string
		{
			return SERVER_NAME;
		}

		public function dkimSelector(): ?string
		{
			return $this->dkim_selector();
		}

		public function dkimKey(): ?string
		{
			return $this->dkim_key();
		}

		public function dkimRecord(): ?string
		{
			return Dkim::DNS_HEADER . $this->dkimKey();
		}

		public function getNameservers(string $domain = null): array
		{
			return $this->dns_get_hosting_nameservers($domain ?? $this->domain());
		}

		/**
		 * Login domain
		 *
		 * @return string
		 */
		public function domain(): string
		{
			return $this->domain;
		}

		public function getAssignedNameservers(string $domain): ?array
		{
			$domain = $domain ?? $this->domain();

			return mute(function () use ($domain) {
				return $this->dns_get_authns_from_host($domain);
			});
		}

		public function usesNameservers(string $domain): bool
		{
			return $this->dns_domain_uses_nameservers($domain ?? $this->domain());
		}

		/**
		 * DNS is propagated. Resolved IP matches configured IP
		 *
		 * @return bool
		 */
		public function fullyPropagated(): bool
		{
			return $this->dnsActive;
		}

		public function hasFtp(): bool
		{
			return $this->ftp_enabled();
		}

		public function hasSsh(): bool
		{
			return $this->ssh_enabled();
		}

		public function sshPort(): int
		{
			static $port;
			if (isset($port)) {
				return $port;
			}

			$afi = apnscpFunctionInterceptor::factory(Auth::context(null));
			$port = $afi->scope_get('cp.bootstrapper', 'sshd_port') ?? 25;
			if (is_array($port)) {
				$port = array_pop($port);
			}

			return $port;
		}
	}