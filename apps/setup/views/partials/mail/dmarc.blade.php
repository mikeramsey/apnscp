<tr>
	<th>
			<span class="ui-action-copy" data-clipboard-text="{{ MAIL_DEFAULT_DMARC  }}"
			      title="Copy to Clipboard" id="clipboardCopyDmarc"
			      data-toggle="tooltip">
				DMARC
			</span>
	</th>
	<td>
		<code>{{ MAIL_DEFAULT_DMARC }}</code>
	</td>
</tr>