@extends('theme::layout')
@section('content')
	@if (\count($domains = $Page->domains()) > 1)
		<div class="d-flex align-items-center">
		<h4 class="mr-2 mb-0">Domain</h4>
		<form method="GET">
			<select name="domain" class="custom-select" id="domain">
				@foreach ($domains as $domain)
					<option @if ($Page->activeDomain() === $domain) SELECTED @endif
						value="{{ $domain }}">{{ $domain }}</option>
				@endforeach
			</select>
		</form>
		</div>
		<hr/>
	@endif
	@includeWhen(!$auth->fullyPropagated(), 'partials.auth.propagation-warning')
	@include('partials.cp')
	@includeWhen($auth->hasDns(), 'partials.dns', ['domain' => $auth->domain()])
	@includeWhen($auth->hasFtp(), 'partials.ftp')
	@includeWhen($auth->hasMail(), 'partials.mail')
	@includeWhen($auth->hasSsh(), 'partials.ssh')
@endsection
