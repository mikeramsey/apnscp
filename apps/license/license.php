<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace apps\license;

	use Opcenter\License;

	class Page extends \Page_Container
	{
		public function _render()
		{
			if (!License::get()->installed()) {
				\Page_Renderer::hide_all();
				echo 'No license present. See ' .
					"<a href='https://docs.apiscp.com/LICENSE#restoring-license-from-command-line'>ApisCP docs: Restoring license from command-line</a>.";
				exit;
			}
			$this->add_javascript('license.js');

			$this->view()->share([
				'license' => License::get(),
			]);
		}

		public function on_postback($params)
		{
			if (isset($params['download'])) {
				\HTML_Kit::makeDownloadable(function() {
					return (string)License::get();
				}, 'license.pem');
			} else if (isset($params['renew'])) {
				if (!$this->admin_renew_license()) {
					return false;
				}

				return info('Licenses will automatically renew 10 days before expiration if everything is OK');
			}
		}
	}