<?php declare(strict_types=1);
define('INCLUDE_PATH', realpath(dirname(__DIR__)));
define('SHIM', true);
include(INCLUDE_PATH . '/lib/apnscpcore.php');
use \Auth as apnscpAuth;
apnscpSession::init();
register_shutdown_function(function () {
        session_destroy();
});
$auth = apnscpAuth::import('CLI');
$auth->handle();

use Composer\XdebugHandler\XdebugHandler;
use PHPStan\Command\AnalyseCommand;

gc_disable(); // performance boost

$autoloaderInWorkingDirectory = getcwd() . '/vendor/autoload.php';
if (is_file($autoloaderInWorkingDirectory)) {
        require_once $autoloaderInWorkingDirectory;
}

if (!class_exists('PHPStan\Command\AnalyseCommand', true)) {
        $composerAutoloadFile = __DIR__ . '/../vendor/autoload.php';
        if (!is_file($composerAutoloadFile)) {
                $composerAutoloadFile = __DIR__ . '/../../../autoload.php';
        }

        require_once $composerAutoloadFile;
}

$xdebug = new XdebugHandler('phpstan', '--ansi');
$xdebug->check();
unset($xdebug);

$version = 'Version unknown';
try {
        $version = \Jean85\PrettyVersions::getVersion('phpstan/phpstan')->getPrettyVersion();
} catch (\OutOfBoundsException $e) {

}

$application = new \Symfony\Component\Console\Application(
        'PHPStan - PHP Static Analysis Tool',
        $version
);
$application->add(new AnalyseCommand());
$application->run();
