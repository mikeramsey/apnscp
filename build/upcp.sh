#!/bin/bash
# shellcheck disable=SC1091
test -f /etc/sysconfig/apnscp && . /etc/sysconfig/apnscp

OPTS="$(getopt -o ablnsw --long flare,skip-code,bootstrap,list,auto,no-migrate,reset,wait -n 'upcp' -- "$@")"
if test $? -ne 0; then
	exit 1
fi
eval set -- "$OPTS"

APNSCP_ROOT="${APNSCP_ROOT:-"$(realpath "$(dirname "$(realpath "$0")")"/../)"}"

# Update code before running other tasks
SKIP_CODE=false
# Run Bootstrapper automatically
BOOTSTRAP=false
# Perform migration automatically
MIGRATE=true
# Panel upgrade policy. Override in /etc/sysconfig/apnscp.
# Allow override via UPDATE_POLICY environment variable
UPDATE_POLICY=${UPDATE_POLICY:-${APNSCP_UPDATE_POLICY-edge}}
# Enable Bootstrapper singleton check
BSCHECK=${BSCHECK:-true}
# Username to run as update
APNSCP_UPDATE_USER="${APNSCP_UPDATE_USER:-${APNSCP_SYSTEM_USER:-apnscp}}"

# Log update effort. Enabled on unassisted updates
LOG_UPDATE=${LOG_UPDATE:-$([[ $# -eq 1 ]] && ! test -t 1 && echo true)}

# Log file for update, by default use failure tracking log
LOGFILE="${LOGFILE:-${APNSCP_ROOT}/storage/.upcp.failure}"

COMPOSERLOCK="$APNSCP_ROOT/composer.lock"
# Upstream name
UPSTREAM="${ORIGIN:-origin}"
# Branch to pull from for non-tagged releases
BRANCH="${BRANCH:-}"
# FLARE check URL
APNSCP_FLARE_URL="${APNSCP_FLARE_URL:-https://flare.apnscp.com}"
# edge-major lockfile
EDGE_LOCK="$APNSCP_ROOT/storage/.edge.lock"

# Doesn't pollute /var/log/secure using PAM
SUIDWRAPPER="/usr/bin/setpriv --nnp --clear-groups --reuid $(id -u "$APNSCP_UPDATE_USER") --regid $(id -g "$APNSCP_UPDATE_USER")"

# shellcheck disable=SC2139
git() {
	if test -z "$SSH_AGENT_PID" || ! ps -p "$SSH_AGENT_PID" >/dev/null; then
		mkdir "${APNSCP_ROOT}/storage/tmp/agent-root"
		chown root:"${APNSCP_UPDATE_USER}" "${APNSCP_ROOT}/storage/tmp/agent-root"
		# Disallow directory enumeration for APNSCP_UPDATE_USER
		chmod 0730 "${APNSCP_ROOT}/storage/tmp/agent-root"
		# shellcheck disable=SC2086
		eval "$(env TMPDIR="${APNSCP_ROOT}/storage/tmp/agent-root" $SUIDWRAPPER ssh-agent -s | grep -v '^echo')"
		export SSH_AUTH_SOCK
		# export root in case needed for git + ssh
		ssh-add 2>/dev/null

	fi
	$SUIDWRAPPER env HOME="${APNSCP_ROOT}" git "$@"
}

trap cleanup 0

# Dearest bash, why must we mix exit codes and booleans
as_boolean() {
	case "${1,,}" in
	false | 0 | "")
		echo false
		return 1
		;;
	*)
		echo true
		return 0
		;;
	esac
}

pushd "$APNSCP_ROOT" >/dev/null || exit

cleanup_agent() {
	[[ -n "$SSH_AGENT_PID" ]] && kill "$SSH_AGENT_PID"
	rm -rf "${APNSCP_ROOT}/storage/tmp/agent-root/"
}
# Post-update cleanup
cleanup() {
	cleanup_agent

	[[ ! -f "$LOGFILE" ]] && exit 0
	/usr/bin/apnscp_php "${APNSCP_ROOT}/bin/cmd" misc:notify-update-failure >/dev/null 2>&1 || true
	exit 1
}

if as_boolean "$LOG_UPDATE" >/dev/null; then
	exec &> >(/bin/tee -a "$LOGFILE")
fi

# Check if wrapped git is possible
git --version >/dev/null
STATUS=$?
# Something's really bad, abort wrapper usage
[[ $STATUS -ne 0 ]] && echo "ERR: git wrapper defective - disabling usage" && unset -f git

# Determine if emergency update pushed
# $? is 0 if FLARE update received
# $? is 1 if no FLARE update received
flare_check() {
	# Remove cleanup trap
	trap cleanup_agent 0
	local REPOTS FLARETS FLAREMARKER
	declare -i REPOTS FLARETS

	FLAREMARKER="${APNSCP_ROOT}/.git/.flare-check"
	REPOTS=999999999999999999
	if test -f "$FLAREMARKER"; then
		# 15 minute buffer just in case
		REPOTS="$(stat -c "%Y" "${FLAREMARKER}")"
	fi
	touch "${FLAREMARKER}"
	FLARETS="$(curl -s --cacert "${APNSCP_ROOT}/resources/apnscp.ca" \
		--cert "${APNSCP_ROOT}/config/license.pem" "${APNSCP_FLARE_URL}")"

	test "$REPOTS" -le "$FLARETS"
	exit $?
}

bootstrapper_running() {
	# Bypass Bootstrapper concurrency check for simple update
	if ! as_boolean "$BSCHECK" >/dev/null; then
		return 1
	fi
	# check if Bootstrapper running, restarting apnscp will
	local PLAYBOOK_LOCK PLAYBOOK_PID PROCNAME
	PLAYBOOK_LOCK="${APNSCP_ROOT}/storage/run/.ansible.lock"
	if [[ ! -f "$PLAYBOOK_LOCK" ]]; then
		return 1
	fi
	PLAYBOOK_PID="$(cat "${PLAYBOOK_LOCK}")"
	if [[ ! -d "/proc/${PLAYBOOK_PID}" ]]; then
		return 1
	fi
	PROCNAME="$(cat /proc/"${PLAYBOOK_PID}"/comm)" || true
	[[ "$PROCNAME" == ansible-* ]] && echo "$PLAYBOOK_PID"
	return $?
}

vercomp() {
	if [[ "$1" == "$2" ]]; then
		return 0
	fi

	IFS=. read -r -a ver1 <<<"$1"
	IFS=. read -r -a ver2 <<<"$2"
	# fill empty fields in ver1 with zeros
	for ((i = ${#ver1[@]}; i < ${#ver2[@]}; i++)); do
		ver1[i]=0
	done
	for ((i = 0; i < ${#ver1[@]}; i++)); do
		if [[ -z ${ver2[i]} ]]; then
			# fill empty fields in ver2 with zeros
			ver2[i]=0
		fi
		if ((10#${ver1[i]} > 10#${ver2[i]})); then
			return 1
		fi
		if ((10#${ver1[i]} < 10#${ver2[i]})); then
			return 2
		fi
	done
	return 0
}

fetch_releases() {
	if "$SKIP_CODE"; then
		return 1
	fi

	# Calc current tag
	# Version compare
	git fetch --tags -f
}

do_upgrade() {
	if "$SKIP_CODE"; then
		return 1
	fi
	if test "$UPDATE_POLICY" == "edge"; then
		return 0
	elif test "$UPDATE_POLICY" != "all" -a \
		"$UPDATE_POLICY" != "major" -a \
		"$UPDATE_POLICY" != "minor"; then
		return 1
	fi

	mapfile -t TAGS < <(git for-each-ref --sort=taggerdate --format '%(tag)' refs/tags | grep '^v')
	MYTAG="$(git describe)"

	if [[ "$MYTAG" =~ -.*$ ]]; then
		# Moving from edge to tagged releases cycles to first minor release
		# Remove commit meta, e.g. "<version>-8-gcb0f323", to avoid this long trek back
		echo "${MYTAG%%-*}"
		return 0
	fi

	if ! test "$MYTAG" -a "${MYTAG:0:1}" == "v"; then
		# upcp can pull panel off a custom tag, e.g. "benchmark"
		# @TODO use git merge-base (fallback timestamp of lservicelib.pht)
		# to determine commit ancestor to avoid replaying all tags
		MYTAG=${TAGS[0]}
	fi

	MYTAGCOMP="${MYTAG%%.*}"
	if test "$UPDATE_POLICY" == "minor"; then
		MYTAGCOMP="$(expr "$MYTAG" : '^\(v[1-9][0-9]*\.[0-9][0-9]*\)')"
	fi

	for TAG in "${TAGS[@]}"; do
		if test "$UPDATE_POLICY" == "major" && test "${TAG%%.*}" != "$MYTAGCOMP"; then
			continue
		elif test "$UPDATE_POLICY" == "minor" && test "$(expr "$TAG" : '^\(v[1-9][0-9]*\.[0-9][0-9]*\)')" != "$MYTAGCOMP"; then
			continue
		fi
		vercomp "${TAG:1}" "${MYTAG:1}"
		if test $? -eq 1; then
			break
		fi
	done
	echo "$TAG"
	test "${TAG}" != "${MYTAG}"
}

# Commit has met requisite version
met_version() {
	# Version locked from
	# must be formatted as vx.y.z
	local WHENCE="$1"
	# Current tag/commit
	local MYTAG
	MYTAG="$(git describe 2>/dev/null)"
	if test "$MYTAG" == ""; then
		return 1
	fi

	if ! test "${WHENCE:0:1}" == "v" || ! test "${MYTAG:0:1}" == "v"; then
		return 1
	fi

	MYTAG="${MYTAG%%-*}"
	vercomp "${WHENCE:1}" "${MYTAG:1}"
	RES=$?
	test "$RES" -eq 2
}

cur_branch() {
	local BRANCH
	BRANCH="$(git rev-parse --abbrev-ref HEAD)"
	# update policy is major/minor, uses version tag
	test "$BRANCH" == "HEAD" && BRANCH="master"
	echo "$BRANCH"
}

cur_commit() {
	git rev-parse HEAD
}

while true; do
	case "$1" in
	--flare)
		flare_check
		exit 1
		;;
	-n | --no-migrate)
		MIGRATE=false
		shift
		;;
	-b | --bootstrap)
		BOOTSTRAP=true
		shift
		;;
	-s | --skip-code)
		SKIP_CODE=true
		MIGRATE=false
		shift
		;;
        -l | --list)
		grep -E '^\s+- [a-z][^/]+/' "${APNSCP_ROOT}/resources/playbooks/bootstrap.yml" | cut -d- --complement -f1 | tr -d ' ' | sort
		exit 0
		;;
	-a | --auto)
		BOOTSTRAP=auto
		shift
		;;
	-w | --wait)
		! bootstrapper_running && exit 1
		while : ; do
			sleep 1
			! bootstrapper_running && exit 0
		done
		;;
	--reset)
		BRANCH=$(cur_branch)
		git fetch && git submodule foreach git fetch
		git reset --hard "$UPSTREAM"/"$BRANCH" && git submodule foreach git reset --hard "$UPSTREAM"/"$BRANCH"
		RET=$?
		find . -user "$(whoami)" -print -exec chown "${APNSCP_UPDATE_USER}:${APNSCP_UPDATE_USER}" '{}' +
		exit $RET
		break
		;;
	--)
		shift
		break
		;;
	*) break ;;
	esac
done

BOOTSTRAPPER_PID="$(bootstrapper_running)"
RET=$?
if [[ $RET -eq 0 ]]; then
	echo "Bootstrapper running with PID ${BOOTSTRAPPER_PID}. upcp will not run until this completes."
	exit 1
fi

fetch_releases
TAG="$(do_upgrade)"
TRUTHY=$?

# Update loop
# Always process updates in case a migration gets amended at a later release
LOOPOK=0

# Incrementally run upgrades if behind
while test "$TRUTHY" -eq 0; do
	OLDHASH="$(cur_commit)"
	COMPOSERTS=$(stat -c "%Y" "$COMPOSERLOCK" 2>/dev/null)
	if test "${UPDATE_POLICY}" == "edge"; then
		if test -z "$BRANCH"; then
			BRANCH="$(cur_branch)"
		fi
		git pull --recurse-submodules --no-edit "$UPSTREAM" "$BRANCH"
		RET=$?

		# Revert to major if limit-edge set
		if test -f "$EDGE_LOCK" && met_version "$(cat "$EDGE_LOCK")"; then
			/usr/bin/apnscp_php bin/cmd scope:set cp.update-policy major 2>&1 || true
			rm -f "$EDGE_LOCK"
		fi
	else
		echo "Updating to ${TAG}"
		git checkout "${TAG}"
		RET=$?
	fi

	if test $RET -ne 0; then
		echo "upcp halted due to errors"
		exit 1
	fi

	git submodule update --init --recursive --checkout .
	if test -d lib/modules/surrogates; then
		pushd lib/modules/surrogates || exit 1
		[[ -d .git ]] && git pull
		popd || exit 1
	fi
	find . -user "$(whoami)" -print -exec chown "${APNSCP_UPDATE_USER}:${APNSCP_UPDATE_USER}" '{}' +

	if test "x$COMPOSERTS" != "x$(stat -c "%Y" "$COMPOSERLOCK")"; then
		COMPOSER_FLAGS=""
		[[ ! -t 1 ]] && COMPOSER_FLAGS="--no-progress"
		chown "$APNSCP_UPDATE_USER" "$COMPOSERLOCK"
		$SUIDWRAPPER env COMPOSER_HOME="$APNSCP_ROOT/storage/.composer" /usr/bin/apnscp_php /usr/bin/composer $COMPOSER_FLAGS install
		rm -f "$APNSCP_ROOT"/storage/cache/{services,packages}.php
	fi

	/usr/bin/apnscp_php bin/cmd misc_flush_cp_version >/dev/null 2>&1 || true
	/usr/bin/apnscp_php "${APNSCP_ROOT}/artisan" clear-compiled
	systemctl restart apiscp

	if "$MIGRATE"; then
		pushd "${APNSCP_ROOT}" >/dev/null && ./artisan migrate --force
		RET=$?
		LOOPOK=$( ([[ $RET -eq 0 ]] && [[ $LOOPOK -eq 0 ]] && echo 0) || echo 1)
	fi

	# edge updates will always be mainline
	if test "$UPDATE_POLICY" == "edge"; then
		break
	elif test "$OLDHASH" == "$(cur_commit)"; then
		# Sanity check to ensure it's updated
		echo "Upgrade requested, but old = new ($OLDHASH)"
		break
	fi

	TAG="$(do_upgrade)"
	TRUTHY=$?
done

[[ $LOOPOK -ne 0 ]] && exit 1

if test "$BOOTSTRAP" == "auto"; then
	BOOTSTRAP=false
	git log --name-only --pretty=oneline --full-index HEAD~..HEAD |
		grep -v "resources/playbooks/migrations" | grep -q "resources/playbooks"
	if test $? -eq 0; then
		BOOTSTRAP=true
	fi
fi

RET=0

if "$BOOTSTRAP"; then
	pushd "${APNSCP_ROOT}"/resources/playbooks >/dev/null || exit 1
	BSTAGS=""
	if test "$#" -gt 0; then
		BSTAGS=$(printf ",%s" "${@}")
		BSTAGS="--tags=${BSTAGS:1}"
	fi

	# shellcheck disable=SC2086
	env ANSIBLE_LOG_PATH="${APNSCP_ROOT}/storage/logs/bootstrapper.log" ANSIBLE_STDOUT_CALLBACK=actionable ansible-playbook -c local bootstrap.yml $BSTAGS ${BSARGS:-}
	RET=$?
	popd >/dev/null || exit 1
fi

[[ $RET -eq 0 ]] && as_boolean "$LOG_UPDATE" >/dev/null && rm -f "$LOGFILE"

popd >/dev/null || exit 0
