<?php
	define('IS_DAV',1);
    if (!defined('INCLUDE_PATH')) {
    	define('INCLUDE_PATH', realpath(__DIR__ . '/../../'));
    }
	include(INCLUDE_PATH . '/lib/apnscpcore.php');
    if (!DAV_ENABLED) {
    	header('File Not Found', true, 404);
    	exit();
    }
    Error_Reporter::set_verbose(0);
    \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
	\Dav\Server::handle();