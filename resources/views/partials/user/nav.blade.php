<nav id="ui-nav-container"
     class="navbar-default navbar-expand-md navbar-toggleable navbar-collapse col-md-3 col-12 ui-flyout @if (\Preferences::get(Page_Renderer::THEME_SWAP_BUTTONS)) ui-flyout-right @endif"
     role="navigation" @if (\Preferences::get(\Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE)) aria-expanded="false" @endif>
	<div class="" id="ui-nav" role="tablist" aria-multiselectable="true">
		<div class="d-flex py-0">
			<input type="search" autocomplete="off" id="ui-search" accesskey="s" class="px-0 ui-search form-control"
			       placeholder="Search" data-title="Use {{ implode(' + ', \HTML_Kit::accesskey('s')) }} for quick access" data-placement="{{ \Preferences::get(\Page_Renderer::THEME_SWAP_BUTTONS) ? 'left' : 'right' }}"/>
		</div>
		@php $Page->printMenu(); @endphp
		@if (UCard::get()->hasPrivilege('site') && \cmd("crm_configured"))
			<a class="ui-menu-category ui-menu-category-feedback ui-menu-link" href="#"
			   id="ui-menu-category-feedback">Feedback</a>
		@endif
		<a class="ui-menu-category ui-menu-category-logout ui-menu-link" href="/logout"
		   id="ui-menu-category-logout">Logout</a>
	</div>
</nav>