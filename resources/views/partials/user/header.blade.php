<div id="ui-heading" class="container-fluid">
	<!-- head -->
	<header id="ui-header" class="row">
		<div class="col-12 d-flex align-items-center">
			@if (Page_Renderer::do_nav())
				<button class="navbar-toggler hidden-md-up collapsed mr-auto btn my-auto" type="button" id="ui-menu-button"
				        data-toggle="collapse" data-target="#ui-nav-container" aria-controls="ui-nav-container"
				        aria-expanded="false" aria-label="Toggle navigation">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="sr-only">Toggle navigation</span>
				</button>

				<button class="collapsed navbar-toggler hidden-sm-down btn my-auto"
				        aria-expanded="{{ \Preferences::get(\Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE) ? 'false' : 'true' }}"
				        id="ui-side-menu-toggle"
				        type="button" data-target="#ui-nav-container" aria-controls="ui-nav-container"
				        aria-label="Toggle navigation">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="sr-only">Toggle navigation</span>
				</button>


			@endif

			<h1 class="mr-auto mb-0 hidden-sm-down" id="logo">@includeIf('theme::partials.user.logo')</h1>

			<div id="ui-account-gauges" class="hidden-down d-flex align-self-center position-relative">
				@includeWhen(floor($storage['total']), 'theme::partials.shared.gauge', [
					'gaugeTitle' => _('Storage'),
					'gaugeClass' => $Page->getGaugeClass($storage, 'storage'),
					'gaugeId'    => 'ui-storage-cluster',
					'fill'       => $storage['used']/1024,
					'max'        => $storage['total']/1024
				])

				@includeWhen(UCard::get()->hasPrivilege('site') && \cmd('bandwidth_enabled'), 'theme::partials.shared.gauge', [
					'gaugeTitle' => _('Bandwidth'),
					'gaugeClass' => $Page->getGaugeClass($bw, 'bandwidth'),
					'gaugeId'    => 'ui-bandwidth-cluster',
					'fill'       => $bw['used'] ?? 0,
					'max'        => $bw['total'] ?? 0,
					'unit'       => 'GB'
				])
				<a class="ui-refresh" data-tooltip="refresh gauges" id="ui-gauge-refresh"></a>
			</div>
			@includeWhen(UCard::get()->hasPrivilege('site', 'admin'), 'theme::partials.user.job-indicator')
			<div id="ui-notification-indicator-container" class="">
				@include('theme::partials.user.notification')
			</div>
			@if (is_debug())
				<div class="d-flex align-content-center mr-4">
					<i class="ui-debug-indicator" title="In debug mode"></i>
				</div>
			@endif
			<div id="ui-account-ctrl" class="">
				@include('theme::partials.user.settings-dropdown')
			</div>
		</div>
	</header>
	<!-- body -->
</div>