@php
	$status = $Page->get_pb_icon();
	$messages = $Page->getMessages();
	\Error_Reporter::sort($messages);
@endphp
<div class="row ">
	<div class="col-12">
		<div id="ui-postback"
		     class="hidden-print m-t-1 ui-postback clearfix alert alert-dismissible {{ $status . '-parent' }} @if ($Page->pb_succeeded()) ui-postback-success @else ui-postback-error @endif">
			<button type="button" class="close mr-1 text-right" data-dismiss="alert"
			        aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div id="ui-postback-result">
				<p id="ui-postback-response" class="ui-postback-response {{ $status }}">
					<i class="fa ui-status-icon"></i>
					{{ $Page->getStatusMessage() }}
				</p>
				@if ($messages)
					<div id="ui-postback-details" class="">
						<ul class="ui-postback-msg list-unstyled mb-0">
							@php
								$err_hidden = sizeof($messages) - 2;
							@endphp
							@foreach ($messages as $msg)
								@if ($loop->index === 2)
									<li class="ui-postback-nonstatus text-nowrap">
										<a href="#ui-postback-extended" data-toggle="collapse" aria-expanded="false"
										   data-target="#ui-postback-extended"
										   aria-controls="ui-postback-extended" id="ui-postback-more">{{ $err_hidden }}
											more {{ \Illuminate\Support\Str::plural('message', $err_hidden) }}</a>
										<a href="#" class="ui-action-expand ui-action col-1 float-right text-right"
										   id="ui-expand-all-postback" data-toggle="tooltip" title="Show all postback"
										   aria-label="Expand"></a>
									</li>
									<li id="ui-postback-extended"
									    class="collapse ui-postback-info-ext ui-postback-nonstatus text-nowrap">
										<ul class="list-unstyled text-nowrap">
											@endif
											<li title="{{ \Error_Reporter::error_type($msg['severity']) }}"
											    class="fa-extend ui-postback-msg ui-postback-msg-{{ \Error_Reporter::error_type($msg['severity']) }}">{{ trim($msg['message']) }}</li>
											@endforeach
											@if ($err_hidden >= 0)
										</ul>
									</li>
								@endif
						</ul>
					</div>
					@php $Page->purgeMessages(); @endphp
				@endif
			</div>
		</div>
	</div>
</div>