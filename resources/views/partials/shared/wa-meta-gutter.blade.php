@if ($pane->getMeta('failed'))
	<i class="text-danger fa fa-exclamation" data-toggle="tooltip"
	   data-title="Last automatic update failed"></i>
@endif

@if ($pane->getApplicationType())
	<span data-toggle="tooltip" class="app d-inline-block" title="{{ $pane->getName() }}" data-title="{{ $pane->getApplicationType() }}" style="max-height: 1em;">
		@includeFirst(['@webapp(' . $pane->getApplicationType(). ')::icon-sm', 'theme::partials.shared.wa-type-indicator'])
	</span>
@endif

<span class="version">{{ $pane->getMeta('version') }}</span>

@if ($pane->getApplicationType() && $pane->getMeta('options.autoupdate', true))
	<i class="fa fa-arrow-circle-up" data-toggle="tooltip" data-title="Auto-updates enabled"></i>
@endif

@if ($pane->getManifest()->exists())
	<i class="fa fa-flask" data-toggle="tooltip" data-title="Uses Manifest"></i>
@endif

@if ($pane->hasGit())
	<i class="fa fa-camera" data-toggle="tooltip" data-title="Snapshots enabled"></i>
@endif
