<div style="text-align: center;">
	<br/>
	All the best!
	<br/><br/>
	<a href="{{ \Auth_Redirect::getPreferredUri() }}">{{ PANEL_BRAND }} Login</a> &bull;
	<a href="mailto:{{\Crm_Module::FROM_ADDRESS}}">{{\Crm_Module::FROM_ADDRESS}}</a> &bull;
	<a href="{{MISC_KB_BASE}}">Knowledge Base</a>
	<br/>
</div>