@component('email.login-btn')
@endcomponent
{{-- Subcopy --}}
@isset($subcopy)
	@slot('subcopy')
		@component('mail::subcopy')
			{{ $subcopy }}
		@endcomponent
	@endslot
@endisset
{{-- Footer --}}
@slot('footer')
	@component('mail::footer')
		© {{ date('Y') }} Apis Networks. All Rights Reserved.
	@endcomponent
@endslot