# see "man logrotate" for details

# rotate logs daily
daily

# keep 4 days worth of backlogs
rotate 4

# create new (empty) log files after rotating old ones
create

# compress logs to save space
compress

# RPM packages drop log rotation information into this directory
include /etc/logrotate.d

