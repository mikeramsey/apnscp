# Templated from {{ $templatePath }}
[Unit]
Description=PHP-FPM pool activation {!! $config->getGroup() !!} - {!! $config->getName() !!}
@if (version_compare(os_version(), '8', '<'))
DefaultDependencies=no
Requires=sysinit.target
@endif
PropagatesReloadTo={!! $config->getServiceName() !!}
BindsTo=@if ($config->getGroup()) {!! $config->getServiceGroupName() !!} @else php-fpm.service @endif

Before={!! $config->getServiceName() !!}
After=@if ($config->getGroup()) {!! $config->getServiceGroupName() !!} @else php-fpm.service @endif

[Socket]
ListenStream={{ $config->getSocketPath() }}
SocketUser={{ \Web_Module::WEB_USERNAME }}
SocketGroup={{  $config->getSysGroup() }}
SocketMode=0660
RemoveOnStop=yes
DirectoryMode=1111

[Install]
WantedBy=@if ($config->getGroup()) {!! $config->getServiceGroupName() !!} @endif sockets.target
