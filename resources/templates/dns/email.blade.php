{{--
	All records must not contain any indention. Validate the template with:
	cpcmd dns:validate-template TEMPLATE_NAME

	Note:
		- dns:validate-template respects provider-specific RR capabilities.
		- host records must include trailing period (foo.bar.com.)
		- IN class is required, but HS and CH may also be used
		- \Regex::DNS_AXFR_REC_DOMAIN is used for validation
		- $ips refers to mail server IPs
--}}
{!! ltrim(implode('.', [$subdomain, $zone]), '.') !!}. {!! $ttl !!} IN MX 10 mail.{{ $zone }}.
{!! ltrim(implode('.', [$subdomain, $zone]), '.') !!}. {!! $ttl !!} IN TXT "{{ MAIL_DEFAULT_SPF }}"
@foreach($ips as $ip)
@php $rr = false === strpos($ip, ':') ? 'A' : 'AAAA'; @endphp
@foreach(['mail','horde','roundcube'] as $mailsub)
{!! ltrim(implode('.', [$mailsub, $zone]), '.') !!}. {!! $ttl !!} IN {!! $rr !!} {!! $ip !!}
@endforeach
@endforeach
@if (isset($dkim) && $record = $dkim->dkimRecord())
{!! $dkim->selectorRecord() . '.' . ltrim(implode('.', [$subdomain, $zone]), '.') !!}. {!! $ttl !!} IN TXT "{!! $record !!}"
@endif
@if (!empty(MAIL_DEFAULT_DMARC))
{!! '_dmarc.' . ltrim(implode('.', [$subdomain, $zone]), '.') !!}. {!! $ttl !!} IN TXT "{!! MAIL_DEFAULT_DMARC !!}"
@endif