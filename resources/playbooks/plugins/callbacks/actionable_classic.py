# actionable callback, filtered "included" statements
#
# apnscp Proprietary License
# (c) 2019 Matt Saladna <matt@apisnetworks.com>

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.plugins.callback.default import CallbackModule as CallbackModule_default

class CallbackModule(CallbackModule_default):
	
	CALLBACK_NAME = 'actionable_classic'

	def set_options(self, task_keys=None, var_options=None, direct=None):
		super(CallbackModule, self).set_options(task_keys=task_keys, var_options=var_options, direct=direct)
		self.display_ok_hosts = False
		self.display_skipped_hosts = False

	def v2_runner_on_start(self, host, task):
		pass

	def v2_playbook_on_include(self, included_file):
		pass
