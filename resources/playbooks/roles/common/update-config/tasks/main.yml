# Nothing for now
---
- name: Improve DNS resolution quality
  block:
    - name: Find matching files
      find:
        paths: '/etc/sysconfig/network-scripts/'
        patterns: 'ifcfg-(?:{{ ansible_interfaces | map("regex_escape") | join("|") }})$'
        use_regex: yes
      register: r
    - name: Set USEDNS=no in interface configuration stubs
      lineinfile:
        path: "{{ item.path }}"
        regexp: '^\s*USEDNS\s*='
        line: 'USEDNS=no'
      loop_control:
        label: "Setting USEDNS=no in {{ item.path }}"
      with_items: "{{ r.files }}"
    - name: Check if NetworkManager present
      stat: path=/etc/NetworkManager/NetworkManager.conf
      register: r
    - name: Disable NetworkManager DNS management
      ini_file:
        path: /etc/NetworkManager/NetworkManager.conf
        section: main
        option: dns
        value: "none"
      when: r.stat.exists

      # Set entries in /etc/resolv.conf
    - set_fact: update_resolv="{{ ansible_distribution_major_version == '7' }}"
      when: canonical_resolv == 'auto'
    - set_fact: update_resolv="{{ canonical_resolv | bool }}"
      when: canonical_resolv != 'auto'
    - name: Configure systemd-resolved
      include_tasks: setup-resolved.yml
      when: ansible_distribution_major_version != '7'

    - name: Remove conflicting resolvers
      lineinfile:
        path: /etc/resolv.conf
        regexp: '^\s*nameserver\s+(?!{{ dns_robust_nameservers | map("regex_escape") | join("|") }}).*$'
        state: absent
      when: update_resolv
    - block:
      - name: Append robust resolvers to /etc/resolv.conf
        lineinfile:
          path: /etc/resolv.conf
          line: "nameserver {{ item }}"
          state: present
        with_items: "{{ dns_robust_nameservers }}"
      rescue:
        - name: Fix blown /etc/resolv.conf
          copy:
            dest: /etc/resolv.conf
            content: |-
              search .
              {% for nameserver in dns_robust_nameservers %}
              nameserver {{ nameserver }}
              {% endfor %}
      when: update_resolv
  when: use_robust_dns | bool

- name: Apply DNS configuration changes
  meta: flush_handlers

# - Trivial to circumvent
# - Breaks unassisted installs
# - Zero upsides
# https://bugzilla.redhat.com/show_bug.cgi?id=1020147
- name: Prune requiretty from /etc/sudoers
  lineinfile:
    path: /etc/sudoers
    regexp: '^\s*Defaults\s+requiretty\s*$'
    state: absent
# packages/install takes a very long time to run, can leave behind orphaned
# installs. Breaks Ansible
- name: CentOS 7 package management
  block:
    - name: "Install yum-utils"
      yum: name=yum-utils state=installed
    - name: "Cleanup incomplete transactions"
      command: /usr/sbin/yum-complete-transaction --cleanup-only --disablerepo=*
      environment:
        LANGUAGE: en_US
      register: o
      changed_when: o.stdout.find("No unfinished transactions left.") == -1
  when: ansible_distribution_major_version == "7"
- name: CentOS 8+ package management
  command: dnf remove -y --duplicates --setopt=protected_packages=
  environment:
     LANGUAGE: en_US
  args:
    warn: False
  register: o
  failed_when: o.rc != 0 and -1 == o.stderr.find("No duplicated packages")
  changed_when: -1 == o.stderr.find("No duplicated packages")
  when: ansible_distribution_major_version != "7"
  retries: "{{ network_max_retries }}"
  until: o is succeeded
# if cloud-init isn't invoked
- name: Enable ApisCP repo
  ini_file:
    path: /etc/yum.repos.d/apnscp.repo
    section: "{{ item }}"
    option: enabled
    value: "1"
    no_extra_spaces: True
  with_items:
    - apnscp
    - apnscp-updates
  when: '"prebuilt-quickstart.yml" in lookup("file", "/proc/self/cmdline")'
