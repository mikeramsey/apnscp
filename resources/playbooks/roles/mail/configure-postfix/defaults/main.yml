---
# INTERNAL
mail_configure_postfix_marker: true
# INTERNAL
postfix_conf_dir: /etc/postfix

# Enable TLSv1.0 and TLSv1.1 connections
postfix_insecure_ssl: "{{ mail_insecure_ssl }}"

# Additional services in master.cf
postfix_custom_master_config: ""

postfix_master_template: "master.cf.j2"

######################################
# Custom Postfix configuration format:
# postfix_custom_config:
#   vmaildrop_destination_rate_delay: 30
#   maximal_backoff_time: 16h
######################################
# Configure in data_center_mode to enable a centralized memcache server
# Server formatted as ip.addr:port, [ip6::addr]:port, or /path/to/unix/domain.sock
postfix_memcache_server:
#
# Smarthost support- relay all outbound mail through
# this nexthop. May be GMail, Mailchannels, etc
#
# host MUST be set and typically follows the form '[some.host.name]'
# omitting "[]" implies an MX check will be done to determine the
# appropriate hostname
#
# This is applied unconditionally to all mail leaving. Use a transport map
# for per-domain filtering
postfix_smarthost_enabled: false

# Credentials to use for smarthost. Use mail.smart-host Scope to set.
postfix_smarthost_credentials:
  host:
  username:
  password:

# INTERNAL
postfix_smarthost_auth_file: "{{ postfix_conf_dir }}/smarthost-auth"

# Always encrypt smarthost support as it almost always is sending credentials. (bool)
postfix_smarthost_tls: "{{ postfix_smarthost_credentials.username | string | length > 0 }}"
# Use old SMTPS protocol.
postfix_smtps_wrapper_mode: "{{ (':465' in (postfix_smarthost_credentials.host | string)) | ternary('yes', 'no')  }}"
# SMTP encryption is useless in non-smarthost environments. Optionally use it when MX reports "STARTTLS" feature when set to "may".
# Encryption is mandatory when relaying credentials with a smarthost
postfix_smtp_tls_security_level: '{{ (postfix_smtps_wrapper_mode|bool or (postfix_smarthost_enabled | bool)) | ternary("encrypt", "may") }}'

# Set default recipient delimiter for subaddressing. +- is default in Postfix 2.11
# but may cause conflict with users that have a hyphen in their name
postfix_recipient_delimiter: "+"

# Default aliases
postfix_aliases:
  site_blackhole: >-
    {{ (spamfilter == 'rspamd') | ternary('learn_spam', '/dev/null') }}
  "{{ apnscp_system_user }}": root

aliases_file: /etc/aliases
access_file: '{{ postfix_conf_dir }}/access'
postfix_data_dir: /var/lib/postfix
# apnscp-specific lookup file for database configuration
postfix_control_file: "{{ postfix_conf_dir }}/mailboxes.cf"
# Allow unauthenticated relaying from 127.0.0.1
# See also StealCat malware
postfix_relay_mynetworks: false
# Add SASL login name to Received: header.
# Note this is shared with the entire world.
postfix_add_sasl_auth_header: false

# Enable deep protocol tests that will require a mail client to reconnect prior to delivery
# An aggregate postscreen memcache database is a requirement with this setting
# or multiple MX records
# See also http://www.postfix.org/POSTSCREEN_README.html#after_220
postfix_use_deep_protocol_tests: false

# Require RFC compliant mailers. Reject UTF-8 without SMTPUTF8 command
# May break things. Use at your own peril.
postfix_strict_envelope: false

postfix_sysuser: postfix
postfix_sysgroup: postfix
postfix_user: postfix
# Override to force a password otherwise randomly assigned
postfix_password:
# Don't touch.
postfix_db: "{{ appliance_db }}"
template_files:
  - mailboxes.cf.j2
  - uids.cf.j2
  - virtual_filter.cf.j2
  - aliases.cf.j2
  - postscreen_access.cidr.j2
  - client_access.j2
  - domains.cf.j2
  - canonical_map_rewrite.cf.j2

postfix_inet_interfaces: "{{ (mail_enabled | bool and not (haproxy_enabled | bool)) | ternary('all','loopback-only') }}"
postfix_mydomain: "{{ ((data_center_mode | bool) or (ansible_nodename | regex_findall('\\.') | length) < 2) | ternary(ansible_nodename, None) }}"
# Same recipient delivery delay. With SpamAssassin, set to a number > 0 to prevent monopolization
# of an inundated mailbox since SA has a very limited number of slots to scan mail.
postfix_local_delivery_delay: "{{ ( (spamfilter == 'spamassassin') and (mail_enabled | bool) )  | ternary('3s', '0') }}"
# PostSRS
postsrsd_sender_map_port: 10001
postsrsd_recipient_map_port: 10002
postsrsd_hash_length: 8

# Intended for cooperation with SpamAssassin when Return-Path is clobbered
postfix_envelope_save_header_name: 'X-Envelope-From'
postfix_envelope_save_header_check: '/([^@]+@.+)/ PREPEND {{ postfix_envelope_save_header_name}}: $1'


# Address verification/postscreen map cache
# When data_center_mode is true, replace the maps
# with an aggregate memcache instance
postfix_local_address_verify_map: "proxy:btree:{{ postfix_data_dir }}/verify_cache"
postfix_local_postscreen_cache_map: "proxy:btree:{{ postfix_data_dir }}/postscreen_cache"

postfix_address_verify_map: "{{ (postfix_memcache_server | default('', true) | length > 0) | ternary('memcache:' + postfix_conf_dir + '/memcache-address_verify.cf', postfix_local_address_verify_map) }}"
postfix_postscreen_cache_map: "{{ (postfix_memcache_server | default('', true) | length  > 0) | ternary('memcache:' + postfix_conf_dir + '/memcache-postscreen_cache.cf', postfix_local_postscreen_cache_map) }}"

# Assign 1 server responsibility of periodically purging Postscreen results
postfix_postscreen_cache_cleanup_interval: "{{ (-1 == postfix_postscreen_cache_map.find('memcache:')) | ternary(86400, 0) }}"

# Remove postscreen from proxymap, errors not permitted when in fact in proxy_write_maps
postfix_memcache_maps:
  - config: "memcache-address_verify.cf"
    params:
      memcache: "{{ (postfix_memcache_server[0] == '/') | ternary('unix', 'inet')}}:{{ postfix_memcache_server }}"
      backup: "{{ postfix_local_address_verify_map }}"
      key_format: "vrfy:%s"
  - config: "memcache-postscreen_cache.cf"
    params:
      memcache: "{{ (postfix_memcache_server[0] == '/') | ternary('unix', 'inet')}}:{{ postfix_memcache_server }}"
      backup: "{{ postfix_local_postscreen_cache_map }}"
      key_format: "ps:%s"

# saslauthd when mail_enabled is false
saslauthd_worker_count: 3
# Additional flags to pass to FLAGS= in /etc/sysconfig/saslauthd
saslauthd_extra_flags: ""
postfix_sasl_cache: /var/lib/postfix/sasl_auth_cache.db

postfix_smtps_port: "{{ haproxy_enabled | ternary(haproxy_smtps_proxied_port, 'smtps') }}"
postfix_smtp_port: "{{ haproxy_enabled | ternary(haproxy_smtp_proxied_port, 'smtp') }}"
postfix_submission_port: "{{ haproxy_enabled | ternary(haproxy_submission_proxied_port, 'submission') }}"
