<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);

	namespace Opcenter;

	use Opcenter\Mail\Contracts\FilterProvider;
	use Opcenter\Mail\Contracts\ListProvider;
	use Opcenter\Mail\Contracts\ServiceDaemon;

	class Mail extends Dns
	{
		use \NamespaceUtilitiesTrait;

		protected static $registeredProviders = [];
		protected static $providers;

		/**
		 * Inbound mail filter provider
		 *
		 * @param string $filter
		 * @return bool
		 */
		public static function filterValid(string $filter): bool
		{
			$cls = self::serviceClass($filter);
			return class_exists($cls) && is_subclass_of($cls, FilterProvider::class);
		}

		/**
		 * Get service class name
		 *
		 * @param string $svc
		 * @return string
		 */
		public static function serviceClass(string $svc): string
		{
			return self::appendNamespace('Mail\\Services\\' . ucwords($svc));
		}

		/**
		 * Get service class
		 *
		 * @param string $svc
		 * @return ServiceDaemon
		 */
		public static function service(string $svc): ServiceDaemon
		{
			$c = static::serviceClass($svc);
			return new $c;
		}

		/**
		 * Mail service exists
		 *
		 * @param string $svc
		 * @return bool
		 */
		public static function serviceExists(string $svc): bool
		{
			return class_exists(static::serviceClass($svc));
		}

		/**
		 * Named list is a valid mailing list provider
		 *
		 * @param string $list
		 * @return bool
		 */
		public static function listValid(string $list): bool
		{
			$cls = self::serviceClass($list);
			return class_exists($cls) && is_subclass_of($cls, ListProvider::class);
		}
	}
