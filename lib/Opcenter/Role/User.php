<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Role;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Create;
	use Opcenter\Contracts\Virtualizable;
	use Opcenter\Process;

	class User implements Virtualizable
	{
		const USER_FILE = '/etc/passwd';
		const MIN_UID = USER_MIN_UID;

		protected $root;

		public function __construct(string $root = '/')
		{
			$this->root = $root;
		}

		/**
		 * Find next available system user that maps to
		 * an underlying site admin
		 *
		 * @return string
		 */
		public static function gen_admin()
		{
			for ($i = 1; $i <= Create::MAX_SITES; $i++) {
				$admin = 'admin' . $i;
				if (posix_getpwnam($admin)) {
					continue;
				}
				$c = static::bindTo('/');
				if (!$c->create($admin, ['nohome' => true])) {
					return error("failed to create site admin `%s' in /etc/passwd", $admin);
				}
				Cardinal::register(Events::FAILURE, static function () use ($admin) {
					\Opcenter\Role\User::delete_sys($admin);
				});

				return $admin;
			}
			fatal("admin user pool exhausted (max number `%d'), check /etc/passwd", Create::MAX_SITES);
		}

		public static function bindTo(string $path = '/')
		{
			$c = new static($path);

			return $c;
		}

		/**
		 * Create a user
		 *
		 * @param string     $user
		 * @param null|array $options additional options
		 * @return bool
		 */
		public function create(string $user, array $options = []): bool
		{
			if ($this->exists($user)) {
				return error("create user failed: user `%s' exists", $user);
			}

			if (!preg_match(\Regex::USERNAME, $user)) {
				return error("create user failed: invalid user `%s'", $user);
			}

			if (!empty($options['passwd'])) {
				$passwd = $options['passwd'];
				if (!\Opcenter\Auth\Password::strong($passwd)) {
					return error("password for user `%s' is not strong", $passwd);
				}
				unset($options['passwd']);
				$options['cpasswd'] = \Opcenter\Auth\Shadow::crypt($passwd);
			}
			$flags = [
				'home'       => 'd',
				'skel'       => 'k',
				'gecos'      => 'c',
				'nohome'     => 'M',
				'cpasswd'    => 'p',
				'shell'      => 's',
				'duplicate'  => 'o',
				'supplement' => 'G',
				'system'     => 'r',
				'gid'        => 'g',
				'uid'        => 'u',
				'nologinit'  => 'l',
				'logindef'   => 'K'
			];
			if (SELINUX) {
				$flags['selinux'] = 'Z';
			}
			$args = [];
			foreach ($options as $opt => $val) {
				if (!isset($flags[$opt])) {
					warn("unrecognized option `%s'", $opt);
					continue;
				}
				$param = '-' . $flags[$opt];
				if (\is_array($val)) {
					$param .= implode(' ' . $param, array_map('escapeshellarg', $val));
				} else {
					if (!\is_bool($val)) {
						// otherwise uid => 0 fails
						$param .= ' ' . escapeshellarg($val === '0' ? '0' : (string)$val);
					}
				}
				$args[] = $param;
			}

			$proc = new \Util_Process_Chroot($this->root);
			$ret = $proc->run('/usr/sbin/useradd %(opts)s %(username)s', [
				'opts'     => join(' ', $args),
				'username' => $user
			]);

			if (!$ret['success']) {
				return error("failed to create user `%s': %s",
					$user,
					$ret['stderr']
				);
			}

			return (bool)$ret['success'];
		}

		public function exists(string $user): bool
		{
			return (bool)$this->getpwnam($user);
		}

		/**
		 * Virtualized getpwnam()
		 *
		 * @param null|string $user dump passwd on null
		 * @return null|array
		 */
		public function getpwnam(?string $user): ?array
		{
			$pwd = array();
			$file = rtrim($this->root, '/') . static::USER_FILE;
			$fp = fopen($file, 'r');
			if (!$fp) {
				error('unable to open /etc/passwd');

				return null;
			}
			$found = false;
			while (false !== ($line = fgets($fp))) {
				$line = explode(':', trim($line));
				if (\count($line) < 6) {
					continue;
				}
				$myuser = $line[0];
				if ($line[0] === $user) {
					$found = true;
				}
				$pwd[$myuser] = array(
					'uid'   => (int)$line[2],
					'gid'   => (int)$line[3],
					'gecos' => $line[4],
					'home'  => $line[5],
					'shell' => $line[6]
				);
			}
			fclose($fp);
			if (!$user) {
				return $pwd;
			} else if (!$found) {
				return null;
			}

			return $pwd[$user];
		}

		/**
		 * Delete system user
		 *
		 * @param string $admin
		 * @return bool
		 */
		public static function delete_sys(string $admin): bool
		{
			$c = static::bindTo('/');

			return $c->delete($admin);
		}

		/**
		 * Delete a user
		 *
		 * @param string $user       username
		 * @param bool   $removeHome remove home directory/mail spool
		 * @return mixed
		 */
		public function delete(string $user, bool $removeHome = false)
		{
			$pwd = $this->getpwnam($user);
			if (!$pwd) {
				return warn("cannot delete user `%s' - does not exist", $user);
			}
			if ($pwd['uid'] >= \User_Module::MIN_UID) {
				Process::killUser($pwd['uid'], $pwd['gid']);
			}
			$proc = new \Util_Process_Chroot($this->root);
			$ret = $proc->run('/usr/bin/env userdel %s %s', $removeHome ? '-r' : null, $user);
			if (!$ret['success']) {
				$reasons = explode("\n", rtrim($ret['stderr']));
				if (\count($reasons) === 1 && false !== strpos($reasons[0], ' mail spool ')) {
					// silence USER mail spool (path) not found warnings
					$ret['success'] = true;
				} else {
					warn("userdel failed: `%s'", $ret['stderr']);
				}
			}

			return $ret['success'];
		}

		/**
		 * Mirror a system user within a virtual environment
		 *
		 * @param string $user
		 * @param array  $options
		 * @return bool
		 */
		public function mirror(string $user, array $options = []): bool
		{
			if ($this->root === '/') {
				return error('cannot mirror user in system root - call bindTo() first');
			}
			if (null === ($pwd = static::bindTo()->getpwnam($user))) {
				return error("cannot mirror user `%s', user unknown to system", $user);
			}
			if ($this->exists($user) && $this->getpwnam($user)['uid'] === posix_getpwnam($user)['uid']) {
				return true;
			}

			return $this->create($user, array_replace($pwd, ['nohome' => true, 'system' => true], $options));
		}

		/**
		 * Change gecos fields of user
		 *
		 * @param string $user
		 * @param array  $attributes
		 * @return bool
		 */
		public function change($user, array $attributes): bool
		{
			$attr2flag = array(
				'gecos'      => '-c',
				'home'       => '-d',
				'username'   => '-l',
				'passwd'     => '-p',
				'pw_expire'  => '-e',
				'pw_disable' => '-e',
				'shell'      => '-s',
				'pw_lock'    => '-L',
				'pw_unlock'  => '-U',
				'move_home'  => '-m'
			);
			$cmd_str = '/usr/sbin/usermod';
			$newuser = null;
			foreach ($attributes as $attr => $attr_val) {
				if (!isset($attr2flag[$attr])) {
					return error('%s: unrecognized attribute', $attr);
				}

				// error checking...
				switch ($attr) {
					case 'gecos':
						if (!$attr_val) {
							$attr_val = '';
						}
						break;
					case 'home':
					case 'pw_lock':
						break;

					case 'username':
						$newuser = $attr_val;
						break;

					case 'pw_unlock':
						break;

					case 'move_home':
						if (!\array_key_exists('username', $attributes)) {
							return error('cannot move home without renaming user');
						}
						$attr_val = null;
						break;

					default:
						if (!$attr_val) {
							return error('%s missing value', $attr);
						}

				}
				$cmd_str .= ' ' . $attr2flag[$attr] . ' ' . ($attr_val ? escapeshellarg($attr_val) : ($attr_val !== null ? "''" : ''));
			}

			$proc = new \Util_Process_Chroot($this->root);
			$ret = $proc->run($cmd_str . ' ' . $user);

			return $ret['success'];
		}

		/**
		 * Request and store a uid
		 *
		 * @param int $siteid
		 * @return int
		 */
		public function captureUid(int $siteid): int
		{
			$db = \Opcenter\Database\PostgreSQL::vendor();
			$query = $db->captureUid($siteid);
			$rs = pg_query($query);
			if (!$rs) {
				fatal('failed to capture uid for user');
			}

			return (int)pg_fetch_object($rs)->uid;
		}

		public function releaseUid(int $uid, int $site_id): bool
		{
			$db = \Opcenter\Database\PostgreSQL::vendor();
			$query = $db->releaseUid($uid, $site_id);
			$rs = pg_query($query);

			return $rs && pg_affected_rows($rs) >= 0;
		}

		/**
		 * Remove all user caches on site
		 *
		 * @todo locate elsewhere
		 * @param \Auth_Info_User $ctx
		 */
		public function flushCache(\Auth_Info_User $ctx): void
		{
			$cache = \Cache_User::spawn($ctx);
			$prefix = $cache->_prefix('');
			foreach ($cache->keys('*') as $key) {
				if (0 === strpos($key, $prefix)) {
					$key = substr($key, \strlen($prefix));
				}
				$cache->del($key);
			}
		}

	}

