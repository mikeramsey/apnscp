<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Common;

	use Opcenter\Service\ServiceValidator;

	class Units extends ServiceValidator
	{
		const DESCRIPTION = 'Supplied value has specified unit';
		const VALUE_RANGE = '[B,KB,MB,GB,TB]';

		public function valid(&$value): bool
		{
			if (!\Formatter::changeBytes(1, 'B', $value)) {
				return error("invalid unit specifier, `%s'", $value);
			}

			return true;
		}

		public function getValidatorRange()
		{
			return ['B', 'KB', 'MB', 'GB', 'TB'];
		}
	}