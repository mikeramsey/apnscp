<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Logs;

	use Opcenter\Provisioning\Logs;
	use Opcenter\Provisioning\Php;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall, ServiceToggle, ServiceReconfiguration
	{
		public const DESCRIPTION = 'Record web server access';

		public function populate(SiteConfiguration $svc): bool
		{
			$this->updateFpmLog($svc);
			Logs::createModuleConfiguration($svc, 'logs.apache-config');
			Logs::createLogrotate($svc);
			Logs::createCronjob($svc);
			// @todo move to Apache or generalized config
			if ($svc->getServiceValue('apache', 'enabled')) {
				Logs::createProfile($svc, 'logs/apache-logrotate', 'httpd');
				if ($svc->getServiceValue('apache', 'jail')) {
					Logs::createProfile($svc, 'logs/fpm-logrotate', 'php-fpm');
				}
			}

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				return $this->depopulate($svc);
			}

			return $this->populate($svc);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}


		public function depopulate(SiteConfiguration $svc): bool
		{
			$this->updateFpmLog($svc);
			Logs::deleteModuleConfiguration($svc->getSite());
			Logs::deleteCronjob($svc);
			if ($svc->getServiceValue('apache', 'enabled')) {
				Logs::deleteProfile($svc, 'httpd');
				if ($svc->getServiceValue('apache', 'jail')) {
					Logs::deleteProfile($svc, 'php-fpm');
				}
			}

			return true;
		}

		public function suspend(SiteConfiguration $svc): bool
		{
			return Logs::hideModuleConfiguration($svc);
		}

		public function activate(SiteConfiguration $svc): bool
		{
			return Logs::unhideModuleConfiguration($svc);
		}

		private function updateFpmLog(SiteConfiguration $svc): void
		{
			if ($this->ctx->serviceValueChanged('apache', 'jail') || !$this->ctx->getServiceValue('apache', 'jail')) {
				return;
			}
			// logs changed, update apache,jail
			// needs better dep tracking
			Php::installFpm($svc);
		}
	}
