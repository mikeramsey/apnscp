<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Apache;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Edit;
	use Opcenter\Filesystem;
	use Opcenter\Migration\Helpers\PathTranslate;
	use Opcenter\Process;
	use Opcenter\Provisioning\Apache;
	use Opcenter\Provisioning\Logs;
	use Opcenter\Provisioning\Php;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\SiteConfiguration;

	class Jail extends \Opcenter\Service\Validators\Common\Enabled implements ServiceReconfiguration, ServiceInstall, ServiceToggle, AlwaysValidate, DefaultNullable
	{
		public function getDescription(): string
		{
			return 'Jail all PHP requests using PHP-FPM';
		}

		public function valid(&$value): bool
		{
			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = (int)$this->getDefault();
			}
			if ($this->ctx['enabled'] && !$value && HTTPD_USE_FPM) {
				// [httpd] => use_fpm implies mod_php isn't present on the server
				warn('[httpd] => use_fpm is enabled. Enabling apache,jail configuration setting as ' .
					'PHP would be inaccessible if apache,jail is disabled');
			}
			if ($value === 1 && !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				warn('cgroup usage is strongly encouraged when using PHP-FPM - enable with cgroup,enabled=1');
			}
			return $value === 0 || $value === 1;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$old && $old === $new) {
				// ignore routines if jail not enabled
				return true;
			} else if ($old === $new) {
				// no change, but let's check if the domain changed
				if ($this->ctx->getOldServiceValue('siteinfo', 'domain') !== $this->ctx->getNewServiceValue('siteinfo',
						'domain'))
				{
					Php::removeFpm($svc);
				}

				// reapply FPM adjustments for now... Once dev settles down remove this
				return $this->populate($svc);
			}

			$docroots = array_flip($svc->getSiteFunctionInterceptor()->web_list_subdomains()) +
				array_flip($svc->getSiteFunctionInterceptor()->web_list_domains());

			if  (!$new) {
				if (HTTPD_FPM_MIGRATION && $old) {
					foreach (array_values($docroots) as $hostname) {
						$svc->getSiteFunctionInterceptor()->php_migrate_directives($hostname, '', 'fpm');
					}
					warn('Paths cannot be reconfigured automatically when switching from jailed to non-jailed');
				}

				return $this->depopulate($svc);
			}


			if (HTTPD_FPM_MIGRATION && !$old) {
				// going from jail=0 to jail=1
				$translator = new PathTranslate();
				// sticky + chown makes this immune to trickery
				$tmp = tempnam($svc->getAuthContext()->domain_fs_path('/tmp'), 'trans');
				$master = $svc->getAuthContext()->domain_fs_path('/tmp/conversion-files.txt');
				if (!file_exists($master)) {
					touch($master);
				}
				chown($master, 0);
				$stat = [$master, $svc->getAuthContext()->user_id, $svc->getAuthContext()->group_id, 0600];
				defer($_, static function () use ($tmp, $stat) {
					file_exists($tmp) && unlink($tmp);
					Filesystem::chogp(...$stat);
				});

				$translator->setLogPath($tmp);

				// prepare paths for translation
				$basepath = $svc->getAuthContext()->domain_fs_path();
				$paths = array_filter(array_map(static function ($docroot) use ($basepath) {
					if (!$docroot) {
						return null;
					}
					$path = $basepath . $docroot;
					$realpath = realpath($path);
					if ($realpath !== false && 0 !== strpos($realpath, $basepath)) {
						fatal('Assertion check failed: %s does not contain %s', $path, $basepath);
					}
					// use realpath() - find in PathTranslate doesn't follow docroot symlinks
					return $realpath;
				}, array_keys($docroots)));

				$translator->translate($svc->getAuthContext()->domain_fs_path() . '/', '/', $paths);

				if (file_put_contents($master, file_get_contents($tmp), FILE_APPEND) === false) {
					warn('Failed to store updated path results');
				}

				foreach (array_values($docroots) as $hostname) {
					$svc->getSiteFunctionInterceptor()->php_migrate_directives($hostname, '', 'isapi');
				}

				$fst = $svc->getAuthContext()->domain_fs_path();
				// restore Auth paths that may have been stripped during conversion
				foreach ($paths as $path) {
					$htaccess = $path . '/.htaccess';
					if (!($resolved = realpath($htaccess)) || 0 !== strpos($resolved, "$fst/")) {
						continue;
					}

					$contents = file_get_contents($htaccess);
					// reapply AuthXXX file references - @todo what else?
					$regex = '/^\s*(Auth[^ ]*?File)\s+(?!' . preg_quote($fst, '/') . ')(\/.*)$/m';
					$result = preg_replace($regex, '\1 ' . $fst . '\2', $contents);
					if ($result !== $contents) {
						$jailedPath = $svc->getSiteFunctionInterceptor()->file_unmake_path($htaccess);
						info('Restored AuthXXX directive paths in %s', $jailedPath);
						$svc->getSiteFunctionInterceptor()->
							file_put_file_contents($jailedPath, $result);
					}
				}
			}

			return $this->populate($svc);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function populate(SiteConfiguration $svc): bool
		{
			if (!$this->ctx['jail']) {
				// counterintuitive, chain this method when apache,enabled=1 but also
				// check to make sure apache,jail is enabled
				return true;
			}

			if ($this->ctx->isEdit() && !$svc->hasValidatorOption('reconfig') &&
				$svc->getOldConfiguration('apache') == $svc->getNewConfiguration('apache') &&
				!array_has($svc->getValidatorOption('runtime'), 'apache.jail'))
			{
				// no-op
				return true;
			}

			Php::installFpm($svc);
			Logs::createProfile($svc, 'logs/fpm-logrotate', 'php-fpm');
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			if (!$this->ctx->getOldServiceValue(null, 'jail')) {
				return true;
			}

			$this->suspend($svc);

			$user = $this->ctx->getOldServiceValue(null, 'webuser');
			Cardinal::register([Edit::HOOK_ID, Events::SUCCESS],
				static function ($event, SiteConfiguration $svc) use ($user) {
					Apache::removeWebUser($svc, $user);
				}
			);

			return Php::removeFpm($svc);
		}

		public function getDefault()
		{
			return (int)HTTPD_USE_FPM;
		}


		public function suspend(SiteConfiguration $svc): bool
		{
			foreach (Process::matchGroup($svc->getServiceValue('siteinfo', 'admin')) as $pid) {
				if (!Process::pidMatches($pid, 'php-fpm')) {
					continue;
				}
				posix_kill($pid, SIGKILL);
			}

			return true;
		}

		public function activate(SiteConfiguration $svc): bool
		{
			return true;
		}
	}
