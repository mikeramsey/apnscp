<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Apache;

	use Opcenter\Http\Apache\Map;
	use Opcenter\Process;
	use Opcenter\Provisioning\Apache;
	use Opcenter\Role\User;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\Service\Validators\Common\Enabled as Base;
	use Opcenter\SiteConfiguration;

	class Enabled extends Base implements ServiceInstall, ServiceReconfiguration, ServiceToggle
	{
		use \FilesystemPathTrait;

		public const DESCRIPTION = 'HTTP usage';

		public function valid(&$value): bool
		{
			if (!$value) {
				$this->ctx['webserver'] = null;
				$this->forceValidation();
			}

			return parent::valid($value);
		}

		public function populate(SiteConfiguration $svc): bool
		{
			Apache::createGroups($svc->getAccountRoot());
			touch($this->domain_info_path(Map::DOMAIN_MAP));
			Apache::populateFilesystem($svc, 'apache');
			Apache::createWebUser($svc, $this->getWebUser());

			return $this->reconfigure('', $this->ctx['enabled'], $svc);
		}

		private function getWebUser(): string
		{
			return $this->ctx['webuser'] ?? \Web_Module::WEB_USERNAME;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$user = $this->getWebUser();

			Apache::removeWebUser($svc, $user);
			$handler = User::bindTo($svc->getAccountRoot());
			if ($handler->exists(\Web_Module::WEB_USERNAME)) {
				$handler->delete(\Web_Module::WEB_USERNAME);
			}
			$map = Map::open(\Web_Module::PROTOCOL_MAP, Map::MODE_WRITE);
			$map->removeValues($svc->getSiteId());
			$map->sync();

			return true;
		}

		public function suspend(SiteConfiguration $svc): bool
		{
			return Apache::suspend($svc);
		}

		public function activate(SiteConfiguration $svc): bool
		{
			return Apache::activate($svc);
		}
	}