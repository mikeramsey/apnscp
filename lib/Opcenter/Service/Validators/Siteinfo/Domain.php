<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Opcenter\Database\PostgreSQL;
	use Opcenter\License;
	use Opcenter\Map;
	use Opcenter\Provisioning\Siteinfo;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\GenericDomainMap;
	use Opcenter\SiteConfiguration;

	class Domain extends GenericDomainMap implements ServiceReconfiguration, ServiceInstall
	{
		use \FilesystemPathTrait;

		const MAP_FILE = 'domainmap';
		const DESCRIPTION = 'Primary domain of the account';

		public function valid(&$value): bool
		{
			$value = strtolower($value);
			if (0 === strncmp($value, 'www.', 4)) {
				$value = substr($value, 4);
			}
			if (!preg_match(\Regex::DOMAIN, $value)) {
				return error("verify conf failed: domain `%s' is not valid", $value);
			}

			if (($id = \Auth::get_site_id_from_domain($value)) && $id !== (int)substr($this->site, 4)) {
				return error("domain `%s' already attached to site id `%d'",
					$value, $id);
			}

			if ($value === SERVER_NAME) {
				return error('Domain name cannot be the same as server name');
			}

			if ($this->ctx->isCreate()) {
				$license = License::get();
				$count = \count(Map::load(Map::home() . '/' . Map::DOMAIN_MAP, 'r-')->fetchAll());
				if ($license->hasDomainCountRestriction() && ++$count > $license->getDomainLimit()) {
					return error('License limit reached for domain count: %(limit)d', ['limit' => $license->getDomainLimit()]);
				}

				if ($license->isDevelopment() && substr($value, -5) !== '.test') {
					return error("License permits only .test TLDs. `%s' provided.", $value);
				}
			}
			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure('', $svc->getServiceValue('siteinfo', 'domain'), $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$this->site = $svc->getSite();
			Siteinfo::setHostname($svc->getAccountRoot(), $new);
			if ($old) {
				if (!\in_array($old, $this->ctx->getNewServiceValue('aliases', 'aliases'), true)) {
					// protected against demoting primary to addon
					Siteinfo::unmap($old, $svc->getSite());
				}
				(new PostgreSQL\Opcenter(\PostgreSQL::pdo()))->changeDomain($svc->getSiteId(), $new);
				$path = FILESYSTEM_VIRTBASE . DIRECTORY_SEPARATOR . $old;
				if (is_link($path)) {
					unlink($path);
				}
			}

			Siteinfo::setSiteLinks($svc);

			$ret = Siteinfo::map($svc->getSite(), $new);
			if ($ret && $old) {
				$this->freshenSite($svc);
			}

			return $ret;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			// leave depopulate() empty
			// Siteinfo/Enabled still needs the domain mapping for cleanup
			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			if ($new) {
				Siteinfo::unmap($new);
			}
			if ($old) {
				Siteinfo::map($svc->getSite(), $old);
			}

			return true;
		}


	}

