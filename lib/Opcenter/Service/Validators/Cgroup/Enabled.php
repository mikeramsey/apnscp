<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Provisioning\Cgroup;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Cgroup\Group;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements AlwaysValidate, ServiceInstall, ServiceReconfiguration
	{
		use \FilesystemPathTrait;
		public const DESCRIPTION = 'Resource enforcement';

		public function valid(&$value): bool
		{
			if ($value && !$this->ctx->isRevalidation() && $this->ctx->isEdit() && !$this->ctx->getOldServiceValue(null, 'enabled'))
			{
				$this->ctx->applyDefaults();
				$this->ctx->revalidateService();
			}
			return parent::valid($value);
		}

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			Cgroup::bindToController($svc);

			return $this->reconfigure('', 1, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				return $old ? $this->depopulate($svc) : true;
			}

			Cgroup::bindToController($svc);
			Cgroup::createModuleConfiguration($svc, 'cgroup.apache-config');
			Cgroup::createControllerConfiguration($svc);

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$group = new Group($svc->getSite());
			Cgroup::deleteModuleConfiguration($svc->getSite());
			Cgroup::removeController($svc);
			\Opcenter\System\Cgroup::removeConfiguration($group);

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}


	}

