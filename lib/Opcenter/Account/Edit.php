<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Account;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Opcenter\Filesystem;
	use Opcenter\SiteConfiguration;
	use Symfony\Component\Yaml\Yaml;

	class Edit extends DomainOperation implements Publisher
	{
		const MAX_SITES = 999;
		const HOOK_ID = 'vdedit';

		use \FilesystemPathTrait;

		// @var array options passed at runtime
		protected $runtimeConfiguration;
		// @var string domain name
		protected $domain;
		// @var int site id
		protected $site_id;
		// @var string "site" + id
		protected $site;
		// @var bool creation status, used to release site id on failure

		protected $options = [
			'reconfig' => false,
			// optional plan name
			'plan' => null,
			'dry'  => false,
			'reset' => false,
			'backup' => false,
			// sanity checks
			'force'  => false,
		];

		/**
		 *
		 * @param string $domain
		 * @param array  $options
		 */
		public function __construct(string $domain, array $options = [])
		{
			// avoid firing callbacks on previously registered iterations
			Cardinal::purge();
			$this->site_id = \Auth::get_site_id_from_anything(strtolower($domain));
			$this->domain = \Auth::get_domain_from_site_id($this->site_id);
			$this->site = 'site' . $this->site_id;
			$this->runtimeConfiguration = $options;
			cli_set_process_title(basename($_SERVER['argv'][0]) . " {$this->site}");
		}

		public function __destruct()
		{
			if ($this->status !== self::RC_FAILURE) {
				return;
			}
			if (\Error_Reporter::is_verbose(9999)) {
				pause('%s (%s) edit failed. Any key to rollback ', $this->domain, $this->site);
			}
			Cardinal::fire([self::HOOK_ID, Events::FAILURE], $this);
		}

		/**
		 * Run account creation
		 */
		public function exec(): bool
		{
			// switchback and populate account configuration markers before
			// mount is called
			if (!$this->installServices()) {
				return false;
			}
			$this->status = static::RC_SUCCESS;

			return (bool)$this->status;
		}

		/**
		 * Populate siteXX/info/ after initial FS population
		 *
		 * @return bool
		 */
		public function installServices()
		{
			$handler = new \Opcenter\SiteConfiguration($this->site, $this->runtimeConfiguration, $this->options);
			$plan = $this->options['plan'] ?? array_get($this->runtimeConfiguration, 'siteinfo.plan', null);
			if ($plan) {
				$handler->setPlanName($plan);
			} else if ($this->options['reset']) {
				$handler->setPlanName($handler->getPlanName());
			}

			$handler->migrateConfiguration();

			if ($this->options['backup']) {
				$path = $handler->getAuthContext()->domain_info_path('backup');
				if (!file_exists($path)) {
					Filesystem::mkdir($path, 'root', 'root', 0700);
				}
				$handler->migrateConfiguration('old', 'backup');
			}
			if (!$handler->verifyAll()) {
				return false;
			}
			Cardinal::preempt([Edit::HOOK_ID, Events::FAILURE], static function () use ($handler) {
				// cascade to rollback services bound in \Opcenter\Service\Validators\Enabled
				return Cardinal::fire([SiteConfiguration::HOOK_ID, Events::FAILURE], $handler);
			});

			// run hooks
			$handler->writePendingConfiguration();

			// dry-run
			if ($this->options['dry']) {
				$this->dryRun($handler);
				exit(0);
			}

			// ensure we cleanup journaled data that completed
			register_shutdown_function(function () use ($handler) {
				$this->fireCleanup($handler);
			});

			Cardinal::register([SiteConfiguration::HOOK_ID, Events::SUCCESS], static function () use ($handler) {
				try {
					if (!\Util_Account_Hooks::instantiateContexted($handler->getAuthContext())->run('edit')) {
						throw new \LogicException('failed to process edit hooks on site');
					}
				} catch (\Error $e) {
					fatal("failed to edit domain, edit hooks failed - %s\n%s", $e->getMessage(), $e->getTraceAsString());
				}
			});

			$this->runUserHooks($handler);

			// run populate()/depopulate() on each service
			if (!Cardinal::fire([SiteConfiguration::HOOK_ID, Events::SUCCESS], $handler)) {
				return Cardinal::fire([SiteConfiguration::HOOK_ID, Events::FAILURE], $handler);
			}

			Cardinal::fire([static::HOOK_ID, Events::SUCCESS], $handler);
			Cardinal::deregisterAll([SiteConfiguration::HOOK_ID, Events::FAILURE]);

			$this->fireCleanup($handler);
			return true;
		}

		private function dryRun(SiteConfiguration $handler): void
		{
			$diff = [
				$handler->getSite() => \Util_PHP::array_diff_assoc_recursive($handler->getNewConfiguration(),
					$handler->getCurrentConfiguration())
			];
			if (array_get($this->options, 'output') === 'json') {
				echo json_encode($diff);
			} else {
				echo Yaml::dump(
					$diff,
					(int)(getenv('YAML_INLINE') ?: 2), 2,
					Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK | Yaml::DUMP_OBJECT_AS_MAP | Yaml::DUMP_OBJECT);
			}
			$this->status = self::RC_SUCCESS;
			foreach ($handler->getServices() as $svc) {
				$handler->cleanService($svc);
			}
			exit(0);
		}

		public function fireCleanup(SiteConfiguration $handler): void
		{
			$handler->getAuthContext()->getAccount()->reset($handler->getAuthContext());
			parent::fireCleanup($handler);
		}

		/**
		 * Set editor option
		 *
		 * @param string $option
		 * @param mixed  $val
		 * @return self
		 */
		public function setOption(string $option, $val): self
		{
			if (!\array_key_exists($option, $this->options)) {
				fatal("Unknown option `%s'", $option);
			}
			$this->options[$option] = $val;
			return $this;
		}

		public function getDomain()
		{
			return $this->domain;
		}

		public function getEventArgs()
		{
			return [
				'site'    => $this->site,
				'domain'  => $this->domain,
				'options' => $this->runtimeConfiguration
			];
		}
	}