<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */


	namespace Opcenter\Account;

	use Opcenter\Map;

	class Enumerate
	{
		use \FilesystemPathTrait;

		/**
		 * List all domains on a server
		 *
		 * @return array
		 */
		public static function domains(): array
		{
			$map = self::loadMap();

			return array_keys($map);
		}

		protected static function loadMap(): array
		{
			$map = Map::load(Map::DOMAIN_MAP);

			return $map->fetchAll();
		}

		/**
		 * List all active accounts
		 *
		 * @return array
		 */
		public static function active(): array
		{
			$sites = self::sites();

			return array_diff($sites, self::disabled());
		}

		/**
		 * List all sites on a server, uniquely identifiable
		 *
		 * @return array
		 */
		public static function sites(): array
		{
			$map = self::loadMap();
			$arr = array_fill_keys(array_values($map), 1);

			return array_keys($arr);
		}

		/**
		 * List all disabled accounts
		 *
		 * @return array
		 */
		public static function disabled(): array
		{
			$sites = self::sites();

			return array_filter(
				$sites,
				[State::class, 'disabled']
			);
		}
	}