<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Provisioning\Traits\ApacheModule;
	use Opcenter\SiteConfiguration;
	use Opcenter\System\Cgroup\Controller;
	use Opcenter\System\Cgroup\Group;

	/**
	 * Class Cgroup
	 *
	 * Categorize all jailed activity to cgroup
	 *
	 * @package Opcenter\Provisioning
	 */
	class Cgroup
	{
		use ApacheModule;
		const CONFIG_NAME = 'cgroup';

		/**
		 * Bind all controller activity to named group
		 *
		 * @param SiteConfiguration $svc service configuration (derives group)
		 * @param string            $controller
		 * @return bool
		 */
		public static function bindToController(SiteConfiguration $svc, $controller = '*'): bool
		{
			$path = static::getConfigPath($svc);
			if (!file_exists($path)) {
				touch($path);
			}

			$group = new Group($svc->getSite());

			if ($controller === '*') {
				$controller = $svc->getSiteFunctionInterceptor()->cgroup_get_controllers();

				return file_put_contents($path, '* ' . implode(',', $controller) . ' ' . $group . "\n") > 0;
			}
			$contents = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			$found = false;
			if (!$found) {
				$contents[] = "* ${controller} " . $group;
			}

			return file_put_contents($path, join("\n", $contents)) > 0;
		}

		/**
		 * Get localized account cgconfig path
		 *
		 * @param SiteConfiguration $svc
		 * @return string
		 */
		private static function getConfigPath(SiteConfiguration $svc = null): string
		{
			return ($svc ? $svc->getAccountRoot() : '') . \Opcenter\System\Cgroup::CGROUP_SITE_CONFIG;
		}

		/**
		 * Remove controller binding from site
		 *
		 * @param SiteConfiguration $svc
		 * @param string            $controller
		 * @return bool
		 */
		public static function removeController(SiteConfiguration $svc, $controller = '*'): bool
		{
			$path = static::getConfigPath($svc);
			if (!file_exists($path)) {
				return warn("cgroup configuration not active on site, `%s' missing?", static::getConfigPath());
			}
			$contents = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			foreach ($contents as $k => $v) {
				strtok($v, ' ');
				if ($controller === '*') {
					unset($contents[$k]);
				} else if (strtok(' ') === $controller) {
					unset($contents[$k]);
				}
			}
			if ($controller === '*') {
				$controller = $svc->getSiteFunctionInterceptor()->cgroup_get_controllers();
			}
			$group = new Group($svc->getSite());
			foreach ((array)$controller as $c) {
				\Opcenter\System\Cgroup::delete($group, Controller::make($group, $c));
			}
			file_put_contents($path, join("\n", $contents));

			return true;
		}

		public static function createControllerConfiguration(SiteConfiguration $svc): bool
		{
			$group = new Group($svc->getSite(), [
				'task' => [
					'uid'   => \Web_Module::WEB_USERNAME,
					'gid'   => $svc->getServiceValue('siteinfo', 'admin'),
					'fperm' => 0770
				]
			]);
			foreach ($svc->getSiteFunctionInterceptor()->cgroup_get_controllers() as $cname) {
				$controller = Controller::make($group, $cname);
				$controller->import($svc->getAuthContext());
				$group->add($controller);
				$controller->create();
			}

			return \Opcenter\System\Cgroup::create($group);
		}
	}


