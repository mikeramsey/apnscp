<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Provisioning;

use Event\Cardinal;
use Event\Events;
use Opcenter\Filesystem;
use Opcenter\Http\Php\Fpm;
use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
use Opcenter\Provisioning\Traits\GroupCreationTrait;
use Opcenter\Provisioning\Traits\MapperTrait;
use Opcenter\SiteConfiguration;
use Opcenter\System\GenericSystemdService;

class Php {
	use FilesystemPopulatorTrait {
		populateFilesystem as populateFilesystemReal;
		depopulateFilesystem as depopulateFilesystemReal;
	}
	use MapperTrait;
	use GroupCreationTrait;

	const TEMPLATE_DIRECTORIES = [
		'/etc/php-fpm.d'   => [null, null, 0755],
		Fpm\Configuration::CONFIGURATION_BASE_PATH => ['root', 'root', 0711],
		'/var/lib/php-fpm' => ['root', APACHE_GID, 0710],
		'/var/run/php-fpm' => ['root', APACHE_GID, 0770],
		'/var/log/php-fpm' => [null, null, 02710]
	];

	const TEMPLATE_FILES = [];
	const SUPPLEMENTAL_GROUPS = [
		\Web_Module::WEB_USERNAME => [\Web_Module::WEB_USERNAME, null],
	];

	/**
	 * Install PHP-FPM handler
	 *
	 * @param SiteConfiguration $svc
	 * @return bool
	 */
	public static function installFpm(SiteConfiguration $svc): bool
	{
		self::populateFilesystemReal($svc);
		$ctx = $svc->getAuthContext();
		$config = Fpm\Configuration::bindTo($svc->getAccountRoot());
		$config->setServiceContainer($svc);

		$fpmClass = new Fpm($config);
		$fpmPath = $config->getFpmConfigurationPath(false);

		if (!is_dir(\dirname($fpmPath))) {
			Filesystem::mkdir(\dirname($fpmPath), $ctx->user_id, $ctx->group_id, 0755);
		}

		$masterPath = GenericSystemdService::SYSTEMD_SERVICE_PATH . DIRECTORY_SEPARATOR . '/php-fpm.service';
		if (!file_exists($masterPath)) {
			static::writeConfiguration($config, 'fpm-service-master', $masterPath);
			Fpm\SystemdSynthesizer::mock('php-fpm')->run('disable');
		}

		if ($config->getGroup()) {
			static::writeConfiguration($config, 'fpm-service-group',
				GenericSystemdService::SYSTEMD_SERVICE_PATH . DIRECTORY_SEPARATOR . $config->getServiceGroupName()
			);
			Fpm\SystemdSynthesizer::mock($config->getServiceGroupName())->run('enable');
		}
		static::writeConfiguration($config, 'fpm-config', $fpmPath) &&
			Filesystem::chogp($fpmPath, 'root', $config->getSysGroup(), 0640);
		static::writeConfiguration($config, 'fpm-service',
			GenericSystemdService::SYSTEMD_SERVICE_PATH . DIRECTORY_SEPARATOR . $config->getServiceName());

		static::writeConfiguration($config, 'fpm-socket-service',
			GenericSystemdService::SYSTEMD_SERVICE_PATH . DIRECTORY_SEPARATOR .$config->getSocketServiceName());

		if (!file_exists($path = $config->getRoot() . $config->getLog())) {
			Filesystem::touch($path, $config->getSysUser(), $ctx->group_id, 0640);
		} else {
			chown($path, $config->getSysUser());
		}
		GenericSystemdService::daemonReload();

		// install socket
		$socketInstance = $fpmClass->mockService(true);

		if ($config->getSysUser() !== array_get(posix_getpwnam(\Web_Module::WEB_USERNAME), 'uid', $config->getSysUser())) {
			$logPath = $config->getRoot() . $config->getLog();
			if (file_exists($logPath)) {
				Filesystem::chogp($logPath, $config->getSysUser(), $config->getSysGroup(), 0640);
			}
		}
		if (!$socketInstance::running()) {
			$socketInstance->async()::run('enable --now');
		} else {
			$fpmClass->mockService()->async()::restart();
		}

		return true;
	}

	/**
	 * Remove PHP-FPM handler
	 *
	 * @param SiteConfiguration $svc
	 * @return bool
	 */
	public static function removeFpm(SiteConfiguration $svc): bool
	{
		$config = Fpm\Configuration::bindTo($svc->getAccountRoot())->setGroup($svc->getSite());
		$fpm = new Fpm($config);
		Fpm::uninstall($fpm->listServices());
		foreach (glob($svc->getAccountRoot() . '/etc/logrotate.d/php-fpm*') as $profile) {
			if (!file_exists($profile) || \is_link($profile)) {
				continue;
			}
			unlink($profile);
		}
		if (file_exists($path = GenericSystemdService::SYSTEMD_SERVICE_PATH . '/' . $config->getServiceGroupName())) {
			Fpm\SystemdSynthesizer::mock($config->getServiceGroupName())->run('disable');
			unlink($path);
		}
		self::depopulateFilesystem($svc);

		return true;
	}

	/**
	 * Write configuration
	 *
	 * @param SiteConfiguration $svc
	 * @param string            $template input template
	 * @param string            $path output file
	 * @return bool
	 */
	public static function writeConfiguration(Fpm\Configuration $config, string $template, string $path): bool
	{
		$config = static::buildConfiguration($config, $template);
		$ret = file_put_contents($path, $config) && Filesystem::chogp($path, 'root', APACHE_GID, 0644);
		return $ret ?: error('Failed to write %s', $path);
	}

	/**
	 * Build configuration from template
	 *
	 * @param Fpm\Configuration $config
	 * @param string            $template
	 * @return string
	 */
	public static function buildConfiguration(Fpm\Configuration $config, string $template): string
	{
		$parsedConfig = (new ConfigurationWriter("apache.php.${template}", $config->getServiceContainer()))
			->compile(['config' => $config]);
		return (string)$parsedConfig;
	}

}
