<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;

	/**
	 * Class Mlist
	 *
	 * @package Opcenter\Provisioning
	 */
	class Mlist
	{
		use FilesystemPopulatorTrait;
		use GroupCreationTrait;

		const TEMPLATE_FILES = [
			'/etc/majordomo.cf' => [null, null, 0644],
		];

		const TEMPLATE_DIRECTORIES = [
			'/var/lib/majordomo' => ['root', 'root', 0755],
		];
	}