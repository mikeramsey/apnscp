<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Auth\Password;
	use Opcenter\Filesystem;
	use Opcenter\Provisioning\Traits\FilesystemPopulatorTrait;
	use Opcenter\Provisioning\Traits\GroupCreationTrait;
	use Opcenter\Role\User;
	use Opcenter\SiteConfiguration;

	class MySQL
	{
		use FilesystemPopulatorTrait;
		use GroupCreationTrait;

		const TEMPLATE_FILES = [];
		const TEMPLATE_DIRECTORIES = [
			'/var/lib/mysql' => [self::SYS_USER, null, 02751]
		];
		const HOSTS = ['127.0.0.1', 'localhost'];
		const SUPPLEMENTAL_GROUPS = [
			self::SYS_USER => [self::SYS_USER, null]
		];
		// @var string system user
		const SYS_USER = 'mysql';


		public static function install(SiteConfiguration $container): bool
		{
			$password = static::getPassword($container);
			foreach (static::HOSTS as $host) {
				\Opcenter\Database\MySQL::createUser($container->getServiceValue('mysql', 'dbaseadmin'), $password,
					$host, [], ['max_user_connections' => MYSQL_CONCURRENCY_LIMIT]);
			}
			$root = $container->getAccountRoot();
			$home = User::bindTo($root)->getpwnam($container->getServiceValue('siteinfo', 'admin_user'))['home'];
			$cnf = $root . "${home}/.my.cnf";
			file_put_contents(
				$cnf,
				static::configTemplate($container->getServiceValue('mysql', 'dbaseadmin'), $password)
			);
			Filesystem::chogp($cnf, $container->getServiceValue('siteinfo', 'admin'),
				$container->getServiceValue('siteinfo', 'admin'), 0600);

			return true;
		}

		protected static function getPassword(SiteConfiguration $configuration): string
		{
			$afi = \apnscpFunctionInterceptor::factory($configuration->getAuthContext());
			if ($p = $afi->mysql_get_password()) {
				return $p;
			}
			if ($p = $configuration->getServiceValue('mysql', 'password')) {
				return $p;
			}
			if ($p = $configuration->getServiceValue('siteinfo', 'tpasswd')) {
				return $p;
			}

			return Password::generate();

		}

		public static function configTemplate(string $user, string $password): string
		{
			return '[client]' . "\n" .
				"user=$user" . "\n" .
				"password=$password" . "\n";
		}

		public static function uninstall(SiteConfiguration $container): bool
		{
			// additional users deleted via MySQL Module
			// grab previous configuration, disabling may set user to null
			$admin = array_get($container->getOldConfiguration('mysql'), 'dbaseadmin');
			if (!$admin) {
				// incomplete creation
				return true;
			}
			\Opcenter\Database\MySQL::deleteUser($admin);
			\Opcenter\Database\MySQL::flush();

			return true;
		}
	}
