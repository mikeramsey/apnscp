<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Provisioning\Traits;

	use Opcenter\Filesystem;
	use Opcenter\Service\Filelists;
	use Opcenter\SiteConfiguration;

	trait FilesystemPopulatorTrait
	{
		/**
		 * Populate filesystem path using template files + optional directories
		 *
		 * @param SiteConfiguration $container current service configuration
		 * @param string            $service   optional filelists to install before populating
		 * @return bool
		 */
		public static function populateFilesystem(SiteConfiguration $container, $service = null): bool
		{
			$path = $container->getAccountRoot();
			$pwd = posix_getpwnam($container->getServiceValue('siteinfo', 'admin'));
			if (!$pwd) {
				return error("Failed to get passwd for `%s'", $container->getServiceValue('siteinfo', 'admin'));
			}
			if (null === ($files = static::getPopulatedFileList())) {
				fatal("class constant TEMPLATE_FILES missing from `%s'", static::class);
			}
			if ($service) {
				if (Filelists::exists($service)) {
					Filelists::install($container->getAccountRoot(), $service);
				} else {
					debug("filelist support requested for service `%s' but none installed on system", $service);
				}
			}
			foreach (static::getPopulatedDirectoriesList() as $dir => $meta) {
				$args = [
					$meta[0] ?? $pwd['uid'],
					$meta[1] ?? $pwd['gid'],
					$meta[2] ?? 0755
				];
				$dir = $path . $dir;
				if (!is_dir($dir) && !Filesystem::mkdir($dir, ...$args)) {
					fatal("failed to create directory `%s'", $dir);
				} else {
					// be lenient
					Filesystem::chogp($dir, ...$args);
				}
			}

			foreach ($files as $file => $meta) {
				$file = $path . $file;
				// allow null uid/gid, which defaults to account uid/gid
				$args = [
					$meta[0] ?? $pwd['uid'],
					$meta[1] ?? $pwd['gid'],
					$meta[2] ?? 0755
				];

				if (!touch($file)) {
					return error("failed to touch file `%s'", $file);
				}
				if (!Filesystem::chogp($file, ...$args)) {
					return error("failed to adjust permissions on `%s'", $file);
				}
			}

			return true;
		}

		/**
		 * Get list of files to populate
		 *
		 * @return array|null
		 */
		protected static function getPopulatedFileList(): ?array
		{
			return \defined('static::TEMPLATE_FILES') ? static::TEMPLATE_FILES : null;
		}

		/**
		 * Get list of directories to populate
		 *
		 * @return array
		 */
		protected static function getPopulatedDirectoriesList(): array
		{
			return \defined('static::TEMPLATE_DIRECTORIES') ? static::TEMPLATE_DIRECTORIES : [];
		}

		/**
		 * Remove service files from account
		 *
		 * @param SiteConfiguration $container
		 * @param string|null       $service
		 * @return bool
		 */
		public static function depopulateFilesystem(SiteConfiguration $container, $service = null): bool
		{
			$fshome = $container->getAccountRoot();
			foreach (array_keys(static::getPopulatedDirectoriesList()) as $dir) {
				$dir = $fshome . $dir;
				if (is_dir($dir) && !is_link($dir) && !Filesystem::rmdir($dir)) {
					fatal("failed to remove directory `%s'", $dir);
				}
			}

			foreach (static::getPopulatedFileList() as $file) {
				$path = $fshome . $file;
				if (!file_exists($path)) {
					warn("file `%s' missing from site", $path);
					continue;
				}
				unlink($path) || warn("failed to unlink `%s'", $path);
			}

			return true;
		}
	}