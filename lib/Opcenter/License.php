<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * ASN routines based off work by Mike Macgrivin, Kris Bailey Oct 2010
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter;

	use GuzzleHttp\Client;
	use GuzzleHttp\Exception\ClientException;
	use Opcenter\Net\Iface;

	/**
	 * Class License
	 *
	 * apnscp license validation and renewal
	 *
	 * @package Opcenter
	 */
	final class License
	{
		private const OID_FINGERPRINT = '1.3.6.1.4.1.51711.1.10';
		private const OID_LIFETIME = '1.3.6.1.4.1.51711.1.11';
		private const OID_PURPOSE = '1.3.6.1.4.1.51711.1.12';

		private const PURPOSES = [
			0 => 'checkDevelopment',
			1 => 'isLocal',
			2 => 'checkTrial',
			3 => 'checkDnsOnly',
			4 => 'checkNetRestriction',
			5 => 'checkDomainCount',
			6 => 'checkLanguage',
		];

		// Trial durations:
		// alpha = 60 days
		// beta = 30 days
		// official = 15 days
		private const MAXIMAL_TRIAL_DURATION = 60;

		private const CA_FINGERPRINT = '0E:04:A7:75:E1:73:60:94:F4:5F:A0:D4:DE:44:0A:93:01:E1:06:79';
		public const LICENSE_FILE = 'license.pem';
		private const LICENSE_URI = 'https://license.apnscp.com';
		private const ASN_MARKERS = [
			'ASN_UNIVERSAL'   => 0x00,
			'ASN_APPLICATION' => 0x40,
			'ASN_CONTEXT'     => 0x80,
			'ASN_PRIVATE'     => 0xC0,

			'ASN_PRIMITIVE'   => 0x00,
			'ASN_CONSTRUCTOR' => 0x20,

			'ASN_LONG_LEN'     => 0x80,
			'ASN_EXTENSION_ID' => 0x1F,
			'ASN_BIT'          => 0x80,
		];
		private const ASN_TYPES = [
			0  => 'ASN_BASE',
			1  => 'ASN_BOOLEAN',
			2  => 'ASN_INTEGER',
			3  => 'ASN_BIT_STR',
			4  => 'ASN_OCTET_STR',
			5  => 'ASN_NULL',
			6  => 'ASN_OBJECT_ID',
			9  => 'ASN_REAL',
			10 => 'ASN_ENUMERATED',
			12 => 'ASN_UTF8_STR',
			13 => 'ASN_RELATIVE_OID',
			19 => 'ASN_PRINT_STR',
			22 => 'ASN_IA5_STR',
			23 => 'ASN_UTC_TIME',
			24 => 'ASN_GENERAL_TIME',
			48 => 'ASN_SEQUENCE',
			49 => 'ASN_SET',
		];
		// @var string license path
		private $path;
		private static $instance;
		private $license;

		public function __construct(string $path = null)
		{
			$this->path = $path ?? $this->getLicensePath();
			if (file_exists($this->path)) {
				$this->license = \openssl_x509_parse(file_get_contents($this->path));
			}
		}

		/**
		 * Get license file
		 *
		 * @return string
		 */
		public function getLicensePath(): string
		{
			return $this->path ?? config_path(self::LICENSE_FILE);
		}

		public function install(): bool
		{
			if ($this->path === config_path(self::LICENSE_FILE)) {
				return warn('License %s already installed', $this->path);
			}
			if (!$this->verify()) {
				return error("Cannot install license `%s' - license invalid", $this->path);
			}
			$path = config_path(self::LICENSE_FILE);

			return copy($this->path, $path) && chown($path, APNSCP_SYSTEM_USER);
		}

		public static function get()
		{
			if (null === self::$instance) {
				self::$instance = new static;
			}

			return self::$instance;
		}

		public function __toString(): string
		{
			return (string)file_get_contents($this->getLicensePath());
		}

		/**
		 * Get certificate common name
		 *
		 * @return null|string
		 */
		public function getCommonName(): ?string
		{
			if (!$license = $this->getLicense()) {
				return null;
			}

			return $license['subject']['CN'] ?? null;
		}

		/**
		 * Reissue certificate
		 *
		 * @return bool
		 */
		public function reissue($force = false): bool
		{
			if (!$this->getLicense()) {
				return error('No license detected');
			}
			if (!$force && !$this->needsReissue()) {
				return info('License does not need reissuance');
			}
			$tmp = tempnam(storage_path(''), 'license');
			try {
				$ret = $this->download($tmp, 'renew');
			} catch (ClientException $e) {
				$msg = $e->getMessage();
				if (false !== strpos($e->getResponse()->getHeaderLine('Content-Type'), 'application/json')) {
					$msg = trim(\json_decode($e->getResponse()->getBody()->getContents(), true));
				}

				return error("failed to fetch license: `%s', will try again later", $msg);
			} finally {
				if (file_exists($tmp)) {
					unlink($tmp);
				}
			}

			return $ret ?? false;
		}

		/**
		 * Issue a new license
		 *
		 * @param string $key activation key via my.apnscp.com
		 * @param string $cn  optional common name to use
		 * @return bool
		 */
		public function issue(string $key, string $cn = null): bool
		{
			if (!preg_match('/^[a-zA-Z0-9-]{10,}$/', $key)) {
				return error('Activation key must consist of characters in a-z, A-Z, and 0-9');
			}
			$tmp = tempnam(storage_path(''), 'license');
			try {
				$uri = 'activate/' . $key;
				if ($cn) {
					$uri .= '/' . rawurlencode($cn);
				}

				return $this->download($tmp, $uri);
			} catch (\Exception $e) {
				return error("failed to renew license: `%s', will try again later", $e->getMessage());
			} finally {
				if (file_exists($tmp)) {
					unlink($tmp);
				}
			}

			return false;
		}

		/**
		 * Certificate needs to be reissued
		 *
		 * @return bool
		 */
		public function needsReissue(): bool
		{
			if ($this->isLifetime()) {
				return false;
			}
			$days = $this->daysUntilExpire();

			return $days < 10 && $days >= 0;
		}

		/**
		 * License is valid for life
		 *
		 * @return bool
		 */
		public function isLifetime()
		{
			$license = $this->getLicense();

			return (bool)self::parseASNString($license['extensions'][self::OID_LIFETIME] ?? null);
		}

		public function daysUntilExpire(): ?int
		{
			if (null === ($license = $this->getLicense())) {
				return null;
			}

			return (int)(new \DateTime())->diff((new \DateTime())->setTimestamp($license['validTo_time_t']))->format('%R%a');
		}

		private function download(string $tmp, string $uri): bool
		{
			$client = new Client([
				'base_uri' => self::LICENSE_URI,
				'timeout'  => 10,
				'future'   => false,
				'curl'     => [
					CURLOPT_CAINFO         => resource_path('apnscp.ca'),
					CURLOPT_SSL_VERIFYPEER => true,
					CURLOPT_SSLCERT        => $this->getLicensePath()
				],
				'sink'  => $tmp
			]);

			$client->post($uri);
			if (!(new License($tmp))->verify()) {
				return error('failed to validate renewed certificate');
			}
			$backup = config_path(self::LICENSE_FILE . '.old');
			if (file_exists($backup)) {
				unlink($backup);
			}

			$license = $this->getLicensePath();
			$moved = (!file_exists($license) || rename($license, $backup)) && rename($tmp, $license);

			if (!$moved || !(new License)->verify()) {
				if (file_exists($backup)) {
					rename($backup, $license);
				}

				return error('Failed to verify new license');
			}
			Filesystem::chogp($license, APNSCP_SYSTEM_USER, APNSCP_SYSTEM_USER, 0600);

			return true;
		}

		/**
		 * License is valid
		 *
		 * @return bool
		 * @throws \Exception
		 */
		public function verify(): bool
		{
			$path = $this->getLicensePath();

			if (!file_exists($path)) {
				return false;
			}

			if (!$this->validateLicense()) {
				return false;
			}

			$license = new static($path);
			if ($license->hasExpired()) {
				return \error('license expired on %s - visit my.apnscp.com to issue a new certificate',
					(new \DateTime())->setTimestamp($license->getLicense()['validTo_time_t'])->format('r'));
			}
			if ($license->revoked()) {
				return \error('license has been revoked - contact license@apisnetworks.com');
			}

			return true;
		}

		/**
		 * Validate license
		 *
		 * @return bool
		 */
		private function validateLicense(): bool
		{
			$ca = resource_path('apnscp.ca');
			if (!\file_exists($ca)) {
				return false;
			}
			$file = $this->getLicensePath();
			$crttxt = \file_get_contents($file);

			if (!\openssl_x509_check_private_key($crttxt, $crttxt)) {
				\error("license key `%s' corrupted", $file);

				return false;
			}
			$catxt = \file_get_contents($ca);
			$x509ca = \openssl_x509_parse($catxt);

			if (($x509ca['extensions']['subjectKeyIdentifier'] ?? '') !== self::CA_FINGERPRINT) {
				\error("`%s' corrupted - fingerprint mismatch", $ca);

				return false;
			}
			$chain = preg_split('/(?<=-----END CERTIFICATE-----$)\n/m', $catxt, -1, PREG_SPLIT_NO_EMPTY);
			if (false === ($x509 = \openssl_x509_parse(file_get_contents($file)))) {
				\error("license file `%s' is invalid or corrupted", $file);

				return false;
			}
			$authority = \array_get($x509, 'extensions.authorityKeyIdentifier', '');
			if (!$this->validateChain($chain, $authority)) {
				\error('invalid certificate chain detected');

				return false;
			}
			if ($this->hasPurpose() && !($reason = $this->validatePurpose())) {
				if (null === $reason) {
					\error('certificate has unknown or unverified purposes - update %s', PANEL_BRAND);
				}

				return false;
			}

			return true;
		}

		/**
		 * Validate all certificates match chain
		 *
		 * @param array  $chain
		 * @param string $authority authority fingerprint
		 * @return bool
		 */
		private function validateChain(array $chain, $authority): bool
		{
			if (!$chain) {
				return true;
			}
			if (!$authority) {
				return \error('No authority found in license?');
			}
			$crt = array_pop($chain);
			if (!$crtx509 = openssl_x509_parse($crt)) {
				return false;
			}
			$x509 = \openssl_x509_parse($crt);
			$fingerprint = $x509['extensions']['subjectKeyIdentifier'];
			$c1 = $this->canonicalizeFingerprint($authority);
			$c2 = $this->canonicalizeFingerprint($fingerprint);
			if ($c1 === $c2) {
				$authority = $x509['extensions']['authorityKeyIdentifier'];
			} else if (!$chain) {
				// end of the road
				return false;
			}

			return $this->validateChain($chain, $authority);
		}

		private function canonicalizeFingerprint($fingerprint): string
		{
			if (0 === strncmp($fingerprint, 'keyid:', 6)) {
				$fingerprint = \substr($fingerprint, 6);
			}

			return \strtolower(\trim(\str_replace(':', '', $fingerprint)));
		}

		private function hasPurpose(): bool
		{
			$license = $this->getLicense();
			if (!$license) {
				return false;
			}

			return isset($license['extensions'][self::OID_PURPOSE]);
		}

		/**
		 * Get x509 license
		 *
		 * @return array|null x509 object from openssl_x509_parse()
		 */
		public function getLicense(): ?array
		{
			return $this->license;
		}

		/**
		 * License installed
		 *
		 * @return bool
		 */
		public function installed(): bool
		{
			return $this->getLicense() !== null;
		}

		private function validatePurpose(): ?bool
		{
			$license = $this->getLicense();
			if (!isset($license['extensions'][self::OID_PURPOSE])) {
				return false;
			}

			$fields = self::parseASNString($license['extensions'][self::OID_PURPOSE]);
			foreach ($fields as $oid => $field) {
				if (!isset(self::PURPOSES[$oid])) {
					return null;
				}
				if (!$field || !method_exists($this, self::PURPOSES[$oid])) {
					continue;
				}
				if (!$this->{self::PURPOSES[$oid]}()) {
					\error('Validation failed on %s', self::PURPOSES[$oid]);

					return false;
				}
			}

			return true;
		}

		/**
		 * Parse ASN.1 string
		 *
		 * @param string|null $string
		 * @param int         $level
		 * @param int|null    $maxLevels
		 * @return mixed array or scalar
		 */
		private static function parseASNString(string $string = null, int $level = 1, int $maxLevels = null)
		{
			if ($maxLevels && $level > $maxLevels) {
				return [
					$string
				];
			}

			$parsed = array();
			$endLength = \strlen((string)$string);
			$p = 0;
			while ($p < $endLength) {
				$type = \ord($string[$p++]);
				if (!$type) { // if we are type 0, just continue
					continue;
				}
				$length = \ord($string[$p++]);
				if ($length & self::ASN_MARKERS['ASN_LONG_LEN']) {
					$tempLength = 0;
					for ($x = 0; $x < ($length & (self::ASN_MARKERS['ASN_LONG_LEN'] - 1)); $x++) {
						$tempLength = \ord($string[$p++]) + ($tempLength * 256);
					}
					$length = $tempLength;
				}
				$data = substr($string, $p, $length);
				$parsed[] = self::parseASNData($type, $data, $level, $maxLevels);
				$p += $length;
			}

			return $level === 1 && !isset($parsed[1]) ? array_pop($parsed) : $parsed;
		}

		/**
		 * Parse an ASN.1 field value.
		 *
		 * This function takes a binary ASN.1 value and parses it according to it's specified type
		 *
		 * @param int    $type      The type of data being provided
		 * @param string $data      The raw binary data string
		 * @param int    $level     The current parsing depth
		 * @param int    $maxLevels The max parsing depth
		 * @return    mixed    The data that was parsed from the raw binary data string
		 */
		private static function parseASNData(int $type, string $data, int $level, int $maxLevels = null)
		{
			switch (self::getASNType($type)) {
				case 'ASN_BOOLEAN':
					return (\ord($data) & 0xFF) === 0xFF;
				case 'ASN_INTEGER':
					return \hexdec(\bin2hex($data));
				case 'ASN_BIT_STR':
					return self::parseASNString($data, $level + 1, $maxLevels);
				case 'ASN_OCTET_STR':
					return $data;
				case 'ASN_NULL':
					return null;
				case 'ASN_REAL':
					return $data;
				case 'ASN_ENUMERATED':
					return self::parseASNString($data, $level + 1, $maxLevels);
				case 'ASN_RELATIVE_OID': // I don't really know how this works and don't have an example :-)
					// so, lets just return it ...
					return $data;
				case 'ASN_UTF8_STR':
					return utf8_decode($data);
				case 'ASN_SEQUENCE':
					return self::parseASNString($data, $level + 1, $maxLevels);
				case 'ASN_SET':
					return self::parseASNString($data, $level + 1, $maxLevels);
				case 'ASN_PRINT_STR':
					return $data;
				case 'ASN_IA5_STR':
					return $data;
				case 'ASN_UTC_TIME':
					return $data;
				case 'ASN_GENERAL_TIME':
					return $data;
				case 'ASN_OBJECT_ID':
					return self::parseOID($data);
				default:
					return $data;
			}
		}

		/**
		 * Get ASN.1 type from data
		 *
		 * @param int $type
		 * @return null|string
		 */
		private static function getASNType(int $type): ?string
		{
			return self::ASN_TYPES[$type % 50] ?? fatal("unknown ASN type `%d'", $type % 50);
		}

		/**
		 * Parse an ASN.1 OID value.
		 *
		 * This takes the raw binary string that represents an OID value and parses it into its
		 * dot notation form.  example - 1.2.840.113549.1.1.5
		 * look up OID's here: http://www.oid-info.com/
		 * (the multi-byte OID section can be done in a more efficient way, I will fix it later)
		 *
		 * @param string $string
		 * @return    string    The OID contained in $data
		 */
		public static function parseOID(string $string): string
		{
			$ret = floor(\ord($string[0]) / 40) . '.';
			$ret .= (\ord($string[0]) % 40);
			$build = array();

			for ($i = 1, $n = \strlen($string); $i < $n; $i++) {
				$v = \ord($string[$i]);
				if ($v > 127) {
					$build[] = \ord($string[$i]) - self::ASN_MARKERS['ASN_BIT'];
				} else if ($build) {
					// do the build here for multibyte values
					$build[] = \ord($string[$i]) - self::ASN_MARKERS['ASN_BIT'];
					// you know, it seems there should be a better way to do this...
					$build = \array_reverse($build);
					$num = 0;
					for ($x = 0, $y = \count($build); $x < $y; $x++) {
						$mult = $x === 0 ? 1 : pow(256, $x);
						if ($x + 1 === \count($build)) {
							$value = (($build[$x] & (self::ASN_MARKERS['ASN_BIT'] - 1)) >> $x) * $mult;
						} else {
							$value = ((($build[$x] & (self::ASN_MARKERS['ASN_BIT'] - 1)) >> $x) ^ ($build[$x + 1] << (7 - $x) & 255)) * $mult;
						}
						$num += $value;
					}
					$ret .= '.' . $num;
					$build = []; // start over
				} else {
					$ret .= '.' . $v;
					$build = [];
				}
			}

			return $ret;
		}

		/**
		 * License has expired and cannot be renewed
		 *
		 * @return bool
		 */
		public function hasExpired(): bool
		{
			if (null === ($license = $this->getLicense())) {
				return true;
			}

			$begin = (int)(new \DateTime())->setTimestamp($license['validFrom_time_t'])->diff(new \DateTime())->format('%R%a');
			$end = $this->daysUntilExpire();

			return $begin >= 0 && $end < 0;
		}

		/**
		 * License has been revoked
		 *
		 * @return bool
		 */
		public function revoked(): bool
		{
			if (null === ($uri = $this->extractOcsp($this->getLicense()))) {
				return true;
			}
			// licenses are good for 40 days at this point, so let's avoid overhead for
			// now and evaluate initial implementation
			return false;
		}

		/**
		 * Get OCSP from x509 details
		 *
		 * @param array $x509details
		 * @return null|string
		 */
		private function extractOcsp(array $x509details): ?string
		{
			$uri = $x509details['extensions']['authorityInfoAccess'] ?? '';
			$pos = \strpos($uri, 'URI:');
			if (!$uri || !$pos) {
				error('Invalid certificate detected');

				return null;
			}

			return \trim(\substr($uri, $pos + 4));
		}

		/**
		 * Get certificate serial number
		 *
		 * @return null|string
		 */
		public function getSerial(): ?string
		{
			$license = $this->getLicense();

			return !$license ? null : $license['serialNumber'];
		}

		/**
		 * Get account ID attached to license
		 *
		 * @return null|string
		 */
		public function getAccountId(): ?string
		{
			$license = $this->getLicense();

			return !$license ? null : self::parseASNString($license['extensions'][self::OID_FINGERPRINT] ?? null);
		}

		/**
		 * License is trial
		 *
		 * @return bool
		 */
		public function checkTrial(): bool
		{
			if (null === ($license = $this->getLicense())) {
				return false;
			}

			// not a trial license
			if (!$this->getPurposeField(2)) {
				return true;
			}
			// only accept certificates created before Oct 1, 2018
			$days = (new \DateTime())->setTimestamp($license['validTo_time_t'])->diff(new \DateTime())->days;

			// validate duration no longer than maximal trial time
			return self::MAXIMAL_TRIAL_DURATION >= $days;
		}

		/**
		 * Get purpose field value
		 *
		 * @param int $purpose
		 *
		 * @return mixed|null
		 */
		private function getPurposeField(int $purpose)
		{
			if (!$license = $this->getLicense()) {
				return null;
			}

			return array_get(self::parseASNString($license['extensions'][self::OID_PURPOSE] ?? null), $purpose, null);
		}

		/**
		 * License is restricted to single-site development
		 *
		 * @return bool
		 */
		public function isDevelopment(): bool
		{
			return (bool)$this->getPurposeField(0);
		}

		/**
		 * License is restricted to IANA hosts
		 *
		 * @return bool
		 */
		public function isLocal(): bool
		{
			return (bool)$this->getPurposeField(1);
		}

		/**
		 * License is trial
		 *
		 * @return bool
		 */
		public function isTrial(): bool
		{
			return (bool)$this->getPurposeField(2);
		}

		/**
		 * License cannot add domains
		 *
		 * @return bool
		 */
		public function isDnsOnly(): bool
		{
			return (bool)$this->getPurposeField(3);
		}

		/**
		 * License may only host .test TLDs
		 *
		 * @return bool
		 */
		private function checkDevelopment(): bool
		{
			if (!$this->isDevelopment()) {
				return true;
			}

			$path = null;
			foreach ([Map::DOMAIN_MAP, Map::DOMAIN_TXT_MAP] as $map) {
				$path = Map::home($map);
				if (file_exists($path)) {
					break;
				}

			}

			// something's awry
			if (!$path) {
				return false;
			}

			foreach (Map::load($path, 'r-')->fetchAll() as $domain => $site) {
				if (substr($domain, -5) !== '.test') {
					return false;
				}
			}

			return true;
		}

		/**
		 * License type adheres to DNS-only restrictions
		 *
		 * @return bool
		 */
		private function checkDnsOnly(): bool
		{
			if (!$this->isDnsOnly()) {
				return true;
			}

			$path = null;
			foreach ([Map::DOMAIN_MAP, Map::DOMAIN_TXT_MAP] as $map) {
				$path = Map::home($map);
				if (file_exists($path)) {
					break;
				}

			}

			// something's awry
			if (!$path) {
				return false;
			}

			return \count(Map::load($path, 'r-')->fetchAll()) === 0;
		}

		/**
		 * License locked to network
		 *
		 * @return bool
		 */
		public function hasNetRestriction(): bool
		{
			return (bool)$this->getPurposeField(4);
		}

		public function hasDomainCountRestriction(): bool
		{
			if ($this->isDnsOnly()) {
				return true;
			}

			return $this->getPurposeField(5) > 0;
		}

		private function checkDomainCount(): bool
		{
			if (!file_exists(Map::home(Map::DOMAIN_MAP))) {
				return true;
			}

			return \count(Map::load(Map::DOMAIN_MAP, 'r-')->fetchAll()) <= $this->getDomainLimit();
		}

		/**
		 * Check license supported languages
		 *
		 * @return bool
		 */
		private function checkLanguage(): bool
		{
			return true;
		}

		/**
		 * Get domain limit for license
		 *
		 * @return int
		 */
		public function getDomainLimit(): int
		{
			return (int)$this->getPurposeField(5);
		}

		/**
		 * Validate license within network restrictions
		 *
		 * @return bool
		 */
		private function checkNetRestriction(): bool
		{
			$ips = $this->getAuthorizedNetworks();
			if (!$ips) {
				return true;
			}
			$routed = inet_pton($this->gateway());
			foreach ($ips as [$min, $max]) {
				if ($min === $max) {
					// first verify IP is attached to server
					if (!Iface::bound($min)) {
						continue;
					}
					// and that gateway is in /24
					$maskFn = 'calcV' . (false === strpos($min, ':') ? '4' : '6') . 'Mask';
					$min = $this->inetNtop($this->inetPton($max) & $this->$maskFn(24));
					$max = $this->netRange($max, 24);
				}

				if ($routed >= inet_pton($min) && $routed <= inet_pton($max)) {
					return true;
				}
			}
			\error('Failed to find a valid IP address for this license');
			return false;
		}

		/**
		 * Get networks license is authorized for
		 *
		 * @return array|null
		 */
		public function getAuthorizedNetworks(): ?array
		{
			if ( !($nets = $this->getPurposeField(4)) ) {
				return null;
			}

			return array_map([$this, 'cidr2Range'], $this->getPurposeField(4));
		}

		private function cidr2Range(string $cidr): array
		{
			if (false === strpos($cidr, '/')) {
				return [$cidr, $cidr];
			}
			[$net, $mask] = explode('/', $cidr);
			return [
				$net,
				$this->netRange($net, (int)$mask)
			];
		}

		/*
		 * **************************************************
		 * Tamper-resistant self contained helper functions *
		 * **************************************************
		 */

		/**
		 * Get public gateway
		 *
		 * Extracted from Opcenter\Net\Iface to prevent tampering
		 *
		 * @return string
		 */
		private function gateway(): string
		{
			$proc = '/proc/net/route';
			if (!file_exists($proc)) {
				fatal('route missing from /proc - is procfs mounted under /proc?');
			}

			$ifaces = array_map(static function ($line) {
				return preg_split('/\s+/', $line);
			}, file($proc, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES));
			$active = array_filter($ifaces, static function ($line) {
				return strspn($line[1], '0') === \strlen($line[1]);
			});
			if (!$active) {
				fatal('no public interface configured with route to 0.0.0.0');
			}
			$hex = array_reverse(str_split(array_pop($active)[2], 2));

			return implode('.', array_map('hexdec', $hex));
		}

		private function netRange(string $net, int $mask): string
		{
			$maskFn = 'calcV' . (false === strpos($net, ':') ? '4' : '6') . 'Mask';
			return $this->inetNtop($this->inetPton($net) | ~($this->$maskFn($mask)));
		}

		/**
		 * Calculate IPv4 mask
		 *
		 * @param int $cidr mask range
		 * @return string
		 */
		private function calcV4Mask(int $cidr): string
		{
			$netmask = ((1 << 32) - 1) << (32 - $cidr);
			return $this->inetPton(long2ip($netmask));
		}

		/**
		 * Calculate IPv6 mask
		 *
		 * @param int $cidr
		 * @return string
		 */
		private function calcV6Mask(int $cidr): string
		{
			$hosts = (128 - $cidr);
			$networks = 128 - $hosts;
			$_m = str_repeat('1', $networks) . str_repeat('0', $hosts);
			$hexmask = null;
			foreach (str_split($_m, 4) as $segment) {
				$hexmask .= base_convert($segment, 2, 16);
			}
			$mask = substr(preg_replace('/([A-f0-9]{4})/', '$1:', $hexmask), 0, -1);

			return $this->inetPton($mask);
		}

		/**
		 * Convert binary IP address into human-readable string
		 *
		 * @param string $str
		 * @return string
		 */
		private function inetNtop(string $str): string
		{
			return inet_ntop(pack('a' . \strlen($str), $str));
		}

		/**
		 * Convert human-readable IP address into binary representation
		 *
		 * @param string $ip
		 * @return string
		 */
		private function inetPton(string $ip): string
		{
			if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
				return current(unpack('a4', inet_pton($ip)));
			} else if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
				return current(unpack('a16', inet_pton($ip)));
			}
			fatal('Invalid address detected');
			exit();
		}
	}
