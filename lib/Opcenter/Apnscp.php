<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace Opcenter;

	use Opcenter\System\GenericSystemdService;

	class Apnscp extends GenericSystemdService
	{
		protected const SERVICE = 'apiscp';
		public const PID = '/storage/run/apnscpd.pid';
		public const HOUSEKEEPER_PID = 'housekeeper.pid';

		/**
		 * Restart apnscp
		 *
		 * @param string $timespec
		 * @return bool
		 */
		public static function restart(string $timespec = null): bool
		{
			if (null === $timespec) {
				$timespec = 'now';
			}

			if ($timespec === 'now') {
				$proc = new \Util_Process_Fork();
			} else {
				$proc = new \Util_Process_Schedule($timespec);
				$proc->setID(self::getScheduleKey());

				if ($proc->preempted()) {
					return true;
				}
				info('%s will restart in %s', PANEL_BRAND, $timespec);
			}
			static::reset();
			$cmd = static::getCommand('restart');
			$proc->setOption('suid', 0);
			$proc->setOption('sgid', 0);
			$res = $proc->run(
				$cmd,
				array('mute_stdout' => true)
			);

			if ($timespec !== 'now') {
				return $res['success'];
			}

			usleep(500000);
			return self::wait();
		}

		/**
		 * Cancel a pending restart
		 *
		 * @return bool
		 */
		public static function cancelPendingRestart(): bool
		{
			$proc = new \Util_Process_Schedule;

			if ($job = $proc->idPending(self::getScheduleKey())) {
				return $proc->cancelJob($job['job']);
			}

			return false;
		}

		/**
		 * Wait for backend activation
		 *
		 * @return bool
		 */
		public static function wait(): bool
		{
			$pid = INCLUDE_PATH . self::PID;
			$self = basename(PHP_BINARY);
			for ($i = 0; $i < 30; usleep(500000), $i++) {
				if (!file_exists($pid) || !file_exists(APNSCPD_SOCKET)) {
					continue;
				}

				// can't see without being part of procviz group, which we aren't
				if (!posix_geteuid() && !Process::pidMatches((int)file_get_contents($pid), $self)) {
					continue;
				}

				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
				try {
					$ds = \DataStream::get();
					if (0 === strncmp((string)@$ds->writeSocket('PING' . \DataStream::MAGIC_MARKER), 'PONG', 4)) {
						return true;
					}
				} catch (\apnscpException $e) {
					continue;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}
			}

			warn('Failed to query backend - is apnscpd dead?');
			return false;
		}

		public static function running(): bool
		{
			if (!file_exists(INCLUDE_PATH . self::PID)) {
				return false;
			}

			if (null === ($pid = self::pid())) {
				return false;
			}

			return \Opcenter\Process::pidMatches($pid, basename(PHP_BINARY));
		}

		public static function pid(): ?int
		{
			if (!file_exists(INCLUDE_PATH . self::PID)) {
				return null;
			}

			return (int)file_get_contents(INCLUDE_PATH . self::PID);
		}
	}