<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	namespace Opcenter;

	use Closure;

	class Process
	{
		/**
		 * @var int killWait increment in us
		 */
		const WAIT_INCREMENT = 250000;

		// @link http://linux.die.net/man/5/proc

		// ends on #25 - rsslim
		const STAT_FIELDS = [
			'pid',
			'comm',
			'stat',
			'ppid',
			'pgrp',
			'session',
			'tty_nr',
			'tpgid',
			'flags',
			'minflt',
			'cminflt',
			'majflt',
			'cmajflt',
			'utime',
			'stime',
			'cutime',
			'cstime',
			'priority',
			'nice',
			'num_threads',
			'itrealvalue',
			'starttime',
			'vsize',
			'rss',
			'rsslim'
		];

		const STAT_SCAN_SPEC =
			'%d %s %c %d %d' .
			'%d %d %d %u %lu' .
			'%lu %lu %lu %lu %lu' .
			'%ld %ld %ld %ld %ld' .
			'%ld %lu %lu %ld %lu';

		const STATM_FIELDS = [
			'size',
			'resident',
			'shared',
			'text',
			'lib',
			'data',
			'dt'
		];

		const STATM_SCAN_SPEC =
			'%lu %lu %lu %lu %lu' .
			'%lu %lu';

		/**
		 * Kill process, wait for exit
		 *
		 * @param int $pid
		 * @param int $signal
		 * @param int $wait
		 * @return bool
		 */
		public static function killWait(int $pid, int $signal, int $wait = 30): bool
		{
			if (!self::kill($pid, $signal)) {
				return false;
			}
			$wait *= floor(1000000 / self::WAIT_INCREMENT);
			for ($i = 0; $i < $wait; $i++) {
				if (!posix_kill($pid, 0)) {
					return true;
				}
				usleep(self::WAIT_INCREMENT);
			}

			return false;
		}

		public static function kill(int $pid, int $signal): bool
		{
			return posix_kill($pid, $signal);
		}

		/**
		 * Send signal to all tasks in tree
		 *
		 * @param int $pgid process group ID
		 * @param int $signal
		 * @return bool
		 */
		public static function killTree(int $pgid, int $signal): bool
		{
			if (!is_dir("/proc/${pgid}/task")) {
				return error("PID `%d' does not exist", $pgid);
			}

			Filesystem::readdir("/proc/${pgid}/task", static function ($pid) use ($pgid, $signal) {
				// flip order so parent cannot respawn?
				$pid = (int)$pid;
				$cfile = "/proc/${pgid}/task/${pid}/children";
				if (is_file($cfile) && ($contents = @file_get_contents($cfile))) {
					$children = explode(' ', $contents);
					foreach ($children as $c) {
						if ($c) {
							static::killTree((int)$c, $signal, $pid);
						}
					}
				}
			});

			return self::kill($pgid, $signal);
		}

		/**
		 * Process group children
		 *
		 * @param int $pgid
		 * @return array
		 */
		public static function children(int $pgid): array
		{
			$children = [];
			Filesystem::readdir("/proc/${pgid}/task", static function ($pid) use ($pgid, &$children) {
				// flip order so parent cannot respawn?
				$pid = (int)$pid;
				$file = "/proc/${pgid}/task/${pid}/children";
				if (is_file($file) && ($contents = rtrim((string)@file_get_contents($file), ' '))) {
					$new = explode(' ', $contents);
					foreach ($new as $c) {
						$children = array_merge($children, [(int)$c], static::children($c));
					}
				}
			});

			return $children;
		}

		/**
		 * Send signal as user
		 *
		 * @param int    $pid
		 * @param int    $signal
		 * @param int|string $user
		 * @return bool
		 */
		public static function killAs(int $pid, int $signal, $user = APNSCP_SYSTEM_USER): bool
		{
			if (\is_string($user)) {
				if (false === ($pwd = posix_getpwnam($user))) {
					fatal("Unknown system user `%s'", $user);
				}
				$user = $pwd['uid'];
			}
			// catching a signal here would force the panel to operate as the effective uid
			// turn off asynchronous handling until signal is processed
			$async = pcntl_async_signals(false);

			// via kill(2)
			// ... or the real or effective user ID of the sending process
			// must equal the real or saved set-user-ID of the target process
			$proc = new \Util_Process();
			$proc->setOption('suid', $user);
			$proc->setOption('mute_stderr', true);
			$ret = $proc->run('kill -%d %d', $signal, $pid);
			pcntl_async_signals($async);
			pcntl_signal_dispatch();
			return array_get($ret, 'success', false);
		}

		/**
		 * Process exists
		 *
		 * @param $pid
		 * @return bool
		 */
		public static function exists(int $pid): bool
		{
			return is_dir('/proc/' . $pid);
		}

		/**
		 * Verify process matches process name
		 *
		 * @param int    $pid
		 * @param string $name absolute path or basename
		 * @return bool
		 */
		public static function pidMatches(int $pid, string $name): bool
		{
			$cmd = '/proc/' . $pid . '/exe';
			if (!is_link($cmd)) {
				return false;
			}
			$link = @readlink($cmd);
			// previously purged link
			if (!file_exists($link) && substr($link, -10) === ' (deleted)') {
				warn("PID %d - original process executable `%s' deleted", $pid, $link);
				$link = substr($link, 0, -10);
			}
			if (!$link) {
				return false;
			}

			if ($name[0] !== '/') {
				$link = basename($link);
			}

			return fnmatch($name, $link);
		}

		/**
		 * Match processes owned by user
		 *
		 * @param int|string $user username
		 * @return array
		 */
		public static function matchUser($user): array
		{
			if (!is_int($user)) {
				$pwd = posix_getpwnam($user);
				if (!$pwd) {
					fatal("getpwnam failed for user `%s'", $user);
				}
				$uid = $pwd['uid'];
			} else {
				$uid = $user;
			}

			return array_filter(Filesystem::readdir('/proc', static function ($file) use ($uid) {
				return @fileowner('/proc/' . $file) === $uid ? (int)$file : false;
			}));
		}

		/**
		 * Match processes owned by group
		 *
		 * @param string $group group name
		 * @return array
		 */
		public static function matchGroup(string $group): array
		{
			$grp = posix_getgrnam($group);
			if (!$grp) {
				fatal("getpwnam failed for group `%s'", $group);
			}
			$gid = $grp['gid'];

			return array_filter(Filesystem::readdir('/proc', static function ($file) use ($gid) {
				return @filegroup('/proc/' . $file) === $gid ? (int)$file : false;
			}));
		}

		/**
		 * Kill processes held by group ID
		 *
		 * @param $group
		 * @return array
		 */
		public static function killGroup($group): bool {
			if (\is_string($group)) {
				$grp = posix_getgrnam($group);
				if (!$grp) {
					fatal("getpwnam failed for group `%s'", $group);
				}
				$group = $grp['gid'];
			}
			if (false === posix_getgrgid($group)) {
				return error('getgrgid failed - unknown id %d', $grp);
			}

			$ret = \Util_Process::exec('/usr/bin/pkill -SIGKILL -G %d', $group, [0, 1]);
			return $ret['success'] ?: error('pkill failed: %s', $ret['stderr']);
		}

		/**
		 * Kill processes held by user ID
		 *
		 * @param string|int $user i
		 * @param int|null $gid optional GID to match
		 * @return array
		 */
		public static function killUser($user, int $gid = null): bool
		{
			if (\is_string($user)) {
				$pwd = posix_getpwnam($user);
				if (!$pwd) {
					fatal("getpwnam failed for user `%s'", $user);
				}
				$user = $pwd['uid'];
			}
			if (0 === $user) {
				return error('Cannot kill root!');
			}
			$gidFlag = null;
			if (null !== $gid) {
				$gidFlag = '-G';
			}
			$ret = \Util_Process::exec('/usr/bin/pkill -SIGKILL -U %d %s %d', $user, $gidFlag, $gid, [0, 1]);

			return $ret['success'] ?: error('pkill failed: %s', $ret['stderr']);
		}

		/**
		 * Set ppid uid as euid if uid is root
		 *
		 * @return int|null new uid or null if no change
		 */
		public static function dropEffectiveUid(): ?int
		{
			// already dropped
			if (0 !== posix_geteuid()) {
				return null;
			}
			$ppid = posix_getppid();
			if (false === ($uid = fileowner("/proc/${ppid}"))) {
				warn('Parent pid %d exited - could not inherit UID, defaulting to %s UID', $ppid, APNSCP_SYSTEM_USER);
				$uid = posix_getpwnam(APNSCP_SYSTEM_USER)['uid'] ?? 0;
			}
			posix_seteuid($uid);
			return $uid;
		}

		/**
		 * Get process stats
		 *
		 * @param int|int[] $pids process id(s) to lookup
		 * @return array|null
		 */
		public static function stat($pids): ?array
		{
			$procs = [];
			foreach ((array)$pids as $pid) {
				$path = '/proc/' . $pid;
				//$cmd = file_get_contents($path . '/cmdline');
				if (!file_exists($path)) {
					continue;
				}
				$user = fileowner($path);
				$cwd = readlink($path . '/cwd');
				$cmdline = file_get_contents($path . '/cmdline');
				$statraw = file_get_contents($path . '/stat');

				$scanraw = sscanf($statraw, static::STAT_SCAN_SPEC); // read up to rsslim
				$stat = array_combine(
					static::STAT_FIELDS,
					$scanraw
				);
				$stat['user'] = $user;
				$stat['cwd'] = $cwd;
				$stat['comm'] = substr($stat['comm'], 1, -1);
				$stat['utime'] /= CPU_CLK_TCK;
				$stat['stime'] /= CPU_CLK_TCK;
				$stat['cutime'] /= CPU_CLK_TCK;
				$stat['cstime'] /= CPU_CLK_TCK;
				$stat['starttime'] /= CPU_CLK_TCK;
				$stat['startutime'] = round($stat['starttime']);
				// drop from bytes to KB
				$stat['vsize'] /= 1024;
				$stat['rss'] *= MEM_PAGESIZE;
				$stat['rsslim'] /= 1024;
				$stat['args'] = explode("\0", $cmdline, -1);
				unset($stat['args'][0]);

				$procs[$stat['pid']] = $stat;
			}

			return !\is_array($pids) ? (current($procs) ?: null) : $procs;
		}

		/**
		 * Get process memory stats
		 *
		 * @param int|int[] $pids process id(s) to lookup
		 * @return array|null
		 */
		public static function statm($pids): ?array
		{
			$procs = [];
			foreach ((array)$pids as $pid) {
				$path = '/proc/' . $pid;
				if (!file_exists($path)) {
					continue;
				}

				$statraw = file_get_contents($path . '/statm');
				$scanraw = sscanf($statraw, static::STATM_SCAN_SPEC);

				$stat = array_combine(
					static::STATM_FIELDS,
					$scanraw
				);
				foreach ($stat as &$val) {
					// all values in kB
					$val *= MEM_PAGESIZE;
				}

				$procs[$pid] = $stat;
			}

			return !\is_array($pids) ? (current($procs) ?: null) : $procs;
		}

		/**
		 * @param int          $pid process identifier
		 * @param Closure|null $cb
		 * @return array
		 */
		public static function descriptors(int $pid, \Closure $cb = null): array
		{
			if (!is_dir("/proc/${pid}/fd")) {
				return [];
			}

			$dh = opendir("/proc/${pid}/fd");
			$files = [];

			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				}

				$referent = readlink("/proc/${pid}/fd/${file}");
				if (null === $cb || $cb($file, $referent)) {
					$files[$file] = $referent;
				}

			}

			closedir($dh);

			return $files;
		}

	}