<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Opcenter\Dns;

use Illuminate\Support\Collection;
use Module\Provider;
use Opcenter\Account\Enumerate;

class Bulk
{
	/**
	 * @var Collection[apnscpFunctionInterceptor]
	 */
	protected Collection $sites;

	public function __construct(array $sites = null) {
		if (null === $sites) {
			$sites = Enumerate::sites();
		}

		$this->sites = collect($sites)->map(static function ($site) {
			if (!$site instanceof \Auth_Info_User) {
				if ($site instanceof \Auth_Info_Account) {
					$site = $site->site;
				}
				if (!($site = \Auth::nullableContext(null, $site))) {
					return null;
				}
			}
			$afi = \apnscpFunctionInterceptor::factory($site);

			// allow direct writes to SOA RR within dns module context
			$module = \Dns_Module::autoloadModule($site);
			$rfxn = new \ReflectionProperty($module, 'permission_level');
			$rfxn->setAccessible(true);
			// @XXX potentially dangerous if this value ever gets cached
			$rfxn->setValue($module, $rfxn->getValue($module) | PRIVILEGE_ADMIN);
			$afi->swap('dns', $module);

			return $afi->dns_configured() ? $afi : null;
		})->filter();
	}

	/**
	 * Add record
	 *
	 * @param Record        $r
	 * @param \Closure|null $where optional closure to evaluate application
	 * @return $this
	 */
	public function add(Record $r, \Closure $where = null): self {
		$i = 0;
		$this->sites->each(function (\apnscpFunctionInterceptor $afi) use ($r, &$i, $where) {
			$domains = array_keys($afi->web_list_domains());
			$ttl = $r['ttl'] ?? $afi->dns_get_default('ttl');
			debug("%d/%d addition - %d domains in batch", ++$i, $this->sites->count(), \count($domains));
			foreach($domains as $domain) {
				if ($afi->dns_parented($domain)) {
					continue;
				}

				if ($where) {
					$rCheck = clone $r;
					$rCheck['zone'] = $domain;
					if (!$where($afi, $rCheck)) {
						debug("Skipping %s - failed assertion", $domain);
						continue;
					}
				}

				$ret = false;
				try {
					$ret = $afi->dns_add_record(
						$domain,
						$r['name'],
						$r['rr'],
						$r['parameter'],
						$ttl
					);
				} catch (\Throwable $e) {
					warn("Failed on %(domain)s (provider: %(provider)s): %(msg)s", [
						'domain' => $domain,
						'provider' => $afi->dns_get_provider(),
						'msg' => $e->getMessage()
					]);
				} finally {
					if (!$ret) {
						warn(
							"Failed to set record on %s",
							$domain
						);
					}
				}
			}
		});

		return $this;
	}

	/**
	 * Remove record
	 *
	 * @param Record   $r
	 * @param \Closure|null $where optional closure to evaluate application
	 * @return $this
	 */
	public function remove(Record $r, \Closure $where = null): self {
		$i = 0;
		$this->sites->each(function (\apnscpFunctionInterceptor $afi) use ($r, &$i, $where) {
			$domains = array_keys($afi->web_list_domains());
			debug("%d/%d removal - %d domains in batch", ++$i, $this->sites->count(), \count($domains));
			foreach ($domains as $domain) {
				if ($afi->dns_parented($domain)) {
					continue;
				}

				if ($where) {
					$rCheck = clone $r;
					$rCheck['zone'] = $domain;
					if (!$where($afi, $rCheck)) {
						debug("Skipping %s - failed assertion", $domain);
						continue;
					}
				}

				$ret = false;
				try {
					$ret = $afi->dns_remove_record(
						$domain,
						$r['name'],
						$r['rr'],
						$r['parameter']
					);
				} catch (\Throwable $e) {
					warn("Failed on %(domain)s (provider: %(provider)s): %(msg)s", [
						'domain' => $domain,
						'provider' => $afi->dns_get_provider(),
						'msg' => $e->getMessage()
					]);
				} finally {
					if (!$ret) {
						warn(
							"Failed to delete record on %s",
							$domain
						);
					}
				}
			}
		});

		return $this;
	}

	/**
	 * Replace DNS record
	 *
	 * NB: $zone field in $replace is substituted with current domain
	 *
	 * @param Record          $search  record to match. name, rr, and parameter must be specified
	 * @param Record|\Closure $replace record to replace or closure to modify result
	 * @return $this
	 */
	public function replace(Record $search, $replace): self
	{
		if (!$replace instanceof \Closure && !$replace instanceof Record) {
			fatal("\$replace is invalid argument type. Must be Closure or Record");
		}
		$i = 0;

		if (!$replace instanceof \Closure) {
			$replacementSpec = clone $replace;
			$replace = static function (\apnscpFunctionInterceptor $afi, Record $r) use ($search, $replacementSpec) {
				$zone = $r['zone'];
				$search['zone'] = $zone;
				if (!$r->is($search)) {
					return false;
				}

				$r->merge($replacementSpec);
				$r['zone'] = $zone;
				return true;
			};
		}

		$this->sites->each(function (\apnscpFunctionInterceptor $afi) use (&$i, $search, $replace) {
			$domains = array_keys($afi->web_list_domains());
			debug(
				"%d/%d batch - %d domains in site %s",
				++$i,
				$this->sites->count(),
				\count($domains),
				$afi->common_get_service_value('siteinfo', 'domain'),
			);
			$set = [];
			foreach ($domains as $domain) {
				if ($afi->dns_parented($domain)) {
					continue;
				}

				$originalRecord = clone $search;
				$originalRecord['zone'] = $domain;

				// callback determines fitness
				$records = $afi->dns_get_records($search['subdomain'] ?? null, $search['rr'] ?? 'any', $domain);
				if (false === $records) {
					error("API problem. Skipping %s", $domain);
					continue;
				}
				foreach ($records as $apiResult) {
					$this->stageMergedRecord($originalRecord, $apiResult);

					$newRecord = clone $originalRecord;

					if (false === ($replace($afi, $newRecord))) {
						debug("Skipping %s - failed assertion", $originalRecord->hostname());
						continue;
					}

					// updating meta updates "parameter" verbatim, which is a composition
					// of meta values. Pluck meta, whose keys are in order,
					// then create a new record to normalize components (e.g. add period to end of hostname)
					$provider = $afi->dns_get_provider();
					$class = Provider::getClass('dns', $provider, 'Module');
					$newRecord = $class::createRecord($domain, $newRecord->toArray());
					if ($originalRecord->toArray() == $newRecord->toArray()) {
						debug("Skipping %s - new record same as previous", $originalRecord->hostname());
						continue;
					}
					$set[] = [$originalRecord, $newRecord];
				}

			}

			foreach ($set as $pair) {
				if (!$this->replaceRecord($afi, ...$pair)) {
					warn(
						"Failed to set record on %s",
						$pair[0]->getZone()
					);
				}
			}
		});

		return $this;
	}

	/**
	 * Prepare record from results
	 *
	 * @param Record $r
	 * @param array  $api
	 */
	private function stageMergedRecord(Record $r, array $api): void
	{
		if (isset($api['name'])) {
			$api['name'] = array_pull($api, 'subdomain', '');
		}
		$domain = $api['domain'];
		unset($api['domain']);

		$r->merge(
			new Record($domain, $api)
		);
	}
	/**
	 * Perform per-record replacement
	 *
	 * @param \apnscpFunctionInterceptor $afi
	 * @param Record                     $old
	 * @param Record                     $new
	 * @return bool
	 */
	private function replaceRecord(\apnscpFunctionInterceptor $afi, Record $old, Record $new): bool
	{
		$ttl = $new['ttl'] ?? $old['ttl'] ?? $afi->dns_get_default('ttl');
		try {
			$ret = $afi->dns_modify_record(
				$old->getZone(),
				$old['name'],
				is_null($old['rr']) || $old['rr'] === 'ANY' ? $new['rr'] : $old['rr'],
				$old['parameter'],
				[
					'name'      => $new['name'] ?? $old['name'],
					'rr'         => $new['rr'] ?? $old['rr'],
					'parameter' => $new['parameter'] ?? $old['parameter'],
					'ttl'       => $new['ttl'] ?? $ttl
				]
			);
			if ($ret) {
				info("Changed %(hostname)s (RR: %(rr)s, parameter: %(parameter)s)", [
					'hostname' => $old->hostname(),
					'rr' => $old['rr'],
					'parameter' => $old['parameter']
				]);
			}

			return $ret;
		} catch (\Throwable $e) {
			warn("Failed on %(domain)s (provider: %(provider)s): %(msg)s", [
				'domain'   => $old->getZone(),
				'provider' => $afi->dns_get_provider(),
				'msg'      => $e->getMessage()
			]);
			return false;
		}
	}
}
