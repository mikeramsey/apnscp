<?php
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

	declare(strict_types=1);

	namespace Opcenter\Dns\Contracts;

	use Opcenter\Service\ConfigurationContext;

	/**
	 * Interface ServiceProvider
	 *
	 * Service modules
	 *
	 * @package Opcenter\Dns\Contracts
	 */
	interface ServiceProvider
	{
		public function valid(ConfigurationContext $ctx, &$var): bool;
	}