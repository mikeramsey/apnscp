<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	/**
	 * Facade functions to hook into VFS
	 */

	namespace Opcenter\Contracts;

	interface VirtualizedContextable
	{
		public static function instantiateContexted(\Auth_Info_User $context);
	}


