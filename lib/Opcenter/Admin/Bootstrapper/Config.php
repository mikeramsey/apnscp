<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Bootstrapper;

	use Illuminate\Contracts\Support\Arrayable;
	use Opcenter\Admin\Bootstrapper;
	use Symfony\Component\Yaml\Yaml;

	class Config implements \ArrayAccess, Arrayable
	{
		const CONFIG_MAPS = [
			Bootstrapper::RUNTIME_VARS                         => 'write',
			'/root/apnscp-vars.yml'                            => 'private',
			'playbooks/roles/common/vars/apnscp-internals.yml' => 'defaults',
			'playbooks/apnscp-vars.yml'                        => 'defaults',
		];


		protected $dirty;
		protected $cfg = [];
		protected $defaults = [];
		protected $saveFile;

		//@var bool perform full read of Yaml, including comments
		protected $full = false;
		//@var raw data to track
		protected $raw = [];

		/**
		 * Config constructor.
		 *
		 * @param bool $full perform full read, including comments
		 */
		public function __construct(bool $full = false)
		{
			$this->full = $full;
			foreach (self::CONFIG_MAPS as $file => $type) {
				if ($file[0] !== '/') {
					$file = resource_path($file);
				}
				$yaml = [];
				if (file_exists($file)) {
					if ($full) {
						$this->raw[$file] = file_get_contents($file);
						$yaml = Yaml::parse($this->raw[$file]);
					} else {
						$yaml = Yaml::parseFile($file);
					}
				}

				if ($type === 'defaults') {
					$this->defaults += $yaml;
				} else if ($type === 'write') {
					$this->saveFile = $file;
				}


				$this->cfg += (array)$yaml;
			}
			if (null === $this->saveFile) {
				warn('No save file (write access) configured in CONFIG_MAPS. Saving Yaml is not possible');
			}
		}

		public function __debugInfo()
		{
			return $this->cfg;
		}

		/**
		 * Config is default
		 *
		 * @param string $key
		 * @return bool
		 */
		public function isDefault(string $key): bool
		{
			$cfg = $this->cfg[$key] ?? null;
			if (!isset($this->cfg[$key]) && $this->raw) {
				foreach ($this->raw as $file => $directives) {
					if (isset($directives[$key])) {
						$cfg = $directives[$key];
						break;
					}
				}
			}

			return !isset($this->cfg[$key]) || $this->defaults[$key] === $cfg;
		}

		/**
		 * Get default setting
		 *
		 * @param string $key
		 * @return mixed|null
		 */
		public function getDefault(string $key)
		{
			return $this->defaults[$key] ?? null;
		}

		/**
		 * Get default value
		 *
		 * @param string $key
		 * @return mixed|null
		 */
		public function default(string $key)
		{
			return $this->defaults[$key] ?? null;
		}

		/**
		 * Prevent automatic saving of config
		 *
		 * @return self
		 */
		public function freeze(): self
		{
			$this->dirty = false;

			return $this;
		}

		public function __destruct()
		{
			if (!$this->dirty) {
				return;
			}

			$this->sync();
		}

		public function sync() {
			if (!$this->saveFile) {
				fatal('Configuration dirty, no save file set, freeze() not called');
			}
			$yaml = Yaml::dump($this->cfg, 2, 2, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK | Yaml::DUMP_OBJECT_AS_MAP);
			if (file_put_contents($this->saveFile, $yaml, LOCK_EX)) {
				$this->dirty = null;
			}
		}

		/**
		 * Load merged configuration from role
		 *
		 * @param string $role
		 * @param string $file
		 * @return array
		 */
		public function loadRole(string $role, string $file = 'defaults/main'): array
		{
			$path = Bootstrapper::rolePath($role) . '/' . preg_replace('/\.yml$/', '', $file) . '.yml';
			if (!file_exists($path)) {
				debug("File `%s' does not exist", $path);
				return [];
			}

			if ($this->full) {
				$this->raw[$path] = file_get_contents($path);
				$yaml = (array)Yaml::parse($this->raw[$path]);
			} else {
				$yaml = (array)Yaml::parseFile($path);
			}

			if (0 === strncmp($file, 'defaults/', 9)) {
				$this->defaults += $yaml;
			}

			return $yaml;
		}

		/**
		 * Get comments from configuration
		 *
		 * @return array
		 */
		public function getComments()
		{
			if (!$this->full) {
				warn('getComments() called without $full parse');

				return [];
			}

			$built = [];
			// @todo extract
			foreach ($this->raw as $comments) {
				$buffer = [];
				foreach (explode("\n", $comments) as $line) {
					if (!isset($line[1])) {
						continue;
					}
					if ($line[0] === '#') {
						if ($line[1] === '#') {
							// dump buffer
							$buffer = [];
							continue;
						}
						$buffer[] = substr($line, 2);
					} else if (ctype_alnum($line[0]) && false !== ($pos = strpos($line, ':'))) {
						$key = substr($line, 0, $pos);
						$built[$key] = implode(' ', $buffer);
						$buffer = [];
					}
				}
			}
			return $built;
		}

		/**
		 * Set YAML output file
		 *
		 * @param string $file
		 * @return Config
		 */
		public function setOutputFile(string $file): self
		{
			if ($file[0] !== '/') {
				$file = resource_path($file);
			}
			$this->saveFile = $file;

			return $this;
		}

		public function offsetExists($offset)
		{
			return array_has($this->cfg, $offset) || array_has($this->defaults, $offset);
		}

		public function offsetGet($offset)
		{
			$val = array_get($this->cfg, $offset, array_get($this->defaults, $offset));
			if ($val === 'no' || $val === 'No' || $val === 'NO') {
				return false;
			} else if ($val === 'yes' || $val === 'Yes' || $val === 'YES') {
				return true;
			}

			return $val;
		}

		public function offsetSet($offset, $value)
		{
			if (null === $this->dirty) {
				$this->dirty = true;
			}
			array_set($this->cfg, $offset, $this->coerce($value));
		}

		/**
		 * Convert yes/no into friendly Ansible values
		 *
		 * @param $value
		 * @return mixed
		 */
		private function coerce($value)
		{
			if (!\is_scalar($value) || is_numeric($value) || \is_bool($value)) {
				return $value;
			}
			$tmp = strtolower((string)$value);
			if ($tmp === 'no') {
				return false;
			} else if ($tmp === 'yes') {
				return true;
			}

			return $value;
		}

		public function offsetUnset($offset)
		{
			if (null === $this->dirty) {
				$this->dirty = true;
			}
			array_forget($this->cfg, $offset);
		}

		public function toArray()
		{
			return array_replace($this->defaults, $this->cfg);
		}


	}