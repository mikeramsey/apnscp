<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Auth;

	use GeoIp2\Exception\AddressNotFoundException;
	use GeoIp2\Exception\AuthenticationException;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Apnscp;
	use Symfony\Component\Yaml\Yaml;

	class GeoipKey implements SettingsInterface
	{
		public function set($val): bool
		{
			if (!is_array($val) && $val !== null) {
				return error("Geoip key must be id/key pair");
			}

			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			$path = config_path('auth.yaml');
			$cfg = Yaml::parseFile($path);

			if ($val === null) {
				$val = [
					'id' => null,
					'key' => null
				];
			} else if (!isset($val['id'])) {
				return error("Pass value as ['key': MAXMIND-KEY, 'id': USER-ID]");
			} else {
				try {
					(new \Util_Geoip($val['id'], $val['key']))->raw(\Auth::client_ip());
				} catch (AuthenticationException $e) {
					return error("Authentication error validating Maxmind key: %s", $e->getMessage());
				} catch (AddressNotFoundException $e) {
					// no-op
				} catch (\Exception $e) {
					return error("Failed to confirm Maxmind usage: %s", $e->getMessage());
				}
			}

			array_set($cfg, 'maxmind.id', $val['id']);
			array_set($cfg, 'maxmind.key', $val['key']);
			file_put_contents($path, Yaml::dump($cfg));
			return Apnscp::restart();
		}

		public function get()
		{
			if (!defined('MAXMIND_GEOIP_ID') || empty(MAXMIND_GEOIP_ID)) {
				return null;
			}
			return ['id' => MAXMIND_GEOIP_ID, 'key' => MAXMIND_GEOIP_KEY];
		}

		public function getHelp(): string
		{
			return 'Manage GeoIP id and key';
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			return null;
		}

	}
