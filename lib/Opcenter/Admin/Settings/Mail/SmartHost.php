<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SmartHost implements SettingsInterface
	{
		const AUTH = 'postfix_smarthost_credentials';
		const FIELDS = ['host', 'username', 'password'];

		public function set($host, ...$val): bool
		{
			$credentials = false;
			if ($host !== false && !($credentials = $this->validate([
					'host' => $host,
					'username' => $val[0] ?? null,
					'password' => $val[1] ?? null
				]))) {
				return false;
			}

			if ($credentials === $this->getCredentials()) {
				return true;
			}

			$cfg = new Config();

			$cfg['postfix_smarthost_enabled'] = $credentials !== false;
			if (!$credentials) {
				$credentials = array_fill_keys(self::FIELDS, null);
			}
			$cfg['postfix_smarthost_tls'] = 'encrypt';
			if (false !== strpos((string)$credentials['host'], ':465')) {
				warn('Smarthost configured to use SMTPS. This implies postfix_smtps_wrapper_mode=True.' .
					'Set postfix_smtps_wrapper_mode=False in cp.bootstrapper Scope if mail fails to securely connect.');
			}
			$cfg['postfix_smarthost_credentials'] = $credentials;
			$cfg->sync();

			Bootstrapper::run('mail/configure-postfix');

			return true;
		}

		/**
		 * Validate credentials
		 *
		 * @param array $creds credentials
		 * @return array|bool
		 */
		private function validate(array $creds) {
			foreach (self::FIELDS as $field) {
				if (!\array_key_exists($field, $creds)) {
					return error('%s is not present in credentials', $field);
				}
			}
			if (false !== ($pos = strpos($creds['host'], '['))) {
				if (false === strpos($creds['host'], ']')) {
					return error('Unbalanced hostname, missing ]');
				}
				if (0 !== $pos) {
					return error("Hostname must begin with [ - first char found `%s'", $creds['host'][0]);
				}
			}

			if (false !== strpos($creds['host'], ']') && false === strpos($creds['host'], '[')) {
				return error('Unbalanced hostname, missing [');
			}

			return $creds;
		}

		public function get()
		{
			if (!$credentials = $this->getCredentials()) {
				return null;
			}
			return array_replace(
				$credentials,
				[
					'password' => '<hidden>'
				]
			);
		}

		private function getCredentials() {
			$config = new Config();
			if (empty($config['postfix_smarthost_enabled'])) {
				return false;
			}

			return array_intersect_key(
				$config['postfix_smarthost_credentials'],
				array_flip(self::FIELDS)
			);
		}

		public function getHelp(): string
		{
			return 'Set next hop for all outbound email';
		}

		public function getValues()
		{
			return 'bool|hostname username password';
		}

		public function getDefault()
		{
			return false;
		}
	}