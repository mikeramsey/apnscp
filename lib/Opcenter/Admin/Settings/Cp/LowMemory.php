<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class LowMemory implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg['has_low_memory'] = (bool)$val;
			unset($cfg);
			Bootstrapper::run('apnscp/bootstrap', 'software/haproxy', 'mail/configure-dovecot', 'mail/configure-postfix', 'apache/modsecurity', 'clamav/setup');

			return true;
		}

		public function get()
		{
			$config = new Config();

			return $config['has_low_memory'];
		}

		public function getHelp(): string
		{
			return 'Enable low memory mode for miserly installs';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}