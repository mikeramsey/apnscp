<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\System\Systemd\Calendar;

	class NightlyUpdates implements SettingsInterface
	{
		public function set($val): bool
		{
			if (is_string($val) && $val === $this->get() || !is_string($val) && $val == $this->get()) {
				return true;
			}
			$cfg = new Config();
			if (!is_string($val)) {
				$val = (bool)$val;
			} else if (!Calendar::canVerify()) {
				warn("Cannot verify calendar format on OS");
			} else if (!Calendar::valid($val)) {
				return false;
			}

			$cfg['apnscp_nightly_update'] = $val;
			unset($cfg);
			Bootstrapper::run('apnscp/install-services');

			return true;
		}

		public function get()
		{
			$config = new Config();

			return $config['apnscp_nightly_update'];
		}

		public function getHelp(): string
		{
			return 'Enable ApisCP automatic updates';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}