<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class UpdatePolicy implements SettingsInterface
	{
		const UPDATE_POLICIES = [
			'default',
			'security',
			'security-severity',
			'minimal',
			'minimal-security',
			'minimal-security-severity',
			false
		];

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			if (!\in_array($val, static::UPDATE_POLICIES, true)) {
				return error("Unknown update policy `%s'", $val);
			}
			$cfg = new Config();
			$cfg['yum_update_policy'] = $val;
			unset($cfg);
			Bootstrapper::run('system/yum');

			return true;
		}

		public function get()
		{
			$config = new Config();

			return $config['yum_update_policy'];
		}

		public function getHelp(): string
		{
			return 'Yum update policy';
		}

		public function getValues()
		{
			return self::UPDATE_POLICIES;
		}

		public function getDefault()
		{
			return 'default';
		}
	}