<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin;

	use Illuminate\Contracts\Queue\ShouldQueue;
	use Lararia\JobDaemon;
	use Lararia\Jobs\BootstrapperTask;
	use Lararia\Jobs\Job;
	use Opcenter\Process;
	use Symfony\Component\Yaml\Yaml;

	class Bootstrapper
	{
		private const PID_FILE = '.ansible.lock';

		public const RUNTIME_VARS = '/root/apnscp-vars-runtime.yml';

		public const ROLE_HOME = 'playbooks/roles/';

		public const PLAYBOOK = 'bootstrap.yml';

		/**
		 * Run a bootstrapper job
		 *
		 * @param mixed $tasks tags are string, extra-vars passed as assoc
		 * @return bool
		 */
		public static function run(...$tasks): bool
		{
			$job = self::job(...$tasks);
			$job->dispatch();
			if (($job instanceof ShouldQueue) && JobDaemon::checkState()) {
				$frag = $job->tags() ? 'with roles: ' . implode(', ', $job->tags()) : '';
				info('Bootstrapper task running in background %s', $frag);
			}

			return true;
		}

		/**
		 * Create Bootstrapper job
		 *
		 * @param mixed ...$tasks
		 * @return BootstrapperTask
		 */
		public static function job(...$tasks): BootstrapperTask
		{
			$tags = [];
			$args = [];
			for ($i = 0, $n = \count($tasks); $i < $n; $i++) {
				$t = $tasks[$i];
				if (\is_string($t)) {
					if (\strlen($t) === strcspn($t, '*?[]')) {
						$tags[] = $t;
					} else {
						$tags = array_merge($tags, self::match($t));
					}
				} else if (\is_array($t)) {
					$args[] = json_encode($t);
				}
			}

			$job = Job::create(BootstrapperTask::class, static::PLAYBOOK, $args);
			if ($tags) {
				$job->setTags($tags);
			}

			return $job;
		}

		/**
		 * Expand role wildcards
		 *
		 * @param string $pattern
		 * @return array
		 */
		public static function match(string $pattern): array
		{
			if (\strlen($pattern) === strcspn($pattern, '*?[]')) {
				return [$pattern];
			}

			$roles = [];
			foreach (self::roles() as $role) {
				if (fnmatch($pattern, $role)) {
					$roles[] = $role;
				}
			}

			return $roles;
		}

		/**
		 * Get roles in chronological order
		 *
		 * @return array
		 */
		public static function roles(): array
		{
			$path = static::playbookPath();
			$yaml = Yaml::parseFile($path);
			return array_get($yaml, '0.roles', []);
		}

		/**
		 * Service is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{
			$pidPath = static::pidFile();
			if (!file_exists($pidPath)) {
				return false;
			}

			$pid = (int)file_get_contents($pidPath);

			if (!Process::exists($pid)) {
				return false;
			}
			$stat = Process::stat($pid);
			if (!$stat) {
				return false;
			}
			// disambiguate "python" commands by checking command-line
			return 0 === strncmp($stat['comm'], 'ansible', 7);
		}

		/**
		 * Get PID file
		 *
		 * @return string
		 */
		private static function pidFile(): string
		{
			return run_path(self::PID_FILE);
		}

		/**
		 * Path for given role
		 *
		 * @param string $role
		 * @return string
		 */
		public static function rolePath(string $role): string
		{
			return resource_path(self::ROLE_HOME . $role);
		}

		/**
		 * Path to playbook
		 *
		 * @return string
		 */
		public static function playbookPath(): string
		{
			return resource_path(dirname(self::ROLE_HOME)) . '/' . static::PLAYBOOK;
		}
	}