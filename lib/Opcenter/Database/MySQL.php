<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database;

	use Opcenter\Map;

	class MySQL extends DatabaseCommon implements DatabaseInterface
	{
		const PREFIX_MAP = 'mysql.prefixmap';
		const USER_MAP = 'mysql.usermap';

		const SVC_NAME = 'mysql';
		const ROOT_CONF = '/root/.my.cnf';
		const USER_TABLES = [
			'columns_priv',
			'db',
			'procs_priv',
			'tables_priv',
			'user'
		];
		const DB_TABLES = [
			'columns_priv',
			'db',
			'event',
			'host',
			'proc',
			'procs_priv',
			'tables_priv'
		];

		// @var array shorthand parameter => option mapping
		const OPTION_MAP = [
			'connections' => 'MAX_USER_CONNECTIONS',
			'query' => 'MAX_QUERIES_PER_HOUR',
			'updates' => 'MAX_UPDATES_PER_HOUR',
			'hourly' => 'MAX_CONNECTIONS_PER_HOUR',
			'issuer' => 'ISSUER',
			'subject' => 'SUBJECT'
		];

		static protected $db;

		/**
		 * Supports users on multiple hosts
		 *
		 * @return bool
		 */
		public static function hasHosts(): bool
		{
			return true;
		}

		/**
		 * Move database
		 *
		 * Database will retain filesystem association
		 *
		 * @param string $old old database name
		 * @param string $new new database name
		 * @return bool
		 */
		public static function moveDatabase(string $old, string $new): bool
		{
			$newpath = \Sql_Module::MYSQL_DATADIR . DIRECTORY_SEPARATOR . self::canonicalize($new);
			$oldpath = \Sql_Module::MYSQL_DATADIR . DIRECTORY_SEPARATOR . self::canonicalize($old);

			if (file_exists($newpath)) {
				return error("destination database path `%s' already exists", basename($newpath));
			}
			if (!file_exists($oldpath)) {
				return error("source database path `%s' does not exist", $oldpath);
			}
			$db = self::connectRoot();
			if (!$db) {
				fatal('failed to connect to mysql database as root');
			}
			$tables = self::getTablesFromDatabase($old);

			$fstBase = substr(\dirname(readlink($oldpath)), 0, -\strlen(\Sql_Module::MYSQL_DATADIR));

			if (trim($fstBase, '/') && !self::createDatabase($new, null, 'localhost', $fstBase)) {
				return error('Failed to create new database %s', $new);
			}

			foreach ($tables as $table) {
				$rs = $db->query('RENAME TABLE ' . $db->escape($old) . '.' . $db->escape($table) . ' TO ' .
					$db->escape($new) . '.' . $db->escape($table));
				if (!$rs) {
					return error('ABORTING! Failed to rename table from %(old)s to %(new)s: %(err)s',
						['old' => $old, 'new' => $new, 'err' => $db->error]);
				}
			}
			if (!is_link($oldpath)) {
				rename($oldpath, $newpath);
			} else {
				$target = readlink($oldpath);
				unlink($oldpath);

				if (\is_dir($target)) {
					$pwd = posix_getpwuid(fileowner($target));
					if (array_get($pwd, 'name') === \Opcenter\Provisioning\MySQL::SYS_USER) {
						// sanity check
						\Opcenter\Filesystem::rmdir($target);
					} else {
						warn("Ownership of `%(path)s' is `%(actual)s'. Expected `%(expected)s'. Skipping removal.",
							['path' => $target, 'actual' => array_get($pwd, 'name'), 'expected' => \Opcenter\Provisioning\MySQL::SYS_USER]);
					}
				}
			}

			$tables = $db->filterTables(static::DB_TABLES, 'db');
			foreach ($tables as $table) {
				$stmt = $db->prepare("UPDATE ${table} SET db = ? WHERE db = ?");
				$stmt->bind_param('ss', $new, $old);
				if (!$stmt->execute()) {
					return error("failed to update table `%s' grants on renaming `%s' to `%s'",
						$table, $old, $new);
				}
			}
			$db->close();

			return true;
		}

		/**
		 * Canonicalize a database to filesystem
		 *
		 * @param string $db
		 * @return string
		 */
		public static function canonicalize(string $db): string
		{
			if (self::version() >= 50100) {
				$db = str_replace(['-','.'], ['@002d','@002e'], $db);
			}

			return $db;
		}

		/**
		 * Get MySQL version
		 *
		 * @return int
		 */
		public static function version(): int
		{
			static $version;
			if (null !== $version) {
				return $version;
			}

			$db = \MySQL::initialize();
			$version = $db->server_version;
			if (false === strpos($db->server_info, 'MariaDB')) {
				return $version;
			}

			// via https://mariadb.com/kb/en/library/mariadb-vs-mysql-compatibility/
			$tokens = explode('-', $db->server_info);
			if (\count($tokens) !== 3) {
				return $version;
			}
			$str = substr($tokens[1], 0, strpos($tokens[1], '.', 3));
			$patch = (int)substr($tokens[1], \strlen($str) + 1);
			switch ($str) {
				case '10.0':
				case '10.1':
					return $version = 506 * 100 + $patch;
			}
			// naieve but allows us to approximate the version
			// so long as patch doesn't exceed 10
			$minor = substr($str, strpos($str, '.')+1);
			return $version = 507 * 100 + ($minor*10 + min($patch, 9));
		}

		/**
		 * Connect as root
		 *
		 * @return \MySQL
		 */
		protected static function connectRoot()
		{
			/**
			 * @var \MySQL
			 */
			static $connection;
			if (null !== $connection) {
				$connection->ping();
				$connection->select_db('mysql');

				return $connection;
			}
			$connection = \MySQL::initialize(
				'localhost',
				\Sql_Module::MASTER_USER,
				self::rootPassword(),
				'mysql'
			);
			if (!$connection->connected()) {
				fatal('failed to connect to mysql database as root');
			}

			return $connection;
		}

		/**
		 * Retrieve root password from /root/.my.cnf
		 *
		 * @return string|null password
		 */
		public static function rootPassword()
		{
			// @xxx maybe change?
			static $password;
			if (null !== $password) {
				return $password;
			} else if (posix_geteuid()) {
				$password = \DataStream::get()->query('mysql_get_elevated_password_backend');
				return $password;
			}
			if (!file_exists(self::ROOT_CONF)) {
				fatal('master my.cnf missing, set %s', self::ROOT_CONF);
			}
			$ini = parse_ini_file(self::ROOT_CONF, true);
			$password = array_get($ini, 'client.password', null);

			return $password;
		}

		/**
		 * Rename a user
		 *
		 * @param string      $old old username
		 * @param string      $new new username
		 * @param string|null $host optional host to match
		 * @param string|null $newhost optional host to rename to
		 * @return bool
		 */
		public static function renameUser(string $old, string $new, string $host = null, string $newhost = null): bool
		{
			$db = self::connectRoot();
			if ($old === $new && (!$newhost || $host === $newhost)) {
				// no op
				return true;
			}
			foreach ([$old, $new] as $user) {
				if (!preg_match(\Regex::SQL_USERNAME, $user)) {
					return error("Cannot rename user - invalid user `%s'", $user);
				} else if (\strlen($user) > ($len = static::fieldLength('user'))) {
					fatal('User exceeds max length %d', $len);
				}
			}
			$hosts = static::findUser($old);
			if ($host) {
				if (!\in_array($host, $hosts, true)) {
					return error("No matching user for `%s' on `%s'", $old, $host);
				}
				$hosts = [$host];
			}
			$safe = [
				'old' => $db->escape_string($old),
				'new' => $db->escape_string($new)
			];

			foreach ($hosts as $host) {
				$safehost = $safenewhost = $db->escape_string($host);
				if ($newhost) {
					// if provided this is a single loop
					$safenewhost = $db->escape_string($newhost);
				}
				$query = "RENAME USER '" . $safe['old'] . "'@'" . $safehost . "' TO '" . $safe['new'] . "'@'" . $safenewhost . "'";
				if (!$db->query($query)) {
					return error("failed to rename user `%s'@`%s' to `%s'@`%s': %s",
						$old, $new, $db->error);
				}
			}
			$db->close();
			return true;
		}

		/**
		 * Find hosts matching user
		 * @param string      $user
		 * @param string|null $host optional host to match against
		 * @return array
		 */
		public static function findUser(string $user, string $host = null): array
		{
			$db = self::connectRoot();
			// MariaDB 10.4 case distinction
			$query = "SELECT host AS host FROM user WHERE user = '" . $db->escape_string($user) . "'";
			if ($host) {
				$query .= " AND host = '" . $db->escape_string($host) . "'";
			}
			return array_column($db->query($query)->fetch_all(MYSQLI_ASSOC) ?? [], 'host');
		}

		public static function dropDatabase(string $db): bool
		{
			$conn = self::connectRoot();
			$conn->query('DROP DATABASE `' . $conn->escape_string($db) . '`');
			if ($conn->error) {
				warn("failed to delete database `%s': (%d) %s", $db,
					$conn->errno,
					$conn->error
				);
			}

			return true;
		}

		/**
		 * @param string       $db
		 * @param null|string  $user optional user to apply grants
		 * @param string       $host
		 * @param string|null  $fstBase base directory to locate database
		 * @return bool|void
		 */
		public static function createDatabase(string $db, ?string $user, string $host, string $fstBase = null)
		{
			if (!preg_match(\Regex::SQL_DATABASE, $db)) {
				return error("invalid database name `%s'", $db);
			}
			if (static::databaseExists($db)) {
				return error("Database `%s' already exists", $db);
			}
			if ($user && !static::userExists($user, $host)) {
				return error("User `%s@%s' does not exist", $user, $host);
			}
			$conn = self::connectRoot();
			if ($fstBase) {
				if (!is_dir($fstBase)) {
					return error("Filesystem base path `%s' does not exist", $fstBase);
				} else if (!static::prepBackend($fstBase, $db)) {
					return error('Database creation failed');
				}
			} else if (!$conn->query('CREATE DATABASE `' . $conn->escape_string($db) . '`')) {
				return error('Failed to create database: %s', $conn->error);
			}

			if (null === $user) {
				return true;
			}
			$safeDb = preg_replace('/(?<!\\\\)_/', '\\_', $conn->escape_string($db));
			$query = 'GRANT ALL ON `' . $safeDb . "`.* to '" . $conn->escape_string($user) . "'@'" . $conn->escape_string($host) . "'";
			return $conn->query($query) || error('Failed to apply grants to %s: %s', $db, $conn->error);
		}

		/**
		 * Prep backend storage for database
		 *
		 * @param string $path
		 * @param string $db
		 * @return bool
		 */
		public static function prepBackend(string $path, string $db): bool {
			$dbcan = static::canonicalize($db);

			if (file_exists(\Mysql_Module::MYSQL_DATADIR . '/' . $dbcan)) {
				return error("database `%s' exists", $db);
			}
			if (!file_exists($path . \Mysql_Module::MYSQL_DATADIR)) {
				return error("base directory for MySQL doesn't exist in %s", $path);
			}
			$path .= \Mysql_Module::MYSQL_DATADIR . '/' . $dbcan;

			if (!file_exists($path)) {
				\Opcenter\Filesystem::mkdir($path, 'mysql', filegroup(\dirname($path)), 02750);
			}
			clearstatcache(true, \Mysql_Module::MYSQL_DATADIR . '/' . $dbcan);
			return symlink($path, \Mysql_Module::MYSQL_DATADIR . '/' . $dbcan);
		}

		/**
		 * Remove user
		 *
		 * @param string      $user user
		 * @param string|null $hostname hostname
		 * @param bool        $cascade cascade deletion to privileges
		 * @return bool
		 */
		public static function deleteUser(string $user, string $hostname = null, bool $cascade = true): bool
		{
			$db = self::connectRoot();
			if (!$hostname) {
				$hostname = self::findUser($user);
			}

			$xtraTables = [];
			if ($cascade) {
				$xtraTables = $db->filterTables(static::USER_TABLES, 'user', 'mysql');
			}

			foreach ((array)$hostname as $host) {
				if (!$db->query("DROP USER '" . $db->escape_string($user) . "'@'" . $db->escape_string($host) . "'")) {
					return error("Failed to delete '%s'@'%s': %s", $user, $host, $db->error);
				}
				foreach ($xtraTables as $table) {
					$q = "DELETE FROM ${table} WHERE user = '" . $db->escape_string($user) . "' AND host = '" . $db->escape_string($host) . "'";
					if (!$db->query($q)) {
						warn("Failed to remove user grants on %s from '%s'@'%s'", $table, $user, $host);
					}
				}
			}
			static::flush();
			$db->close();
			return true;
		}

		/**
		 * Unconditionally remove database grants
		 *
		 * @param string $db
		 * @return bool
		 */
		public static function dropDatabaseGrants(string $db): bool
		{
			$conn = self::connectRoot();
			$status = $conn->query("DELETE FROM db WHERE db = '" . $conn->escape_string($db) .
				"' OR db = '" . $conn->escape_string(preg_replace('/_/', '\\_', $db, 1)) . "'");
			return $status && $conn->affected_rows() > 0;
		}

		public static function alterUser(string $user, string $host, array $opts): bool
		{
			if (!preg_match(\Regex::SQL_USERNAME, $user)) {
				fatal("invalid user `%s'", $user);
			} else if (\strlen($user) > ($len = static::fieldLength('user'))) {
				fatal('User exceeds max length %d', $len);
			}

			$db = self::connectRoot();
			$query = "ALTER USER '${user}'@'" . $db->escape_string($host) . "'";
			if (isset($opts['password'])) {
				$tmp = '';
				if (static::passwordCrypted($opts['password'])) {
					$tmp .= 'PASSWORD ';
				}
				$query .= 'IDENTIFIED BY ' . $tmp . "'" . $db->escape_string($opts['password']) . "'";
			}
			$ssl = array_intersect_key($opts, array_fill_keys(['ssl', 'ssl_type', 'cipher', 'subject', 'issuer'], null));
			$limits = array_intersect_key($opts, array_fill_keys(['connections', 'updates', 'query', 'hourly'], null));
			if (false === ($fragment = static::convertOptionsToQuery($ssl, $limits))) {
				return false;
			}

			$query .= $fragment;
			$rs = $db->query($query);
			if (!$rs) {
				return error("failed to alter user `%s' on `%s'", $user, $host);
			}
			if (isset($opts['host'])) {
				return static::renameUser($user, $user, $host, $opts['host']);
			}
			return true;
		}

		/**
		 * Password already crypted
		 *
		 * @param string $password
		 * @return bool
		 */
		private static function passwordCrypted(string $password): bool
		{
			return strncmp($password, '*', 1) === 0 && \strlen($password) === 41 && \ctype_xdigit(\substr($password, 1));
		}

		/**
		 * @param string $user username
		 * @param string $password password
		 * @param string $hostname hostname
		 * @param array  $ssl ssl connection restrictions
		 * @param array  $limits resource limits
		 * @return bool
		 */
		public static function createUser(string $user, string $password, string $hostname = 'localhost', array $ssl = [], array $limits = []): bool
		{
			if (!preg_match(\Regex::SQL_USERNAME, $user)) {
				fatal("invalid user `%s'", $user);
			} else if (\strlen($user) > ($len = static::fieldLength('user'))) {
				fatal('User exceeds max length %d', $len);
			}

			$db = self::connectRoot();
			$ispass = static::passwordCrypted($password) ? 'PASSWORD' : '';
			$query = "CREATE USER '${user}'@'" . $db->escape_string($hostname) . "' IDENTIFIED BY $ispass '" .
				$db->escape_string($password) . "'";
			if (false === ($fragment = static::convertOptionsToQuery($ssl, $limits))) {
				return false;
			}

			$query .= $fragment;
			$rs = $db->query($query);
			if (!$rs) {
				return error("failed to create user `%s' on `%s'", $user, $hostname);
			}
			return true;
		}

		/**
		 * Get field maximum size
		 *
		 * @param string $field
		 * @param string $database
		 * @return int|null
		 */
		public static function fieldLength(string $field, string $database = 'mysql'): ?int
		{
			static $cache = array();
			if (!isset($cache[$database])) {
				$cache[$database] = [];
			}
			if (isset($cache[$database][$field])) {
				return $cache[$database][$field];
			}
			$db = self::connectRoot();

			$q = 'SELECT CHARACTER_MAXIMUM_LENGTH AS maxlen FROM INFORMATION_SCHEMA.COLUMNS WHERE ' .
				"TABLE_SCHEMA = '" . $db->escape($database) . "' AND TABLE_NAME = 'db' " .
				"AND COLUMN_NAME = '" . $db->escape($field) . "'";
			$res = $db->query($q);
			if (!$res) {
				error("schema inquiry failed, `%s'", $db->error);
				return null;
			}

			$rs = $res->fetch_object();

			return $cache[$database][$field] = (int)$rs->maxlen;
		}

		/**
		 * Convert SSL + resource limits into query fragment
		 *
		 * @param array $ssl
		 * @param array $limits
		 * @return bool|string
		 */
		private static function convertOptionsToQuery(array $ssl, array $limits = [])
		{
			$query = '';
			$db = self::connectRoot();

			foreach (['ssl' => $ssl, 'resources' => $limits] as $varGroup => $groupVars) {
				$groupSettings = [];
				$preamble = $varGroup === 'ssl' ? 'REQUIRE' : 'WITH';

				foreach ($groupVars as $opt => $val) {
					if ($val === '' || $val === null) {
						continue;
					}
					if (!\is_scalar($val)) {
						fatal("Invalid option value found for `%s': %s", $opt, $val);
					}

					if ($varGroup === 'ssl') {
						if ($opt === 'ssl_type') {
							if (!\in_array($val, ['SSL', 'X509', 'ANY'], true)) {
								return error("Unknown SSL option `%s'", $val);
							}
							if ($val === 'SSL' || $val === 'ANY') {
								// REQUIRE SSL
								array_set($groupSettings, 'use_ssl', $val === 'SSL' ? 'SSL' : 'NONE');
								$val = null;
							}
							$opt = '';
						} else if ($opt === 'ssl') {
							$val =  $val ? 'SSL' : 'NONE';
							$opt = '';
						}
					}

					if ($opt && \is_string($val)) {
						$val = "'" . $db->escape_string($val) . "'";
					}
					if (isset(static::OPTION_MAP[$opt])) {
						$opt = static::OPTION_MAP[$opt];
					}
					$groupSettings[$opt] = $val;
				}
				if ($groupSettings) {
					$join = $varGroup === 'ssl' ? ' AND ' : ' ';
					$query .= ' ' . $preamble . ' ' . implode($join, array_key_map(static function ($k, $v) {
						if ($v === null) {
							return $k;
						}
						return $k . ' ' . $v;
					}, $groupSettings));
				}
			}
			return $query;
		}

		/**
		 * Flush credential cache
		 *
		 * @return bool
		 */
		public static function flush(): bool
		{
			$db = self::connectRoot();

			return (bool)$db->query('FLUSH PRIVILEGES');
		}

		public static function userExists(string $user, string $host = null): bool
		{
			$db = self::connectRoot();
			$query = "SELECT user FROM user WHERE user = '" . $db->real_escape_string($user) . "'";
			if ($host) {
				$query .= " AND host = '" . $db->escape_string($host) . "'";
			}
			$rs = $db->query($query);
			if (!$rs) {
				return error('failed to query database');
			}

			return $rs->num_rows > 0;
		}

		public static function flushTables(): bool
		{
			$db = self::connectRoot();

			return (bool)$db->query('FLUSH TABLES');
		}

		/**
		 * Get all tables from database
		 *
		 * @param string $db
		 * @return array
		 */
		public static function getTablesFromDatabase(string $db): array
		{
			$conn = self::connectRoot();
			$query = "SELECT table_name FROM INFORMATION_SCHEMA.tables WHERE table_type = 'base table' AND 
				table_schema = '" . $conn->escape($db) . "'";
			if ( !($res = $conn->query($query)) ) {
				return [];
			}

			return array_column($res->fetch_all(MYSQLI_NUM), 0);
		}

		/**
		 * Set .pgpass field value
		 *
		 * @param string       $path
		 * @param string|array $field
		 * @param mixed        $val
		 * @param string       $group configuration group
		 * @return bool
		 */
		public static function setUserConfigurationField(
			string $path,
			string $field,
			string $val = null,
			string $group = 'client'
		): bool {
			Map::load($path, 'cd', 'inifile')->quoted(true)->section($group)[$field] = $val;

			return chmod($path, 0600);

		}

		public static function getUserConfiguration(string $path, string $group = null): array
		{
			if (!file_exists($path)) {
				return [];
			}

			return Map::load($path, 'r', 'inifile')->quoted(true)->section($group)->fetchAll();
		}

		/**
		 * Verify if database exists
		 *
		 * @param string      $dbname database name
		 * @param string|null $user   optional user to confirm against
		 * @return bool
		 */
		public static function databaseExists(string $dbname, string $user = null): bool
		{
			$conn = self::connectRoot();
			$dbsafe = $conn->escape_string($dbname);
			$query = 'SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA';
			if ($user) {
				$query .= ' JOIN INFORMATION_SCHEMA.SCHEMA_PRIVILEGES ON (SCHEMA_NAME = TABLE_SCHEMA)';
			}
			$query .= ' WHERE SCHEMA_NAME = \'' . $dbsafe . '\'';
			if ($user) {
				$query .= ' AND GRANTEE LIKE \'\\\'' . str_replace(['%', '_'], ['\\%', '\\_'], $conn->escape_string($user)) . '\\\'@%\'';
			}

			return ($rs = $conn->query($query)) && ($rs->num_rows > 0);
		}

	}