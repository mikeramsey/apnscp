<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database\PostgreSQL\Generic;

	class Query
	{
		/**
		 * Common wrapper to create tablespace
		 *
		 * @param string $name
		 * @param string $location
		 * @param string $owner
		 * @return string
		 */
		public function createTablespace($name, $location, $owner = 'postgres'): string
		{
			return 'CREATE TABLESPACE "' . $name . '" OWNER "' . $owner . "\" LOCATION '" . $location . "'";
		}

		public function userExists($user): string
		{
			return 'SELECT 1 FROM pg_authid WHERE rolname = ' . pg_escape_literal($user);
		}

		/**
		 * @param string      $database
		 * @param string|null $tblspace optional tablespace check
		 * @return string
		 */
		public function ownerFromDatabase(string $database, string $tblspace = null): string
		{
			$query = 'SELECT pg_catalog . pg_get_userbyid(d . datdba) as "owner"
				FROM pg_catalog.pg_database d
				JOIN pg_catalog.pg_roles r ON d.datdba = r.oid 
				JOIN pg_catalog.pg_tablespace t on d.dattablespace = t.oid  WHERE d.datname = \'' . pg_escape_string($database) . '\'';
			if ($tblspace) {
				$query .= 'AND t.spcname = \'' . pg_escape_string($tblspace) . '\'';
			}

			return $query;
		}

		/**
		 * @param string      $database
		 * @return string
		 */
		public function tablesFromDatabase(string $database): string
		{
			return 'SELECT table_schema || \'.\' || table_name FROM information_schema.tables WHERE 
				table_type = \'BASE TABLE\' AND table_schema NOT IN (\'pg_catalog\', \'information_schema\')';
		}

		/**
		 * Change database owner
		 *
		 * @param string $database
		 * @param string $owner
		 * @return string
		 */
		public function changeDatabaseOwner(string $database, string $owner): string
		{
			return 'ALTER DATABASE ' . pg_escape_identifier($database) . ' OWNER TO ' . pg_escape_identifier($owner);
		}

		public function userHasTablespace($user): string
		{
			return $this->getTablespaceFromUser($user);
		}

		public function getTablespaceFromUser($user): string
		{
			return 'SELECT spcname FROM pg_tablespace INNER JOIN pg_roles ON ' .
				'(pg_tablespace.spcowner = pg_roles.oid) WHERE pg_roles.rolname = ' . pg_escape_literal($user);
		}

		public function dropTablespace($tablespace): string
		{
			return 'DROP TABLESPACE ' . pg_escape_identifier($tablespace);
		}

		public function deleteUser($user, $host): string
		{
			return 'DROP ROLE "' . pg_escape_string($user) . '"';
		}

		public function renameUser($old, $new): string
		{
			return 'ALTER USER "' . pg_escape_string($old) . '" RENAME TO "' . pg_escape_string($new) . '"';
		}

		public function dropDatabase($dbname): string
		{
			return 'DROP DATABASE "' . pg_escape_string($dbname) . '"';
		}

		public function moveDatabase($old, $new): string
		{
			return 'ALTER DATABASE "' . pg_escape_string($old) . '" RENAME TO "' . pg_escape_string($new) . '"';
		}

		public function moveTablespace($old, $new): string
		{
			return 'ALTER TABLESPACE ' . pg_escape_identifier($old) . ' RENAME TO ' . pg_escape_identifier($new);
		}

		public function changeTablespaceOwner(string $tblspace, string $newowner): string
		{
			return 'ATNER TABLESPACE ' . pg_escape_identifier($tblspace) . ' OWNER TO ' . pg_escape_identifier($newowner);
		}

		public function tablespaceExists($tablespace): string
		{
			return 'SELECT 1 FROM pg_tablespace WHERE spcname =' . pg_escape_literal($tablespace);
		}

		public function databaseExists($db, $user = null): string
		{
			$query = 'SELECT 1 FROM pg_database WHERE datname = ' . pg_escape_literal($db);
			if ($user) {
				$query .= 'AND datdba = (SELECT oid FROM pg_roles  WHERE rolname = ' . pg_escape_literal($user) . ')';
			}

			return $query;
		}

		public function placeInRole(string $user, string $role): string
		{
			return 'ALTER ROLE "' . $user . '" SET ROLE "' . $role . '"';
		}

		public function createUser($user, $password): string
		{
			return 'CREATE ROLE "' . $user . '" WITH NOCREATEDB NOCREATEROLE ' .
				"LOGIN NOINHERIT PASSWORD '" . $password . "';";
		}

		public function setMaxConnections($user, $limit = -1): string
		{
			return 'ALTER ROLE "' . $user . '" WITH CONNECTION LIMIT ' . \intval($limit);
		}

		public function captureUid(int $siteid): string
		{
			return 'WITH cte AS (SELECT uid FROM free_uids WHERE site_id IS NULL LIMIT 1 FOR UPDATE) UPDATE free_uids f SET site_id = ' .
				$siteid . ' FROM cte WHERE f.uid = cte.uid RETURNING f.uid';
		}

		public function releaseUid(int $uid, int $site_id): string
		{
			return 'UPDATE free_uids SET site_id = NULL where uid = ' . $uid . ' AND site_id = ' . $site_id;
		}
	}