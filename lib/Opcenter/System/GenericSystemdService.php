<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	declare(strict_types=1);

	namespace Opcenter\System;

	abstract class GenericSystemdService
	{
		protected const SERVICE = null;
		protected const STATUS_REGEX = null;

		public const SYSTEMD_SERVICE_PATH = '/etc/systemd/system';
		// @var array via systemctl list-units
		private const HEADER_FIELDS = [
			'unit',
			'load',
			'active',
			'sub',
			'description'
		];
		// @var bool process service request asynchronously
		public static $async = false;

		/**
		 * Verify this class has been properly extended
		 */
		private static function check(): void
		{
			if (null === static::SERVICE) {
				fatal('Incomplete implementation');
			}
		}
		/**
		 * Service has failed/exited abnormally
		 *
		 * @return bool
		 */
		public static function irregular(): bool
		{
			self::check();
			$ret = \Util_Process::exec('systemctl is-failed ' . static::getService(), null, [0, 1]);

			return \in_array(rtrim($ret['stdout']), ['failed', 'inactive']);
		}

		/**
		 * Service has failed
		 *
		 * @return bool
		 */
		public static function failed(): bool
		{
			self::check();
			$ret = \Util_Process::exec('systemctl is-failed ' . static::getService(), null, [0, 1]);

			return rtrim($ret['stdout']) === 'failed';
		}

		/**
		 * Reset service state
		 *
		 * @return bool
		 */
		public static function reset(): bool
		{
			self::check();
			$ret = \Util_Process::exec('systemctl reset-failed ' . static::getService());

			return $ret['success'];
		}

		/**
		 * Service is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{

			$ret = \Util_Process::exec(static::getCommand('is-active'), null, [0, 1]);
			return $ret['success'] && rtrim($ret['stdout']) === 'active';
		}

		/**
		 * Get full service status
		 *
		 * @param $fields optional fields to filter
		 * @return array
		 */
		public static function status(array $fields = []): array
		{
			self::check();
			$fieldQuery = $fields ? ('-p ' . implode(',', $fields)) : null;
			$ret = \Util_Process::exec(static::getCommand('show') . ' --no-pager %s', $fieldQuery, [0,1], ['environment' => ['LANGUAGE' => 'en_US']]);
			// https://bugzilla.redhat.com/show_bug.cgi?id=1853736
			if (!$ret['success'] || (
				$ret['return'] === 1 && (false === strpos($ret['stderr'], "Invalid argument") || version_compare(posix_uname()['release'], '4.18.0', '<')))
			) {
				error('Failed to query %s: %s', static::getService(), $ret['stderr']);
				return [];
			}
			if (empty($ret['stdout'])) {
				// case where service doesn't exist or grouped
				return [];
			}
			return array_build(explode("\n", rtrim($ret['stdout'])), static function ($i, $line) {
				return explode('=', $line, 2);
			});
		}

		/**
		 * Get service status from services implementing reporting
		 *
		 * @return array|null
		 */
		public static function getReportedServiceStatus(): ?array
		{
			if (!static::STATUS_REGEX) {
				fatal('Service %s claims reporting but fails to implement regex match', static::class);
			}

			$txt = array_get(static::status(['StatusText']), 'StatusText', '');
			$res = preg_match(static::STATUS_REGEX, $txt, $matches);

			if (!$res) {
				return [];
			}

			return array_filter($matches, '\is_string', ARRAY_FILTER_USE_KEY);
		}

		/**
		 * Service exists on machine
		 *
		 * @return bool
		 */
		public static function exists(): bool
		{
			self::check();
			$ret = \Util_Process::exec(static::getCommand('show') . ' -p LoadState --no-pager', null, [0,4]);
			if (!$ret['success']) {
				return error('Failed to query %s: %s', static::getService(), $ret['stderr']);
			}
			return $ret['return'] === 0 && false === strpos($ret['stdout'], "=not-found");
		}

		/**
		 * Reload service
		 *
		 * @param string $timespec
		 * @return bool
		 */
		public static function reload(string $timespec = null): bool
		{
			if (static::irregular()) {
				debug("`%s' service marked as failed. Upgraded reload to restart request to clear state", static::getService());
				// @XXX check back later on service states
				return static::run('reload-or-restart', $timespec);
			}

			return static::run('reload', $timespec);
		}

		/**
		 * Restart the service
		 *
		 * @param string|null $timespec
		 * @return bool
		 */
		public static function restart(string $timespec = null): bool
		{
			return static::run('restart', $timespec);
		}

		/**
		 * Run systemd command
		 *
		 * @param string      $command  systemd command to run on service
		 * @param string|null $timespec optional asynchronous delay
		 * @return bool
		 */
		public static function run(string $command, string $timespec = null): bool
		{
			if ($timespec) {
				$proc = new \Util_Process_Schedule($timespec);
				$proc->setID(static::getScheduleKey($command));
				if ($id = $proc->preempted()) {
					return true;
				}
			} else {
				$proc = static::$async ? new \Util_Process_Fork() : new \Util_Process();
			}
			$res = $proc->run(
				static::getCommand($command),
				[
					'mute_stdout' => true,
					'mute_stderr' => true,
					'reporterror' => true
				]

			);

			return $res['success'] ?? false;
		}

		/**
		 * Get scheduling key
		 *
		 * @param string $suffix scheduling key marker
		 * @return string
		 */
		protected static function getScheduleKey(string $suffix = 'rld'): string
		{
			self::check();
			// merge events
			if ($suffix === 'reload' || $suffix === 'restart') {
				$suffix = 'rld';
			}
			return static::getService() . ':' . $suffix;
		}

		/**
		 * Translate action into systemd directive
		 *
		 * @param string $mode
		 * @return string
		 */
		protected static function getCommand(string $mode): ?string
		{
			self::check();
			$mode = strtok($mode, ' ');
			$args = strtok("\n");
			switch (strtolower($mode)) {
				case 'start':
				case 'stop':
				case 'reload':
				case 'restart':
				case 'try-restart':
				case 'reload-or-restart':
				case 'reload-or-try-restart':
				case 'isolate':
				case 'kill':
				case 'mask':
				case 'unmask':
				case 'disable':
				case 'enable':
				case 'reenable':
				case 'is-active':
				case 'is-failed':
				case 'status':
				case 'show':
					return "/usr/bin/systemctl $args $mode " . static::getService();
				default:
					fatal("Unknown %s command `%s'", static::getService(), $mode);
			}
			return null;
		}

		/**
		 * Reload systemd daemon
		 *
		 * @return bool
		 */
		public static function daemonReload(): bool
		{
			$ret = \Util_Process_Fork::exec('/usr/bin/systemctl daemon-reload');
			return $ret['success'];
		}

		/**
		 * Show all services
		 *
		 * @param string      $pattern service pattern to match
		 * @param string|null $state optional state
		 * @return array
		 */
		final public static function listAll($pattern = '*', $state = null): array
		{
			$flag = null;
			if ($state) {
				if (!\in_array($state, ['failed', 'active', 'inactive', 'activating', 'deactivating'], true)) {
					error('Unknown/invalid state requested: %s', $state);
					return [];
				}
				$flag = '--state=';
			}
			$ret = \Util_Process_Safe::exec('systemctl list-units --full --no-legend --plain %s %s%s', [
				$pattern,
				$flag,
				$state
			]);
			$services = [];
			if (!$ret['success']) {
				error($ret['stderr']);
				return $services;
			}
			$lines = explode("\n", $ret['stdout']);
			$header = current($lines);
			$headerFields = static::HEADER_FIELDS;
			$headerLength = \count($headerFields);

			while (false !== ($line = next($lines))) {
				if (!$line) {
					continue;
				}
				$tokens = [];
				$token = strtok($line, ' ');
				for ($i = 1; $i < $headerLength; $i++, $token = strtok(' ')) {
					if (false === $token) {
						break;
					}
					$tokens[] = $token;
				}
				$tokens[] = strtok("\n");
				if (\count($tokens) !== \count($headerFields)) {
					debug('Garbage output detected: %s', $line);
					continue;
				}
				$services[] = array_combine(
					$headerFields,
					$tokens
				);
			}

			return $services;
		}

		/**
		 * Wait for service activation
		 *
		 * @param int $seconds maximum time
		 * @return bool
		 */
		public static function waitFor(int $seconds = 10): bool
		{
			$sleep = 500000;
			for ($i = 0, $n = ceil($seconds*1000000/$sleep); $i < $n; $i++) {
				if (self::running()) {
					return true;
				}
				usleep($sleep);
			}
			return false;
		}

		/**
		 * Get service name
		 *
		 * @return string
		 */
		protected static function getService(): string
		{
			static::check();

			return static::SERVICE;
		}

		/**
		 * Convert systemd path into service
		 */
		public static function escapePath(string $path): string {
			return str_replace(
				['.', '-'],
				['\\x' . bin2hex('.'), '\\x' . bin2hex('-')],
				$path
			);
		}

		/**
		 * Service is present based on configuration
		 *
		 * @link exists()
		 * Lenient form of exists(), checks by configuration not actual presence
		 *
		 * @return bool
		 */
		public static function present(): bool
		{
			return static::exists();
		}
	}