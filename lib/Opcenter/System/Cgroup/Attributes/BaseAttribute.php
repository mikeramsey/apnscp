<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\Attributes;

	use Opcenter\System\Cgroup\Contracts\ControllerAttribute;
	use Opcenter\System\Cgroup\Controller;

	abstract class BaseAttribute implements ControllerAttribute
	{
		use \NamespaceUtilitiesTrait;
		protected $value;
		protected $controller;

		public function __construct($value, Controller $controller)
		{
			$this->controller = $controller;
			if (null === $value) {
				$value = $this->read();
			}
			$this->value = $value;

		}

		public function __toString(): string
		{
			$parts = explode('\\', ltrim(substr(static::class, \strlen(self::getNamespace(self::class))), '\\'));

			return strtolower($parts[0]) . (isset($parts[1]) ? '.' . snake_case($parts[1]) : '');
		}

		/**
		 * Activate configuration change
		 *
		 * @return bool
		 */
		public function activate(): bool
		{
			$path = $this->getAttributePath();
			if (null === ($value = $this->getValue())) {
				debug('Skipping cgroup %s => %s', [(string)$this->controller, $path]);
				return false;
			}

			return $this->write($path, $value);
		}

		protected function getAttributePath(): string
		{
			return $this->controller->getPath() . '/' . (string)$this;
		}

		protected function write(string $path, $value): bool
		{
			return file_put_contents($path, (string)$value) > 0;
		}

		public function read()
		{
			if (!file_exists($this->getAttributePath())) {
				return null;
			}
			return trim(file_get_contents($this->getAttributePath()));
		}

		public function getValue()
		{
			return $this->value;
		}

		public function deactivate(): bool
		{
			$path = $this->getAttributePath();

			return file_put_contents($path, 0) > 0;
		}
	}