<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Controller;

	class Cpuacct extends Controller
	{
		protected const ATTRIBUTES = [];

		public const LOGGABLE_METRICS = [
			'usage' => [
				'label'   => 'CPU Usage (sec)',
				'type'    => 'monotonic',
				'counter' => 'cpuacct.usage'
			],
			'user' => [
				// centiseconds!
				'label'   => 'CPU User Usage (sec)',
				'type'    => 'monotonic',
				'counter' => 'cpuacct.ustat'
			],
			'system' => [
				// centiseconds!
				'label'   => 'CPU Stat Usage (sec)',
				'type'    => 'monotonic',
				'counter' => 'cpuacct.sstat'
			]

		];

		/**
		 * Convert counter data into centiseconds
		 *
		 * @param string $path
		 * @return string
		 */
		protected function readRawCounter(string $path): string
		{
			$ext = substr($path, -6);
			if ($ext === '.ustat' || $ext === '.sstat') {
				$path = \dirname($path) . '/cpuacct.stat';
			}
			$val = parent::readRawCounter($path);
			if ($ext === '.usage') {
				// convert nano to seconds
				$val = (int)$val / 1e9;
			} else {
				$ctr = strtok($val,' ');
				do {
					$val = strtok("\n");
					if (($ext === '.ustat' && $ctr === 'user') ||
						($ext === '.sstat' && $ctr === 'system'))
					{
						break;
					}
				} while (false !== ($ctr = strtok(' ')));
				$val = (int)$val/CPU_CLK_TCK;
			}

			return (string)($val * 100);
		}
	}
