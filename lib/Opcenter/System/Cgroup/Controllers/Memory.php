<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Controller;

	class Memory extends Controller
	{
		protected const ATTRIBUTES = [
			'memory' => 'memory.limit_in_bytes'
		];

		public const LOGGABLE_METRICS = [
			'used' => [
				'label'   => 'Memory used (KB)',
				'type'    => 'value',
				'counter' => 'memory.usage_in_bytes'
			],
			'peak' => [
				'label'   => 'Memory peak (KB)',
				'type'    => 'value',
				'counter' => 'memory.max_usage_in_bytes'
			],
			'oom' => [
				'label'   => 'Memory maxed condition',
				'type'    => 'monotonic',
				'counter' => 'memory.failcnt'
			]
		];

		public function readRawCounter(string $path): string
		{
			$counter = parent::readRawCounter($path);
			if (substr($path, -4) === '/oom') {
				return $counter;
			}

			return (string)((int)$counter/1024);
		}

	}
