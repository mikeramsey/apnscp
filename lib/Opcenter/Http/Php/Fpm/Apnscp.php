<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\Process;

class Apnscp {
	private const EXEC_NAME = 'apnscp_php-fpm';
	private const PID_FILE = 'php-fpm.pid';
	private const CHECK_URL = \Opcenter\Http\Apnscp::CHECK_URL;

	/**
	 * Verify frontend is handling requests
	 *
	 * @return bool
	 */
	public static function check(): bool
	{
		if (APNSCPD_HEADLESS) {
			return true;
		}

		// @todo systemd notify socket

		return self::running();

	}

	/**
	 * Stop frontend
	 *
	 * @return bool
	 */
	public static function stop(): bool
	{
		if (!static::running()) {
			info('Dispatcher is not running - attempting stop anyway');
		}

		$pid = self::getPid();

		return Process::kill($pid, SIGTERM);
	}

	/**
	 * apnscpd frontend is running
	 *
	 * @return bool
	 */
	public static function running(): bool
	{
		if (!($pid = static::getPid())) {
			return false;
		}
		if (Process::pidMatches($pid, '*php-fpm*') && posix_kill($pid, 0)) {
			return true;
		}

		return false;
	}

	public static function restart(): bool
	{
		if (APNSCPD_HEADLESS) {
			return warn('Running in headless mode');
		}

		return ( (self::running() && self::stop()) || true ) && self::start();
	}

	private static function getPid(): ?int
	{
		if (!file_exists($file = run_path(self::PID_FILE))) {
			return null;
		}

		return (int)file_get_contents($file);
	}

	/**
	 * Start apnscpd frontend
	 *
	 * @return bool
	 */
	public static function start(): bool
	{
		if (APNSCPD_HEADLESS) {
			return warn('Cannot start %(brand)s dispatcher - running in headless mode',
				['brand' => MISC_PANEL_BRAND]
			);
		} else if (FRONTEND_DISPATCH_HANDLER !== 'fpm') {
			return error('Dispatch handler is not configured to use FPM');
		}

		if (static::running()) {
			return true;
		}

		if (!static::running() && file_exists($pidFile = run_path(self::PID_FILE))) {
			unlink($pidFile);
		}

		$command = new \Util_Process_Fork();
		$command->setOption('user', APNSCP_SYSTEM_USER);
		$command->setOption('group', APNSCP_SYSTEM_USER);
		$command->setEnvironment('APNSCP_ROOT', INCLUDE_PATH);
		$command->setOption('mute_stderr', true);
		// closing STDIN creates ECONNRESET errors
		$command->setOption('mute_stdin', false);
		$ret = $command->run('%(exec)s --daemonize --fpm-config %(config)s/php-fpm.conf', [
			'exec'   => static::getExec(),
			'config' => config_path()
		]);

		if ($ret['return']) {
			info("Frontend started with PID `%d'", $ret['return']);
		}

		return $ret['return'] > 0;
	}

	/**
	 * Get executable path
	 *
	 * @return string
	 */
	protected static function getExec(): string
	{
		return bin_path('php-bins/' . self::EXEC_NAME);
	}

	/**
	 * Reload frontend
	 *
	 * @return bool
	 */
	public static function reload(): bool
	{
		if (APNSCPD_HEADLESS) {
			return true;
		}
		if ( !($pid = self::getPid()) ) {
			return false;
		}
		return Process::kill($pid, SIGUSR2);
	}
}