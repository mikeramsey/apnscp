<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Http\Php;

use Opcenter\Filesystem;
use Opcenter\Http\Php\Fpm\Configuration;
use Opcenter\Http\Php\Fpm\SystemdSynthesizer;
use Opcenter\System\GenericSystemdService;

class Fpm
{
	// @var Configuration
	protected $config;
	public const SERVICE_NAMESPACE = 'php-fpm-';

	public function __construct(Configuration $cfg)
	{
		$this->config = $cfg;
	}

	/**
	 * Mock up a systemd service
	 *
	 * @return GenericSystemdService
	 */
	public function mockService(bool $socket = false): GenericSystemdService
	{
		$service = $socket ? $this->config->getSocketServiceName() : $this->config->getServiceName();
		return SystemdSynthesizer::mock($service);
	}

	/**
	 * List PHP-FPM services
	 *
	 * @return array
	 */
	public function listServices(): array
	{
		return static::getServicesFromGroup($this->config->getGroup());
	}

	/**
	 * Enumerate all services for group
	 *
	 * @param string $group
	 * @return array
	 */
	public static function getServicesFromGroup(string $group): array
	{
		$path = GenericSystemdService::SYSTEMD_SERVICE_PATH . '/' .
			static::SERVICE_NAMESPACE . $group . '-*.{socket,service}';

		return array_map('basename', glob($path, GLOB_NOSORT | GLOB_BRACE));
	}

	/**
	 * Get service pattern from group
	 *
	 * @param string $group
	 * @return string
	 */
	public static function getServicePatternFromGroup(string $group): string
	{
		return static::SERVICE_NAMESPACE . "${group}-*.service";
	}

	/**
	 * Get pool names from group
	 *
	 * @param string $group
	 * @return array
	 */
	public static function getPoolsFromGroup(string $group): array
	{
		return array_values(array_filter(array_map(static function (string $service): ?string {
			if (substr($service, -8) !== '.service') {
				return null;
			}

			return substr(substr($service, \strlen(Fpm::SERVICE_NAMESPACE)), 0, -8 /** strlen('.service') */);
		}, Fpm::getServicesFromGroup($group))));
	}

	/**
	 * Terminate services
	 *
	 * @param array|null $services fully qualified service or alias (php-fpm-siteXX.. or apnscp.test)
	 * @return bool
	 */
	public function terminate(array $services = null): bool
	{
		if ($services === null) {
			$services = $this->listServices();
		} else {
			$services = $this->filterInstalled($services);
		}
		foreach ($this->filterInstalled($services) as $service) {
			static::terminate($service);
		}

		return true;
	}

	/**
	 * Filter installed services from service list
	 *
	 * @param array $services
	 * @return array
	 */
	private function filterInstalled(array $services) {
		$activeServices = $this->listServices();
		$group = $this->config->getGroup();
		return array_filter($activeServices, static function ($name) use ($services, $group) {
			if (\in_array($name, $services)) {
				return true;
			}
			foreach ($services as $filter) {
				if (0 !== strpos($name, static::SERVICE_NAMESPACE . $group . '-' . $filter)) {
					continue;
				}
				// passed as .socket or .service
				if (substr($name, -\strlen($filter)) === $filter) {
					return true;
				}
				$serviceAlias = substr($name, \strlen(static::SERVICE_NAMESPACE . $group . '-'));
				// filter out php-fpm-siteX-foo.service.service and php-fpm-siteX-foo.service
				if (substr($serviceAlias, 0, strrpos($serviceAlias, '.'))) {
					return true;
				}

				return false;
			}

			return false;
		});
	}

	/**
	 * Services installed for FPM group
	 *
	 * @return bool
	 */
	public function hasServices(): bool
	{
		if (!($group = $this->config->getGroup())) {
			return false;
		}

		return \count($this->listServices()) > 0;
	}

	/**
	 * Remove services from system
	 *
	 * @param array|string $services
	 * @return bool
	 */
	public static function uninstall($services): bool
	{
		foreach ((array)$services as $service) {
			if (0 !== strpos($service, Fpm::SERVICE_NAMESPACE)) {
				warn("Service `%s' does not begin with namespace %s - skipping", $service, Fpm::SERVICE_NAMESPACE);
				continue;
			}
			$systemd = SystemdSynthesizer::mock($service);
			if ($systemd::exists()) {
				if ($systemd::running() && !$systemd::run('stop')) {
					warn("Failed to stop service `%s'", $service);
				}
				$systemd::run('disable');
			}
			$path = GenericSystemdService::SYSTEMD_SERVICE_PATH . "/$service";
			if (!file_exists($path)) {
				warn("systemd service path `%s' not found for `%s'", $path, $service);
				continue;
			}
			unlink($path);
			if (is_dir($path . '.d')) {
				Filesystem::rmdir($path . '.d');
			}
		}

		return true;
	}
}
