<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Net;

	class Iface
	{

		const NAMEBASED_INTERFACE_FILE = 'interface';

		/**
		 * Get interface for IP address
		 *
		 * @param string $ip
		 * @return null|string interface or null if not bound
		 */
		public static function ip_iface(string $ip): ?string
		{
			if (false === inet_pton($ip)) {
				return null;
			}
			foreach (self::interfaces() as $iface) {
				if (self::bound($ip, $iface)) {
					return $iface;
				}
			}

			return null;
		}

		/**
		 * Get available interfaces
		 *
		 * In multi-interfaced environments, interface selection is round-robin
		 *
		 * @return array
		 */
		public static function interfaces(): array
		{
			// default in case file is missing
			$file = \Opcenter::mkpath(static::NAMEBASED_INTERFACE_FILE);
			if (!file_exists($file)) {
				$iface = static::detect();
				warn("missing interface file `%s', assuming main iface is `%s'",
					$file,
					$iface
				);

				return [$iface];
			}
			$iface = file($file, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

			return $iface;
		}

		/**
		 * Detect the public interface name
		 *
		 * @return string
		 */
		public static function detect(): string
		{
			$proc = '/proc/net/route';
			if (!file_exists($proc)) {
				fatal('route missing from /proc - is procfs mounted under /proc?');
			}
			$ifaces = array_map(static function ($line) {
				return preg_split('/\s+/', $line);
			}, file($proc, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES));
			$active = array_filter($ifaces, static function ($line) {
				return strspn($line[1], '0') === \strlen($line[1]);
			});
			if (!$active) {
				fatal('no public interface configured with route to 0.0.0.0');
			}

			return array_pop($active)[0];
		}

		/**
		 * Get node gateway
		 *
		 * @return string
		 */
		public static function gateway(): string
		{
			$proc = '/proc/net/route';
			if (!file_exists($proc)) {
				fatal('route missing from /proc - is procfs mounted under /proc?');
			}

			$ifaces = array_map(static function ($line) {
				return preg_split('/\s+/', $line);
			}, file($proc, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES));
			$active = array_filter($ifaces, static function ($line) {
				return strspn($line[1], '0') === \strlen($line[1]);
			});
			if (!$active) {
				fatal('no public interface configured with route to 0.0.0.0');
			}
			$hex = array_reverse(str_split(array_pop($active)[2], 2));
			return implode('.', array_map('hexdec', $hex));
		}

		/**
		 * Get default routed IP address
		 *
		 * @return string|null
		 */
		public static function routed(): ?string {
			$sock = socket_create(AF_INET, SOCK_DGRAM, IPPROTO_IP);
			socket_connect($sock, \Opcenter\Net\Iface::gateway(), 0);
			$ret = socket_getsockname($sock, $addr);
			socket_close($sock);
			return $ret ? $addr : null;
		}

		/**
		 * Check if IP address bound to server
		 *
		 * @param string $ip
		 * @param string $dev optional interface to restrict query
		 * @return bool
		 */
		public static function bound(string $ip, string $dev = null): bool
		{
			if (false === inet_pton($ip)) {
				return error("invalid IP address `%s'", $ip);
			}
			if (!$dev) {
				$dev = static::detect();
			}
			$is6 = false !== strpos($ip, ':');
			$ret = \Util_Process_Safe::exec(
				'ip %(is6)s addr show dev %(dev)s to %(addr)s',
				['is6' => $is6 ? '-6' : null, 'dev' => $dev, 'addr' => $ip]
			);

			return $ret['success'] && !empty(trim($ret['output']));
		}

		public static function bind(string $ip, string $dev = null): bool
		{
			if ($dev !== ($primary = self::detect())) {
				return error("IP address `%s' device `%s' must match `%s'", $ip, $dev, $primary);
			}
			if (false === inet_pton($ip)) {
				return error("invalid IP address `%s'", $ip);
			}
			$is6 = false !== strpos($ip, ':');
			$ret = \Util_Process::exec('ip %(is6)s addr add %(ip)s/%(subnet)d dev %(dev)s',
				[
					'is6'    => $is6 ? '-6' : null,
					'ip'     => $ip,
					'subnet' => $is6 ? 64 : 24,
					'dev'    => $dev
				]
			);

			return $ret['success'];
		}

		public static function unbind(string $ip, string $dev = null): bool
		{
			if ($dev !== ($primary = self::detect())) {
				return error("IP address `%s' device `%s' must match `%s'", $ip, $dev, $primary);
			}
			if (false === inet_pton($ip)) {
				return error("invalid IP address `%s'", $ip);
			}
			$is6 = false !== strpos($ip, ':');
			$ret = \Util_Process::exec('ip %(is6)s addr delete %(ip)s/%(subnet)d dev %(dev)s',
				[
					'is6'    => $is6 ? '-6' : null,
					'ip'     => $ip,
					'subnet' => $is6 ? 64 : 24,
					'dev'    => $dev
				]
			);

			return $ret['success'];
		}
	}