<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Contracts\Subscriber;
	use Event\Events;
	use Opcenter\Account\Create;

	class Filesystem implements Subscriber, Publisher
	{
		const HOOK_ID = 'FILESYSTEM';

		use \FilesystemPathTrait;

		protected $site;

		public function __construct(string $site)
		{
			$this->site = $site;
		}

		/**
		 * Create filesystem skeleton
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function create(string $site): bool
		{
			$class = new static($site);

			return $class->populate();
		}

		/**
		 * Populate filesystem
		 *
		 * @return bool
		 */
		public function populate(): bool
		{
			// hook in to clean up in case things fail
			Cardinal::register([Create::HOOK_ID, Events::FAILURE], [$this, 'depopulate']);
			$paths = $this->getBasePaths();
			foreach ($paths as $p => $perms) {
				if (file_exists($p)) {
					warn("path `%s' already exists", $p);
					continue;
				}
				if (!static::mkdir($p, ...$perms)) {
					return error("populate filesystem failure couldn't mkdir `%s'", $p);
				}
			}
			if (!Cardinal::fire([self::HOOK_ID, Events::CREATED], $this)) {
				fatal('failed to create account');
			}

			return true;

		}

		/**
		 * Get preliminary paths
		 *
		 * @return array
		 */
		protected function getBasePaths(): array
		{
			$base = \dirname($this->domain_fs_path());

			return [
				$base                       => ['root', 'root', 0711],
				$this->domain_fs_path()     => ['root', 'root', 0751],
				$this->domain_info_path()   => ['root', 'root', 0700],
				$this->domain_shadow_path() => ['root', 'root', 0751],
			];
		}

		/**
		 * Create a directory
		 *
		 * @param string     $path
		 * @param string|int $owner
		 * @param string|int $group
		 * @param int|string $perm octal permissions
		 * @param bool       $recursive
		 * @return bool
		 */
		public static function mkdir($path, $owner = 'root', $group = 'root', $perm = 0755, bool $recursive = true)
		{
			if ($recursive && !is_dir(\dirname($path))) {
				static::mkdir(\dirname($path), $owner, $group, $perm, $recursive);
			}
			if (!mkdir($path) && !is_dir($path)) {
				return error("failed to create directory `%s'", $path);
			}

			return self::chogp($path, $owner, $group, $perm);
		}

		/**
		 * Change owner, group, permissions of a file
		 *
		 * @param string     $path
		 * @param string|int $owner user name or user id
		 * @param string|int $group group name or group id
		 * @param int        $perm  octal permission (leading 0)
		 * @return bool
		 */
		public static function chogp(string $path, $owner = 'root', $group = 'root', int $perm = 0755): bool
		{
			$perm = decoct($perm);

			return chown($path, $owner) && chgrp($path, $group) && chmod($path, octdec($perm))
				|| error("failed to chogp `%s'", $path);
		}

		/**
		 * Delete site filesystem
		 *
		 * @param string $site
		 * @return bool
		 */
		public static function delete(string $site): bool
		{
			$class = new static($site);

			return $class->depopulate();
		}

		public function depopulate(): bool
		{
			if (!FILESYSTEM_VIRTBASE ||
				0 !== strpos($this->domain_shadow_path(), FILESYSTEM_VIRTBASE) ||
				0 !== strpos($this->domain_info_path(), FILESYSTEM_VIRTBASE)) {
				fatal('filesystem virtbase corrupted?');
			}

			\Util_Process::exec('rm --one-file-system -rf %(shadow)s %(info)s', [
				'shadow' => $this->domain_shadow_path(),
				'info'   => $this->domain_info_path()
			]);
			(new Service\ServiceLayer($this->site))->unmountAll();
			// ensure everything is unmounted
			if (is_dir($this->domain_fs_path()) && !@rmdir($this->domain_fs_path())) {
				// @XXX leaky ext4 file descriptor. Problem appears idiosyncratic to 3.10 kernels
				$orphanPath = static::getOrphanPath();
				\is_dir($orphanPath) || Filesystem::mkdir($orphanPath, 'root', 'root', 0700);
				$relocatedPath = tempnam($orphanPath, $this->site . '-');
				unlink($relocatedPath);
				return rename(\dirname($this->domain_fs_path()), $relocatedPath) ||
					warn('Failed to relocate orphan path %s!', $this->domain_fs_path());
			}
			// this should be empty, so to be safe
			rmdir(\dirname($this->domain_fs_path()));

			return true;
		}

		/**
		 * Requested site ID is a pending orphan
		 *
		 * @param int $siteid
		 * @return bool
		 */
		public static function pendingOrphan(int $siteid): bool
		{
			return \count(glob(static::getOrphanPath() . "/site${siteid}-*", GLOB_NOSORT)) > 0;
		}

		private static function getOrphanPath(): string
		{
			return FILESYSTEM_VIRTBASE . '/.orphans';
		}

		/**
		 * Touch a file creating or altering its mtime
		 *
		 * @param string     $path
		 * @param string|int $owner user name or user id
		 * @param string|int $group group name or group id
		 * @param int        $perm  octal permission (leading 0)
		 * @return bool
		 */
		public static function touch(string $path, $owner, $group, int $perm = 0755): bool
		{
			return touch($path) && static::chogp($path, $owner, $group, $perm);
		}

		/**
		 * Remove a directory and its descendents
		 *
		 * @param string $path
		 * @return bool
		 */
		public static function rmdir(string $path): bool
		{
			return null !== static::readdir($path, static function ($file) use ($path) {
					$file = $path . DIRECTORY_SEPARATOR . $file;
					if (!is_link($file) && is_dir($file)) {
						return static::rmdir($file);
					}
					unlink($file);
				}) && rmdir($path);
		}

		/**
		 * Read a directory ignoring self-referential and parent directory hard links
		 *
		 * @param string   $directory
		 * @param \Closure $cb optional callback for each file
		 * @return array|null
		 */
		public static function readdir(string $directory, \Closure $cb = null): ?array
		{
			$dh = opendir($directory);
			if (!$dh) {
				error("failed to open directory `%s'", $directory);

				return null;
			}
			$files = [];
			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				}
				$ret = $file;
				if ($cb && null === ($ret = $cb($file))) {
					continue;
				}
				$files[] = $ret;
			}
			closedir($dh);

			return $files;
		}

		public function update($event, Publisher $caller)
		{
			// doesn't matter, callbacks are registered anonymously
			$event = explode('.', $event);
			if ($event[0] === Create::HOOK_ID) {
				switch ($event[1]) {
					case Events::FAILURE:
						return $this->depopulate();
				}
			}
			fatal("unknown event `%s'", join('.', $event));
		}

		public function getEventArgs()
		{
			return [];
		}
	}