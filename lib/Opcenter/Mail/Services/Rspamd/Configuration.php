<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Opcenter\Mail\Services\Rspamd;

use Opcenter\Map\UnboundedJson;

class Configuration extends UnboundedJson
{
	protected const CONF_DIR = '/etc/rspamd/local.d';

	public function __construct(string $name, string $mode = 'r')
	{
		$path = static::makePath($name);

		$chkPath = realpath($path);
		if ($chkPath && 0 !== strpos($chkPath, self::CONF_DIR . '/')) {
			fatal('Configuration must be within %s', self::CONF_DIR);
		}

		parent::__construct($path, $mode);
	}

	/**
	 * Configuration file exists
	 *
	 * @param string $map
	 * @return bool
	 */
	public static function configurationExists(string $map): bool
	{
		return file_exists(self::makePath($map));
	}


	/**
	 * Configuration path from name
	 *
	 * @param string $map
	 * @return string
	 */
	private static function makePath(string $map): string
	{
		$path = static::CONF_DIR . "/${map}";
		if (false !== strpos($map, '..')) {
			fatal("Path cannot descend");
		}

		return $path;
	}
}
