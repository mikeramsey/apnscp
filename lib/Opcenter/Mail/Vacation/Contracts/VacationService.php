<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Contracts;

	interface VacationService
	{
		public function __construct(\Auth_Info_User $user = null);

		public function getOptions(): ?array;

		public function setOption(string $option, $value): bool;

		public function getOption(string $option);

		public function hasOption(string $option);

		public function getDefaults(): array;

		/**
		 * Test configurated vacation message
		 *
		 * @return null|string
		 */
		public function test(): ?string;

		public function getCommand(): string;

		/**
		 * Save and reload vacation preferences
		 */
		public function sync(): bool;

		/**
		 * Update vacation configuration recipe
		 *
		 * @return bool
		 */
		public function save(): bool;

		/**
		 * Enable vacation service for a user
		 *
		 * @return bool
		 */
		public function enable(): bool;

		/**
		 * Disable Vacation service for a user
		 *
		 * @return bool
		 */
		public function disable(): bool;

		/**
		 * Verify Vacation service enabled for user
		 *
		 * @return bool
		 */
		public function enabled(): bool;
	}