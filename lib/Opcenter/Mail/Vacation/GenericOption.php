<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation;

	use Opcenter\Mail\Vacation\Contracts\VacationOption;
	use Opcenter\Mail\Vacation\Contracts\VacationService;

	class GenericOption implements VacationOption
	{
		use \NamespaceUtilitiesTrait;
		use \ContextableTrait;
		/**
		 * @var string mailbot flag
		 */
		const FLAG = null;
		/**
		 * @var mixed default value
		 */
		const DEFAULT = null;
		/**
		 * @var mixed optional var val
		 */
		protected $value;
		/**
		 * @var VacationService configuration container
		 */
		protected $container;

		public function setContainer(VacationService $container)
		{
			$this->container = $container;
		}

		public function getFlag(): string
		{
			if (null === $this->getValue()) {
				return '';
			}

			return static::FLAG;
		}

		/**
		 * Get option value
		 *
		 * @return null|string
		 */
		public function getValue(): ?string
		{
			return $this->value ?? $this->getDefault();
		}

		/**
		 * Set option value
		 *
		 * @param mixed $value
		 * @return bool
		 */
		public function setValue($value): bool
		{
			if (!is_scalar($value)) {
				return error('value passed must be scalar, %s passed', \gettype($value));
			}
			$this->value = $value;

			return true;
		}

		/**
		 * Get default value
		 *
		 * @return mixed
		 */
		public function getDefault()
		{
			return static::DEFAULT;
		}

		public function __toString()
		{
			return (string)$this->getValue();
		}

		/**
		 * Get input parameter name
		 *
		 * @return string
		 */
		public function getInputName(): string
		{
			return strtolower(static::getBaseClassName(static::class));
		}

		/**
		 * Verify value set
		 *
		 * @return bool
		 */
		public function present(): bool
		{
			return null !== $this->value;
		}

		/**
		 * Get sibling option value
		 *
		 * @param string $name
		 * @return null|string
		 */
		public function getOption(string $name): ?string
		{
			return $this->container->hasOption($name) ? $this->container->getOption($name) :
				null;
		}

	}