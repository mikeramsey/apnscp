<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\SiteConfiguration;

	class PsqlUsersSql extends ConfigurationBuilder
	{
		const PATHMAP_PATH = 'psql_users.sql';

		public function parse(): bool
		{
			// @TODO automate imports
			if (!file_exists($path = $this->getReader()->getPathname())) {
				return true;
			}

			// callback once account is processed
			Cardinal::register(
				[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $p) {
					$name = \basename($this->getReader()->getPathname());
					$relocatedPath = $p->getSiteFunctionInterceptor()->user_get_home() . "/${name}";

					return false !== $p->getSiteFunctionInterceptor()->
						file_put_file_contents(
							$relocatedPath,
							file_get_contents($this->getReader()->getPathname())
						);

				}
			);

			return true;
		}
	}


