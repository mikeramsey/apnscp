<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Note;
	use Opcenter\Migration\Notes\NetInfo;
	use Opcenter\Migration\Notes\User;
	use Opcenter\Net\Ip4;
	use Opcenter\Service\Validators\Mysql\Dbaseprefix;
	use Opcenter\SiteConfiguration;

	class MysqlSql extends ConfigurationBuilder
	{
		const PATHMAP_PATH = 'mysql.sql';
		const USER_GRANT_MATCH = '/^GRANT USAGE ON \*\.\* TO \'(?<user>[^\']+)\'@\'(?<host>[^\']+)\' IDENTIFIED BY PASSWORD \'(?<password>[^\']+)\';/';
		const DB_GRANT_MATCH = '/^GRANT (?<privileges>.*?)(?= ON ) ON `(?<db>[^`]+)`\.(?<table>[^ ]+) TO \'(?<user>[^\']+)\'@\'(?<host>[^\']+)\';/';

		// @var string prefix inferred from dump, must include "_"
		private $prefix;

		// @var string primary username
		private $primaryUser;

		// @var array mapping new db name to old name if applicable
		private $dbRemap = [];

		private $hostRemap = [];

		public function parse(): bool
		{
			if (!file_exists($path = $this->getReader()->getPathname())) {
				return true;
			}
			$dbgrants = $usergrants = [];

			// extract
			$lines = file($path, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);

			foreach ($lines as $line) {
				if (preg_match(static::DB_GRANT_MATCH, $line, $match)) {
					if (! ($grants = $this->extractDatabase($match)) ) {
						continue;
					}

					$dbgrants[] = $grants;
				} else if (preg_match(static::USER_GRANT_MATCH, $line, $match)) {
					if (!$grants = $this->extractUser($match)) {
						continue;
					}
					$usergrants[] = $grants;
				} else if (0 !== strncmp($line, '-- ', 3)) {
					warn('Unparseable line. Skipping: %s', $line);
				}
			}
			if (null === $this->prefix) {
				warn('No database prefix detected. Prefix will be automatically assigned');
			} else {
				$this->bill->set('mysql', 'dbaseprefix', $this->prefix);
			}
			if (($user = $this->bill->get('mysql', 'dbaseadmin')) && $user !== $this->bill->get('siteinfo','admin_user')) {
				return error(
					"Database user detected `%(dbuser)s' does not match account admin `%(admin)s' - does user not have password set in MySQL?", [
					'dbuser' => $this->bill->get('mysql', 'dbaseadmin'),
					'admin' => $this->bill->get('siteinfo', 'admin_user')
				]);
			}

			// callback once account is processed
			Cardinal::register(
				[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $p) use ($dbgrants, $usergrants) {
					$newip = Ip4::my_ip();
					$oldip = $this->bill->getNote(NetInfo::class)->getIp() ?? $newip;
					$afi = $p->getSiteFunctionInterceptor();
					if ($afi->common_get_service_value('mysql','dbaseadmin') !== $this->primaryUser) {
						fatal("Primary user `%s' differs from dbaseadmin `%s'", $this->primaryUser, $afi->common_get_service_value('mysql','dbaseadmin'));
					}
					/**
					 * Create users first
					 */
					$usergrants = array_filter($usergrants);
					$dbgrants = array_filter($dbgrants);
					foreach ($usergrants as &$user) {
						if ($user['user'] === $this->primaryUser) {
							// @TODO no way to recover original password for SSO
							if ($user['host'] === 'localhost') {
								foreach (['localhost', '127.0.0.1'] as $host) {
									$afi->mysql_edit_user($user['user'], $host, ['password' => $user['password']]);
								}
							}
							// skip primary user
							continue;
						}

						if ($user['host'] === $oldip) {
							$this->hostRemap[$user['host']] = '127.0.0.1';
							info('Converted user grant %s@%s to %s@%s', $user['user'], $user['host'], $user['user'],
								$newip);
							$user['host'] = $newip;
						} else if ($user['host'] === $this->bill->getNote(Note::symbolic(NetInfo::class))->getHostname()) {
							$this->hostRemap[$user['host']] = '127.0.0.1';
							info('Converted user grant %s@%s to %s@%s', $user['user'], $user['host'], $user['user'],
								'127.0.0.1');
							$user['host'] = '127.0.0.1';
						} else if ($user['host'] !== 'localhost' &&
							strspn($user['host'], '0123456789.[]:%*') !== \strlen($user['host']))
						{
							warn('Entry %s@%s is not supported - skipping', $user['user'], $user['host']);
							continue;
						}

						if ($afi->mysql_user_exists($user['user'], $user['host'])) {
							$afi->mysql_delete_user($user['user'], $user['host']);
						}
						$afi->mysql_add_user($user['user'], $user['host'], $user['password']);
					}
					unset($user);
					/**
					 * Create databases, then apply grants
					 */
					$uniqueDbs = array_unique(array_column(array_filter($dbgrants), 'db'));
					foreach ($uniqueDbs as $db) {
						if ($afi->mysql_database_exists($db)) {
							$afi->mysql_delete_database($db);
						}
						if ($afi->mysql_create_database($db)) {
							$afi->mysql_add_backup($db, 'zip', 3);
						}
						// @todo extract mysql:import into higher level function
						$newfile = tempnam($p->getAccountRoot() . '/tmp/', 'db-import');
						chmod($newfile, 644);
						copy($this->stream->getPrefix() . '/mysql/' . $this->dbRemap[$db] . '.sql', $newfile);
						try {
							$afi->mysql_import($db, $afi->file_unmake_path($newfile));
						} catch (\apnscpException $e) {
							warn('Failed to import %s: %s', $db, $e->getMessage());
						} finally {
							file_exists($newfile) && unlink($newfile);
						}
					}

					foreach ($dbgrants as &$db) {
						if ($db['user'] === $this->bill->getNote(User::class)->getOriginalUser()) {
							info('Primary user always granted access to all databases - skipping grant for %s on %s',
								$db['user'],
								$db['db']
							);

							continue;
						}

						if (isset($this->hostRemap[$db['host']])) {
							$newip = $this->hostRemap[$db['host']];
							info('Converted DB grant on %s.%s from %s@%s to %s@%s',
								$db['db'],
								$db['table'],
								$db['user'],
								$db['host'],
								$db['user'],
								$newip
							);
							$db['host'] = $newip;
						}

						$afi->mysql_set_privileges(
							$db['user'],
							$db['host'],
							$db['db'],
							$db['privileges']
						);
					}
					unset($db);
				}
			);

			return true;
		}

		/**
		 * Extract database from match
		 *
		 * @param array $m
		 * @return array|null
		 */
		private function extractDatabase(array $m): ?array
		{
			$normalizedDB = preg_replace('/(?:\\\\_)/', '_', $m['db']);

			$originalUser = $this->bill->getNote(User::class)->getOriginalUser();
			if (0 === strpos($normalizedDB, "cptmpdb_${originalUser}_")) {
				warn("Skipping temporary cPanel database grant `%s'", $m['db']);
				return null;
			}
			$convertedDB = $normalizedDB;
			// update prefix
			$this->assignPrefix($convertedDB);

			if ($convertedDB[-1] !== '%' && !preg_match(\Regex::SQL_DATABASE, $convertedDB)) {
				fatal("Invalid database name detected `%s'", $m['db']);
			}
			$priv = $m['privileges'];
			if (\in_array(strtoupper($priv), ['ALL PRIVILEGES', 'ALL'], true)) {
				// simplified expression
				$priv = [
					'read' => true,
					'write' => true
				];
			} else {
				$priv = array_fill_keys(preg_split('/\s*,\s*/', $priv), true);
			}
			$this->dbRemap[$convertedDB] = $normalizedDB;
			return [
				'db'         => $convertedDB,
				'table'      => $m['table'],
				'user'       => $m['user'],
				'host'       => $m['host'],
				'privileges' => (array)$priv
			];
		}

		/**
		 * Extract user from match
		 *
		 * @param array $m
		 * @return array|null
		 */
		private function extractUser(array $m): ?array
		{
			$this->assignPrefix($m['user']);

			if (false === strpos($m['user'], '_')) {
				if (null === $this->primaryUser || $m['user'] === $this->bill->get('siteinfo', 'admin_user', null))
				{
					$this->primaryUser = $m['user'];
					$this->bill->set('mysql','dbaseadmin', $this->primaryUser);
				} else {
					warn("Non-namespaced user detected `%(user)s' which does not match admin user `%(admin)s' - ignoring record %(user)s@%(host)s",
						[
							'user' => $m['user'],
							'host' => $m['host'],
							'admin' => $this->bill->get('siteinfo', 'admin_user')
						]
					);
					return null;
				}
			}

			if ($m['user'] === $this->bill->get('siteinfo','admin_user', null) &&
				($newadmin = $this->task->getConfigurationOverrides()->offsetGet('siteinfo.admin_user')))
			{
				// dbaseadmin override, dbaseadmin must match admin_user
				// @XXX note this will require additional checks in the future if dbaseadmin is ever
				// permitted to deviate
				$m['user'] = $newadmin;
			}
			return [
				'user'       => $m['user'],
				'host'       => $m['host'],
				'password'   => $m['password'],
			];
		}

		/**
		 * Update prefix from record
		 *
		 * @param string $prefix
		 * @return bool resolved conflict-free
		 */
		private function assignPrefix(string &$prefix): bool
		{
			$prefix = preg_replace('/\\\\_/', '_', $prefix);
			if (false === ($delim = strpos($prefix, '_'))) {
				return true;
			}

			if (!$this->prefix && ($chk = array_get($this->task->getConfigurationOverrides(), 'mysql.dbaseprefix', null))) {
				// @xxx this is crappy - when dbaseprefix checker drifts it requires updating this parcel of code for consistency
				if ($chk[-1] !== '_') {
					warn('%s always expects prefix to terminate with _ - appending to %s', 'mysql.dbaseprefix', $chk);
					$chk .= '_';
				}
				$this->prefix = $chk;
			}

			if (null === $this->prefix) {
				$this->prefix = substr($prefix, 0, $delim+1);
			}

			if (0 !== strpos($prefix, $this->prefix)) {
				$new = $this->prefix . substr($prefix, $delim+1);
				warn("Prefix `%s' conflicts with new prefix `%s'. Changing %s => %s. Conflicting web app configuration MUST be updated",
					substr($prefix, 0, $delim+1), $this->prefix, $prefix, $new);
				$prefix = $new;
				return false;
			}
			return true;
		}
	}

