<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel;

	use Opcenter\Migration\Formats\ConfigurationOrigin;
	use Opcenter\Migration\Formats\Cpanel\Pathmap\Cp;

	abstract class ConfigurationBuilder extends ConfigurationOrigin
	{
		use \NamespaceUtilitiesTrait;

		// @var array dependency map
		const DEPENDENCY_MAP = [
			Cp::class
		];
	}

