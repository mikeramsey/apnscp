<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Migration;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Module\Support\Webapps\App\Loader;
	use Module\Support\Webapps\Finder;
	use Module\Support\Webapps\Updater;
	use Opcenter\Account\Edit;
	use Opcenter\Service\Plans;
	use Opcenter\SiteConfiguration;
	use Symfony\Component\Yaml\Yaml;

	class Import extends BaseMigration
	{
		// @var Locker
		protected $lock;
		/**
		 * @var Bill
		 */
		protected $bill;

		/**
		 * @var bool create account during import
		 */
		protected $opts = [
			// dump proposed creation
			'dump'   => false,
			// create account
			'create' => true,
			// scan for webapps/update after import
			'scan'   => true,
			// activate DNS at end
			'activate' => true,
			// import DNS records from backup,
			'dns'   => true,
			// remove backup at end
			'delete' => false,
			// try built-in handler
			'builtin' => true,
			// silent fail on forwarded catch-all
			'drop-forwarded-catchalls' => false,
			// fortification profile to apply if any
			'fortification' => null,
			// apply late quota enforcement
			'late-quota' => false,
			// reset to plan specs after import
			'reset' => false,
			// perform dry-run on leg
			'dry-run' => false,
			// permit importing of tainted data
			'unsafe'  => false,
			// bootstrap SSL
			'bootstrap' => true
		];

		/**
		 * @var string conflict resolution on email
		 */
		protected $remediator;

		/**
		 * Import constructor.
		 *
		 * @param string $path
		 * @param array  $opts additional options
		 */
		protected function __construct(string $path, array $opts = [])
		{
			$this->lock = Locker::lock($path);
			if (!isset($opts['format'])) {
				$opts['format'] = (new Detection($this))->match(static::getFormats());
			}
			if (!$opts['format']) {
				fatal("Could not detect format from `%s'", $path);
			}
			if (!\in_array($opts['format'], static::getFormats(), true)) {
				fatal("Unknown format specified `%s'", $opts['format']);
			}
			$this->format = $opts['format'];
			$this->opts = array_replace_recursive($this->opts,
				[
					'activate'      => empty($opts['no-activate']),
					'create'        => empty($opts['no-create']),
					'scan'          => empty($opts['no-scan']),
					'builtin'       => empty($opts['no-builtin']),
					'dump'          => !empty($opts['dump']),
					'delete'        => !empty($opts['delete']),
					'drop-forwarded-catchalls' => !empty($opts['drop-forwarded-catchalls']),
					'fortification' => $opts['apply-fortification'] ?? null,
					'late-quota'    => !empty($opts['late-quota']),
					'reset'         => !empty($opts['reset']),
					'dry-run'       => !empty($opts['dry-run']),
					'unsafe'        => !empty($opts['unsafe']),
					'bootstrap'     => empty($opts['no-bootstrap']),
					'dns'           => empty($opts['no-dns'])
				]
			);

			$this->cfgOverrides = new ConfigurationOverrides(new SiteConfiguration(null, $opts['conf'] ?? []));
			if (!empty($opts['plan'])) {
				if (!Plans::exists($opts['plan'])) {
					fatal("Unknown plan `%s'", $opts['plan']);
				}
				$this->plan = $opts['plan'];
			}
			$this->remediator = $opts['conflict'] ?? 'fail';
		}

		/**
		 * Initiate import from backup file
		 *
		 * @param string $file    file/directory to import from
		 * @param array  $options import options
		 * @return bool
		 */
		public static function fromFile(string $file, array $options = []): bool
		{
			$task = new static($file, $options);
			if (!$task->run()) {
				return false;
			}
			$siteid = \Auth::get_site_id_from_anything($task->getDomain());
			if (null === $siteid) {
				return error('Failed to create domain %s, aborting import!', $task->getDomain());
			}

			$sitesvc = new SiteConfiguration("site${siteid}");
			return Cardinal::fire([\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS], $sitesvc);
		}

		/**
		 * Run task
		 *
		 * @return bool
		 */
		public function run(): bool
		{
			$class = Format::classFrom($this->format);

			$ret = $class::import($this) && !\Error_Reporter::is_error();

			Cardinal::register(
				[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $s) {
					if ($this->getOption('delete')) {
						$this->lock->setDeleteOnRelease(true);
					}

					if (!$this->getOption('create') && $this->getOption('bootstrap', LETSENCRYPT_AUTO_BOOTSTRAP)) {
						// handled by ssl,enabled service definition otherwise
						$s->getSiteFunctionInterceptor()->letsencrypt_bootstrap(null);
					}

					if (!$this->getOption('scan')) {
						return;
					}
					info('Scanning %s for Web Apps', $this->getDomain());
					$finder = new Finder($s->getAuthContext());
					$finder->searchSite();
					foreach ($finder->getActiveApplications() as $path => $meta) {
						if (null === $this->getOption('fortification')) {
							continue;
						}
						$loader = Loader::fromDocroot($meta['type'], $path, $s->getAuthContext());

						if ('release' === ($mode = $this->getOption('fortification'))) {
							$mode = false;
						}

						if (!$loader->setOption('fortify', $mode)) {
							warn('Problem fortifying %(hostname)s/%(path)s in %(fspath)s',
								['hostname' => $meta['hostname'], 'path' => $meta['path'], 'fspath' => $path]);
						}
						$loader->fortify();
					}

					Updater::launch()->limitSite($this->getDomain())->run();
					// post-update hook
					Cardinal::fire(
						[\Opcenter\Account\Import::CALLBACK_HOOK_ID, Events::SUCCESS],
						new class($s, $this->format, $this->getFile()) implements Publisher {
							// @var array publish args
							private $args = [];
							public function __construct(SiteConfiguration $s, string $format, string $file)
							{
								$this->args = [$s, $format, $file];
							}

							public function getEventArgs()
							{
								return $this->args;
							}
						}
					);

					if ($this->getOption('reset')) {
						$isDry = $this->getOption('dry-run');
						if (!(new Edit($s->getSite()))->setOption('dry', $isDry)->setOption('reset', true)->exec())
						{
							warn('Failed to edit domain after import');
						}
					}

					if ($this->getOption('late-quota')) {
						$afi = $s->getSiteFunctionInterceptor();
						$usage = $afi->site_get_account_quota();
						if ($afi->site_amnesty_active() && $usage['qsoft'] && ($usage['qused'] >= ($usage['qsoft']/QUOTA_STORAGE_BOOST)) ) {
							warn('Consumed storage %.2f MB is beyond imposed site quota %.2f MB. ' .
								'Amnesty revocation in %s will force the site to go over storage!',
								\Formatter::changeBytes($usage['qused'], 'MB', 'KB'),
								\Formatter::changeBytes(
									$afi->common_get_service_value('diskquota', 'quota'),
									'MB',
									$afi->common_get_service_value('diskquota','units')
								),
								\Formatter::time(QUOTA_STORAGE_DURATION)
							);
						}
					}
				}
			);
			return $ret;
		}

		/**
		 * Get import file
		 *
		 * @return string
		 */
		public function getFile(): string
		{
			return $this->getLock()->getFile();
		}

		public function getLock(): Locker {
			return $this->lock;
		}

		/**
		 * Detect format from file
		 *
		 * @param string $file
		 * @return string|null
		 */
		protected function detectFormat(string $file): ?string
		{
			foreach (static::getFormats() as $format) {
				if (Format::is($format, $file)) {
					return $format;
				}
			}

			return null;
		}

		/**
		 * Set migration task
		 *
		 * @param Bill $bill
		 */
		public function setMigrationJob(Bill $bill) {
			$this->bill = $bill;
		}

		public function getDomain(): ?string
		{
			return $this->bill->getDomain();
		}

		/**
		 * Get conflict resolution on duplicate addresses
		 *
		 * @return string
		 */
		public function getConflictPolicy(): string
		{
			// hold the symbolic reference until it can be instantiated in hook
			return $this->remediator instanceof Remediator ? $this->remediator->getStrategy() : $this->remediator;
		}

		/**
		 * Commit migration
		 *
		 * @return bool
		 */
		public function commit(): bool
		{
			if (!$this->getOption('create')) {
				return info('Bypassing creation');
			}
			if ($this->getOption('dump')) {
				print Yaml::dump($this->bill->toArray());
				exit(0);
			}
			$ctx = \Auth::context(\Auth::get_admin_login());
			$module = \Admin_Module::instantiateContexted($ctx);
			$cfg = $this->bill->toArray();
			$admin = array_get($cfg, 'siteinfo.admin_user');

			$flags = [];
			if (null !== ($val = $this->getOption('bootstrap'))) {
				$flags['bootstrap'] = $val;
			}

			if (!$module->add_site($this->getDomain(), $admin, $cfg, $flags)) {
				return false;
			}
			if ($this->getOption('late-quota')) {
				$afi = \apnscpFunctionInterceptor::factory(\Auth::context(null, $this->getDomain()));
				$afi->site_storage_amnesty();
			}
			return true;
		}

		/**
		 * Get remediation strategizer for conflicts
		 *
		 * @param SiteConfiguration $s site configuration to derive context
		 * @return Remediator
		 */
		public function getRemediator(SiteConfiguration $s): Remediator {
			if (!$this->remediator instanceof Remediator) {
				$this->remediator = Remediator::create($this->remediator, $s->getAuthContext());
			}
			return $this->remediator;
		}
	}