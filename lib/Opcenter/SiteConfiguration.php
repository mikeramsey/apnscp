<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Illuminate\Contracts\Support\Arrayable;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\ModulePriority;
	use Opcenter\Service\Plans;
	use Opcenter\Service\ServiceLayer;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Siteinfo\Plan;

	/**
	 * Class SiteConfiguration
	 *
	 * Service composition and validation
	 *
	 * Service validation flow:
	 * 1. Take command-line arguments, put in $new
	 * 2. Copy $cur to $old or populate w/ base package config
	 * 3. Validate configuration
	 * 4. For each svc that validates, merge $new => $old, replace $cur
	 *
	 * @package Opcenter
	 */
	class SiteConfiguration implements Arrayable, Publisher
	{
		use \FilesystemPathTrait;
		use \apnscpFunctionInterceptorTrait;

		const STRICT_INTERPRETATION = OPCENTER_STRICT_SVC_CONFIG;
		const DEFAULT_SVC_NAME = OPCENTER_DEFAULT_PLAN;
		const HOOK_ID = 'service';

		/**
		 * backwards compatibility with older platforms
		 */
		const MODULE_REMAP = [
			'bind'      => 'dns',
			'sendmail'  => 'mail',
			'openssl'   => 'ssl',
			'majordomo' => 'mlist',
			'proftpd'   => 'ftp',
			'weblogs'   => 'logs'
		];

		/**
		 * service => module remap
		 */
		const MODULE_CLASS_MAP = [
			'siteinfo'  => 'site',
			'quota'     => 'diskquota',
			'mail'      => 'email',
			'mlist'     => 'majordomo',
			'apache'    => 'web',
			'ipinfo6'   => 'ipinfo',
			'vacation'  => 'email',
			'users'     => 'user',
			'files'     => 'file',
			'logrotate' => 'logs'
		];

		/**
		 * fields ignored when calling --reset
		 */
		const RESET_IGNORED_FIELDS = [
			'siteinfo',
			'ipinfo',
			'ipinfo6',
			'dns.proxy6addr',
			'dns.proxyaddr',
			'dns.key',
			'dns.provider',
			'mail.provider',
			'mail.key',
			'apache.webuser',
		];

		/**
		 * fields when null are applied in --reset
		 *
		 * @TODO inventory resettable fields from service class
		 */
		const RESET_NULLABLE_OK = [
			'aliases.max',
			'apache.subnum',
			'cgroup',
			'diskquota',
			'mlist.max',
			'mysql.dbasenum',
			'pgsql.dbasenum',
			'rampart.max',
			'users.max'
		];

		protected $planName = OPCENTER_DEFAULT_PLAN;

		/**
		 * @var string service configuration site target
		 */
		protected $site;

		/**
		 * Read-only service defaults
		 *
		 * @var array
		 */
		protected $default;
		/**
		 * Old configuration
		 *
		 * @var array
		 */
		protected $old;

		/**
		 * New configuration
		 *
		 * @var array
		 */
		protected $new;

		/**
		 * Current configuration synthesized from new + old + defaults
		 *
		 * @var array
		 */
		protected $cur;

		protected $options;

		/**
		 * @var \Auth_Info_User auth context
		 */
		protected $authContext;

		/**
		 * Service constructor.
		 *
		 * @param null|string $site             site
		 * @param array  $new              new runtime service values, name => conf
		 * @param array  $validatorOptions validator options
		 */
		public function __construct(?string $site, array $new = [], array $validatorOptions = [])
		{
			$this->site = $site ?? 'site0';
			$current = $this->getCurrentConfiguration();
			$masterDefaults = $this->getDefaultConfiguration();

			if (null !== ($plan = array_get($current, ServiceValidator::configize(Plan::class))) && $plan !== Plans::default()) {
				$this->planName = $plan;
				// reinitialize defaults
				$this->default = null;
				$this->getDefaultConfiguration();
			}

			$this->options = $validatorOptions + ['runtime' => $new];
			if (self::STRICT_INTERPRETATION) {
				foreach ($new as $svc => $conf) {
					if (!isset($masterDefaults[$svc])) {
						fatal("service `%s' unknown - disable strict interpretation or install service", $svc);
					}
					$unknown = array_diff_key($conf, $masterDefaults[$svc]);
					if ($unknown) {
						fatal("unknown configuration value(s) `%s' located in service `%s'",
							join(', ', array_keys($unknown)),
							$svc
						);
					}
				}
			}
			if (null !== ($newplan = array_get($new, ServiceValidator::configize(Plan::class)))) {
				$this->setPlanName($newplan);
			}
			// resume failed edit-in-place
			$this->new = array_replace($this->getNewConfiguration(), $new);
			$this->old = array_replace($this->getOldConfiguration(), $current);
		}

		/**
		 * Get plan defaults
		 *
		 * @param string $svc
		 * @return array
		 */
		public function getDefaultConfiguration(string $svc = null): array
		{
			if (null === $this->default) {
				$path = Plans::path($this->planName);
				// merge .skeleton as needed
				$this->default = array_replace_recursive(
					$this->mergeOfficialDefaults(),
					(array)$this->configurationEnumerator($path)
				);
			}

			return (array)array_get($this->default, $svc, []);
		}

		/**
		 * Merge defaults from .skeleton/
		 *
		 * @return array
		 */
		private function mergeOfficialDefaults(): array {
			$key = 'sc.defaults';
			$cache = \Cache_Global::spawn();
			$defaults = $cache->get($key);
			$defaultPath = Plans::path('.skeleton');
			$mtime = filemtime($defaultPath);
			if (!$defaults || $defaults['ttl'] < $mtime) {
				$cfg = $this->configurationEnumerator($defaultPath);
				$defaults = [
					'cfg' => $cfg,
					'ttl' => $mtime
				];
				$cache->set($key, $defaults, 0);
			}
			return $defaults['cfg'];
		}

		/**
		 * Parse configuration from location
		 *
		 * @param string $path
		 * @return array|null
		 */
		protected function configurationEnumerator(string $path): ?array
		{
			if (!is_dir($path)) {
				return null;
			}
			$svcs = [];
			$dh = opendir($path);
			if (!$dh) {
				fatal("failed to open directory `%s'", $path);
			}
			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				}
				$filepath = "${path}/${file}";
				if (!is_file($filepath)) {
					continue;
				}
				$svcs[$file] = $this->scan($filepath);
			}
			closedir($dh);

			return $svcs ?? null;
		}

		/**
		 * Parse text service file
		 *
		 * @param string $file absolute path
		 * @return array
		 */
		protected function scan(string $file): array
		{
			return \Util_Conf::parse_ini($file);
		}

		/**
		 * Get pending configuration
		 *
		 * Not present on deletion
		 *
		 * @param string $svc
		 * @return array
		 */
		public function getNewConfiguration(string $svc = null): array
		{
			if (null === $this->new) {
				$path = $this->domain_info_path('new');
				$this->new = $this->configurationEnumerator($path) ?? [];
			}

			return array_get($this->new, $svc, []);
		}

		/**
		 * Get prior configuration
		 *
		 * Not present on creation
		 *
		 * @param string $svc
		 * @return array
		 */
		public function getOldConfiguration(string $svc = null): array
		{
			if (null === $this->old) {
				$path = $this->domain_info_path('old');
				$this->old = $this->configurationEnumerator($path) ?? [];
			}

			return array_get($this->old, $svc, []);
		}

		/**
		 * Get current site configuration
		 *
		 * @param string $svc
		 * @return array
		 */
		public function getCurrentConfiguration(string $svc = null): array
		{
			if (null === $this->cur) {
				$path = $this->domain_info_path('current');
				$this->cur = (array)$this->configurationEnumerator($path);
			}

			return array_get($this->cur, $svc, []);
		}

		/**
		 * Create instance from context
		 *
		 * @param \Auth_Info_User $user
		 * @return SiteConfiguration
		 */
		public static function import(\Auth_Info_User $user): self
		{
			return new static($user->site);
		}

		/**
		 * Create shallow instance
		 *
		 * Used for templating in an unprivileged environment
		 *
		 * @param \Auth_Info_User $user
		 * @return static
		 */
		public static function shallow(\Auth_Info_User $user): self
		{
			/** @var self $self */
			$self = (new \ReflectionClass(static::class))->newInstanceWithoutConstructor();
			$self->site = $user->site;
			[$self->new, $self->old, $self->cur] = [$user->getAccount()->new, $user->getAccount()->old, $user->getAccount()->cur];

			return $self;
		}

		/**
		 * Get remapped module name on platform change
		 *
		 * @param string $name
		 * @return string remapped name if supported or $name
		 */
		public static function getModuleRemap(string $name): string
		{
			return static::MODULE_REMAP[$name] ?? $name;
		}

		/**
		 * Build simple module priority listing from all modules
		 *
		 * @return array
		 */
		public static function prioritizeAllModules(): array
		{
			$defs = (new static(''))->getDefaultConfiguration();

			return ModulePriority::prioritize(
				array_keys($defs),
				self::buildModuleDependencies(array_keys($defs))
			);
		}

		/**
		 * Validator option is set
		 *
		 * @param string $option
		 * @return mixed|null
		 */
		public function hasValidatorOption(string $option)
		{
			return $this->options[$option] ?? null;
		}

		public function getValidatorOption(string $option, $default = null)
		{
			return $this->options[$option] ?? $default;
		}

		public function __call(string $function, array $args = null)
		{
			fatal("unknown function `%s' in `%s'", $function, \get_class($this));
		}

		/**
		 * Create afi bound to site
		 *
		 * @return \Module_Skeleton
		 */
		public function getSiteFunctionInterceptor(): \apnscpFunctionInterceptor
		{
			return \apnscpFunctionInterceptor::factory($this->getAuthContext());
		}

		public function getAuthContext(): \Auth_Info_User
		{
			if (null === $this->site) {
				fatal('$site property not yet populated - called %s too soon', __METHOD__);
			}
			try {
				if ($this->authContext === null) {
					$this->authContext = \Auth::context(null, $this->site);
				}
			} catch (\TypeError $e) {
				fatal('failed to initialize user');
			}

			return $this->authContext;
		}

		/**
		 * Account is sufficiently provisioned to create afi instance
		 *
		 * @return bool
		 */
		public function canInstantiateInterceptor(): bool
		{
			return null !== \Auth::get_admin_from_site_id($this->getSiteId());
		}

		/**
		 * Get site id
		 *
		 * @return int
		 */
		public function getSiteId(): int
		{
			return (int)substr($this->site, 4);
		}

		public function toArray()
		{
			return $this->cur;
		}

		public function setPlanName(string $plan): self
		{
			if ($plan === DefaultNullable::NULLABLE_MARKER) {
				$plan = Plans::default();
				info('%s is an internal placeholder. Switched to plan name %s',
					DefaultNullable::NULLABLE_MARKER,
					$plan
				);
			}

			if (!Plans::exists($plan)) {
				fatal("Unknown plan `%s'", $plan);
			}
			array_set($this->new, ServiceValidator::configize(Plan::class), $plan);
			$oldPlan = $this->getPlanName();
			$this->planName = $plan;
			// force config reload
			$this->default = null;
			$defaults = $this->getDefaultConfiguration();
			if (!$this->dummy() && $oldPlan !== $plan && !$this->hasValidatorOption('reset')) {
				// plan has changed, apply differences in plan
				$this->new += Plans::diff($plan, $oldPlan);
			} else if ($this->hasValidatorOption('reset')) {
				// --reset used, reapply plan defaults factoring in any runtime overrides
				// @TODO apply these filters pragmatically
				array_forget($defaults, static::RESET_IGNORED_FIELDS);
				$filtered = $nullable = [];
				foreach (static::RESET_NULLABLE_OK as $key) {
					array_set($nullable, $key, 1);
				}

				foreach ($defaults as $service => $arr) {
					$filtered[$service] = array_filter($arr, static function ($v, $param) use ($service, $nullable) {
						// custom metadata
						if (\is_array($v)) {
							return false;
						}

						// whitelisted in RESET_NULLABLE_OK
						if (isset($nullable[$service]) &&
							(!\is_array($nullable[$service]) || isset($nullable[$service][$param]))) {

							return true;
						}

						return null !== $v;
					}, ARRAY_FILTER_USE_BOTH);
				}
				$this->new = $this->new + $filtered;
			}

			return $this;
		}

		/**
		 * Get active plan name
		 *
		 * @return null|string
		 */
		public function getPlanName(): ?string
		{
			return $this->planName;
		}

		/**
		 * Migrate configuration
		 *
		 * @param string $from from service path
		 * @param string $to   to service path
		 * @return bool
		 */
		public function migrateConfiguration(string $from = 'current', string $to = 'old'): bool
		{
			$curpath = $this->domain_info_path($from);
			$oldpath = $this->domain_info_path($to);
			if (!is_dir($curpath) || !is_dir($oldpath)) {
				return error('%(from)s or %(to)s path missing from configuration definitions',
					['from' => $from, 'to' => $to]);
			}
			$success = true;
			$dh = opendir($curpath);
			if (!$dh) {
				fatal("failed to open directory `%s'", $curpath);
			}
			while (false !== ($file = readdir($dh))) {
				if ($file === '.' || $file === '..') {
					continue;
				}
				$filepath = "${curpath}/${file}";
				if (!is_file($filepath)) {
					continue;
				}
				$success &= copy($filepath, "$oldpath/${file}");
			}
			closedir($dh);

			return (bool)$success;
		}

		/**
		 * Get composite service value
		 *
		 * new => old => default => $default parameter
		 *
		 * @param string $svc     service class
		 * @param string $name    service name
		 * @param mixed  $default default value
		 * @return mixed
		 */
		public function getServiceValue(string $svc, ?string $name, $default = null)
		{
			if (isset(static::MODULE_REMAP[$svc])) {
				$svc = static::MODULE_REMAP[$svc];
			}
			if (!$name) {
				return array_replace($this->getDefaultConfiguration($svc), $this->getOldConfiguration($svc),
					$this->getNewConfiguration($svc));
			}
			if (isset($this->new[$svc]) && \array_key_exists($name, $this->new[$svc])) {
				return $this->new[$svc][$name];
			}
			if (isset($this->old[$svc]) && \array_key_exists($name, $this->old[$svc])) {
				return $this->old[$svc][$name];
			}
			if (isset($this->default[$svc]) && \array_key_exists($name, $this->default[$svc])) {
				return $this->default[$svc][$name];
			}

			return $default;
		}

		/**
		 * Remove journaled service configuration
		 *
		 * @param string $svc service name
		 * @return bool
		 */
		public function cleanService(string $svc): bool
		{
			$old = $this->domain_info_path("old/${svc}");
			$new = $this->domain_info_path("new/${svc}");
			$files = [
				$old,
				$new
			];
			foreach ($files as $file) {
				if (!file_exists($file)) {
					if ($file === $old && file_exists($new)) {
						// new service injected, needs EditDomain on all accounts
						warn("`%s' service missing from `%s' - run EditDomain on all accounts to update account metadata",
							$svc,
							\dirname($file),
							$this->site
						);
					} else {
						warn("`%s' service missing from `%s'", $svc, \dirname($file));
					}
					continue;
				}
				unlink($file);
			}

			return true;
		}

		/**
		 * Verify configuration
		 *
		 * @return bool
		 */
		public function verifyAll(): bool
		{
			$configuration = $this->getModulesPrioritized();
			foreach ($configuration as $svc) {
				$ctx = $this->makeConfigurationContext($svc);
				if (!$this->verify($svc, $ctx) || \Error_Reporter::is_error()) {
					return error("failed verification on service `%s'", $svc);
				}
				$this->new[$svc] = $ctx->toArray();
			}

			return true;
		}

		/**
		 * Build prioritized module map
		 *
		 * @return array
		 */
		private function getModulesPrioritized(): array
		{
			return ModulePriority::prioritize(
				array_keys($this->default),
				self::buildModuleDependencies(array_keys($this->default))
			);
		}

		/**
		 * Get dependency map from modules
		 *
		 * @param array $modules module list
		 * @return array
		 */
		public static function buildModuleDependencies(array $modules): array
		{
			$deps = [];
			foreach ($modules as $config) {
				if (null === ($module = self::getModuleFromService($config))) {
					continue;
				}
				$moduleClass = \apnscpFunctionInterceptor::get_autoload_class_from_module($module);
				$deps[$config] = $moduleClass::DEPENDENCY_MAP;
			}

			return $deps;
		}

		/**
		 * Get module name from configuration service
		 *
		 * @param string $svc
		 * @return null|string
		 */
		public static function getModuleFromService(string $svc): ?string
		{
			$module = array_get(self::MODULE_CLASS_MAP, $svc, $svc);
			if (!\apnscpFunctionInterceptor::module_exists($module)) {
				return null;
			}

			return $module;
		}

		public function makeConfigurationContext(string $service): ConfigurationContext
		{
			return new ConfigurationContext($service, $this);
		}

		/**
		 * Verify individual service configuration
		 *
		 * @param string               $svc service name
		 * @param ConfigurationContext $ctx configuration context
		 * @return bool
		 */
		public function verify(string $svc, ConfigurationContext $ctx): bool
		{
			if (!$ctx->preflight()) {
				return false;
			}

			// update new with recomputed values, e.g. dbaseprefix corrections
			$this->new[$svc] = $ctx->getRecomputedValues();

			if (null === ($module = $this->getModuleFromService($svc))) {
				return true;
			}

			$ret = $this->getApnscpFunctionInterceptor()->runHook('_verify_conf', $module, $ctx);

			return $ret;
		}

		public function forceReconfiguration()
		{
			$this->options['reconfig'] = true;
		}

		/**
		 * Write pending configuration to account meta
		 *
		 * @return bool
		 */
		public function writePendingConfiguration(): bool
		{
			$configs = [
				'old'     => $this->getOldConfiguration() ?: $this->getDefaultConfiguration(),
				'current' => $this->getCurrentConfiguration(),
				'new'     => $this->getNewConfiguration()
			];
			foreach ($configs as $ctype => $c) {
				if (!$this->write($ctype, $c)) {
					fatal("failed to commit metadata for `%s'", $ctype);
				}
			}
			touch($this->domain_info_path());
			// mount laayers if necessary, event args are unnecessary
			Cardinal::fire(
				[ServiceLayer::HOOK_ID, Events::SUCCESS],
				new class implements Publisher
				{
					public function getEventArgs()
					{
						return [];
					}
				}
			);

			return true;
		}

		/**
		 * Write configuration
		 *
		 * @param string  $ctype  configuration type [old, new, cur]
		 * @param array[] $config service configuration
		 * @return bool
		 */
		public function write(string $ctype, array $config): bool
		{
			if ($ctype === 'cur') {
				warn("shorthand configuration state `cur' chan`ged to `current'");
				$ctype = 'current';
			}
			if ($ctype !== 'current' && $ctype !== 'new' && $ctype !== 'old') {
				fatal("unknown service meta status `%s'", $ctype);
			}
			$infopath = $this->domain_info_path($ctype);
			if (!is_dir($infopath) && !Filesystem::mkdir($infopath, 'root', 'root', 0700)) {
				fatal("failed to create info directory `%s'", $infopath);
			}

			foreach ($config as $svc => $values) {
				$svcpath = "${infopath}/${svc}";
				$built = \Util_Conf::build_ini($values);
				if (!file_put_contents($svcpath, '[DEFAULT]' . PHP_EOL . $built)) {
					fatal("failed to write configuration to `%s'", $svcpath);
				}
			}

			return true;
		}

		/**
		 * Delete configuration storage on exit
		 */
		public function releaseOnShutdown(): void
		{
			if ($this->site !== 'site0') {
				fatal('Cannot release non-null site!');
			}
			$base = $this->domain_info_path();
			register_shutdown_function(static function () use ($base) {
				if (!file_exists($base)) {
					return;
				}

				silence(static function () {
					Filesystem::delete('site0');
				});
			});
		}

		/**
		 * Get site
		 *
		 * @return string
		 */
		public function getSite(): string
		{
			return $this->site;
		}

		/**
		 * Get service names
		 *
		 * @return array
		 */
		public function getServices(): array
		{
			return $this->getModulesPrioritized();
		}

		/**
		 * Invoked against actual site
		 *
		 * @return bool
		 */
		private function dummy(): bool
		{
			return $this->site === 'site0';
		}

		/**
		 * Get account root
		 *
		 * @return string
		 */
		public function getAccountRoot(): string
		{
			return $this->domain_fs_path();
		}

		public function getEventArgs()
		{
			return $this;
		}
	}
