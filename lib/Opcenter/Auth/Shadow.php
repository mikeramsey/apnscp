<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Auth;

	use Opcenter\Contracts\Virtualizable;

	class Shadow implements Virtualizable
	{
		const SHADOW_FILE = '/etc/shadow';

		protected $root;

		public function __construct(string $root = null)
		{
			$this->root = $root;
		}

		public static function bindTo(string $root = '/')
		{
			$instance = new static($root);

			return $instance;
		}

		/**
		 * Password is valid crypted format
		 *
		 * @param string $cpassword
		 * @return bool
		 */
		public static function valid_crypted(string $cpassword): bool
		{
			if ($cpassword[0] != '$' || ($cpassword[2] != '$' && $cpassword[3] != '$')) {
				// blowfish uses 2x notation
				return error('password must be encrypted via crypt()');
			} else if (!\in_array($cpassword[1], ['1', '2', '5', '6'])) {
				/**
				 * Supported crypt types from crypt(3)
				 */
				return error("invalid crypt type `%s'", $cpassword[1]);
			} else if (!self::hash_supported($cpassword[1])) {
				return error("unsupported crypt type, `%d'", $cpassword[1]);
			}

			return true;
		}

		/**
		 * Password matches hash
		 *
		 * @param string $password plain-text password
		 * @param string $crypt crypted password
		 * @return bool
		 */
		public static function verify(string $password, string $crypt): bool
		{
			return password_verify($password, $crypt);
		}

		/**
		 * Query system for support for given hash indicator
		 *
		 * @param string $version
		 * @return bool
		 */
		public static function hash_supported(string $version): bool
		{
			switch ($version) {
				case '6':
					$hash = 'CRYPT_SHA512';
					break;
				case '5':
					$hash = 'CRYPT_SHA256';
					break;
				case '2':
				case '2a':
					$hash = 'CRYPT_BLOWFISH';
					break;
				case '1':
					$hash = 'CRYPT_MD5';
					break;
				default:
					return false;

			}

			return \defined($hash) && \constant($hash);
		}

		/**
		 * Crypt a password optionally using the best supported cipher
		 *
		 * @param string      $password plain-text password
		 * @param string|null $salt     optional salt to use
		 * @return bool|string
		 */
		public static function crypt(string $password, $salt = null)
		{
			if (!$salt) {
				$salt = self::mksalt();
			} else if ($salt[0] != '$') {
				return error("malformed salt `%s'", $salt);
			} else if (!self::hash_supported($salt[1])) {
				return error("unknown hash requested `%s'", $salt[1]);
			} else {
				return crypt($password, $salt);
			}
			// let the API autoselect the best possible hash
			$platform_hashes = array(
				'CRYPT_SHA512'   => '6',
				'CRYPT_SHA256'   => '5',
				'CRYPT_BLOWFISH' => '2a',
				'CRYPT_MD5'      => '1'
			);
			$hash = $marker = null;
			foreach ($platform_hashes as $h => $m) {
				if (self::hash_supported($m)) {
					$hash = $h;
					$marker = $m;
					break;
				}
			}
			if (!$hash) {
				return error('no suitable hashes supported on platform');
			}
			$salt = '$' . $marker . '$' . $salt;

			return crypt($password, $salt);
		}

		/**
		 * Generate a salt for crypt
		 *
		 * @return string
		 */
		public static function mksalt(): string
		{
			$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
			$salt = '';
			// 16 is max length for sha512. crypt() will truncate to acceptable
			// length, with 8 being the smallest
			for ($i = 0, $len = \strlen($chars) - 1; $i < CRYPT_SALT_LENGTH; $i++) {
				$r = random_int(0, $len);
				$salt .= $chars[$r];
			}

			return $salt;
		}

		/**
		 * Set crypted password for user
		 *
		 * @param string $cpassword
		 * @param string $user
		 * @return bool
		 */
		public function set_cpasswd(string $cpassword, string $user): bool
		{
			$user = strtolower($user);
			$proc = new \Util_Process_Chroot($this->root);
			$status = $proc->run('/usr/sbin/usermod -p %(passwd)s %(user)s', [
				'passwd' => escapeshellarg($cpassword),
				'user'   => escapeshellarg($user)
			]);

			return $status['success'];
		}

		/**
		 * Virtualized getspnam()
		 *
		 * @param null|string $user dump shadow on null
		 * @return null|array
		 */
		public function getspnam(?string $user): ?array
		{
			$pwd = array();
			$file = rtrim($this->root, '/') . static::SHADOW_FILE;
			$fp = fopen($file, 'r');
			if (!$fp) {
				error('unable to open %s', static::SHADOW_FILE);

				return null;
			}
			$found = false;
			while (false !== ($line = fgets($fp))) {
				$line = explode(':', trim($line));
				if (\count($line) < 6) {
					continue;
				}
				$myuser = $line[0];
				if ($line[0] === $user) {
					$found = true;
				}
				$pwd[$myuser] = array(
					'shadow'   => $line[1],
					'lastchg'  => (int)$line[2],
					'mindays'  => (int)$line[3],
					'maxdays'  => (int)$line[4],
					'warndays' => (int)$line[5],
					'inactive' => (int)$line[6],
					'expire'   => (int)$line[7],
					// compatibility with cPanel that leaves this field optional
					'flag'     => (int)($line[8] ?? 0)
				);
			}
			fclose($fp);
			if (!$user) {
				return $pwd;
			} else if (!$found) {
				return null;
			}

			return $pwd[$user];
		}
	}