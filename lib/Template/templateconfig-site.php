<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */
	/** @var Template_Engine $templateClass */
	$templateClass->create_category(
		'Account', // canonical name
		true,               // assertions (may be an array)
		'/images/template/app-index/account.png',  // image for the category
		'account',             // internal name
		'/images/template/app-icons/account.png' // Optional mini icon, 16x16

	);

	$templateClass->create_category(
		'Users',
		\cmd('common_get_service_value', 'users', 'enabled'),
		'/images/template/app-index/users.png',
		'users'
	);

	$templateClass->create_category(
		'Mail',
		\cmd('email_enabled'),
		'/images/template/app-index/mail.png',
		'mail'
	);

	$templateClass->create_category(
		'Files',
		true,
		'/images/template/app-index/files.png',
		'files'
	);

	$templateClass->create_category(
		'Databases',
		\cmd('mysql_enabled') || \cmd('pgsql_enabled'),
		'/images/template/app-index/databases.png',
		'databases'
	);

	$templateClass->create_category(
		'DNS',
		true,
		'/images/template/app-index/dns.png',
		'dns'
	);


	$templateClass->create_category(
		'Web',
		\cmd('common_get_service_value', 'apache', 'enabled'),
		'/images/template/app-index/web.png',
		'web'
	);

	$templateClass->create_category(
		'Tools',
		false,
		'/images/template/app-index/tools.png',
		'tools'
	);


	$templateClass->create_category(
		'Dev',
		true,
		'/images/template/app-index/goodies.png',
		'dev'
	);

	$templateClass->create_category(
		'Reports',
		true,
		'/images/template/app-index/reports.png',
		'reports'
	);

	$templateClass->create_category(
		'Help',
		true,
		'/images/template/app-index/help.png',
		'help'
	);

	$templateClass->create_category(
		'Miscellaneous',
		true,
		null,
		'misc'
	)->hide();

	#/*********************/
	#/*   Link Creation   */
	#/*********************/
	#/*
	#*  Site Maintenance
	#*/

	/***************** begin Maintenance links *****************/
	$templateClass->create_link(
		'Dashboard',     // name of the link to where it's displayed
		array('/apps/dashboard', '/apps/dashboard-old'), // href to the link
		true,                     // conditions required to be true to show
		null,                     // icon to display, if you like icons.
		null                      // categorical reference (uses short name), null is top-level
	);
	$templateClass->create_link(
		'Summary',
		'/apps/summary',
		true,
		null,
		'account'
	);
	$templateClass->create_link(
		'Settings',
		array('/apps/changeinfo'),
		true,
		null,
		'account'
	);

	$templateClass->create_link(
		'Whitelisting',
		'/apps/whitelist',
		\cmd('rampart_can_delegate'),
		null,
		'account'
	);

	$templateClass->create_link(
		'Login History',
		'/apps/loginhistory',
		true,
		null,
		'account'
	);

	/***************** end Maintenance links *****************/

	/***************** begin User Management links *****************/
	$templateClass->create_link(
		'Create User',
		'/apps/useradd',
		true,
		null,
		'users'
	);

	$templateClass->create_link(
		'Manage Users',
		array(
			'/apps/usermanage',
			'/apps/useredit'
		),
		true,
		null,
		'users'
	);

	$templateClass->create_link(
		'Set User Defaults',
		'/apps/userdefaults',
		true,
		null,
		'users'
	);
	/***************** end User Management links *****************/

	/***************** begin Reports links *****************/
	$templateClass->create_link(
		'Bandwidth Breakdown',
		'/apps/bandwidthbd',
		\cmd('bandwidth_enabled'),
		null,
		'reports'
	);

	$templateClass->create_link(
		'Bandwidth Statistics',
		'/apps/bandwidthstat',
		\cmd('bandwidth_enabled'),
		null,
		'reports'
	);

	$templateClass->create_link(
		'Storage Tracker',
		'/apps/quotatracker',
		true,
		null,
		'reports'
	);

	$templateClass->create_link(
		'Server Info',
		'/apps/stats',
		true,
		null,
		'reports'
	);

	$templateClass->create_link(
		'Log Rotation',
		'/apps/logrotate',
		true,
		null,
		'reports'
	);

	/***************** end Reports links *****************/

	/***************** begin Databases links *************/
	$templateClass->create_link(
		'MySQL Manager',
		'/apps/changemysql',
		\cmd('mysql_enabled'),
		null,
		'databases'
	);

	$templateClass->create_link(
		'phpMyAdmin',
		'/apps/phpmyadmin',
		\cmd('mysql_enabled'),
		null,
		'databases'
	);

	$templateClass->create_link(
		'MySQL Backups',
		'/apps/mysqlbackup',
		\cmd('mysql_enabled'),
		null,
		'databases'
	);

	$templateClass->create_link(
		'PostgreSQL Manager',
		'/apps/changepgsql',
		\cmd('pgsql_enabled'),
		null,
		'databases'
	);

	$templateClass->create_link(
		'phpPgAdmin',
		'/apps/phppgadmin',
		\cmd('pgsql_enabled'),
		null,
		'databases'
	);

	$templateClass->create_link(
		'PostgreSQL Backups',
		'/apps/pgsqlbackup',
		\cmd('pgsql_enabled'),
		null,
		'databases'
	);
	/***************** end Databases links ****************/

	/***************** begin Web Server links *************/

	$templateClass->create_link(
		'Web Apps',
		'/apps/webapps',
		true,
		null,
		'web'
	);

	$templateClass->create_link(
		'Site Optimizer',
		'/apps/pagespeed',
		true,
		null,
		'web'
	);

	$templateClass->create_link(
		'Subdomains',
		['/apps/subdomains?mode=add', '/apps/subdomains'],
		true,
		null,
		'web'
	);

	$templateClass->create_link(
		'.htaccess Manager',
		'/apps/personalities',
		true,
		null,
		'web'
	);

	$templateClass->create_link(
		'PHP Pools',
		'/apps/php-pools',
		\cmd('php_jailed'),
		null,
		'web'
	);

	/**
	 * Make it accessible, but change the title
	 */

	$ssl = $templateClass->create_link(
		'SSL Certificates',
		'/apps/ssl',
		true,
		null,
		'web'
	);

	$templateClass->create_link(
		'Granular Logging',
		'/apps/weblogging',
		false,
		null,
		'web'
	);

	$templateClass->create_link(
		'WebDAV',
		'/apps/webdav',
		DAV_APACHE,
		null,
		'web'
	);

	$templateClass->create_link(
		'FrontPage Extensions',
		'/apps/frontpage',
		version_compare(platform_version(), '6.5', '<'),
		null,
		'web'
	);

	/***************** end Web Server links ****************/

	/***************** begin Files links *****************/
	$templateClass->create_link(
		'File Manager',
		'/apps/filemanager',
		true,
		null,
		'files'
	);


	$templateClass->create_link(
		'Storage Usage',
		'/apps/diskbd',
		true,
		null,
		'files'
	);

	$templateClass->create_link(
		'WebDisk',
		'/apps/webdisk',
		cmd('auth_user_permitted', \Session::get('username'), 'dav'),
		null,
		'files'
	);

	$templateClass->create_link(
		'Backup',
		'/apps/backup',
		false,
		null,
		'files'
	);
	$templateClass->create_link(
		'* Scheduled Backups',
		'/apps/schedbackup',
		false,
		null,
		'files'
	);
	$templateClass->create_link(
		'Restore',
		'/apps/restore',
		false,
		null,
		'files'
	);
	$templateClass->create_link(
		'* ACL Manager',
		'/apps/aclmanager',
		false,
		null,
		'files'
	);

	/***************** end Files links *****************/

	/***************** begin E-Mail links *************/



	$templateClass->create_link(
		'Create Mailbox',
		'/apps/mailboxcreate',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Manage Mailboxes',
		'/apps/mailboxroutes',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Webmail',
		'/apps/webmail',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Mail Routing',
		'/apps/mailertable',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Forward Admin E-mail',
		'/apps/forwarder',
		false,
		null,
		'mail'
	);

	$templateClass->create_link(
		'Mailing Lists',
		'/apps/majordomo',
		\cmd('majordomo_enabled'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Spam Filter',
		'/apps/saconfig',
		\cmd('email_configured'),
		null,
		'mail'
	);

	$templateClass->create_link(
		'Mail Logs',
		'/apps/maillog',
		false,
		null,
		'mail'
	);

	$templateClass->create_link(
		'Vacation Responder',
		'/apps/vacation',
		\cmd('email_configured'),
		null,
		'mail'
	);


	/***************** end E-Mail links ****************/

	/***************** begin DNS links *************/
	$templateClass->create_link(
		'DNS Manager',
		'/apps/dns',
		cmd('common_get_service_value', 'dns', 'enabled'),
		null,
		'dns'
	);
	$val = $this->common_get_service_value('aliases', 'max');
	$templateClass->create_link(
		'Addon Domains',
		'/apps/domainmanager',
		$val !== 0,
		null,
		'dns'
	);

	$templateClass->create_link(
		'CloudFlare',
		'/apps/cloudflare',
		is_debug(),
		null,
		'dns'
	);

	$templateClass->create_link(
		'SPF Setup',
		'/apps/spf',
		true,
		null,
		'dns'
	);

	$templateClass->create_link(
		'Traceroute',
		'/apps/traceroute',
		true,
		null,
		'dns'
	);

	$templateClass->create_link(
		'Whois',
		'/apps/whois',
		true,
		null,
		'dns'
	);

	/***************** end DNS  links ****************/

	/***************** begin resources links **************/
	$templateClass->create_link(
		'Setup Instructions',
		'/apps/setup',
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'Help Center',
		MISC_KB_BASE,
		(bool)MISC_KB_BASE,
		null,
		'help'
	);



	$templateClass->create_link(
		'API Docs',
		'https://api.apnscp.com/',
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'Terminal',
		'/apps/terminal',
		SSH_EMBED_TERMINAL && Template_Engine::call('ssh_enabled') && !Template_Engine::call('auth_is_demo'),
		null,
		'dev'
	);

	$templateClass->create_link(
		'Code Frameworks',
		'/apps/prlanguages',
		true,
		null,
		'dev'
	);

	$templateClass->create_link(
		'Version Control',
		'/apps/verco',
		true,
		null,
		'dev'
	);

	$templateClass->create_link(
		'Package Manager',
		'/apps/packman',
		true,
		null,
		'dev'
	);

	$templateClass->create_link(
		'API Keys',
		'/apps/soapkeys',
		true,
		null,
		'dev'
	);

	$templateClass->create_link(
		'API Explorer',
		'/apps/apiexplorer',
		is_debug(),
		null,
		'dev'
	);

	$templateClass->create_link(
		'Task Scheduler',
		'/apps/crontab',
		Template_Engine::call('crontab_permitted', array('ssh', 'enabled')),
		null,
		'dev'
	);

	/*************************
	 *  Miscellaneous apps referenced elsewhere
	 *  within the CP
	 *************************/
	$templateClass->create_link(
		'Sitemap',
		'/apps/sitemap',
		true,
		null,
		'misc'
	);

	$templateClass->create_link(
		'Changelog',
		'/apps/changelog',
		true,
		null,
		'misc'
	);

	// general error handler
	$templateClass->create_link(
		'Error',
		'/apps/error',
		true,
		null,
		'misc'
	);

	/***************** end resources links **************/
	/*
	*  Site Info Table Information
	*/

	$function = apnscpFunctionInterceptor::init();

	/*
	$templateClass->create_info(
		"Web Server Name",                       // Name
		$function->common_get_web_server_name(), // Value
		false,                                    // Assertions
		NULL,                                    // DOM ID
		1,                                       // Span value
		NULL,                                    // Help token
		NULL                                     // Help title
	);
	*/
	// triplet */


	$expirations = Template_Engine::call('dns_get_pending_expirations', array(180));
	if (!$expirations) {
		$expstr = '(n/a)';
	} else {
		$expstr = "<span class='overview-pair'>" .
			$expirations[0]['domain'] . '</span>' .
			"<span class='d-block quiet overview-pair'>" . date('F jS, Y', $expirations[0]['ts']) . '</span>';
	}
	$templateClass->create_info(
		'Expiring Domains',
		$expstr,
		true,
		'domain-exp',
		1,
		'Domain expiration dates that are known and within 60 days of expiration are listed.  ' .
		'This is a reminder system and <em>should not</em> be relied upon to provide accurate information.<br /><br />' .
		'Expiration reminders are sent as a courtesy to our users &ndash; just make sure the e-mail address is up-to-date .  ' .
		"The primary account address can be changed thru 'Settings'.",
		'Domain Expirations'
	);

	if (Template_Engine::call('billing_configured')) {
		$standing = Template_Engine::call('billing_get_status');
		if ($standing === 0) {
			$hash = Template_Engine::call('billing_get_renewal_hash');
		}
		$templateClass->create_info(
			'Billing Status',
			'<span class="overview-pair">' .
			(($standing === 1) ? 'Automatic Renewal' :
				(($standing === -1) ? 'Manual Renewal' :
					(($standing === 0) ? "Pending Suspension</span><span class='overview-pair d-block quiet'><a href='" . \cmd('billing_get_renewal_link', $hash) . "'>Renew</a>"
						: 'UNKNOWN'))) . '</span>',
			true,
			null,
			1,
			'<em>Automatic Renewal</em>&ndash; payment will be automatically withdrawn from the payment source on <em>Next Billing Date</em>' .
			'<br /><br /><em>Manual Renewal</em>&ndash; automated billing has been removed from the account.  Account must be paid in full before <em>Next Billing Date</em> to avoid interruption.' .
			'<br /><br /><em>Pending Suspension</em>&ndash; payment is within 30 days of due date or payment failed resulting in subscription cancellation.' .
			'  The account will be suspended 30 days after non-payment.',
			'Status Types'
		);

		$next_date = Template_Engine::call('billing_get_next_payment');
		$templateClass->create_info(
			'Next Billing Date',
			'<span class="overview-pair">' . ($next_date['date'] > 0 ? date('F j, Y',
					$next_date['date']) . "</span><span class='overview-pair d-block quiet'>" . sprintf('$%.2f',
					$next_date['amount']) . '</span>' : '</span>n/a'),
			true,
			null,
			1,
			'Payment <em>' . ($standing === 1 ? 'will' : 'will not') . '</em> be automatically withdrawn on this date.',
			'Next Billing Date'
		);
	}


	// triplet */

	$loads = Template_Engine::call('common_get_load');
	if (!is_array($loads)) {
		Error_Reporter::report(var_export($loads, true));
		$loads = array(null, null, null);
	}
	$templateClass->create_info(
		'Load Average',
		implode(', ', $loads),
		true,
		null,
		1,
		'LAs tell how many processes are enqueued, pending execution.  Lower numbers ' .
		'imply more time spent waiting, while higher numbers are indicative of bottlenecks and
            degraded server performance.' .
		'<br /><br />A LA of <em>' . sprintf('%.1f',
			NPROC) . ' is optimal</em> for this server &ndash; CPUs are always working ' .
		'and never wasting energy.',
		'Load Averages'
	);

	$rebootdays = Template_Engine::call('common_get_uptime', array(false));
	$reboot = time() - $rebootdays;
	$templateClass->create_info(
		'Last Reboot',
		date('F j, Y', $reboot) . ' (' . round($rebootdays / 86400) . ' days)',
		true,
		null,
		1,
		'Duration since the last server reboot.',
		'Uptime'
	);
	if (Template_Engine::call('billing_configured')) {
		$mult = Template_Engine::call('billing_referral_multiplier');
		if (!Template_Engine::call('billing_referral_profile_exists')) {
			$tip = 'Earn revenue for referring clients to our services.  Get $5 just for signing up!  Access ' .
				'Account -&gt; Client Referrals';
		} else {
			$tip = 'Current mulitplier is <strong>' . round($mult, 1) . 'X</strong>.<br /><br />';
			$needed = Template_Engine::call('billing_referral_upgrade_needed');
			if (is_null($needed['next'])) {
				$tip .= 'Congrats!  You are at the current maximum reward level!';
			} else {
				if ($needed['method'] == 'client') {
					$tip .= 'Referring ' . $needed['next'] . ' more client' . ($needed['next'] > 1 ? 's' : '') . ' will ';
					$tip .= 'increase your referral multiplier to <strong>(' . round($mult + 0.5, 1) . 'X)</strong>';
				} else {
					if ($needed['next'] > 180) {
						$time = round($needed['next'] / 30) . ' months';
					} else {
						$time = $needed['next'] . ' days';
					}
					$tip .= 'After <strong>' . $time . '</strong> the referral multiplier will increase to ' .
						'<strong>(' . round($mult + 0.5, 1) . 'X)' . '</strong>';
				}
			}
		}

		$formatter = NumberFormatter::create(\Auth::profile()->language, NumberFormatter::CURRENCY);
		$templateClass->create_info(
			'Referral Credit',
			$formatter->format(Template_Engine::call('billing_referral_balance')) . ' (' . round($mult, 1) . 'X)',
			true,
			null,
			1,
			$tip,
			'Referrals'
		);
	}

	$templateClass->create_info(
		'Admin Email',
		Template_Engine::call('common_get_admin_email'),
		false,
		null,
		1,
		'Always ensure this value is up-to-date.  The administrative e-mail ' .
		'for an account acts as the primary liasion for keeping you current ' .
		'with billing and domain notices.  In the event you change your e-mail ' .
		'address, please use the &quot;Settings&quot; link in the ' .
		'control panel to update it.',
		'Admin E-mail'
	);


	// triplet */

	$ucard = UCard::init();
	$firstLogin = !$ucard->lastAccess();

	$templateClass->create_info(
		'Server Name',
		SERVER_NAME,
		$firstLogin,
		null
	);

	$templateClass->create_info(
		'IP Address',
		Template_Engine::call('dns_get_public_ip'),
		$firstLogin,
		null
	);

	if (\Template_Engine::call('common_get_service_value', array('ipinfo', 'namebased'))) {
		$preview = MISC_KB_BASE . '/dns/previewing-your-domain/';
	} else {
		$preview = 'http://' . Template_Engine::call('common_get_service_value',
				array('ipinfo', 'ipaddrs'))[0] . '/';
	}
	$templateClass->create_info(
		'Domain Preview',
		'<a href="' . $preview . '">Instructions</a>',
		$firstLogin,
		null
	);

	// triplet */

	// triplet */
	$ver = Template_Engine::call('sql_mysql_version', array(true));
	$templateClass->create_info(
		'MySQL Version (MariaDB)',
		sprintf('%s (%d.%d.%d)', $ver, ...str_split(MySQL::initialize()->server_version, 2)),
		$firstLogin,
		null
	);

	$templateClass->create_info(
		'Perl Version',
		Template_Engine::call('perl_version'),
		$firstLogin
	);

	$templateClass->create_info(
		'PHP Version',
		Template_Engine::call('php_version'),
		$firstLogin
	);

	$userCount = Template_Engine::call('user_get_user_count');
	$templateClass->create_info(
		'# Users',
		sprintf('%u', $userCount['users']),
		$firstLogin,
		null
	);

	$sincetitle = 'Active Since';
	if (Template_Engine::call('billing_configured')) {
		$sincetitle = 'Customer Since';
		$type = Template_Engine::call('billing_get_package_type');
		$templateClass->create_info(
			'Package Type',
			!$type ? 'UNKNOWN' : $type,
			$firstLogin,
			null
		);
	}

	$customer_since = Template_Engine::call('billing_get_customer_since');
	$templateClass->create_info(
		$sincetitle,
		($customer_since ? date('F Y', $customer_since) : 'UNKNOWN'),
		$firstLogin,
		null
	);

	$templateClass->create_info(
		'Account ID',
		$_SESSION['site_id'],
		$firstLogin,
		null,
		'Each account is assigned a unique number between 1 and 999 used with filesystem structure and TCP services (if enabled).'
	);

	$ports = 'N/A';
	if (Template_Engine::call('ssh_enabled')) {
		$ports = Template_Engine::call('ssh_port_range');
		if (!empty($ports)) {
			if (!is_array($ports[0])) {
				$ports = '[' . implode(',', $ports) . ']';
			} else {
				$ptmp = array();
				foreach ($ports as $p) {
					list($min, $max) = $p;
					$ptmp[] = '[' . $min . ',' . $max . ']';
				}
				$ports = implode(', ', $ptmp);
			}
		}
	}
	if (SSH_USER_DAEMONS) {
		$templateClass->create_info(
			'Reserved Ports',
			$ports ?: _('N/A'),
			$firstLogin,
			null,
			1,
			'Want to run a daemon on the server?  The following port range is reserved on the server specifically for your services.',
			'Reserved Ports'
		);
	}
