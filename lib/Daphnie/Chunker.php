<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2020
 */

namespace Daphnie;

use Opcenter\Apnscp;
use Opcenter\Process;

/**
 * Class Chunker
 *
 * Metric chunk management
 *
 * @package Daphnie
 */
class Chunker {
	/**
	 * ───▐▀▄──────▄▀▌───▄▄▄▄▄▄▄─────────────
	 * ───▌▒▒▀▄▄▄▄▀▒▒▐▄▀▀▒██▒██▒▀▀▄──────────
	 * ──▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄────────
	 * ──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄──────
	 * ▀█▒▒█▌▒▒█▒▒▐█▒▒▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
	 * ▀▌▒▒▒▒▒▀▒▀▒▒▒▒▒▀▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐───▄▄
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌▄█▒█
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐▒█▀─
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐▀───
	 * ▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌────
	 * ─▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐─────
	 * ─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
	 * ──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐──────
	 * ──▐▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▌──────
	 * ────▀▄▄▀▀▀▀▄▄▀▀▀▀▀▀▄▄▀▀▀▀▀▀▄▄▀────────
	 *
	 * Special notes on older/newer usage:
	 *
	 * $older<TYPE> parameters specify any record older than TS
	 * when $newer<TYPE> is provided, it brackets the range
	 *
	 * older: OLDER < RECORDTS
	 * older + newer: OLDER < RECORDTS AND RECORDTS > NEWER (or RECORDTS ε (NEWER, OLDER))
	 *
	 */

	protected $db;

	// @var string[] decompressed chunks
	private static $decompressed = [];

	// @var int[] job scheduling suspended
	private static $suspended = [];

	public function __construct(\PDO $db)
	{
		$this->db = $db;
	}

	public function __destruct()
	{
		if (!self::$decompressed && !self::$suspended) {
			return;
		}

		$this->release();
	}

	/**
	 * Release a paused compression routine
	 */
	public function release()
	{
		while (null !== ($chunk = array_shift(self::$decompressed))) {
			$this->compress($chunk);
		}

		while (null !== ($jobId = array_shift(self::$suspended))) {
			$this->resumeJob($jobId);
		}
	}

	/**
	 * List data segments
	 *
	 * @param string|null $olderInt
	 * @param string|null $newerInt
	 * @return array
	 */
	public function listChunks(?string $olderInt = null, string $newerInt = null): array
	{
		if ($olderInt && !$this->validInterval($olderInt)) {
			return [];
		} else if ($newerInt && !$this->validInterval($newerInt)) {
			return [];
		}

		$params = [
			$this->db->quote('metrics')
		];
		if ($olderInt) {
			$params['older_than'] = 'INTERVAL ' . $this->db->quote($olderInt);
		}
		if ($newerInt) {
			$params['newer_than'] = 'INTERVAL ' . $this->db->quote($newerInt);
		}

		$rs = $this->db->query('SELECT show_chunks(' . implode(',', $params) . ');');
		if (!$rs) {
			error('Chunk failed: %s', $this->db->errorInfo()[2]);
			return [];
		}

		return $rs->fetchAll(\PDO::FETCH_COLUMN);
	}

	/**
	 * Metric storage has compression
	 *
	 * @return bool
	 */
	public function hasCompression(): bool
	{
		$rs = $this->db->query("SELECT * FROM timescaledb_information.compressed_hypertable_stats WHERE hypertable_name::varchar = 'metrics'");
		return $rs && $rs->rowCount() > 0;
	}

	/**
	 * Synchronously decompress a range of data segments
	 *
	 * @param int|null $olderTs
	 * @param int|null $newerTs
	 * @return int|null
	 */
	public function decompressRange(?int $olderTs, int $newerTs = null): ?int
	{
		$pid = $this->suspendJobRunner();
		defer($_, function () use ($pid) {
			$this->resumeJobRunner($pid);
		});

		$this->getJobs();
		foreach ($this->getJobs() as $job) {
			$this->pauseJob($job['job_id']);
		}

		$chunks = $this->getChunksFromRange($olderTs, $newerTs);
		$cnt = 0;
		if (!$chunks) {
			return $cnt;
		}
		foreach ($chunks as $chunk) {
			debug("Decompressing $chunk");
			$cnt += (int)$this->decompress($chunk);
		}

		return $cnt;
	}

	/**
	 * Clear chunks marked for compression
	 */
	public function clearTransientRecompressionChunks(): void
	{
		self::$decompressed = [];
		self::$suspended = [];
	}

	/**
	 * Table was previously compressed
	 *
	 *
	 *
	 * @return bool
	 */
	public function hasCompressionArtifacts(): bool
	{
		$res = $this->db->query("SELECT compressed_hypertable_id 
			FROM _timescaledb_catalog.hypertable WHERE table_name = 'metrics' AND compressed_hypertable_id IS NOT NULL");
		if (!$res) {
			$err = $this->db->errorInfo();
			return error('Failed to query artifacts: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
		}

		return $res->rowCount() > 0;
	}

	/**
	 * Synchronously compress a range of data segments
	 *
	 * @param int|null $olderTs
	 * @param int|null $newerTs
	 * @return int|null
	 */
	public function compressRange(?int $olderTs, int $newerTs = null): ?int
	{
		$chunks = $this->getChunksFromRange($olderTs, $newerTs);
		$cnt = 0;
		if (!$chunks) {
			return $cnt;
		}
		foreach ($chunks as $chunk) {
			debug("Compressing $chunk");
			$cnt += (int)$this->compress($chunk);
		}
		$this->resumeJobRunner();

		return $cnt;
	}

	private function suspendJobRunner(): ?int {
		$path = run_path(Apnscp::HOUSEKEEPER_PID);
		if (!file_exists($path)) {
			return null;
		}
		$pid = (int)file_get_contents($path);
		if (!Process::pidMatches($pid, PHP_BINARY)) {
			return null;
		}
		posix_kill($pid, SIGTSTP);

		return $pid;
	}

	private function resumeJobRunner(int $pid = null): void {
		if (!$pid) {
			$path = run_path(Apnscp::HOUSEKEEPER_PID);
			if (!file_exists($path)) {
				return;
			}
			$pid = (int)file_get_contents($path);
		}

		if (!Process::pidMatches($pid, PHP_BINARY)) {
			return;
		}

		posix_kill($pid, SIGCONT);
	}

	/**
	 * Get chunks
	 *
	 * @link https://docs.timescale.com/latest/api#show_chunks
	 *
	 * Unlike show_chunks(), returned chunk list is inclusive of $olderTs
	 *
	 * @param int|null $olderTs older_than
	 * @param int|null $newerTs newer_than
	 * @param bool     $boundaryAlignment chunks must be inclusive of timestamps
	 * @return array
	 */
	public function getChunksFromRange(?int $olderTs, int $newerTs = null, bool $boundaryAlignment = true): array
	{
		if ($boundaryAlignment) {
			// ensure chunks include $olderTs/$newerTs records
			$accessoryChunks = $this->getChunkStats();
			reset($accessoryChunks);

			// $newerTs must be older than $olderTs if present, no need to reset
			while ($newerTs && false !== ($e = current($accessoryChunks))) {
				if ($e['ranges'][1] > $newerTs) {
					debug('Boundary alignment: $newerTs from %d to %d', $newerTs, $e['ranges'][0]);
					$newerTs = $e['ranges'][0];
					break;
				}
				next($accessoryChunks);
			}

			while ($olderTs && false !== ($e = current($accessoryChunks))) {
				if ($e['ranges'][1] > $olderTs) {
					debug('Boundary alignment: $olderTs from %d to %d', $olderTs, $e['ranges'][1]);
					$olderTs = $e['ranges'][1];
					break;
				}
				next($accessoryChunks);
			}
		}

		$newerSql = null === $newerTs ? 'NULL' : "TO_TIMESTAMP(${newerTs})";
		$olderSql = null !== $olderTs ? "TO_TIMESTAMP(${olderTs})" : 'null';

		$rs = $this->db->query("SELECT * FROM show_chunks('metrics', $olderSql, $newerSql)");
		if (false === $rs) {
			error('Failed to fetch chunks from range: %s', $this->db->errorInfo()[2]);
			return [];
		}

		return array_column($rs->fetchAll(\PDO::FETCH_ASSOC), 'show_chunks');
	}
	/**
	 * Get active jobs on metrics
	 *
	 * @return array
	 */
	public function getJobs(): array
	{
		$rs = $this->db->query("SELECT * FROM timescaledb_information.policy_stats 
			where hypertable::varchar = 'metrics'");
		if (!$rs) {
			fatal('%s', $this->db->errorInfo()[2]);
		}

		return $rs->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Pause background compression job
	 *
	 * @param int $jobid
	 * @return bool
	 */
	public function pauseJob(int $jobid): bool
	{
		$rs = $this->db->exec("SELECT alter_job_schedule(${jobid}, next_start=>'infinity');");
		if ($rs === false) {
			return error('Job pause failed: %s', $this->db->errorInfo()[2]);
		}

		self::$suspended[] = $jobid;

		return true;
	}

	/**
	 * Resume suspended job
	 *
	 * @param int $jobid
	 * @return bool
	 */
	public function resumeJob(int $jobid): bool
	{
		$rs = $this->db->exec("SELECT alter_job_schedule(${jobid}, next_start=>now())");
		if (false !== ($key = array_search($jobid, self::$suspended, true))) {
			unset(self::$suspended[$key]);
		}
		if ($rs === false) {
			return error('Job resume failed: %s', $this->db->errorInfo()[2]);
		}

		return true;
	}

	/**
	 * Synchronously decompress hypertable chunk
	 *
	 * decompress() does not lock. See {@link decompressRange()}
	 *
	 * @param string $chunk hypertable name
	 * @return bool
	 */
	public function decompress(string $chunk): bool {
		if (!$this->isCompressed($chunk)) {
			return debug("Chunk `%s' reported as decompressed", $chunk);
		}
		$res = $this->db->exec('SELECT decompress_chunk(' . $this->db->quote($chunk) . ')');
		if ($res === false) {
			$err = $this->db->errorInfo();

			return error('Compression failed: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
		}

		self::$decompressed[] = $chunk;
		return true;
	}

	/**
	 * Disable archival compression in TimescaleDB
	 *
	 * From local testing, this left behind empty compressed hypertables
	 * @see hasCompressionArtifacts()
	 *
	 * @return bool
	 */
	public function disableArchivalCompression(): bool {
		$res = $this->db->exec('ALTER TABLE metrics set (timescaledb.compress=false);');
		if ($res) {
			return true;
		}
		$err = $this->db->errorInfo();
		return error('Decompression flag failed: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
	}

	/**
	 * Synchronously compress hypertable chunk
	 *
	 * compress() does not lock. See {@link compressRange()} for locking
	 *
	 * @param string $chunk hypertable name
	 * @return bool
	 */
	public function compress(string $chunk): bool {
		if ($this->isCompressed($chunk)) {
			return debug("Chunk `%s' reported as compressed", $chunk);
		}
		$res = $this->db->exec('SELECT compress_chunk(' . $this->db->quote($chunk) . ')');
		if ($res === false) {
			$err = $this->db->errorInfo();

			return error('Compression failed: (%(errcode)s) %(err)s', ['errcode' => $err[0], 'err' => $err[2]]);
		}

		if (false !== ($key = array_search($chunk, self::$decompressed, true))) {
			unset(self::$decompressed[$key]);
		}

		return true;
	}

	/**
	 * Data segment uses Timescale compression
	 *
	 * @param string $chunk chunk name
	 * @return bool
	 */
	public function isCompressed(string $chunk): bool
	{
		$safe = $this->db->quote($chunk);
		$rs = $this->db->query('SELECT 1 FROM timescaledb_information.compressed_chunk_stats ' .
			'WHERE chunk_name::varchar = ' . $safe . ' AND compression_status::varchar = \'Compressed\'');

		return $rs->rowCount() > 0;
	}

	/**
	 * Get data segment compression statistics
	 *
	 * @param string $chunk
	 * @return array
	 */
	public function getCompressionStats(string $chunk): array
	{
		$safe = $this->db->quote($chunk);
		$rs = $this->db->query('SELECT * FROM timescaledb_information.compressed_chunk_stats ' .
			'WHERE chunk_name::varchar = ' . $safe);
		$res = array_get($rs->fetchAll(\PDO::FETCH_ASSOC), 0, []);
		foreach ($res as $k => $v) {
			if (substr($k, -6) === '_bytes') {
				$res[$k] = \Formatter::changeBytes($v);
			}
		}

		return $res;
	}

	/**
	 * Get data chunk information
	 *
	 * "ranges" is [min,max)
	 *
	 * @param string $chunk
	 * @return array
	 */
	public function getChunkStats(string $chunk = null): array
	{
		$query = "SELECT * FROM chunk_relation_size('metrics')";
		if ($chunk) {
			$query .= ' WHERE chunk_table = ' . $this->db->quote($chunk);
		}
		$rs = $this->db->query($query);
		if (false === $rs) {
			$err = $this->db->errorInfo();

			error('Chunk retrieval failed: (%errcode)s) %(err)s',
				['errcode' => $err[0], 'err' => $err[2]]
			);

			return [];
		}

		$recs = $rs->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($recs as &$rec) {
			$ranges = array_map('\intval', explode(',', trim($rec['ranges'], '{"}[')));
			$rec['ranges'] = [
				$ranges[0]/1000000,
				$ranges[1]/1000000
			];
		}

		return $chunk ? array_get($recs, 0, []) : $recs;
	}

	/**
	 * Discard data
	 *
	 * Note: $after is used to clear accounting irregularities with clocks
	 *
	 * @param string|null $olderInt oldest interval to preserve
	 * @param string|null $newerInt newest future-dated interval to discard (NOW() + $after)
	 * @return bool
	 */
	public function removeData(?string $olderInt, string $newerInt = null): bool
	{
		if ($olderInt && !$this->validInterval($olderInt)) {
			return false;
		} else if ($newerInt && !$this->validInterval($newerInt)) {
			return false;
		}

		$window = [];

		if ($olderInt) {
			$window[] = 'older_than => interval ' . $this->db->quote($olderInt);
		}
		if ($newerInt) {
			$window[] = 'newer_than => NOW() + interval ' . $this->db->quote($newerInt);
		}

		$res = $this->db->exec("SELECT drop_chunks('metrics', " . implode(',', $window) . ');');
		if ($res === false) {
			$err = $this->db->errorInfo();

			return error('Metric data deletion failed: (%errcode)s) %(err)s',
				['errcode' => $err[0], 'err' => $err[2]]
			);
		}

		return true;
	}

	/**
	 * Apply TS compression policy
	 *
	 * @link https://docs.timescale.com/latest/using-timescaledb/compression
	 *
	 * @param string $timespec
	 * @return bool
	 */
	public function setCompressionPolicy(string $timespec): bool
	{
		if (!$this->validInterval($timespec)) {
			return false;
		}
		$sqlspec = $this->db->quote($timespec);
		$q = "SELECT add_compress_chunks_policy('metrics', INTERVAL $sqlspec);";
		if (false === $this->db->exec($q)) {
			return error('Compression policy failed: %s', $this->db->errorInfo()[2]);
		}

		return true;
	}

	/**
	 * Given interval is valid
	 *
	 * @param string $interval
	 * @return bool
	 */
	private function validInterval(string $interval): bool
	{
		$sqlspec = $this->db->quote($interval);
		if (false === $this->db->exec("SELECT ${sqlspec}::interval")) {
			fatal("Invalid/unknown timespec `%(timespec)s': %(err)s",
				['timespec' => $interval, 'err' => $this->db->errorInfo()[2]]
			);

			return false;
		}

		return true;
	}
}
