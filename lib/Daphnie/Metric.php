<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Daphnie;

use Daphnie\Contracts\MetricProvider;

abstract class Metric implements MetricProvider
{
	use \NamespaceUtilitiesTrait;

	protected const METRIC_GROUP = false;

	/**
	 * Metric is a prefixed group
	 *
	 * Prefixes consist of a collection of metrics
	 *
	 * @return bool
	 */
	public function isGroup(): bool
	{
		return false;
	}

	/**
	 * Get metric group
	 *
	 * @return array
	 */
	public function getGroup(): array
	{
		return [];
	}

	/**
	 * Mutate metric handler
	 *
	 * @param string $attr
	 * @return $this
	 */
	public function mutate(string $attr): self
	{
		fatal('Unsupported');
	}

	/**
	 * Fill metric attributes
	 *
	 * @param int $val
	 * @return array
	 */
	public static function fill(int $val) {
		return array_fill_keys(array_values(static::ATTRVAL_MAP), $val);
	}

	/**
	 * Get metric as database attribute name
	 *
	 * @see Collector::lookupAttribute()
	 *
	 * @return string
	 */
	public function metricAsAttribute(): string
	{
		return strtolower(static::getBaseClassName());
	}
}