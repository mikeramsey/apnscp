<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Daphnie;

use Daphnie\Contracts\AnonymousLogging;

/**
 * Metrics interactions for DAPHNIE:
 *
 * Distributed Analytics and Predictive Hot Naieve Isostatic Economizer
 */
class Collector
{
	protected static $anonymousCollections = [];

	protected $metrics = [];
	protected $db;

	protected static $attributeCache;

	public function __construct(\PDO $db)
	{
		$this->db = $db;
		if (null === static::$attributeCache) {
			$this->buildAttributeCache();
			// build extra from config/app.php
			$app = \Lararia\Bootstrapper::minstrap();
			$user = array_get($app->make('config'), 'app.metrics', []) + MetricBroker::getPreload();
			foreach ($user as $group => $class) {
				MetricBroker::register($class, $group);
			}
		}
	}

	public function __destruct()
	{
		if ($this->metrics) {
			$this->sync();
		}
		unset($this->db);
	}

	/**
	 * Flush attribute cache
	 */
	public static function flush(): void
	{
		static::$attributeCache = null;
	}

	/**
	 * Add metric
	 *
	 * @param int|string   $attr
	 * @param int|null $id
	 * @param          $value
	 * @param null     $ts
	 */
	public function add($attr, ?int $siteid, $value, int $ts = null) {
		$attrId = \is_int($attr) ? $attr : $this->lookupAttribute($attr);
		$this->metrics[] = [
			'attr'    => $attrId,
			'site_id' => $siteid,
			'value'   => $value,
			'ts'      => $ts === null ? 'NOW()' : "TO_TIMESTAMP($ts)"
		];
	}

	/**
	 * Lookup attribute from database optionally storing
	 *
	 * @param string $attr
	 * @return null|int
	 */
	public function lookupAttribute(string $attr): int
	{
		if (!isset(static::$attributeCache[$attr])) {
			static::$attributeCache[$attr] = $this->resolve($attr);
		}

		return static::$attributeCache[$attr];
	}

	private function resolve(string $attr): int
	{
		// see who knows what "$attr" is
		if (null === ($metric = MetricBroker::resolve($attr))) {
			fatal("Unknown metric attribute `%s' encountered", $attr);
		}
		$stmt = $this->db->prepare('INSERT INTO metric_attributes(name, label, type) VALUES(:name, :label, :type)');
		if (!$rs = $stmt->execute([
			':name'  => $attr,
			':label' => $metric->getLabel(),
			':type'  => $metric->getType()
		])) {
			fatal(
				'Failed to add metric %s: (%s) %s',
				$attr,
				$stmt->errorCode(),
				array_get($stmt->errorInfo(), 2, 'Unknown')
			);
		}

		return (int)$this->db->lastInsertId('metric_attributes_attr_id_seq');
	}

	/**
	 * Build metric attribute cache
	 *
	 * @return void
	 */
	protected function buildAttributeCache(): void
	{
		$query = 'SELECT attr_id, name FROM metric_attributes';
		if (!$rs = $this->db->query($query)) {
			warn('Failed to collect attribute tokens');
			return;
		}

		static::$attributeCache = array_build($rs->fetchAll(\PDO::FETCH_ASSOC),
			static function ($k, $v)
			{
				return [$v['name'], $v['attr_id']];
			}
		);
	}

	public function all(): array
	{
		return static::$attributeCache;
	}

	/**
	 * Sync data
	 *
	 * @return bool
	 */
	public function sync(): bool {
		if (!$this->metrics) {
			return true;
		}

		$baseQuery = 'INSERT INTO metrics(attr_id, site_id, ts, value) VALUES ';

		$query = implode(',', array_map(function ($data) {
			$value = $data['value'];
			if (null === $value) {
				$value = 'NULL';
			} else if (!is_numeric($value)) {
				$value = $this->db->quote($data['value']);
			}

			return '(' .
				$data['attr'] . ',' .
				($data['site_id'] === null ? 'NULL' : $data['site_id']) . ',' .
				$data['ts'] . ',' .
				$value .
				')';
		}, $this->metrics));
		if (!$this->db->query($baseQuery . $query)) {
			dlog('Query failed: %s - REASON: %s', $query, array_get($this->db->errorInfo(), 2, ''));
			return false;
		}
		$this->metrics = [];
		return true;
	}

	/**
	 * Get latest metric value
	 *
	 * Performs a partial scan in last 12 hours
	 *
	 * @param string|array $metric metric name (dot notation)
	 * @param int|null     $site_id
	 * @return int|int[]|null
	 */
	public function get($metric, int $site_id = null)
	{
		if (null === ($id = $this->translateMetric($metric))) {
			return null;
		}

		$pdo = $this->db;
		$params = [];
		$siteRestrictor = 'site_id IS NULL';
		if ($site_id) {
			$params[':site_id'] = $site_id;
			$siteRestrictor = 'site_id = :site_id';
		}
		$query = 'SELECT
				name,
				value,
				EXTRACT(EPOCH FROM ts)::INTEGER AS ts
				FROM
				metrics
				JOIN metric_attributes USING (attr_id)
				WHERE attr_id IN (' . $id . ')
				AND
				ts >= TO_TIMESTAMP(' . (time() - 43200) . ')
				AND
				' . $siteRestrictor . ' ORDER BY ts DESC LIMIT 1';

		$stmt = $pdo->prepare($query);
		if (!$stmt->execute($params)) {
			$frag = 'Unknown error';
			if (is_debug()) {
				$err = $stmt->errorInfo();
				$frag = sprintf('(%s) %s', $err[0], $err[2]);
			}
			error('Failed to execute query: %s', $frag);

			return null;
		}

		if (\is_array($metric)) {
			return $stmt->fetchAll(\PDO::FETCH_COLUMN | \PDO::FETCH_GROUP);
		}

		return array_get($stmt->fetchAll(\PDO::FETCH_ASSOC), 0);
	}

	/**
	 * Map metric into database ID
	 *
	 * @param string|string[] $metric
	 * @return string|int|null
	 */
	private function translateMetric($metric)
	{
		if (!\is_array($metric)) {
			return $this->metricAsId($metric);
		}

		foreach ($metric as &$m) {
			if (null === ($m = $this->metricAsId($m))) {
				return null;
			}
		}

		return implode(',', $metric);
	}

	/**
	 * Translate metric into ID
	 *
	 * @param string|int $metric
	 * @return int|null metric ID
	 */
	public function metricAsId($metric): ?int
	{
		if (is_int($metric)) {
			return $metric;
		}

		if (null === ($id = $this->lookupAttribute($metric))) {
			error("Invalid metric `%s'", $metric);

			return null;
		}

		return $id;
	}

	/**
	 * Get metric range
	 *
	 * @param          $metric
	 * @param int      $begin begin ts (inclusive)
	 * @param int|null $end   end_ts (exclusive)
	 * @param int|null|array $site_id
	 * @param string|bool     $summable sum data (bool) or chunk into interval (string)
	 * @return int[]|int|null
	 */
	public function range($metric, int $begin, ?int $end = null, $site_id = null, $summable = true)
	{
		if (null === ($id = $this->translateMetric($metric))) {
			return null;
		}

		$pdo = $this->db;
		$params = [
			':begin' => $begin
		];
		$siteRestrictor = 'site_id IS NULL';
		if ($site_id) {
			if (\is_array($site_id)) {
				foreach ($site_id as $tmp) {
					if (!\is_int($tmp)) {
						error("Unknown site_id specified `%s'", $tmp);
						return null;
					}
				}
				$siteRestrictor = 'site_id IN(' . implode(',', $site_id) . ')';
			} else {
				$params[':site_id'] = $site_id;
				$siteRestrictor = 'site_id = :site_id';
			}
		}

		if ($begin < 0) {
			$params[':begin'] = time() + $begin;
		}

		if ($end) {
			$params[':end'] = $end;
			$siteRestrictor .= ' AND ts < TO_TIMESTAMP(:end)';
		}

		$tsField = 'EXTRACT(EPOCH FROM ts)::INTEGER';
		if (!\is_bool($summable)) {
			$tsField = 'EXTRACT(EPOCH FROM TIME_BUCKET(:interval, ts))::INTEGER';
			$params[':interval'] = $summable;
		}

		$query = 'SELECT
			(CASE
				WHEN type = \'monotonic\'
				THEN (
					CASE
						WHEN value >= lag(value) OVER w
						THEN value - lag(value) OVER w
						WHEN lag(value) OVER w IS NULL THEN NULL
						ELSE value
					END
				)
				ELSE value END
			) AS "val",
			name,
			site_id,
			' . $tsField . ' AS ts,
			type
			FROM
			metrics
			JOIN metric_attributes USING (attr_id)
			WHERE attr_id IN(' . $id . ')
			AND
			ts >= TO_TIMESTAMP(:begin) 
			AND
			' . $siteRestrictor . ' WINDOW w AS (PARTITION BY site_id, attr_id ORDER BY ts)';
		if ($summable) {
			$summableColumn = \is_string($summable) ? 'ts,' : '';
			if (\is_array($site_id)) {
				$query = 'SELECT site_id, name, ' . $summableColumn . '
					(CASE WHEN type = \'monotonic\' THEN SUM(val) ELSE AVG(val) END)::BIGINT AS val
					FROM (' . $query . ') w GROUP BY (name, site_id, ' . $summableColumn . ' type);';
			} else {
				$query = 'SELECT name, ' . $summableColumn .
					'(CASE WHEN type = \'monotonic\' THEN SUM(val) ELSE AVG(val) END)::BIGINT AS val 
					FROM (' . $query . ') w GROUP BY (name, '. $summableColumn . ' type);';
			}
		}
		$stmt = $pdo->prepare($query);
		$stmt->execute($params);

		// multiple sites, key by site
		$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if (\is_array($site_id)) {
			if (!\is_array($metric)) {
				return array_combine(
					array_column($results, 'site_id'),
					array_column($results, 'val')
				);
			}

			$processed = [];
			foreach ($results as $r) {
				$name = $r['name'];
				$siteid = $r['site_id'];
				$val = $r['val'];
				if (!isset($processed[$name])) {
					$processed[$name] = [];
				}
				$processed[$name][$siteid] = $val;
			}
			return $processed;
		}

		if (\is_array($metric)) {
			return $summable === true ? array_column($results, 'val', 'name') : $results;
		}

		return $summable === true ? array_get($results, '0.val') : $results;
	}

	/**
	 * Compress metrics
	 *
	 * @param int    $begin
	 * @param int    $end
	 * @param string $specificity window to merge
	 * @param bool   $merge merge same-value data
	 * @return bool
	 */
	public function zip(int $begin, int $end, string $specificity = '5 minutes', bool $merge = false): bool
	{
		if ($begin > $end) {
			warn("\$begin, \$end arguments flipped, switching");
			$begin ^= $end;
			$end = $begin ^ $end;
			$begin ^= $end;
		}

		$chunker = new Chunker($this->db);
		$chunker->decompressRange($end, $begin);
		// partial decompression, avoids-
		// (54000) ERROR:  too many range table entries
		$chunks = $chunker->getChunksFromRange($end, $begin);
		// @XXX chunk size is completely arbitrary at this point
		for ($i = 0, $n = \count($chunks) - TELEMETRY_ARCHIVAL_CHUNK_SIZE; $i < $n;) {
			// current
			$chunk = $chunks[$i];
			$beginChunk = $chunker->getChunkStats($chunk);
			$i += TELEMETRY_ARCHIVAL_CHUNK_SIZE;
			$endChunk = $chunker->getChunkStats($chunks[$i]);
			if (!$this->runBatch($chunker, $beginChunk['ranges'][0], $endChunk['ranges'][1], $specificity, $merge)) {
				return false;
			}
			$begin = $endChunk['ranges'][1];
		}

		return $this->runBatch($chunker, $begin, $end, $specificity, $merge);
	}

	/**
	 * @param Chunker $chunker
	 * @param int     $begin
	 * @param int     $end
	 * @param string  $specificity
	 * @param bool    $merge
	 * @return bool
	 */
	private function runBatch(Chunker $chunker, int $begin, int $end, string $specificity, bool $merge): bool
	{
		// tricky!
		// rewind the $begin ts so that it fits within the specificity interval otherwise if:
		// $end yesterday and $begin today can overlap resulting in record duplication
		$begin = (new \DateTime)->setTimestamp($begin)->sub(\DateInterval::createFromDateString($specificity))->getTimestamp();
		$groupAs = [
			'site_id',
			'ts',
			'attr_id',
			'type'
		];
		$this->db->beginTransaction();
		$rollback = true;
		defer($_, function () use (&$rollback) {
			if ($rollback) {
				$this->db->rollBack();
			}
		});

		$stmt = $this->db->prepare('
			SELECT
				attr_id,
				site_id,
				TIME_BUCKET(:specificity, ts) AS ts,
				CASE
					WHEN type = \'value\' THEN AVG(value)
					WHEN type != \'value\' THEN (MAX(ARRAY[EXTRACT(\'EPOCH\' FROM ts), value]))[2]
				END AS value INTO TEMPORARY TABLE zip_ctx FROM metrics
			JOIN metric_attributes USING(attr_id)
			WHERE ts >= TO_TIMESTAMP(:begin) AND ts < TO_TIMESTAMP(:end) AND value > 0
			GROUP BY (' . implode(',', $groupAs) . ')'
		);
		$rs = $stmt->execute([
			':specificity' => $specificity,
			':begin'       => $begin,
			':end'         => $end
		]);
		if (!$rs) {
			return error('Failed to collapse metrics using spec %s: (%s) %s',
				$specificity,
				$stmt->errorCode(),
				$stmt->errorInfo()[2]
			);
		}
		// binning oddity
		$stmt = $this->db->prepare('
			DELETE FROM metrics WHERE ts >= TO_TIMESTAMP(:begin) AND ts < TO_TIMESTAMP(:end)
		');
		$rs = $stmt->execute([
			':begin' => $begin,
			':end'   => $end
		]);
		if (!$rs) {
			return error('Failed to delete metrics in range [%s, %s): %s',
				$begin,
				$end,
				$stmt->errorInfo()[2]
			);
		}

		if ($merge) {
			$stmt = $this->db->prepare('DELETE FROM zip_ctx m USING (
				SELECT
					attr_id,
					site_id,
					value,
					ts,
					ROW_NUMBER() OVER (PARTITION BY attr_id, site_id, value ORDER BY ts) AS row_num
				FROM
					metrics) sub
				WHERE
					sub.row_num > 1 AND
					m.ts >= TO_TIMESTAMP(:begin) AND
					m.ts < TO_TIMESTAMP(:end) AND
					m.ts = sub.ts AND
					m.attr_id = sub.attr_id AND
					m.site_id = sub.site_id');
			$rs = $stmt->execute([
				':begin' => $begin,
				':end'   => $end
			]);

			if (!$rs) {
				return error('Failed to merge metrics in window [%(min)d,%(max)d): (%(errcode)s) %(errmsg)s', [
					'min'     => $begin,
					'max'     => $end,
					'errcode' => $stmt->errorCode(),
					'errmsg'  => $stmt->errorInfo()[2]
				]);
			}

			info('Merged %d duplicate records', $stmt->rowCount());
		}

		if (false === $this->db->exec('INSERT INTO metrics SELECT * FROM zip_ctx ON CONFLICT (site_id, attr_id, ts) DO NOTHING')) {
			return error("Failed to repopulate compressed metrics: %s", $this->db->errorInfo()[2]);
		}

		$this->db->exec('DROP TABLE zip_ctx');
		$rollback = false;
		$this->db->commit();

		if (TELEMETRY_ARCHIVAL_COMPRESSION) {
			$chunker->compressRange($end, $begin);
		}

		info('Zipped metrics (specificity: %s) range [%d, %d)',
			$specificity,
			$begin,
			$end
		);

		return true;
	}

	/**
	 * Register an anonymous telemetry colleciton
	 *
	 * @param             $class
	 * @param string|null $metric
	 */
	public static function registerCollection($class, string $metric = null): void
	{
		if (\is_string($class)) {
			$class = new $class;
		}

		if (null === $metric) {
			$metric = $class->metricAsAttribute();
		}

		if (!\in_array(AnonymousLogging::class, class_implements($class), true)) {
			fatal('Metric does not implement %s', AnonymousLogging::class);
		}

		static::$anonymousCollections[$metric] = $class;
	}

	/**
	 * Get anonymous collections
	 *
	 * @return AnonymousLogging[]
	 */
	public function getAnonymousCollections(): array {
		return static::$anonymousCollections;
	}
}