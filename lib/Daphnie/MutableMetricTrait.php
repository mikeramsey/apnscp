<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Daphnie;

use Daphnie\Contracts\MetricProvider;

/**
 * Trait MutableMetricTrait
 *
 * Convert metric into group
 *
 * @package Daphnie
 */
trait MutableMetricTrait {

	// @var string mutated attribute
	private $attr;

	public function __construct(string $attr = '')
	{
		$this->attr = $attr;
	}

	/**
	 * Mutate attribute into metric
	 *
	 * @param string $attr
	 *
	 * @return Metric
	 */
	public function mutate(string $attr): Metric
	{
		return new static($attr);
	}

	/**
	 * Metric is a prefixed group
	 *
	 * Prefixes consist of a collection of metrics
	 *
	 * @return bool
	 */
	public function isGroup(): bool
	{
		return true;
	}

	public function getLabel(): string
	{
		return array_get($this->getBinding($this->attr), 'label', 'Unknown metric');
	}

	public function getType(): string
	{
		return array_get($this->getBinding($this->attr), 'type', MetricProvider::TYPE_MONOTONIC);
	}

	protected function getBinding(string $metric): array
	{
		return array_get(static::getAttributes(), $metric, []);
	}

	/**
	 * Get all metric names in group
	 *
	 * @return array
	 */
	public function getGroup(): array
	{
		return array_keys(static::getAttributes());
	}

	/**
	 * Get data-source attributes
	 *
	 * @return array
	 */
	public static function getAttributes(): array
	{
		return static::ATTR_BINDINGS;
	}

	/**
	 * Get metric attributes from group
	 *
	 * @return array
	 */
	public static function getAttributeMap(): array
	{
		return static::ATTRVAL_MAP;
	}


	/**
	 * Get metric as database attribute name
	 *
	 * @return string
	 * @see Collector::lookupAttribute()
	 *
	 */
	public function metricAsAttribute(): string
	{
		return strtolower(static::getBaseClassName() . '-' . $this->attr);
	}
}