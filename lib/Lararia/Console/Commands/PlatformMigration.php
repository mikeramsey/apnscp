<?php

	namespace Lararia\Console\Commands;

	use Illuminate\Console\ConfirmableTrait;
	use Illuminate\Database\Console\Migrations\BaseCommand;
	use Illuminate\Database\Migrations\Migrator;

	class PlatformMigration extends BaseCommand
	{
		use ConfirmableTrait;

		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'migrate:platform {--database= : The database connection to use.}
		{--force : Force the operation to run when in production.}
		{--path= : The path of migrations files to be executed.}
		{--pretend : Dump the SQL queries that would be run.}
		{--rollback : Rollback platform migration (dangerous).}
		{--ff : Fast-forward all pending migrations.}
		{--step : Force the migrations to be run so they can be rolled back individually.}';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Perform platform migration';

		/**
		 * @var Migrator
		 */
		protected $migrator;

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();
			/**
			 * @var Migrator $migration
			 */
			$this->migrator = app('platformmigrator');
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			if (!$this->confirmToProceed()) {
				return;
			}
			// report to terminal in real-time
			$this->migrator->setOutput($this->output);
			$this->migrator->setConnection($this->option('database'));
			$method = $this->option('rollback') ? 'rollback' : 'run';
			$opts = [
				'pretend'  => $this->option('pretend'),
				'step'     => (int)$this->option('step'),
				'rollback' => $this->option('rollback'),
				'ff'       => $this->option('ff')
			];
			if (!empty($opts['ff']) && \count(array_filter($opts)) > 1) {
				throw new \RuntimeException('Cannot mix other options with fast-forward');
			}
			$this->migrator->{$method}($this->getMigrationPaths(), $opts);
		}

		protected function getMigrationPath()
		{
			return resource_path('playbooks/migrations');
		}
	}
