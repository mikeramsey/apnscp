<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace Lararia\Http;

use Illuminate\Http\JsonResponse as NativeJson;
use Illuminate\Http\Response;
use Lararia\Http\JsonResponse as ApnscpJson;
use Illuminate\Routing\Router;

class DispatchCompatibilityHandler extends Router {

	/**
	 * Permit interopability with controller methods with older ApisCP features
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param mixed                                     $response
	 * @return NativeJson|\Illuminate\Http\Response
	 */
	public static function toResponse($request, $response)
	{
		if ($request->wantsJson()) {
			// already encoded
			if ($response instanceof Response) {
				return parent::toResponse($request, $response);
			}

			if (!$response instanceof NativeJson) {
				$headers = [];
				if (!$response instanceof ApnscpJson) {
					if ($response instanceof \Symfony\Component\HttpFoundation\Response) {
						$headers = $response->headers->all();
						$response = array_get(json_decode($response->getContent(), true), 'original');
					}
					$response = new ApnscpJson($response);
				}
				$response = new NativeJson($response->toArray(), $response->getStatusCode(), $headers);
			}
		}

		return parent::toResponse($request, $response);
	}
}