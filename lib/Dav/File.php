<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Dav;

	use Sabre\DAV as Provider;

	class File extends Provider\File implements Provider\PartialUpdate\IPatchSupport
	{

		use \apnscpFunctionInterceptorTrait;
		use \FilesystemPathTrait;
		use StatCacheTrait;

		private $myPath;
		private $mimeCache;

		public function __construct($myPath)
		{
			$this->myPath = $myPath;
		}

		public function getName()
		{
			return basename($this->myPath);
		}

		public function setName($newName)
		{
			if ($newName[0] != '/') {
				$newName = \dirname($this->myPath) . '/' . $newName;
			}
			if (!$this->file_move($this->myPath, $newName)) {
				return false;
			}

			return true;
		}

		public function getLastModified()
		{
			$stat = $this->cacheCheck(\dirname($this->myPath), basename($this->myPath));
			if ($stat) {
				return $stat['mtime'];
			}
			$stat = $this->file_stat($this->myPath);

			return $stat['mtime'];
		}

		public function delete()
		{
			return $this->file_delete($this->myPath);
		}

		public function get()
		{
			$pointer = $this->file_expose($this->myPath);
			if (!$pointer) {
				throw new Provider\Exception\Forbidden('failed to access file');
			}

			return fopen($this->domain_fs_path() . $pointer, 'r');
		}

		public function put($data)
		{
			if (\is_resource($data)) {
				$pointer = $this->file_expose($this->myPath, 'write');
				if (!$pointer) {
					throw new Provider\Exception\Forbidden('failed to modify file');
				}
				$fp = fopen($this->domain_fs_path() . $pointer, 'w');
				stream_copy_to_stream($data, $fp);
				fclose($fp);
			} else if (!file_put_file_contents($this->myPath, $data, true)) {
				/**
				 * docs suggest data can arrive as string
				 *
				 * @link https://github.com/fruux/sabre-dav/blob/master/lib/DAV/File.php
				 */
				throw new Provider\Exception\Forbidden('failed to modify file');
			}

			return $this->getETag();
		}

		public function getETag()
		{
			$hash = $this->file_etag($this->myPath);
			if (!$hash) {
				return $hash;
			}

			return '"' . $hash . '"';
		}

		/**
		 * Updates the file based on a range specification.
		 *
		 * The first argument is the data, which is either a readable stream
		 * resource or a string.
		 *
		 * The second argument is the type of update we're doing.
		 * This is either:
		 * * 1. append
		 * * 2. update based on a start byte
		 * * 3. update based on an end byte
		 *;
		 * The third argument is the start or end byte.
		 *
		 * After a successful put operation, you may choose to return an ETag. The
		 * ETAG must always be surrounded by double-quotes. These quotes must
		 * appear in the actual string you're returning.
		 *
		 * Clients may use the ETag from a PUT request to later on make sure that
		 * when they update the file, the contents haven't changed in the mean
		 * time.
		 *
		 * @param resource|string $data
		 * @param int             $rangeType
		 * @param int             $offset
		 * @return string|null
		 */
		public function patch($data, $rangeType, $offset = null)
		{
			throw new Provider\Exception\BadRequest('not implemented yet');
			switch ($rangeType) {
				case 1 :
					$f = fopen($this->path, 'a');
					break;
				case 2 :
					$f = fopen($this->path, 'c');
					fseek($f, $offset);
					break;
				case 3 :
					$f = fopen($this->path, 'c');
					fseek($f, $offset, SEEK_END);
					break;
			}
			if (\is_string($data)) {
				fwrite($f, $data);
			} else {
				stream_copy_to_stream($data, $f);
			}
			fclose($f);
			clearstatcache(true, $this->path);

			return $this->getETag();
		}

		public function getSize(): int
		{
			$stat = $this->cacheCheck(\dirname($this->myPath), basename($this->myPath));
			if ($stat) {
				return $stat['size'];
			}
			$stat = $this->file_stat($this->myPath);

			return (int)$stat['size'];
		}

		public function getContentType()
		{
			if (null === $this->mimeCache) {
				$this->mimeCache = $this->file_get_mime_type($this->myPath);
			}

			return $this->mimeCache;
		}

	}
