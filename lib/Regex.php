<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Regex
	{
		public const EMAIL = '!^(?:(?:(?:[a-zA-Z0-9][\.\-\+_]?)*)[a-zA-Z0-9_])+\@(?:(?:(?:[a-zA-Z0-9][\.\-_]?){0,62})[a-zA-Z0-9])+\.[a-zA-Z0-9]{2,16}$!';
		public const DOMAIN = '/^(?:[a-z0-9]+[a-z0-9-]*(?<![-_])\.?)+\.[a-z]+$/';

		// core expressions
		public const DOMAIN_WC = '!^(?:\*\.)?(?:|[a-z0-9]+[a-z0-9-]*[a-z0-9]+\.?)+\.[a-z]+$!';
		public const SUBDOMAIN = '!^([a-z0-9][a-z0-9-]*)(\.[a-z0-9][a-z0-9-]+)*$!';
		public const HTTP_HOST = '!^[a-z0-9]+(.?(?>[a-z0-9-]*?[a-z0-9]+)|[a-z0-9])*\.[a-z\.]+$!';
		public const USERNAME = '!^(?:[0-9]*[-a-z][a-z0-9_.-]*[a-z0-9]+|[0-9]*[a-z][a-z0-9]*)$!';
		public const PASSWORD = '!^[0-9a-zA-Z\!@#$%^ \(\)&\+\*_\.`~\-{}\[\]\|]{4,}$!';
		public const GROUPNAME = self::USERNAME;
		/**
		 * Note the second field stores the salt ID, which can be 3 values
		 *
		 * via crypt(3)
		 *
		 * If salt is a character string starting with the characters
		 * "$id$" followed by a string terminated by "$":
		 *
		 *    $id$salt$encrypted
		 *
		 * ID  | Method
		 * ---------------------------------------------------------
		 * 1   | MD5
		 * 2a  | Blowfish (not in mainline glibc; added in some
		 *     | Linux distributions)
		 * 5   | SHA-256 (since glibc 2.7)
		 * 6   | SHA-512 (since glibc 2.7)
		 *
		 * So $5$salt$encrypted is an SHA-256 encoded password and
		 *  $6$salt$encrypted is an SHA-512 encoded one.
		 *
		 */
		public const SHADOW_PHY_ENTRY = '/^(?<user>[a-z][a-z0-9_\.-]*[a-z0-9]+|[a-z][a-z0-9]*):
                (?<salt>\$(?:1|2[axy]|5|6)\$(?:[a-zA-Z0-9\/._]{8,}|(?<=2[axy]\$)\d{2}))\$(?<hash>[a-zA-Z0-9\/\.]{13,99}):
                (?<mtime>[[:digit:]]{1,}): 
                (?<minage>[[:digit:]]{1,}):
                (?<maxage>[[:digit:]]{1,}):
                (?<minwarn>[[:digit:]]{0,}):
                (?<expdate>[[:digit:]]{0,}):
                (.*)$/x';
		public const PASSWD_USER_ENTRY = '/^([a-z][a-z0-9_\.-]*[a-z0-9]+|[a-z][a-z0-9]*):x:([0-9]+):([5-9][0-9]{2,}|[1-9][0-9]{3,}+):([^:]+):([a-zA-Z\/_.0-9]+):([a-zA-Z\/_.0-9]*)$/';
		public const URL = '!^https?://[a-z0-9]+([a-z0-9-]*\.?[a-z0-9]+)\.[a-z\.]+(?::\d+)?!';
		public const ADDON_DOMAIN_PATH = '!^(?:/home|/var/www|/usr/local)/[a-zA-Z0-9/\._-]+$!';
		public const CRON_TASK = '/^[[:space:]]*(?<disabled>\#?)(?:(?<token>@(?:reboot|hourly|daily|weekly|monthly|annually|yearly)) |
            [[:space:]]*(?<min>[^\s=]+)
            [[:space:]]+(?<hour>[^\s]+)
            [[:space:]]+(?<dom>[^\s]+)
            [[:space:]]+(?<month>[^\s]+)
            [[:space:]]+(?<dow>[^\s]+))
            [[:space:]]+(?<cmd>.+)$/x';
		public const CRON_COMMENT = '/^[[:space:]]*#/';
		public const CRON_MAILTO = '/^[[:space:]]*MAILTO=((?>["\'])|)(?<email>[^ ]*?(?=\1))(?>\1)\s*$/m';

		// Argos
		public const ARGOS_SERVICE_STATUS = '/^(?<type>Process|Filesystem|Program|System) \'(?<proc>[^\']+)\'$[\r\n]+|(?:^\s*(?<name>[^\r\n]+?)\s{2,}(?<value>[^\r\n]+)$[\r\n]*)/m';
		// Crontab
		public const MAILING_LIST_NAME = '!^[a-z0-9_-]+$!';
		public const MAJORDOMO_CONFIG_ENTRY = '/^\s*((?!#)[^\s]+)[ \t]+(?:<<[ ]+END[\s]*|=[ ]*)?([\w\r\n\s]+(?=(?:^END))|[^\r\n]*)(?:END)?$/m';

		// Majordomo
		public const HTTP_LOG_FILE = '!^[a-zA-Z0-9_/\.-]{2,}$!';
		public const QUOTA_USRGRP = '!\((?:uid\s(?<uid>\d+)|gid\s(?<gid>\d+))\):\s+ # uid/gid
                                 [\w\s]+\n
                                 (?:^\s*(?:(?=%(device)s)|(?>.+$)))*
                                (
                                 \s*(?<device>/dev/\S+)           # fs device
                                 \s+(?<qused>\d+)(?<qover>\*?)    # blocks
                                 \s+(?<qsoft>\d+)                 # soft quota
                                 \s+(?<qhard>\d+)                 # hard
                                 \s+(?<qgrace>\d+|(?:\d+days|[0-9]{1,}:[0-9]{2}|none)?)       # grace
                                 \s+(?<fileused>\d+)(?<fover>\*?) # num files
                                 \s+(?<filesoft>\d+)              # file soft quota
                                 \s+(?<filehard>\d+)              # file hard quota
                                 \s+(?<filegrace>\d+|(?:\d+days|[0-9]{1,}:[0-9]{2}|none)?)    # file grace
                               )
                             !mx';
		public const QUOTA_REPQUOTA_C = '!^\#(?<id>%(DIRECTIVE)s)                              # uid/gid
								\s+(?<qover>[\-\+])(?<fover>[\-\+])                            # quota/file over
								\s+(?<qused>\d+)                                               # blocks used 
								\s+(?<qsoft>\d+)                                               # soft quota
								\s+(?<qhard>\d+)                                               # hard quota
								\s+(?<qgrace>\d+|(?:\d+days|[0-9]{1,}:[0-9]{2}|none)?)         # grace
								\s+(?<fileused>\d+)                                            # blocks used 
								\s+(?<filesoft>\d+)                                            # soft quota
								\s+(?<filehard>\d+)                                            # hard quota
								\s+(?<filegrace>\d+|(?:\d+days|[0-9]{1,}:[0-9]{2}|none)?)      # grace
							!mx';
		// Redis
		public const REDIS_NICKNAME = '/^[a-zA-Z0-9][a-z0-9A-Z\.-]{1,}[a-z0-9A-Z]+$/';
		// Logs
		public const GETFACL_ACL = '/^(USER|GROUP|user|group|mask|other)\s+([a-z0-9]{1,})\s+([RWXrxw-]{3})/';
		// Misc
		public const CHANGELOG_COMMIT = '/^commit\s+(?<commit>[a-f0-9]{32,})$[\r\n]{1,2}
            (?:^Merge:[^\r\n]*$[\r\n]{1,2})*
            ^Author:\s+(?<author>[^\n]*)$[\r\n]{1,2}
            ^Date:\s+(?<date>[^\n]*)$[\r\n]{1,2}
            ^(?>(?<msg>(?:^\s+[^\r\n]{1,}$[\r\n]{1,})++))/mx';
		// User
		public const FILE_HDR_ZIP = '!([0-9]+)\s+
            (Defl:.?|Stored)\s+
            ([0-9]+)\s+
            [0-9]+%\s+
            ([0-9\-]+\s+[0-9][0-9]?:[0-9][0-9])\s+
            ([0-9a-f]+)\s+
            (.+)
            $!x';

		// File
		public const FILE_HDR_GZIP = '!^
            (?<method>\w+)\s*
            (?<crc>[a-f0-9]{8})\s+
            (?<ts>\w{3}\s+\d{1,2}\s+(?:2[0-4]|[01][0-9]):[0-5][0-9])\s+
            (?<csize>\d+)\s+
            (?<size>\d+)\s+
            (?<ratio>([\d]{1,2}\.\d{1,2}))%\s+
            (?<name>.+)
            $!x';

		// Compression Interface
		public const FILE_HDR_TAR = '!^
            (?<permissions>[ds-][rwx-]{9}\w{0,2})\s*
            (?<user>[^/ ]*)\s*
            (?<group>[^ ]*)\s*
            (?<size>\d+)\s*
            (?<ts>\d{4}-(?:0[1-9]|1[0-2])-(?:[0-2]\d|3[01])\s(?:[01][0-9]|2[0-3])(?::[0-5]\d){1,2})\s* #timestamp
            (?<name>.+)
            $!x';
		public const GEM_YAML = '!^([^(\n]+) \(([0-9,\. ]+)\)$!m';
		public const DNS_AXFR_REC = '/^([^\s]+)\s+((?>[0-9]+|\s*))\s*(IN|HS|CH)?\s+(%(rr)s)\s+(.+?)$/m';

		// Web
		public const DNS_IPV4 = '/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/';
		public const DAV_CONFIG = '!^\s+<Directory "(?<path>(.+))">(?:[\n\s\S]*?)Dav\s+(?<provider>\S+)!m';

		// DNS
		public const DNS_AXFR_REC_DOMAIN = '/^(?<subdomain>[^\s]*(?=\.?%(domain)s))(?:\.?%(domain)s\.)\s+
            (?<ttl>[0-9]+)\s+(?<class>IN|HS|CH)\s+(?<rr>%(rr)s)\s+(?<parameter>.+)$/mix';
		public const MISC_INI_DIRECTIVE_C = '/^\s*%(DIRECTIVE)s\s*=?+\s*(.*)$/m';

		public const MISC_GOOGLE_API_CLIENT_ID = '/^\d{10,}[a-z\.\-0-9]+$/';

		// SQL
		// max length of {MySQL, PostgreSQL}
		public const SQL_USERNAME = '!^(?:[a-zA-Z0-9][\.a-zA-Z0-9-_]{0,79})$!';
		public const SQL_PGPASS = '/^
            (?<hostname>[^:]+):
            (?<port>(?:\d|\*)):
            (?<database>[^:]*):
            (?<username>[^:]*):
            (?<password>.+)$/x';
		public const SQL_DATABASE = '/^[a-zA-Z_0-9-\.]+$/';

		// Misc
		// AXFR with self-referential domain
		public const SQL_PREFIX = '/^(?!-)[a-z0-9-]{1,%(max)d}_$/';
		public const SQL_MYSQL_IP_WILDCARD = '/^(?:[0-9%_]{1,3}\.){0,3}(?:[0-9%_]{1,3})$/';
		public const SQL_MYSQL_DEFINER = '/DEFINER=`(?<user>[^`]+)`@`(?<host>[^`]+)`.*?(?<type>EVENT|PROCEDURE|FUNCTION|TRIGGER) (`|)?(?<name>[^ `]+)\4/m';

		public const ENSIM_STATUS = '/^\s*-\s*\((?<type>(?:WARNING|ERROR|SUCCESS)):
            [\s\t]*(?<code>[a-f0-9]+)\)
            \sfield\s\'(?<service>[^\']+)\':
            \s*(?<message>.*?)$|(?<extra>^.*?$)/smx';
		public const TOMCAT_VERSION = '/^Server number:[\s\t]*(?<version>\d+\.\d+\.\d+\.\d*)\s*$/mi';

		public const COMMON_BASH_TZ = '/\b(TZ=(?>"?(?<timezone>[^"]+)"?)|(?:export (?=TZ\s*$))TZ)(?:\b|(?<="$))/m';

		// SSL
		public const SSL_CRT_URI = '/\bURI:(?<url>[^\s]++)\b/';

		// Email
		public const EMAIL_MTA_IP_RECORD = '/^\s*(?:%(ip)s)\b\s+internal-multihome\b.*$/';
		public const EMAIL_MAILDIR_FOLDER = '/^\.(?<folder>(?:[A-Za-z0-9]\.)*[A-Z0-9a-z]+)$/';
		public const EMAIL_LOCAL_PART = '/^[a-zA-Z0-9_.+-]+$/';

		// SSL
		public const LETSENCRYPT_ACME_CMD_MESSAGE = '/\b\[(?<datetime>[^\]]+)\]\s*ACME\.(?<cls>[^:]+):\s*(?<msg>.+)$/m';
		// Cloudflare
		public const CLOUDFLARE_NAMESERVER_TARGET = '/\b(?>[^. ]+\.ns\.cloudflare\.com)/';

		// Joomla
		// Not really Joomla, but used with Joomla
		public const JOOMLA_VERSIONS = '/\s(\d+(?:\.[\w-]+?\.(?!x)\d*))\s/';

		// general block of code
		public const BLOCK_C = '/^%(begin)s$(?<body>.*?)^%(end)s$/ms';
		public const LAZY_SUB = '/(?<!%)%([^%]+)%(?!%)/';
		public const PHP_COMPILABLE_STRIP_NONHTTP_APACHE_CONTAINER = '}^\s*(?:<VirtualHost [^:]*:(?!%(port)s)\d+>(.+?)</VirtualHost>\s*|Listen [^:]*:(?!%(port)s)\d+\s*)$}sm';
		// Redis
		public const REDIS_DIRECTIVE_C = '/^\s*%(directive)s\s+%(value:.+)s$/ms';
		// Node
		public const NVM_NODES = '}^(?<alias>\S*+)\s*+(?>->\s*|\s*)v?(?<version>(?!N/A)\S+)(?>(?:.(?!N/A)|$)*)$}m';


		/**
		 * Compile a regex by substituting placeholder variables
		 *
		 * Performs a search and replace of the following format specifiers:
		 * %s, %d, %f
		 *
		 * Named placeholders and backreferences are supported also:
		 * %(foo)s, %s ... %1s
		 *
		 * Default substitution specified by :param.
		 * %(foo:bar)s uses "bar" if foo missing
		 *
		 * @param string $regex
		 * @param mixed  $args
		 * @return string
		 */
		public static function compile(string $regex, $args): string
		{
			$args = (array)$args;
			$tok = strtok($regex, '%');
			$ntok = $compiled = '';
			// occurrences and position
			$cnt = -1;
			// numeric reference pointer, %s, %d, %s...
			$num_ref = 0;

			// format specifier that references itself elsewhere
			// e.g. %1s or %(str)s
			$backrefs = array();
			$offset = 0;
			for (; $tok !== false; $tok = $ntok, $cnt++) {
				$compiled .= $tok;
				$offset += strlen($tok);
				$ntok = strtok('%');
				if (isset($ntok[0])) {
					$nchar = $ntok[0];
					// literal %
					if ($nchar === '%') {
						$ntok = strtok('%');
						$cnt--;
						continue;
					}
					if ($nchar !== 's' && $nchar !== 'd' && $nchar !== 'f' && $nchar !== '('
						&& ($nchar < '0' || $nchar > '9')
					) {
						warn("offset $offset: invalid expression `$ntok'");
						continue;
					}
					$ref = null;
					// back reference
					if (($nchar >= '0' && $nchar <= '9') || $nchar === '(') {
						if ($nchar === '(') {
							// format specifier
							// named backreference
							$ref = substr($ntok, 1, strpos($ntok, ')', 2) - 1);
							// truncate 3 chars for named br covering charset "()s"
							$ntok = substr($ntok, 3);
							$cnt--;
						} else {
							// numeric backreference
							$ref = '';
							for ($i = 0, $sz = strlen($ntok); $i < $sz; $i++) {
								// %1
								if ($ntok[$i] >= '0' && $ntok[$i] <= '9') {
									$ref .= $ntok[$i];
									continue;
								} // %1s...
								else if ($ntok[$i] == 's' ||
									($ntok[$i] == 'd' && $ntok[$i] == 'f')) {
									$ref = (int)$ref - 1;
									$cnt--;
								} // garbage
								else {
									$ref = false;
								}
								// truncate 1 char for numeric br
								$ntok = substr($ntok, 1);
								break;
							}
						}
						if (null === $ref) {
							continue;
						}
						// backreference exists already
						if (isset($backrefs[$ref])) {
							// don't increment the seen counter, since this has already been seen
							//$cnt--;
						} else {
							$cnt++;
							if (!is_int($ref) && isset($args[$ref])) {
								$val = $args[$ref];
								//unset($args[$ref]);
								//$cnt--;
							} else if (is_int($ref)) {
								$val = $args[$ref];
							} else {
								if (!isset($args[$num_ref])) {
									if (false !== ($pos = strpos($ref, ':'))) {
										// default present
										$args[$ref] = substr($ref, 0, $pos++);
										$val = substr($ref, $pos);
									} else {
										warn("named format arg `%s' not present in argument list", $ref);
										$val = '';
									}
								} else {
									$val = $args[$num_ref];
								}
								$num_ref++;
							}
							$backrefs[$ref] = $val;
						}
					}
					// finally do some processing
					if (null !== $ref) {
						$newstr = $backrefs[$ref];
					} else {
						$newstr = $args[$num_ref];
						$ref = $num_ref;
						$num_ref++;
					}
					$ntok = substr($ntok, strlen($ref));
					$compiled .= $newstr;
				}
			}
			$nextra = count($args) - $cnt;
			if ($nextra) {
				if ($nextra > 0) {
					warn("compile: `$nextra' args ignore while compiling regex " . $regex);
				} else {
					warn('compile: missing `' . ($nextra * -1) . "' additional arguments while compiling regex " . $regex);
				}
			}

			return $compiled;
		}

		// Php, requires compiling

		/**
		 * Merge one regex into another regex
		 *
		 * Sample call:
		 * $re = '!(?<domain>' . Regex::recompose(Regex::DOMAIN,'!') . ')'
		 * . '\b\s+(?:(?<url>https?://.+)|(?<path>/.+))$!m'
		 *
		 * @param string $regex    regex to merge into existing regex
		 * @param string $escdelim delimiter used in source regular expression
		 * @param bool   $anchor   keep regex anchor
		 * @return string recomposed regex
		 */
		public static function recompose($regex, $escdelim = '/', $anchor = false): string
		{
			$start = 1;
			$delim = $regex[0];
			$end = strrpos($regex, $delim) - 1;
			if (!$anchor) {
				if ($regex[$start] === '^') {
					$start++;
				}
				if ($regex[$end] === '$') {
					$end -= 2;
				}
			}
			$regex = substr($regex, $start, $end);

			return str_replace($escdelim, '\\' . $escdelim, $regex);

		}
	}


