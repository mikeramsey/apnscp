<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class MySQL extends mysqli
	{
		// singleton instance of MySQL database
		const MYSQL_GONE_AWAY_CODE = 2006;
		protected const host = APNSCP_HOST;
		protected const database = APNSCP_DATABASE;
		protected const username = APNSCP_USER;
		protected const password = APNSCP_PASSWORD;
		// @var \MySQL
		protected static $pinged;
		// @var array saved credentials for reconnect
		protected static $pdoConnection;
		static private $instance;
		public $qHandler;
		public $num_rows;
		public $affected_rows;
		/**
		 * @var false
		 */
		protected $connected;
		private $credentials;
		private $stmt;
		private $query_str;

		/**
		 * Create a MySQL connection without connecting to database
		 *
		 * @return \MySQL
		 */
		static public function stub()
		{
			$reflection = new ReflectionClass(__CLASS__);
			$instance = $reflection->newInstanceWithoutConstructor();
			$instance->init();

			return $instance;
		}

		/**
		 * Get a PDO connection of the primary DB conn
		 *
		 * @return \PDO
		 */
		static public function pdo()
		{
			if (null === self::$pdoConnection) {
				self::$pdoConnection = new PDO(
					'mysql:host=' . self::host . ';dbname=' . self::database,
					self::username,
					self::password,
					[
						PDO::ATTR_PERSISTENT       => false,
						PDO::ATTR_EMULATE_PREPARES => false
					]
				);
			}

			return self::$pdoConnection;

		}

		static public function forceInitialize()
		{
			self::$instance = self::$pdoConnection = null;
			return self::initialize();
		}

		static public function initialize(
			$mHost = null,
			$mUsername = null,
			$mPasswd = null,
			$mDBName = null,
			$mPort = null,
			$mSocket = null
		): \MySQL {
			if (null === $mHost && null !== self::$instance) {
				$db = self::$instance;
				$db->ping();
				$db->select_db(self::database);

				return $db;
			}

			if (null === $mHost) {
				$mHost = self::host;
			}
			if (null === $mUsername) {
				$mUsername = self::username;
			}
			if (null === $mPasswd) {
				$mPasswd = self::password;
			}
			if (null === $mDBName) {
				$mDBName = self::database;
			}
			$db = new static($mHost, $mUsername, $mPasswd, $mDBName, $mPort, $mSocket);
			if ($mDBName === self::database && $mUsername === self::username && $mHost === self::host) {
				self::$instance = $db;
			}

			return $db;
		}

		public function select_db($db = null)
		{
			if (!$db) {
				$db = $this->credentials['db'];
			} else {
				$this->credentials['db'] = $db;
			}
			$this->ping();

			return parent::select_db($db);
		}

		public function ping()
		{
			// no longer implemented in mysqlnd
			if ($this->connected && !@parent::query('SELECT 1')) {
				return $this->reconnect();
			}
			$reconnect = !$this->connected || $this->errno === static::MYSQL_GONE_AWAY_CODE;
			if ($reconnect) {
				$this->reconnect();
			}
			self::$pinged = true;

			return true;
		}

		public function reconnect()
		{
			$c = $this->credentials;
			$this->__construct($c['host'], $c['username'], $c['password'], $c['db'], $c['port'], $c['socket']);

			return true;
		}

		/***
		 * unified MySQL queries should be placed in here
		 */

		/* database connection */
		public function __construct(
			$mHost = null,
			$mUsername = null,
			$mPasswd = null,
			$mDBName = null,
			$mPort = null,
			$mSocket = null
		) {
			$c = $this->credentials = [
				'host'     => $mHost,
				'username' => $mUsername,
				'password' => $mPasswd,
				'db'       => $mDBName,
				'port'     => $mPort ?? ini_get('mysqli.default_port'),
				'socket'   => $mSocket ?? ini_get('mysqli.default_socket')
			];
			parent::init();
			if (!parent::real_connect($c['host'], $c['username'], $c['password'], $c['db'], $c['port'], $c['socket'])) {
				fatal('failed to connect to database');
			}
			$this->connected = true;
		}

		public function __toString()
		{
			return (string)$this->connected();
		}

		public function prepare($mStmt)
		{
			$this->stmt = $this->stmt_init();
			$this->stmt->prepare($mStmt);

			return $this->stmt;
		}


		public function execute()
		{
			$this->stmt->execute();

			return $this;
		}

		public function __clone()
		{
			throw new apnscpError('Cannot clone MySQL class');
		}

		public function close()
		{
			$this->connected = false;
			if (!parent::close()) {
				return false;
			}
			if (static::$instance === $this) {
				static::$instance = null;
			}
			return true;
		}

		public function connect(
			$host = null,
			$username = null,
			$password = null,
			$database = null,
			$port = null,
			$socket = null
		) {
			$this->__construct($host, $username, $password, $database, $port, $socket);

			return $this;
		}

		public function start_stmt()
		{
			$dh = $this->stmt_init();

			return $dh;
		}

		public function insert_id()
		{
			return $this->insert_id;
		}

		public function affected_rows()
		{
			return $this->affected_rows;
		}

		public function num_rows()
		{
			return $this->num_rows;
		}

		public function escape($mStr)
		{
			return $this->escape_string($mStr);
		}

		public function escape_string($mString)
		{
			return $this->real_escape_string($mString);
		}

		public function fetch_object($mHandle = null)
		{
			if (!is_null($this->stmt)) {
				$mHandle = $this->stmt;
			} else {
				if (is_bool($this->qHandler) && is_debug()) {
					print $this->error;
					debug_print_backtrace();
					print $this->query_str;
				}
				if (is_null($mHandle)) {
					$row = $this->qHandler->fetch_object();
					$mHandle = $this->qHandler;
				} else {
					$row = $mHandle->fetch_object();
				}

			}
			if ($row === false || null === $row) {

				//print "Got false";
				if (isset($mHandle->error)) {
					throw new MySQLError('Error in row retrieval: ' . Error_Reporter::get_last_php_msg());

				}
				$this->stmt = null;
				$mHandle->free_result();

				return false;
			}

			return $row;
		}

		public function query($query, $result_mode = 0)
		{
			static $pinged = 0;
			$this->query_str = $query;
			$this->qHandler = @parent::query($query);
			if (!$this->qHandler) {
				if ($pinged < 5 && $this->errno === static::MYSQL_GONE_AWAY_CODE && $this->ping()) {
					// let's retry 5 times...
					$pinged++;

					return $this->query($query);
				}
				if (is_debug()) {
					print 'Query error: ' . $this->error . "\n";
					Error_Reporter::print_stack();
				} else {
					Error_Reporter::report('Query error while executing query ' .
						$this->error . ' || query: ' . $query);
				}
				fatal('Database error');
			}

			$this->num_rows = 0;
			if ($num = $this->num_rows) {
				$this->num_rows = $num;
			} else if ($num = $this->affected_rows) {
				$this->num_rows = $num;
			}
			$pinged = 0;

			return $this->qHandler;
		}

		public function connected()
		{
			return !$this->connect_errno;
		}

		/**
		 * Filter a list of tables for a contained column
		 *
		 * @param array  $tables
		 * @param string $column
		 * @param string $database
		 * @return array
		 */
		public function filterTables(array $tables, string $column, string $database = null): array
		{
			$filtered = [];
			foreach ($tables as $table) {
				if (!$this->tableExists($table, $database)) {
					continue;
				}
				if (!$this->columnExists($column, $table, $database)) {
					continue;
				}
				$filtered[] = $table;
			}

			return $filtered;

		}

		/**
		 * Table exists
		 *
		 * @param string $table
		 * @param null   $db
		 * @return bool
		 */
		public function tableExists(string $table, $db = null): bool
		{
			if (null === $db) {
				$db = 'DATABASE()';
			} else {
				$db = "'" . parent::escape_string($db) . "'";
			}

			$rs = parent::query('SELECT ENGINE FROM information_schema.tables WHERE table_schema = ' .
				$db . " AND table_name = '" . parent::escape_string($table) . "' LIMIT 1");
			if ($rs->num_rows < 1) {
				return false;
			}

			return $rs->num_rows >= 1;
		}

		/**
		 * Get table type
		 * @param string      $table
		 * @param string|null $db
		 * @return null|string null if table does not exist or is view
		 */
		public function tableType(string $table, $db = null): ?string
		{
			if (null === $db) {
				$db = 'DATABASE()';
			} else {
				$db = "'" . parent::escape_string($db) . "'";
			}
			$rs = parent::query('SELECT ENGINE FROM information_schema.tables WHERE table_schema = ' .
				$db . " AND table_name = '" . parent::escape_string($table) . "' AND ENGINE IS NOT NULL LIMIT 1");
			if ($rs->num_rows < 1) {
				return null;
			}
			return $rs->fetch_object()->ENGINE;
		}

		public function columnExists($column, $table, $db = null)
		{
			if (null === $db) {
				$db = 'DATABASE()';
			} else {
				$db = "'" . parent::escape_string($db) . "'";
			}
			$rs = parent::query('SELECT 1 FROM information_schema.columns WHERE table_schema = ' .
				$db . " AND table_name = '" . parent::escape_string($table) . "' AND column_name = '" .
				parent::escape_string($column) . "' LIMIT 1");

			return $rs->num_rows >= 1;
		}
	}
