<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;

	/**
	 * Class Generate
	 *
	 * Generate an index of RPM files
	 */
	class Generate extends Synchronizer
	{
		public function __construct()
		{
			// noop
		}

		public function run()
		{
			SynchronizerCache::get()->generate();
		}
	}