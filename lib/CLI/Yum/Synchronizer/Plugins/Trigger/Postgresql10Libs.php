<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Postgresql10Libs
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Postgresql10Libs extends PostgresqlAnyVersion
	{
		/**
		 * @var array
		 */
		protected $alternatives = [
			[
				'name'     => 'pgsql-ld-conf',
				'src'      => '/etc/ld.so.conf.d/postgresql-pgdg-libs.conf',
				'dest'     => '/usr/pgsql-{pg_version}/share/postgresql-{pg_version}-libs.conf',
				'priority' => 1000
			],
		];

		public function update(string $package): bool
		{
			if (!parent::update($package)) {
				return false;
			}

			return $this->ldconfig();
		}

		public function install(string $package): bool
		{
			if (!parent::install($package)) {
				return false;
			}

			return $this->ldconfig();
		}

		private function ldconfig(): bool
		{
			$proc = new \Util_Process_Chroot(Utils::getServicePath('siteinfo'));
			return $proc->run('ldconfig')['success'] ?? false;
		}

	}