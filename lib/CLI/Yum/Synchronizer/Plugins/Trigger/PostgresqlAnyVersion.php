<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;

	/**
	 * Class Binutils
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class PostgresqlAnyVersion extends AlternativesTrigger
	{
		use \NamespaceUtilitiesTrait;

		/**
		 * Substitute {pg_version} placeholder
		 *
		 * @return array
		 */
		public function getAlternatives(): array
		{
			$version = $this->extractVersion(static::getBaseClassName());

			return array_map(static function ($alternative) use ($version) {
				$alternative['dest'] = str_replace('{pg_version}', $version, $alternative['dest']);

				return $alternative;
			}, parent::getAlternatives());
		}

		private function extractVersion(string $package): ?string
		{
			$tmp = substr($package, \strlen('Postgresql'));
			if (false === ($len = strspn($tmp, '0123456789'))) {
				return null;
			}

			return substr($tmp, 0, $len);
		}
	}