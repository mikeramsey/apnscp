<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Coreutils
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * Update sudo map when system files updated
	 * Requires sudo v1.8.7+
	 *
	 * @todo    support other packages
	 *
	 */
	class Coreutils extends Trigger
	{
		use \FilesystemPathTrait;
		/**
		 * @var string sudo template
		 */
		const SUDO_INPUT = 'templates/sudoers';
		/**
		 * @var string sudo output relative to filesystem template "siteinfo"
		 */
		const SUDO_PATH = '/etc/sudoers.d/apnscp';
		/**
		 * @var string system sudo path
		 */
		const SYS_SUDO_PATH = '/etc/sudoers';

		// limited support for 256 last checked
		const HASH_ALGO = 'sha224';
		/**
		 * @var array template hash and file correspondence
		 */
		protected static $hashed = [
			'rmhash'    => '/bin/rm',
			'chownhash' => '/bin/chown',
			'cphash'    => '/bin/cp'
		];


		public function update(string $package): bool
		{
			// update sudoers
			$svc = Utils::getServiceFromPackage($package);
			$path = Utils::getServicePath($svc);
			$this->pruneWheelUsage($path);
			return $this->generateSudoMap($path);
		}

		/**
		 * Ensure /etc/sudoers remains unlinked and secured
		 *
		 * @param string $path
		 * @return bool
		 */
		protected function pruneWheelUsage(string $path): bool
		{
			$sudoers = $path . self::SYS_SUDO_PATH;
			if (!file_exists($sudoers)) {
				return true;
			}

			if (file_exists(self::SYS_SUDO_PATH)) {
				$inode = fileinode(self::SYS_SUDO_PATH);
			}

			if (fileinode($sudoers) === $inode) {
				unlink($sudoers);
				copy(self::SYS_SUDO_PATH, $sudoers);
				chown($sudoers, fileowner(self::SYS_SUDO_PATH)) &&
					chmod($sudoers, fileperms(self::SYS_SUDO_PATH)) &&
					chgrp($sudoers, filegroup(self::SYS_SUDO_PATH));
			}

			$contents = preg_replace(
				'/^\s*%wheel\s+ALL\s*=\s*\(ALL\)\s+ALL/m',
				'',
				file_get_contents($sudoers)
			);
			file_put_contents($sudoers, $contents, LOCK_EX);
			return true;

		}

		/**
		 * Create sudo map from template
		 *
		 * @param string $base filesystem basepath to look for files
		 * @return bool
		 */
		protected function generateSudoMap(string $base): bool
		{
			if (!SSH_SUDO_SUPPORT) {
				return true;
			}
			$in = resource_path(self::SUDO_INPUT);
			if (!file_exists($in)) {
				warn("sudo template `%s' does not exist, ignoring sudo support", $in);
			}
			$template = file_get_contents($in);
			$hashes = $this->calculateHashes(self::$hashed, $base);
			if (!$hashes) {
				return false;
			}
			$transformed = str_replace(array_keys($hashes), array_values($hashes), $template);
			$outfile = $base . self::SUDO_PATH;
			if (!file_put_contents($outfile, $transformed, LOCK_EX)) {
				return error("failed to save sudoers file in `%s'", $outfile);
			}

			/**
			 * @TODO dump OFS cache
			 */
			return true;
		}

		/**
		 * Calculate hashes for updated system files
		 *
		 * @param array  $hashes hash template
		 * @param string $base   base fst
		 * @return array|bool
		 */
		protected function calculateHashes(array $hashes, string $base)
		{
			$transformed = [];
			foreach ($hashes as $name => $bin) {
				$path = $base . $bin;
				if (!file_exists($path)) {
					return error("sudo hash file `%s' missing, cannot generate sudoers!", $bin);
				}
				$transformed["%(${name})s"] = base64_encode(hash_file(self::HASH_ALGO, $path, true));
			}

			return $transformed;
		}

		public function install(string $package): bool
		{
			$svc = Utils::getServiceFromPackage($package);
			$path = Utils::getServicePath($svc);
			$this->pruneWheelUsage($path);
			return $this->generateSudoMap($path);
		}
	}