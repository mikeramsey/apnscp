<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Install;
	use CLI\Yum\Synchronizer\Plugins\Filelist\PhpAnyVersion as AnyVersionList;
	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use Opcenter\Filesystem;
	use Opcenter\Service\ServiceLayer;

	/**
	 * Class PhpAnyVersion
	 *
	 * Remi-compatible triggers
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class PhpAnyVersion extends Trigger
	{
		use \NamespaceUtilitiesTrait;
		use \FilesystemPathTrait;

		// @var string reserved
		const PHP_VERSION = null;

		const RELOCATION_BASE = FILESYSTEM_SHARED . '/php/multiphp';
		const SCL_BASE = '/opt/remi';
		const RELOCATABLES = [
			'/var' . self::SCL_BASE,
			self::SCL_BASE,
			'/etc/' . self::SCL_BASE
		];

		// @var string service Remi is located
		const SERVICE = 'siteinfo';

		/**
		 * @inheritDoc
		 */
		public function install(string $package): bool
		{
			// relocate /opt/remi to /.socket/php/multiphp
			if (!is_dir(static::RELOCATION_BASE) && !Filesystem::mkdir(static::RELOCATION_BASE)) {
				fatal("Failed to create any-version shared base `%s'", static::RELOCATION_BASE);
			}

			$this->relocatePaths();

			$sclPath = $this->service_template_path(static::SERVICE) . $this->getSclConfigurationPath();
			if (!file_exists($sclPath)) {
				file_put_contents($sclPath, static::SCL_BASE . "\n");
				register_shutdown_function(static function () {
					(new ServiceLayer(null))->dropVirtualCache();
				});
			}
			return parent::install($package);
		}

		/**
		 * Get SCL path
		 *
		 * @return string
		 */
		private function getSclConfigurationPath(): string
		{
			return AnyVersionList::SCL_PREFIX . DIRECTORY_SEPARATOR . $this->getPhpVersion();
		}

		private function relocatePaths(): void
		{
			$fstBase = $this->service_template_path(static::SERVICE);

			foreach (static::RELOCATABLES as $f) {
				if (!file_exists($f)) {
					warn("Path `%s' missing", $f);
					continue;
				}

				$path = static::RELOCATION_BASE . $f;

				if (! (is_dir($f) || is_link($f)) ) {
					if ( !(is_dir($parent = \dirname($path)) || Filesystem::mkdir($parent)) ) {
						fatal("Failed to create any-version path `%s'", $path);
					}

					if (!(rename($f, $path) && symlink($path, $f))) {
						fatal("Failed to relocate to any-version path `%s'", $path);
					}
				}

				// link Remi to FST/service
				$fstPath = $fstBase . $f;
				if (file_exists($fstPath)) {
					continue;
				}
				if ( !(is_dir($parent = \dirname($fstPath)) || Filesystem::mkdir($parent)) ) {
					fatal("Failed to create directory `%s' in FST", $parent);
				}

				is_link($fstPath) || symlink($path, $fstPath);
			}
		}

		/**
		 * @inheritDoc
		 */
		public function remove(string $package): bool
		{
			$fstPath = $this->service_template_path(static::SERVICE);
			foreach (static::RELOCATABLES as $f) {
				$path = $f . DIRECTORY_SEPARATOR . $this->getPhpVersion();
				if (is_link($path)) {
					unlink($path);
				}

				if (is_link($fstPath . DIRECTORY_SEPARATOR . $path)) {
					unlink($fstPath . DIRECTORY_SEPARATOR . $path);
				}
			}

			if (file_exists($fstPath . DIRECTORY_SEPARATOR . $this->getSclConfigurationPath())) {
				unlink($fstPath . DIRECTORY_SEPARATOR . $this->getSclConfigurationPath());
			}

			return parent::remove($package);
		}

		/**
		 * @inheritDoc
		 */
		public function update(string $package): bool
		{
			return $this->install($package);
		}

		/**
		 * Get version from package name
		 *
		 * @return string formatted as php<major><minor>
		 */
		private function getPhpVersion(): string
		{
			$class = static::PHP_VERSION ?? snake_case(static::getBaseClassName());

			return strtok($class, '_');
		}
	}