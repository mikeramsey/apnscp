<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins;

	abstract class Filelist implements PluginInterface
	{
		use LimitPackage;

		const EXCLUDED_FILES = [];

		// @var array cached filtered files
		private $filtered;

		public function install(string $package): bool
		{
			return true;
		}

		public function remove(string $package): bool
		{
			return true;
		}

		public function update(string $package): bool
		{
			return true;
		}

		/**
		 * Reject files from RPM list
		 *
		 * @param array $files
		 * @return array resolved filelist
		 */
		public function filterFiles(array $files): array
		{
			if (!static::EXCLUDED_FILES) {
				return $files;
			}

			if (null === $this->filtered) {
				$this->filtered = array_filter($files, static function ($v) {
					return !\in_array($v, static::EXCLUDED_FILES, true);
				});
			}

			return $this->filtered;
		}

		/**
		 * Append files to filelist
		 *
		 * @param array $files
		 * @return array resolved filelist
		 */
		public function appendFiles(array $files): array {
			return $files;
		}
	}