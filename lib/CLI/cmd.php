<?php

	namespace cli;

	use Auth;
	use Error_Reporter;
	use Opcenter\CliParser;
	use Symfony\Component\Yaml\Yaml;

	if (!\defined('INCLUDE_PATH')) {
		\define('INCLUDE_PATH', \dirname(__DIR__, 2));
	}

	if (!\defined('IS_CLI')) {
		\define('IS_CLI', 0);
	}
	if (!\defined('NO_AUTH')) {
		\define('IS_ISAPI', 0);
		\define('NO_AUTH', 1);
	}

	include(INCLUDE_PATH . '/lib/apnscpcore.php');
	Auth::use_handler_by_name('CLI');
	Auth::import('CLI');

	ini_set('html_errors', 0);

	\Lararia\Bootstrapper::minstrap();

	function flush_reporter_buffer()
	{
		$severity = \Error_Reporter::get_severity();
		$buffer = \Error_Reporter::get_buffer();
		if (!$buffer || \Error_Reporter::is_verbose(4)) {
			return;
		}

		fwrite(STDERR, str_repeat('-', 40) . "\n");
		fwrite(STDERR, 'MESSAGE SUMMARY' . "\n" . 'Reporter level: ' . \Error_Reporter::errno2str($severity) . "\n");
		foreach ($buffer as $err) {
			$severity = $err['severity'];
			$msg = $err['message'];
			fwrite(STDERR, strtoupper(Error_Reporter::error_type($severity)) . ': ' . $msg . "\n");
		}
		fwrite(STDERR, str_repeat('-', 40) .  "\n");
	}

	function getInvoker(): \apnscpFunctionInterceptor
	{
		static $invoker;
		if (null === $invoker) {
			$invoker = \Auth::get_driver()->getInvoker();
		}
		return $invoker;
	}

	function __call($cmd, $args)
	{
		if (!getInvoker()->verify_args($cmd, $args)) {
			return null;
		}
		return getInvoker()->call($cmd, $args);
	}

	function raw($cmd)
	{
		$cmd = '<?php ' . $cmd . '?>';
		$tokens = token_get_all($cmd);
		$new = array();
		foreach ($tokens as $i => $token) {

			if (!\is_array($token)) {
				$new[] = $token;
				continue;
			}

			if ($token[0] == T_STRING && !\defined($token[1]) && $token[1] !== 'null' &&
				$token[1] !== 'false' && $token[1] !== 'true' &&
				!\is_callable($token[1])
			) {
				$new[] = '$c->';
			}
			$new[] = $token[1];
		}
		$semi = 0;
		// -1: ? >, 0: <?
		for ($i = sizeof($new) - 2; $i > 0; $i--) {
			if ($new[$i] == ';') {
				if ($semi) {
					$semi = $i;
					break;
				}
				$semi = -$i;
				continue;
			}
		}
		if ($semi < 0) {
			$semi = 0;
		}
		$semi++;
		if ($semi > 0 && $new[$semi + 1] != 'return') {
			$new[$semi] = ' return ' . $new[$semi];
		}
		unset($new[0]);
		array_pop($new);
		$cmd = join('', $new);

		return eval($cmd);
	}

	function reload_shell()
	{
		save_history();
		pcntl_exec(PHP_BINARY, $_SERVER['argv'], $_ENV);
	}

	function save_history()
	{
		if (\extension_loaded('readline')) {
			$pwd = posix_getpwnam(posix_getlogin());

			return readline_write_history($pwd['dir'] . '/.ais_history');
		}

		$ppid = posix_getppid();
		$hist = '/tmp/hist.' . $ppid;
		umask(077);
		file_put_contents($hist, serialize($GLOBALS['history']));
	}

	function load_history()
	{
		$GLOBALS['history'] = array();
		if (\extension_loaded('readline')) {
			$pwd = posix_getpwnam(posix_getlogin());
			$history = $pwd['dir'] . '/.ais_history';
			if (!file_exists($history)) {
				return true;
			}
			readline_read_history($history);
			$GLOBALS['history'] = explode("\n", file_get_contents($history));

			return true;
		}

		$hist = '/tmp/hist.' . posix_getppid();
		if (!file_exists($hist)) {
			return false;
		}
		$GLOBALS['history'] = \Util_PHP::unserialize(file_get_contents($hist));
		unlink($hist);

		return true;
	}

	function show_help()
	{
		print 'Usage: ' . basename($_SERVER['argv'][0]) . " -u USER [options ...] -i | cmd [args ...]\n" .
			'  ' . 'cmd' . "\t" . 'Command to execute in module_method format, including optional <args>' . "\n" .
			'  ' . '--interactive' . "\t\t" . 'run interactively' . "\n" .
			'  ' . '-m | --multi' . "\t\t" . 'separate multiple API commands with a semicolon (;) - cli input only!' . "\n" .
			'  ' . '-l | --list-commands' . "\t" . 'list API commands available to role' . "\n" .
			'  ' . 'UNIVERSAL ARGUMENTS' . "\n" .
			'  ' . '-u' . "\t" . 'Username' . "\n" .
			'  ' . '-p' . "\t" . 'Password to use.  If none is given, then read from tty.' . "\n" .
			'  ' . '-d' . "\t" . 'Domain, leave blank for non account-level roles.' . "\n" .
			'  ' . '-D' . "\t" . "Disable output delimiter. (default \"\\n\")" . "\n" .
			'  ' . '-o' . "\t" . 'Set output (yaml, json, var_dump, print, cli, serialize). Default "auto" ("print" scalar, "yaml" complex)' . "\n" .
			'  ' . '-i' . "\t" . 'Set input (json, cli, serialize)' . "\n" .
			'  ' . '-r' . "\t" . 'Treat arguments as native PHP code' . "\n" .
			'  ' . "\n\nNote: arrays are formatted as [key:val,...] or simply [val], \"\" recommended" . "\n";
		exit(255);
	}

	function prompt($msg)
	{
		fwrite(STDOUT, $msg . ":\n");
	}

	function list_history()
	{
		for ($histsz = sizeof($GLOBALS['history']), $i = max(0, $histsz - 1000);
			 $i < $histsz; $i++) {
			printf("%4d %s\n", $i + 1, $GLOBALS['history'][$i]);
		}
	}

	function exec_history($n, $interp)
	{
		$n = ltrim($n, '!');
		if (!isset($GLOBALS['history'][$n - 1])) {
			print (($n + 1) . ': invalid history index');

			return null;
		}

		return array($GLOBALS['history'][$n - 1]);

	}

	function launch_shell()
	{
		$__lines = true;
		$__shell = new \PHP_Shell();

		$c = Auth::get_driver()->getInvoker();
		$GLOBALS['c'] = &$c;
		$GLOBALS['history'] = array();
		load_history();

		$__shell_exts = \PHP_Shell_Extensions::getInstance();
		$__shell_cmd = \PHP_Shell_Commands::getInstance();
		$__shell_cmd->registerCommand('#^history\b#', 'list_history', 'history', 'get command history');
		$__shell_cmd->registerCommand('#^!\d{1,}\b#', 'exec_history', '!n', 'execute n-th command in history');
		if (\extension_loaded('pcntl')) {
			$__shell_cmd->registerCommand('#^r(?:eload)?\b#', 'reload_shell', 'r[eload]', 'reload shell');
		}

		$__shell_exts->registerExtensions(array(
			'options' => \PHP_Shell_Options::getInstance(), /* the :set command */

			'autoload'       => new \PHP_Shell_Extensions_Autoload(),
			'autoload_debug' => new \PHP_Shell_Extensions_AutoloadDebug(),
			'colour'         => new \PHP_Shell_Extensions_Colour(),
			'exectime'       => new \PHP_Shell_Extensions_ExecutionTime(),
			'inlinehelp'     => new \PHP_Shell_Extensions_InlineHelp(),
			'verboseprint'   => new \PHP_Shell_Extensions_VerbosePrint(),
			'loadscript'     => new \PHP_Shell_Extensions_LoadScript(),
		));


		$__shell->init();
		print $__shell_exts->colour->getColour('default');
		while ($__shell->input()) {
			try {
				$__shell_exts->exectime->startParseTime();
				$code = $__shell->getCode();
				if (empty($code) && isset($__retval)) {
					if (is_scalar($__retval)) {
						print $__retval;
					} else {
						var_dump($__retval);
					}
					continue;
				}

				if ($__shell->parse() == 0) {
					$code = $__shell->getCode();
					$GLOBALS['history'][] = rtrim($code, ';');
					## we have a full command, execute it

					$__shell_exts->exectime->startExecTime();
					$__shell_retval = $__retval = eval($code);

					if (!\is_null($__shell_retval)) {
						print $__shell_exts->colour->getColour('value');
						if (\function_exists('__shell_print_var')) {
							__shell_print_var($__shell_retval, $__shell_exts->verboseprint->isVerbose());
						} else {
							print "\n";
							if (is_scalar($__retval)) {
								print $__retval;
							} else {
								if (\is_array($__retval)) {
									print 'array(' . \count($__retval) . ')';
								} else {
									if (\is_object($__retval)) {
										print 'object(' . \get_class($__retval) . ')';
									} else {
										if (\is_resource($__retval)) {
											print 'resource(' . $__retval . ')';
										} else {
											print 'unknown(' . \gettype($__retval) . ')';
										}
									}
								}
							}
						}
					}
					flush_reporter_buffer();

					## cleanup the variable namespace
					unset($__shell_retval);
					$__shell->resetCode();
				}
			} catch (Exception $__shell_exception) {
				print $__shell_exts->colour->getColour('exception');
				printf('%s (code: %d) got thrown' . PHP_EOL, \get_class($__shell_exception),
					$__shell_exception->getCode());
				print $__shell_exception;

				$__shell->resetCode();

				## cleanup the variable namespace
				unset($__shell_exception);
			}

			print $__shell_exts->colour->getColour('default');
			$__shell_exts->exectime->stopTime();
			if ($__shell_exts->exectime->isShow()) {
				printf(' (parse: %.4fs, exec: %.4fs)',
					$__shell_exts->exectime->getParseTime(),
					$__shell_exts->exectime->getExecTime()
				);
			}
		}

		print $__shell_exts->colour->getColour('reset');
	}

	function read_password($mask = false)
	{
		prompt('Enter Password');
		// Get current style
		$oldStyle = shell_exec('stty -g');

		if (!$mask) {
			shell_exec('stty -echo');
			$password = rtrim(fgets(STDIN), "\n");
		} else {
			shell_exec('stty -icanon -echo min 1 time 0');
			$password = '';
			while (true) {
				$char = fgetc(STDIN);

				if ($char === "\n") {
					break;
				} else {
					if (\ord($char) === 127) {
						if (\strlen($password) > 0) {
							fwrite(STDOUT, "\x08 \x08");
							$password = substr($password, 0, -1);
						}
					} else {
						fwrite(STDOUT, '*');
						$password .= $char;
					}
				}
			}
		}

		// Reset old style
		shell_exec('stty ' . $oldStyle);

		// Return the password
		return $password;
	}


	function parse(&$options = null, $short = '', $long = [])
	{
		if (\is_null($options)) {
			$options = array();
		}
		$options['verbose'] = 0;
		$options['interactive'] = 0;
		$options['raw'] = 0;
		$options['delimiter'] = "\n";
		$options['output'] = 'auto';
		$options['input'] = 'cli';
		$options['multimode'] = false;
		$user = ''; //getenv('LOGNAME');
		$domain = '';//getenv('HOSTNAME');
		$password = '';

		$long[] = 'interactive';
		$long[] = 'multi';
		$long[] = 'list-commands::';
		$long[] = 'help';
		$long[] = 'output:';
		$skippedOptions = buildSkipList($short, $long);
		$argc = 1;
		$argv = $_SERVER['argv'];
		$optind = null;
		$opts = getopt('hri:u:p:d:D:o:ml::' . $short, $long, $optind);
		foreach ($opts as $opt => $optval) {
			if ($optval != false && ltrim($argv[$argc], '-') == $opt) {
				$argc++;
			}
			$argc++;
			switch ($opt) {
				case 'u':
					$user = $optval;
					break;
				case 'p':
					if ($optval) {
						$password = $optval;
					} else {
						$password = read_password();
					}
					break;
				case 'd':
					$domain = $optval;
					break;
				case 'interactive':
					$options['interactive'] = 1;
					break;
				case 'r':
					$options['raw'] = 1;
					break;
				case 'D':
					$options['delimiter'] = $optval;
					break;
				case 'o':
				case 'output':
					$options['output'] = $optval;
					break;
				case 'i':
					if (!\in_array($optval, ['cli', 'json', 'serialize'], true)) {
						fatal("Unknown input format specified `%s'", $optval);
					}
					$options['input'] = $optval;
					break;
				case 'm':
				case 'multi':
					$options['multimode'] = true;
					break;
				case 'list-commands':
				case 'l':
					$args = false === $optval ? [$optval] : [];
					$response = cmd($user, $domain, $password)->call('misc_list_commands', $args);
					print formatResponse($response, 'yaml');
					exit(0);
				case 'h':
				case 'help':
					if ($optind === \count($argv) - 1) {
						// cpcmd --help command:name
						$cmd = normalizeCommand($argv[$optind]);
						$resp = cmd($user, $domain, $password)->call('misc_i', [$cmd]);
						if ($resp) {
							// command found, let's be nice
							formatResponse($resp, $options['output']);
							exit(0);
						}
					}
					// odd it doesn't flow into the next block...
					show_help();
				case '?':
				default:
					// allow overrides via parse()
					if (isset($skippedOptions[$opt])) {
						$options[$opt] = $optval;
						break;
					}
					show_help();
			}
		}
		$output = $options['output'];
		register_shutdown_function(static function () use ($output) {
			if (!\Error_Reporter::is_error()) {
				return;
			}
			if (is_debug()) {
				// already listed or to be listed
				return;
			}
			if ($output === 'yaml' || $output === 'json' || $output === 'serialize') {
				formatResponse(\Error_Reporter::get_errors(), $output, STDERR);
				return;
			}
			foreach (\Error_Reporter::get_errors() as $e) {
				fwrite(STDERR, \Error_Reporter::errno2str(\Error_Reporter::E_ERROR) . ": $e\n");
			}
		});

		$afi = cmd($user, $domain, $password);
		if (!$afi) {
			exit(-1);
		}

		if ($options['interactive']) {
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Options.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Commands.php';
			include INCLUDE_PATH . '/lib/CLI/Shell.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/Prototypes.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/Autoload.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/AutoloadDebug.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/Colour.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/ExecutionTime.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/InlineHelp.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/VerbosePrint.php';
			include INCLUDE_PATH . '/lib/CLI/Shell/Extensions/LoadScript.php';
			launch_shell();
			exit(0);
		}

		return \array_slice($argv, $argc);
	}

	function buildSkipList(string $short, array $long): array
	{
		$skip = preg_split('//', str_replace(':', '', $short), -1, PREG_SPLIT_NO_EMPTY);
		return array_flip($skip + append_config(array_map(static function($v) {
			return rtrim($v, ':');
		}, $long)));
	}

	/**
	 * @TODO dislike present layout, should classify implementation
	 */
	function get_instance()
	{
		return getInvoker();
	}

	/** main */
	function main()
	{
		$opts = array();
		$args = parse($opts);

		if (!isset($args[0]) || $args[0][0] == '-') {
			show_help();
		}
		$resp = null;
		if ($opts['raw']) {
			$resp = raw(join(' ', $args));
		} else if ($opts['input'] === 'serialize') {
			while (false !== $v = next($args)) {
				$args[key($args)] = \Util_PHP::unserialize($v, true);
			}
			reset($args);
			$resp = __call(str_replace([':', '-'], '_', $args[0]), \array_slice($args, 1));
		} else if ($opts['input'] === 'json') {
			array_walk($args, static function (&$v) {
				$tmp = json_decode($v, true);
				if (null === $tmp && 'null' === ($tmp = json_decode('"' . str_replace('"', '\\"', $v) . '"', true))) {
					$v = null;
				} else {
					$v = $tmp;
				}
			});
			$resp = __call(str_replace([':', '-'], '_', $args[0]), \array_slice($args, 1));
		} else if ($opts['input'] === 'cli') {
			$cmd = normalizeCommand($args[0]);
			$args = \array_slice($args, 1);
			$cmdargs = $cmds = array();
			$key = null;
			for ($i = 0, $n = \count($args); $i < $n; $i++) {
				$arg = \Opcenter\CliParser::parseArgs($args[$i]);
				/**
				 * support spanning multiple commands in a single cpcmd
				 * cpcmd -m fn arg1 ; fn2 arg2 arg3
				 *
				 * This breaks down if fn arg is ";",
				 * so lookahead to see if the argument is a delimiter or clever payload
				 *
				 * In theory it could confuse the parser, but cpcmd operates in limited roles
				 */
				if ($arg === ';' && $opts['multimode']) {
					$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
					try {
						if (!getInvoker()->verify_args($cmd, $cmdargs)) {
							continue;
						}
						$cmds[] = array($cmd, $cmdargs);
						$cmdargs = [];
						$cmd = normalizeCommand($args[++$i]);
					} catch (\apnscpException $e) {
						$cmdargs[] = $arg;
						// incomplete command
						debug("Command `%s' appears incomplete with arguments - ignoring", $cmd);
					} finally {
						\Error_Reporter::exception_upgrade($oldex);
					}
					continue;
				}
				$cmdargs[] = $arg;
			}
			$cmds[] = array($cmd, $cmdargs);

			foreach ($cmds as $c) {
				[$cmd, $args] = $c;
				// normalize command
				$resp = __call($cmd, $args);
			}
		}

		$success = Error_Reporter::get_severity();

		$exit = 0;
		if ($success & Error_Reporter::E_ERROR) {
			$exit = 255;
		}

		flush_reporter_buffer();

		formatResponse($resp, $opts['output']);
		echo $opts['delimiter'];
		exit($exit);
	}

	/**
	 * @param        $resp    response data
	 * @param string $format  output format
 	 * @param null   $channel channel
	 */
	function formatResponse($resp, string $format,  $channel = null): void
	{
		if (null === $channel) {
			$channel = STDOUT;
		}
		/**
		 * Format objects as arrays if present
		 */
		if (\is_array($resp)) {
			$resp = array_map(static function ($resp) {
				if (\is_object($resp) && method_exists($resp, 'toArray')) {
					return $resp->toArray();
				}
				return $resp;
			}, $resp);
		}

		switch ($format) {
			case 'json':
				fwrite($channel, \GuzzleHttp\json_encode($resp, JSON_OBJECT_AS_ARRAY));
				break;
			case 'serialize':
				fwrite($channel, serialize($resp));
				break;
			case 'yaml':
				fwrite($channel, Yaml::dump($resp, getenv('YAML_INLINE') ?: 2, 2, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK | Yaml::DUMP_OBJECT_AS_MAP | Yaml::DUMP_OBJECT));
				break;
			case 'var_dump':
				if ($channel !== STDOUT) {
					debug('var_dump may only report on STDOUT');
				}
				var_dump($resp);
				break;
			case 'cli':
				fwrite($channel, CliParser::collapse($resp));
				break;
			case 'print':
				if (\is_array($resp)) {
					fwrite($channel, print_r($resp, true));
				} else {
					fwrite($channel, $resp);
				}
				break;
			case 'auto':
				if (is_scalar($resp)) {
					formatResponse($resp, 'print');
				} else {
					formatResponse($resp, 'yaml');
				}
				break;
			default:
				fatal('???');
		}

	}

	function normalizeCommand(string $command): string
	{
		return str_replace([':', '-'], '_', $command);
	}

	function cmd($user = null, $domain = null, $password = null): \apnscpFunctionInterceptor
	{
		try {
			if (session_status() === PHP_SESSION_ACTIVE) {
				session_write_close();
			}
		} catch (\Throwable $e) {
			warn("Failed to close existing session - destroying");
			session_destroy();
		}
		// create new auth instance
		$domainold = $domain;
		$auth = \Auth::get_driver();
		$user = $user ?: $auth->get_user($domain);
		$domain = $auth->get_domain($domain);

		if (!$user) {
			$uid = posix_getuid();
			$pwd = posix_getpwuid($uid);
			$file = \apnscpFunctionInterceptor::get_autoload_class_from_module('auth')::ADMIN_AUTH;
			$user = strtok(file_get_contents($file), ':') ?: $pwd['name'];
		}

		if (!$domain && $domainold) {
			fatal("error: domain $domainold does not exist");
		}
		$_ENV['USERNAME'] = $user;
		$_ENV['DOMAIN'] = $domain;
		$_ENV['PASSWORD'] = $password;
		if (!$auth->session_valid() && !$auth->authenticate()) {
			$auth->invalidate_session();

			fatal("Invalid session");
		}

		$context = \Auth::context($user, $domain);
		$auth->setID($context->id);
		if (session_status() !== PHP_SESSION_ACTIVE) {
			session_id($context->id);
		}

		$invoker = $auth->getInvoker();
		$invoker->set_session_id($context->id);

		return $auth->getInvoker();
	}

	function dump_buffer($buffer)
	{
		if (!empty($buffer)) {
			foreach ($buffer as $b) {
				if (!($b['severity'] & Error_Reporter::E_ERROR)) {
					continue;
				}
				$msg = strtoupper(Error_Reporter::error_type($b['severity']))
					. ': ' . $b['message'];
				dlog($msg);
			}
			foreach ($buffer as $b) {
				$msg = strtoupper(Error_Reporter::error_type($b['severity']))
					. ': ' . $b['message'];
				// picked up automatically
				if ($b['severity'] & Error_Reporter::E_ERROR) {
					continue;
				}
				if (!is_debug()) {
					dlog($msg);
				}
				if ($b['severity'] & Error_Reporter::E_WARNING) {
					fwrite(STDERR, $msg . "\n");
				} else {
					fwrite(STDOUT, $msg . "\n");
				}

			}
		}
	}
