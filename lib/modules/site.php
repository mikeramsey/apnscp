<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Module\Support\Auth;
	use Opcenter\Filesystem\Quota;
	use Opcenter\Map;
	use Opcenter\Process;
	use Opcenter\Service\Validators\Billing\ParentInvoice;

	/**
	 * Provides site administrator-specific functionality
	 *
	 * @package core
	 *
	 */
	class Site_Module extends Auth
		implements Opcenter\Contracts\Hookable
	{
		use ImpersonableTrait;
		const MIN_STORAGE_AMNESTY = QUOTA_STORAGE_WAIT;

		// time in seconds between amnesty requests
		const AMNESTY_DURATION = QUOTA_STORAGE_DURATION;
		// 24 hours
		const AMNESTY_MULTIPLIER = QUOTA_STORAGE_BOOST;
		const DEPENDENCY_MAP = [];
		const AMNESTY_JOB_MARKER = 'amnesty-%(site)s';

		protected $exportedFunctions = [
			'*'               => PRIVILEGE_SITE,
			'get_admin_email' => PRIVILEGE_SITE | PRIVILEGE_USER,
			'ip_address'      => PRIVILEGE_SITE | PRIVILEGE_USER,
			'split_hostname'  => PRIVILEGE_SITE | PRIVILEGE_USER
		];

		/**
		 * bool user_service_enabled(string, string)
		 *
		 * @param string $mUser username
		 * @param string $mSrvc service name, possible values are ssh proftpd and imap
		 * @return bool
		 */
		public function user_service_enabled(string $user, string $service): bool
		{
			$svc_cache = $this->user_svc_cache;
			$svc_file = $this->domain_fs_path() . '/etc/' . $service . '.pamlist';
			$site = $this->site_id;

			if (!file_exists($svc_file)) {
				return error("Invalid service name `%s'", $service);
			}

			/** check local cache */
			if (!isset($svc_cache[$site]) ||
				filemtime($svc_file) > $svc_cache[$site]['mtime']
			) {
				$fp = fopen($svc_file, 'r');
				$contents = fread($fp, filesize($svc_file));
				foreach (explode("\n", $contents) as $line) {
					$svc_cache[$site]['users'][trim($line)][$service] = 1;
				}

				fclose($fp);
				$svc_cache[$site]['mtime'] = filemtime($svc_file);
			}

			return isset($svc_cache[$site]['users'][$user][$service]);

		}

		/**
		 *  array get_bandwidth_usage(string)
		 *
		 * @privilege PRIVILEGE_SITE
		 * @param int $type type of bandwidth usage to retrieve
		 * @return array|bool indexes begin, rollover, and threshold
		 */
		public function get_bandwidth_usage(int $type = null)
		{
			deprecated_func('Use bandwidth_usage()');

			return $this->bandwidth_usage($type);
		}

		/**
		 * Retrieve day on which banwidth rolls over to 0
		 *
		 * @return int
		 */
		public function get_bandwidth_rollover(): int
		{
			deprecated_func('Use bandwidth_rollover');

			return $this->bandwidth_rollover();
		}

		// }}}

		/**
		 * bool set_admin_email(string email)
		 *
		 * @privilege PRIVILEGE_SITE
		 * @return bool true on success, false on failure
		 * @param string $email e-mail address to update the record to
		 *                      Backend PostgreSQL operation to update it in the db
		 */
		public function set_admin_email(string $email): bool
		{
			if (!preg_match(Regex::EMAIL, $email)) {
				return error('Invalid e-mail address, ' . $email);
			}
			$oldemail = $this->getConfig('siteinfo', 'email');
			$pgdb = \PostgreSQL::initialize();
			$pgdb->query("UPDATE siteinfo SET email = '" . $email . "' WHERE site_id = '" . $this->site_id . "';");
			// no need to trigger a costly account config rebuild
			$this->setConfig('siteinfo', 'email', $email);

			$ret = $pgdb->affected_rows() > 0;
			if (!$ret) {
				return false;
			}
			parent::sendNotice('email', [
				'email' => $oldemail,
				'ip'    => \Auth::client_ip()
			]);

			return true;
		}

		/**
		 * Destroy all processes matching user
		 *
		 * @param string $user
		 * @return bool
		 */
		public function kill_user(string $user): bool
		{
			if (!IS_CLI) {
				return $this->query('site_kill_user', $user);
			}

			if (!($uid = $this->user_get_uid_from_username($user))) {
				return error("Failed to lookup user `%s'", $user);
			}

			if ($uid < \User_Module::MIN_UID) {
				return error("User `%s' is system user", $user);
			}

			foreach (Process::matchUser($uid) as $pid) {
				Process::killAs($pid, SIGKILL, $uid);
			}

			return true;
		}

		/**
		 * Get admin email
		 *
		 * @return string
		 */
		public function get_admin_email(): string
		{
			return $this->getConfig('siteinfo', 'email');
		}


		/* }}} */

		// {{{ ip_address()

		/**
		 * Get IP address attached to account
		 *
		 * @return string
		 */
		public function ip_address(): string
		{
			$addr = $this->common_get_ip_address();

			return is_array($addr) ? array_pop($addr) : $addr;
		}

		// }}}

		/**
		 * Get quota for an account
		 *
		 * qused: disk quota used in KB
		 * qsoft: disk quota soft limit in KB
		 * qhard: disk quota hard limit in KB
		 * fused: files used
		 * fsoft: files soft limit
		 * fhard: files hard limit
		 *
		 * @return array
		 *@see User_Module::get_quota()
		 */
		public function get_account_quota(): array
		{
			if (!IS_CLI) {
				return $this->query('site_get_account_quota');
			}
			return Quota::getGroup($this->group_id);
		}

		/**
		 * Get port range allocated to account
		 *
		 * @deprecated see ssh_port_range()
		 * @return array
		 */
		public function get_port_range(): array
		{
			deprecated_func('Use ssh_port_range()');
			return $this->ssh_port_range();
		}

		/**
		 * Wipe an account, reinitializing it to its pristine state
		 *
		 * @param string $token confirmation token
		 * @return bool|string wipe status or confirmation token
		 */
		public function wipe($token = '')
		{
			$token = strtolower((string)$token);
			$calctoken = $this->_calculateToken();
			if (!$token) {
				// allow wiping via AJAX, Account > Settings
				if (defined('AJAX') && AJAX) {
					return $calctoken;
				}
				$msg = 'This is the most nuclear of options. ' .
					"Respond with the following token `%s' to confirm";

				return warn($msg, $calctoken);
			}

			if ($token !== $calctoken) {
				$msg = "provided token `%s' does not match confirmation token `%s'";

				return error($msg, $token, $calctoken);
			}

			if (!IS_CLI) {
				return $this->query('site_wipe', $token);
			}
			if (!Crm_Module::COPY_ADMIN) {
				return error('Admin reminder address not setup - disallowing account reset');
			}
			$editor = new Util_Account_Editor($this->getAuthContext()->getAccount());
			// assemble domain creation cmd from current config
			$editor->importConfig();
			$afi = $this->getApnscpFunctionInterceptor();
			$modules = $afi->list_all_modules();
			foreach ($modules as $m) {
				$c = $afi->get_class_from_module($m);
				$class = $c::instantiateContexted($this->getAuthContext());
				$class->_reset($editor);
			}
			$addcmd = $editor->setMode('add')->getCommand();
			// send a copy of the command in case the account gets wiped and
			// never comes back from the dead
			Mail::send(Crm_Module::COPY_ADMIN, 'Account Wipe', $addcmd);
			$delproc = new Util_Account_Editor($this->getAuthContext()->getAccount());
			if (!$delproc->delete()) {
				return false;
			}
			$proc = new Util_Process_Schedule('now');
			$ret = $proc->run($addcmd);

			return $ret['success'];
		}

		/**
		 * Token confirmation to delete site
		 *
		 * @return string
		 */
		private function _calculateToken(): string
		{

			$inode = fileinode($this->domain_info_path());
			$hash = hash('crc32', (string)$inode);

			return $hash;
		}

		/**
		 * Request a temporary bump to account storage
		 *
		 * @see MIN_STORAGE_AMNESTY
		 * @return bool
		 */
		public function storage_amnesty(): bool
		{
			if (!IS_CLI) {
				return $this->query('site_storage_amnesty');
			}

			$last = $this->getServiceValue('diskquota', 'amnesty');
			$now = coalesce($_SERVER['REQUEST_TIME'], time());
			if (self::MIN_STORAGE_AMNESTY > ($now - $last)) {
				$aday = self::MIN_STORAGE_AMNESTY / 86400;

				return error('storage amnesty may be requested once every %d days, %d days remaining',
					$aday,
					$aday - ceil(($now - $last) / 86400)
				);
			}

			$storage = $this->getServiceValue('diskquota', 'quota');
			$newstorage = $storage * self::AMNESTY_MULTIPLIER;
			$acct = new Util_Account_Editor($this->getAuthContext()->getAccount(), $this->getAuthContext());
			$acct->setConfig('diskquota', 'quota', $newstorage)->setConfig('diskquota', 'amnesty', $now);
			$ret = $acct->edit();
			if ($ret !== true) {
				Error_Reporter::report(var_export($ret, true));

				return error('failed to set amnesty on account');
			}
			$acct->setConfig('diskquota', 'quota', $storage);
			$cmd = $acct->getCommand();
			$proc = new Util_Process_Schedule('+' . self::AMNESTY_DURATION . ' seconds');
			$proc->setID(ArgumentFormatter::format(self::AMNESTY_JOB_MARKER, ['site' => $this->site]));
			$ret = $proc->run($cmd);
			$msg = sprintf("Domain: %s\r\nSite: %d\r\nServer: %s", $this->domain, $this->site_id, SERVER_NAME_SHORT);
			Mail::send(Crm_Module::COPY_ADMIN, 'Amnesty Request', $msg);

			return $ret['success'];
		}

		/**
		 * Account is under amnesty
		 *
		 * @return bool
		 */
		public function amnesty_active(): bool
		{
			$time = $_SERVER['REQUEST_TIME'] ?? time();
			$amnesty = $this->getServiceValue('diskquota', 'amnesty');
			if (!$amnesty) {
				return false;
			}
			return ($time - $amnesty) <= self::AMNESTY_DURATION;
		}

		/**
		 * Assume the role of a secondary user
		 *
		 * @param string $user username or domain if parentage supported
		 * @return string
		 */
		public function hijack(string $user): string
		{
			if ($this->user_exists($user)) {
				if ($user === $this->username) {
					return $this->session_id;
				}

				return $this->impersonateRole($this->site, $user);
			}

			if (AUTH_SUBORDINATE_SITE_SSO && ($invoice = $this->getServiceValue('billing', 'invoice'))
				&& ($siteid = \Auth::get_site_id_from_anything($user)))
			{
				// permit SSO if config.ini permits, billing,invoice is set, and target site
				// is within invoice cluster
				$site = "site${siteid}";
				$parent = Map::load(ParentInvoice::MAP_FILE, 'r-')->fetch($site);
				if ($parent === $this->site) {
					return $this->impersonateRole($site);
				}
			}

			error("unknown user `%s'", $user);

			return $this->session_id;
		}

		public function _create()
		{
			$conf = $this->getServiceValue('siteinfo');
			$db = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP, 'wd');
			if (!$db->exists($conf['domain'])) {
				// @TODO remove once Opcenter is done
				$db->set($conf['domain'], $this->site);
			}
			$db->close();
		}

		public function _delete()
		{
			$db = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP, 'wd');
			$domain = array_get($this->getAuthContext()->conf('siteinfo'), 'domain', []);
			$db->delete($domain);
			$db->close();
		}

		public function _edit()
		{
			$new = $this->getAuthContext()->conf('siteinfo', 'new');
			$old = $this->getAuthContext()->conf('siteinfo', 'old');
			if ($new['domain'] === $old['domain']) {
				return;
			}
			// domain rename
			$db = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP, 'wd');
			$db->delete($old['domain']);
			$db->insert($new['domain'], $this->site);
			$db->close();
		}

		/**
		 * Configuration verification
		 *
		 * @param \Opcenter\Service\ConfigurationContext $ctx
		 * @return bool
		 */
		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			if ($usernew === $userold) {
				return;
			}
		}

		public function _create_user(string $user)
		{
		}

		public function _delete_user(string $user)
		{
		}


	}