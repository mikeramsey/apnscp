<?php declare(strict_types=1);

	use Opcenter\Admin\Settings\Setting;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */
	class Scope_Module extends Module_Skeleton
	{
		const SCOPE_CACHE_KEY = 'admin.syscfg';
		protected $exportedFunctions = ['*' => PRIVILEGE_ADMIN];

		public function __construct()
		{
			parent::__construct();
			if (!AUTH_ADMIN_API) {
				$this->exportedFunctions = ['*' => PRIVILEGE_NONE];
			}
		}

		/**
		 * Set server var
		 *
		 * @param string $name
		 * @param mixed  $val
		 * @return bool
		 */
		public function set(string $name, ...$val): bool
		{
			if (!IS_CLI && posix_getuid()) {
				if ($name === 'cp.restart') {
					/**
					 * Special case. Restart triggers HTTP restart, which drops
					 * connection. Send asynchronously and hope for the best.
					 */
					DataStream::get($this->getAuthContext())->
						setOption(\apnscpObject::NO_WAIT)->query('scope_set', $name, ...$val);
					return success('Panel restarting now. Service will be briefly interrupted.');
				}
				return $this->query('scope_set', $name, ...$val);
			}
			$c = Setting::className($name);
			if (!$c) {
				return error("Unknown admin setting `%s'", $name);
			}
			if (!\array_key_exists(0, $val)) {
				return error("`scope_set %s' requires one or more values. See `scope_info %s' for more information",
					$name,
					$name
				);
			}

			return (new $c)->set(...$val);
		}

		/**
		 * Alias to list()
		 *
		 * @see list()
		 *
		 * @param string $filter
		 * @return array
		 */
		public function l(string $filter = ''): array
		{
			return $this->list($filter);
		}

		/**
		 * List available configuration settings
		 *
		 * @param string $filter filter using glob-style rules (e.g. apache.*
		 * @return array
		 */
		public function list(string $filter = ''): array
		{
			$cache = Cache_Global::spawn();
			$filter = str_replace(':', '.', $filter);

			if (is_debug() || false === ($c = $cache->get(self::SCOPE_CACHE_KEY))) {
				$c = Opcenter\Admin\Settings\Setting::list();
				asort($c);
				$c = array_values($c);
				$cache->set(self::SCOPE_CACHE_KEY, $c);
			}

			return !$filter ? $c : array_values(array_filter($c, static function ($scope) use ($filter) {
				return fnmatch($filter, $scope);
			}));
		}

		/**
		 * Alias for info()
		 *
		 * @see info()
		 * @param string $name
		 * @return array|null
		 */
		public function i(string $name): ?array
		{
			return $this->info($name);
		}

		/**
		 * Get Scope information
		 *
		 * @param string $name
		 * @return array|null
		 */
		public function info(string $name): ?array
		{
			if (!IS_CLI && posix_getuid()) {
				return $this->query('scope_info', $name);
			}
			$c = Setting::className($name);
			if (!$c) {
				return null;
			}
			$class = new $c;

			return [
				'info'     => $class->getHelp(),
				'value'    => $class->get(),
				'settings' => $class->getValues(),
				'default'  => $class->getDefault()
			];
		}

		/**
		 * Get server var
		 *
		 * @param string $name
		 * @param array  $val optional subselection
		 * @return mixed
		 */
		public function get(string $name, ...$val)
		{
			if (!IS_CLI && posix_getuid()) {
				return $this->query('scope_get', $name, ...$val);
			}
			$c = Setting::className($name);
			if (!$c) {
				return error("Unknown admin setting `%s'", $name);
			}

			return (new $c)->get(...$val);
		}

		public function _housekeeping()
		{
			Cache_Global::spawn()->delete(self::SCOPE_CACHE_KEY);
		}
	}
