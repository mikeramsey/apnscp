<?php declare(strict_types=1);

	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */
	class Argos_Module extends Module_Skeleton
	{
		const DEFAULT_BACKEND = 'default';

		protected $exportedFunctions = ['*' => PRIVILEGE_ADMIN];

		/**
		 * Get monitored service status
		 *
		 * @param null|string $service   optional service status
		 * @return array
		 */
		public function status(string $service = null): array
		{
			if (!IS_CLI) {
				return $this->query('argos_status', $service);
			}

			$ret = \Util_Process_Safe::exec('monit -B status %s', $service);

			if (!$ret['success']) {
				error('Failed to query status: %s', $ret['stderr']);
				return [];
			}

			$status = $this->filterStatus($ret['stdout']);

			return $service ? $status[$service] ?? [] : $status;
		}

		/**
		 * Restart service
		 *
		 * @param string $service
		 * @return bool
		 */
		public function restart(string $service): bool
		{
			if (!IS_CLI) {
				return $this->query('argos_restart', $service);
			}

			$ret = Util_Process_Safe::exec('monit restart %s', $service);

			return $ret['success'] ?: error('Failed to restart %s service: %s ', $service, $ret['stderr']);
		}

		/**
		 * Stop service monitoring
		 *
		 * @param string $service service name or "all"
		 * @return bool
		 */
		public function unmonitor(string $service): bool
		{
			if (!IS_CLI) {
				return $this->query('argos_unmonitor', $service);
			}

			$ret = \Util_Process_Safe::exec('monit unmonitor %s', $service);

			return $ret['success'] ?: error('Failed to stop %s monitoring: %s ', $service, $ret['stderr']);
		}

		/**
		 * Resume service monitoring
		 *
		 * @param string $service service name or "all"
		 * @return bool
		 */
		public function monitor(string $service): bool
		{
			if (!IS_CLI) {
				return $this->query('argos_monitor', $service);
			}

			$ret = \Util_Process_Safe::exec('monit monitor %s', $service);

			return $ret['success'] ?: error('Failed to start %s monitoring: %s ', $service, $ret['stderr']);
		}

		/**
		 * List monitored items
		 *
		 * @return array
		 */
		public function list_monitored(): array
		{
			$ret = \Util_Process::exec('monit summary -B');
			if (!$ret['success']) {
				error('Failed to query monit');
				return [];
			}

			if (!preg_match_all('/^\s(?!Service Name)(\S+)/m', $ret['stdout'], $matches, PREG_SET_ORDER)) {
				return [];
			}

			return array_column($matches, 1);
		}

		/**
		 * Get a count of failed processes
		 *
		 * @param bool $down headcount for downed services or up
		 * @return int
		 */
		public function headcount(bool $down = true): int
		{
			if (!IS_CLI) {
				return $this->query('argos_headcount', $down);
			}

			$ret = \Util_Process_Safe::exec('monit report ' . ($down ? 'down' : 'up'));

			return $ret['success'] ? (int)$ret['stdout'] : (int)error('Failed to query Monit: %s', $ret['stderr']);
		}

		/**
		 * Validate all services are active
		 *
		 * @return array status
		 */
		public function validate_all(): array
		{
			if (!IS_CLI) {
				return $this->query('argos_validate_all');
			}

			// 1 used in clear/failed modes
			$ret = \Util_Process_Safe::exec('monit validate', [0, 1]);

			if (!$ret['success']) {
				error('Failed to validate services: %s ', $ret['stdout']);
				return [];
			}

			return $this->filterStatus($ret['stdout']);
		}

		private function filterStatus(string $response): array
		{
			$serviceBuckets = preg_split('/^(?=Process|Filesystem|Program|System\s+)/m', $response);
			array_shift($serviceBuckets); // uptime
			$services = [];
			foreach ($serviceBuckets as $bucket) {
				if (!preg_match_all(Regex::ARGOS_SERVICE_STATUS, $bucket, $matches, PREG_SET_ORDER)) {
					continue;
				}

				$processName = null;
				$fields = [
					// in ms
					'timing' => null
				];

				foreach ($matches as $m) {
					if ($m['proc']) {
						$processName = $m['proc'];
						$fields['type'] = strtolower($m['type']);
						continue;
					}

					$var = str_replace(' ', '_', $m['name']);
					$value = $m['value'];

					switch ($var) {
						case 'inodes_free':
							$value = strtok($value, ' ');
						case 'pid':
						case 'parent_pid':
						case 'uid':
						case 'gid':
						case 'threads':
						case 'children':
						case 'effective_uid':
						case 'last_exit_value':
						case 'inodes_total':
							$value = (int)$value;
							break;
						case 'unix_socket_response_time':
						case 'port_response_time':
							$fields['timing'] = (float)strtok($value, ' ');
							break;
						// in hundredths
						case 'cpu':
						case 'cpu_total':
							if (false === strpos($value, ' ')) {
								$value = (float)$value / 100;
							}
							break;
						case 'memory':
						case 'memory_total':
							$fields[$var . '_raw'] = (int)Formatter::changeBytes(str_replace(' ', '', substr($value, strpos($value, '[') + 1, -1)));
							break;
						case 'disk_write':
						case 'disk_read':
							$fields[$var . '_raw'] = (int)Formatter::changeBytes(str_replace(' ', '',
								substr($value, $pos = strpos($value, '[') + 1, strrpos($value, ' ') - $pos)));
							$fields[$var . '_bw_raw'] = (int)Formatter::changeBytes(str_replace(' ', '',
								substr($value, 0, strpos($value, '/'))));
							break;
						case 'block_size':
							$value = (int)Formatter::changeBytes($value, 'B');
							break;
						case 'read':
						case 'write':
							$fields[$var . '_bw_raw'] = (int)Formatter::changeBytes(
								str_replace(' ', '', substr($value, 0, strpos($value, '/')))
							);

							$fields[$var . '_iops_raw'] = (float)strtok(substr(
								$value,
								$pos = strpos($value, ',')+1
							), ' ');

							break;
						case 'data_collected':
							// via libmonit/src/system/Time.h, but ignores TZ. Use system default
							$value = DateTime::createFromFormat('D, d M Y H:i:s', $value,
								new DateTimeZone(TIMEZONE))->getTimestamp();
							break;
					}
					$fields[$var] = $value;
					$fields['failed'] = $fields['status'] !== 'OK';
					$fields['monitored'] = 0 !== strcasecmp($fields['status'], 'not monitored');
				}

				$services[$processName] = $fields;
			}

			return $services;
		}

		public function config(string $backend, ?array $newparams)
		{
			deprecated_func('use config_relay');
			return $this->config_relay($backend, $newparams);
		}

		/**
		 * Set configuration relay backend for Argos
		 *
		 * @param string     $backend   backend name
		 * @param array|null $newparams parameters to apply, null to delete backend
		 * @return bool|array
		 */
		public function config_relay(string $backend, ?array $newparams)
		{
			if (!IS_CLI) {
				return $this->query('argos_config_relay', $backend, $newparams);
			}

			if (!\in_array($backend, \Opcenter\Argos\Config::get()->getBackends(), true)) {
				return error("Unknown backend `%s'", $backend);
			}

			// reset backend with null
			if (null === $newparams) {
				$provider = array_get(\Opcenter\Argos\Config::get()->backend($backend), 'backend', $backend);
				if (!\Opcenter\Argos\Config::get()->deleteBackend($backend)) {
					warn("Failed to delete backend `%s'", $backend);
				}
				if (!\Opcenter\Argos\Config::get()->createBackend($provider, $backend)) {
					return error("Failed to create backend `%s'", $backend);
				}

				return true;
			}

			$cfg = \Opcenter\Argos\Config::get();
			$backend = $cfg->backend($backend);
			// writing backend vars
			foreach ($newparams as $k => $v) {
				$backend[$k] = $v;
			}

			return true;
		}

		/**
		 * Get Argos configuration relay
		 *
		 * @param string $backend
		 * @param null   $param
		 * @return array|null
		 * @throws ReflectionException
		 */
		public function get_config_relay(string $backend, $param = null): ?array
		{
			if (!IS_CLI) {
				return $this->query('argos_get_config_relay', $backend);
			}

			if (!\in_array($backend, \Opcenter\Argos\Config::get()->getBackends(), true)) {
				error("Unknown backend `%s'", $backend);
				return null;
			}

			// reading backend vars
			$cfg = \Opcenter\Argos\Config::get()->backend($backend)->toArray();

			return $param ? array_get($cfg, $param, null) : $cfg;
		}

		/**
		 * Set default backend
		 *
		 * @param $backend
		 * @return bool
		 */
		public function set_default_relay($backend)
		{
			if (!IS_CLI) {
				return $this->query('argos_set_default_relay', $backend);
			}
			$backends = $this->get_backends();
			foreach ((array)$backend as $b) {
				if (!\in_array($b, $backends, true)) {
					return error("Invalid backend `%s'", $b);
				}
			}

			return \Opcenter\Argos\Config::get()->setDefault($backend);
		}

		/**
		 * Get configured Argos backends
		 *
		 * @return array|null
		 */
		public function get_backends(): ?array
		{
			if (!IS_CLI) {
				return $this->query('argos_get_backends');
			}

			if (!($cfg = \Opcenter\Argos\Config::get())) {
				return null;
			}

			return $cfg->getBackends();
		}

		/**
		 * Create a new backend
		 *
		 * @param string $name
		 * @param string $driver
		 * @return bool
		 */
		public function create_backend(string $name, string $driver): bool
		{
			if (!IS_CLI) {
				return $this->query('argos_create_backend', $name, $driver);
			}
			if (\in_array($name, $this->get_backends(), true)) {
				return error("Backend `%s' already exists", $name);
			}
			if (!\in_array($driver, $this->get_backend_relays(), true)) {
				return error("Invalid backend relay `%s'. Use get_backend_relays() to view all", $driver);
			}

			$conf = \Opcenter\Argos\Config::get();
			$conf->createBackend($driver, $name);
			$conf->sync();

			return true;
		}

		/**
		 * Get relays for backend
		 *
		 * @return array
		 */
		public function get_backend_relays(): array
		{
			if (!IS_CLI) {
				return $this->query('argos_get_backend_relays');
			}

			return \Opcenter\Argos\Backend::getBackends();
		}

		/**
		 * Test Argos configuration
		 *
		 * @param string $backend
		 * @return mixed
		 */
		public function test(string $backend = null)
		{
			return $this->send('Argos test alert', $backend, '💯 test');
		}

		/**
		 * Relay a message through Argos
		 *
		 * @param string      $msg
		 * @param string      $backend
		 * @param string|null $title
		 * @return mixed
		 */
		public function send(string $msg, string $backend = null, string $title = null)
		{
			if (!IS_CLI) {
				return $this->query('argos_send', $msg, $backend, $title);
			}

			if (!file_exists(\Opcenter\Argos\Config::CONFIGURATION_FILE)) {
				return error(
					"%s is missing - run argos.init Scope first. See Monitoring.md in docs/",
					\Opcenter\Argos\Config::CONFIGURATION_FILE
				);
			}
			if ($title) {
				$title = '-t ' . escapeshellarg($title);
			}
			if ($backend) {
				$backend = '-b ' . escapeshellarg($backend);
			}

			return array_get(
				\Util_Process_Safe::exec('ntfy -c %(config)s ' . $title . ' ' . $backend . ' send %(msg)s',
					[
						'config' => \Opcenter\Argos\Config::CONFIGURATION_FILE,
						'msg'    => $msg,
					]
				),
				'success',
				false
			);
		}

		/**
		 * Argos monitor active
		 *
		 * @return bool
		 */
		public function active(): bool
		{
			return array_get(\Util_Process::exec('systemctl is-active monit', null, [0, 3]), 'return', null) === 0;
		}

		/**
		 * Reset monitoring
		 *
		 * @return bool
		 */
		public function reset_all(): bool
		{
			$ret = \Util_Process_Safe::exec('monit monitor all', [0]);
			return $ret['success'] ?: error('Failed to reset monitoring services: %s ', $ret['stdout']);
		}

		public function _housekeeping()
		{
			$this->reset_all();
		}

	}
