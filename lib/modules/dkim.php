<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

	use Opcenter\Dns\Bulk;
	use Opcenter\Dns\Record;
	use Opcenter\Mail\Services\Rspamd;
	use Opcenter\Mail\Services\Rspamd\Dkim;

	/**
	 * DKIM signing
	 *
	 * @package core
	 */
	class Dkim_Module extends Module_Skeleton
	{
		protected $exportedFunctions = [
			'roll'    => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'has'     => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'key'     => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'expire'  => PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'selector'=> PRIVILEGE_SITE | PRIVILEGE_ADMIN,
			'enable'  => PRIVILEGE_SITE,
			'disable' => PRIVILEGE_SITE,

		];

		/**
		 * Get DKIM key for site
		 *
		 * @return string|null
		 */
		public function key(): ?string
		{
			if (!IS_CLI) {
				return $this->query('dkim_key');
			}

			if (!$this->has()) {
				return null;
			}

			return Dkim::instantiateContexted($this->getAuthContext())->publicKey();
		}

		/**
		 * Disable DKIM signing for account
		 *
		 * @return bool
		 */
		public function disable(): bool
		{
			if (!IS_CLI) {
				return $this->query('dkim_disable');
			}

			if (!$this->dns_configured()) {
				return error("Cannot automatically manage DKIM on DNS-less site");
			}

			if (!$this->has()) {
				return error("DKIM not enabled");
			}

			$dkim = Dkim::instantiateContexted($this->getAuthContext());
			// bulk change DNS
			$r = new Record('_bogus-domain.com', [
				'name' => $dkim->selectorRecord(),
				'rr'   => 'TXT'
			]);
			(new Bulk([$this->site]))->remove($r, function (\apnscpFunctionInterceptor $afi, Record $r) {
				return $afi->email_transport_exists($r->getZone());
			});

			return true;
		}

		/**
		 * Enable DKIM signing for account
		 *
		 * @return bool
		 */
		public function enable(): bool
		{
			if (!IS_CLI) {
				return $this->query('dkim_enable');
			}

			if (!$this->dns_configured()) {
				return error("Cannot automatically manage DKIM on DNS-less site");
			}

			if (!$this->has()) {
				return error("DKIM not enabled");
			}

			$dkim = Dkim::instantiateContexted($this->getAuthContext());
			// bulk change DNS
			$r = new Record('_bogus-domain.com', [
				'name'      => $dkim->selectorRecord(),
				'rr'        => 'TXT',
				'parameter' => $dkim->dkimRecord()
			]);
			(new Bulk([$this->site]))->add($r, function (\apnscpFunctionInterceptor $afi, Record $r) {
				return $afi->email_transport_exists($r->getZone());
			});

			return true;
		}

		/**
		 * Get active selector name
		 *
		 * @return string|null
		 */
		public function selector(): ?string
		{
			if (!IS_CLI) {
				return $this->query('dkim_selector');
			}

			if (!Rspamd::present()) {
				return null;
			}

			return Dkim::instantiateContexted($this->getAuthContext())->selector();
		}

		/**
		 * Expire disused DKIM selector
		 *
		 * @param string $selector
		 * @return bool
		 */
		public function expire(string $selector): bool
		{
			if (!IS_CLI && posix_getuid()) {
				return $this->query('dkim_expire', $selector);
			}

			if (!$this->has()) {
				return error("DKIM not enabled");
			}

			if (Dkim::singleKey() && !$this->permission_level & PRIVILEGE_ADMIN) {
				return error("Site owners cannot expire global DKIM key");
			}

			return Dkim::instantiateContexted($this->getAuthContext())->expireSelector($selector);
		}

		/**
		 * Account has DKIM key
		 *
		 * @return bool
		 */
		public function has(): bool
		{
			if (!IS_CLI) {
				return $this->query('dkim_has');
			}

			if (($this->permission_level & PRIVILEGE_SITE) && $this->email_get_provider() !== 'builtin') {
				return false;
			}

			return file_exists(Dkim::globalKeyPath($this->selector()));
		}

		/**
		 * Rotate DKIM key
		 *
		 * @param string|null $into optional selector to roll into
		 * @return bool
		 */
		public function roll(string $into = null): bool
		{
			if (!IS_CLI && posix_getuid()) {
				return $this->query('dkim_roll', $into);
			}

			if (!$this->has()) {
				return warn("DKIM not configured");
			}

			if (Dkim::singleKey() && !$this->permission_level & PRIVILEGE_ADMIN) {
				return error("Site owners cannot roll global DKIM key");
			}

			return Dkim::instantiateContexted($this->getAuthContext())->rollKey($into);
		}
	}
