<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	include_once('tar.php');

	/**
	 * Provides gzip compression/decompression routines in the file manager
	 *
	 * @package Compression
	 */
	class Gzip_Filter extends Tar_Filter
	{
		public static function extract_files($archive, $destination, array $files = null, array $opts = null)
		{
			$tar = (substr($archive, -4) == '.tgz' ||
				substr($archive, -7) == '.tar.gz');
			$cmd = 'gzip ' . (!$tar ? '-d' : '-d -c') . ' %s';
			$proc = parent::exec(
				$cmd,
				$archive,
				array(0),
				array('run' => !$tar)
			);

			if ($tar) {
				return self::extract_files_pipe($archive, $destination, $files, $opts);
			}
			if ($proc['success']) {
				self::$fc->file_delete($archive);
			}

			return $proc;
		}

		public static function list_files($archive, array $opts = null)
		{
			$tar = substr($archive, -4) == '.tgz' ||
				substr($archive, -7) == '.tar.gz';
			$cmd = 'gzip ' . (!$tar ? '-v -l' : '-d -c') . ' %s';
			$proc = parent::exec(
				$cmd,
				$archive,
				array(0),
				array('run' => !$tar)
			);
			if ($tar) {
				return self::list_files_pipe($archive, $opts);
			}


			$files = array();
			foreach (explode("\n", $proc['output']) as $line) {
				if (!preg_match(Regex::FILE_HDR_GZIP, $line, $entry)) {
					continue;
				}

				$files[] = array(
					'file_name'   => basename($entry['name']),
					'file_type'   => 'file',
					'can_read'    => true,
					'can_write'   => true,
					'can_execute' => true,
					'size'        => $entry['size'],
					'packed_size' => $entry['csize'],
					'crc'         => $entry['crc'],
					'link'        => 0,
					'date'        => strtotime($entry['ts'])
				);
			}

			return $files;

		}

	}

?>
