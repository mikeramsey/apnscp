<?php

	use Illuminate\Routing\Controller as BaseController;
	use Illuminate\Support\Facades\Route;
	use Symfony\Component\Yaml\Yaml;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Bindless_Proxy
	{
		private static $instance;
		private $apnscpFunctionInterceptor;

		private function __construct(\Auth_Info_User $ctx)
		{
			$this->apnscpFunctionInterceptor = apnscpFunctionInterceptor::factory($ctx);
		}

		public static function getInstance(\Auth_Info_User $ctx = null): self
		{
			if (!$ctx && !isset(self::$instance)) {
				self::$instance = new Bindless_Proxy(\Auth::profile());
				return self::$instance;
			}

			return new Bindless_Proxy($ctx);

		}

		public function __call($func, $args)
		{
			return $this->apnscpFunctionInterceptor->call($func, $args);
		}
	}

	class Page_Container extends BaseController
	{

		use apnscpFunctionInterceptorTrait {
			__call as afiCall;
		}
		use ContextableTrait;

		const FAILED_MSG = 'Action failed';
		const SUCCEEDED_MSG = 'Action succeeded';
		const MISC_MSG = 'Action succeeded with notes';
		const WARNING_MSG = 'Action succeeded with warnings';
		/**
		 * @var \BladeLite
		 */
		protected static $view;

		private static $page;
		public $is_postback;
		/**
		 * @var Template_Engine
		 */
		protected $templateClass;
		// instance of self
		protected $apnscpFunctionInterceptor;
		protected $help_expanded;

		// pb executed
		protected $body_classes = [];
		protected $msg_buffer = array();
		protected $hookCss = array();
		private $postback_params;
		private $title;
		private $app_id;
		private $ucard;
		protected $pb_flushed = true;
		private $severity = Error_Reporter::E_OK;
		private $callbacks;
		private $do_bind = false;
		private $theme;
		/**
		 * @var array vars defined in help
		 */
		private $pageVars = [];

		// @var apnscpFunctionInterceptor
		// @deprecated
		protected $afi;

		// Additional CSS resources
		private $css = array(
			'internal' => array(),
			'external' => array()
		);

		// Additional JavaScript scripts
		private $js = array(
			'external'         => array(),
			'external-preload' => array(),
			'internal'         => array(
				/**
				 * loaded before DOM is processed
				 * $(document).ready()
				 */
				0 => array(
					0 => array(),  // inserted at end of body
					1 => array()
				), // inserted in head
				1 => array(
					0 => array(),  // inserted at end of body
					1 => array()
				), // inserted in head
			)
		);

		// additional headers, e.g. inline CSS
		private $head = array();
		private $src_path;

		public function __construct($doPostback = true)
		{

			// deprecation wrapper for now...
			// @XXX remove by 3.2
			$this->afi = new class($this) {
				protected $forwarded;

				public function __construct(\Page_Container $parent) {
					$this->forwarded = $parent;
				}

				public function __call($func, $args) {
					deprecated_func("Use 'apnscpFunctionInterceptor' property instead of 'afi'");
					return $this->forwarded->__call($func, $args);
				}
			};

			if ($doPostback) {
				$driver = Auth::get_driver();
				if (method_exists($driver, 'restore_postback')) {
					$driver->restore_postback();
				}
			}
			$this->_checkAJAX();
			$this->_init();

			if (\Session::has('pb-pending')) {
				\Error_Reporter::merge_buffer(\Session::get('pb-pending'));
				$this->_postbackNotifier();
				\Session::forget('pb-pending');
			}
		}

		/**
		 * Stick application in AJAX mode if necessary
		 */
		private function _checkAJAX()
		{
			if (defined('AJAX')) {
				return AJAX;
			}

			if (HTML_Kit::jsonify()) {
				Error_Reporter::set_verbose(0);
				define('AJAX', true);
			}

			return true;
		}

		protected function _init()
		{
			$this->setApnscpFunctionInterceptor(apnscpFunctionInterceptor::factory($this->getAuthContext()));
			$this->detectApp();

			$viewBase = dirname($this->src_path) . '/views';
			$paths = [
				'index'
			];
			$requesturi = strtok($_SERVER['REQUEST_URI'], '?');
			if (basename($requesturi) !== $this->getApplicationID()) {
				$prefix = Template_Engine::BASE_DIR . '/' . $this->getApplicationID();
				array_unshift($paths, explode('/', substr($requesturi, strlen($prefix) + 1), 2)[0]);
			}

			foreach ($paths as $p) {
				if (file_exists($viewBase . '/' . $p . '.blade.php')) {
					$this->loadBlade($p);
					$this->src_path = $p . '.blade.php';
					break;
				}
			}

			if ($this->isAjax()) {
				return;
			}

			if (!\Auth_UI::public_page()) {
				if (\Preferences::get(\Page_Renderer::THEME_SWAP_BUTTONS)) {
					$this->addBodyClass('ui-swap-buttons');
				}
				$this->addBodyClass('role-' . \UCard::get()->getRoleAsString());
			}

			$this->addBodyClass(self::browser_classes());
		}

		/**
		 * Detect requested application
		 *
		 */
		protected function detectApp()
		{
			if (empty($_SERVER['REQUEST_URI'])) {
				return null;
			}
			$prefix = Template_Engine::BASE_DIR . '/';
			$requesturi = strtok($_SERVER['REQUEST_URI'], '?');
			if (0 === strpos($requesturi, $prefix)) {
				if (false !== ($len = strpos($requesturi, '/', \strlen($prefix)))) {
					$len -= strlen($prefix);
				}
				$requesturi = substr(
					$requesturi,
					strlen(Template_Engine::BASE_DIR . '/'),
					$len ?: 1024
				);

			}
			$appid = basename($requesturi, '.php');
			$this->app_id = $appid;
			$this->src_path = self::resolve($appid);

			if (null === $this->src_path && !\Auth::authenticated()) {
				// spam bots screwing around probing, e.g. /apps/login/wp-login.php
				$this->src_path = self::resolve('login');
			}
		}

		/**
		 * Resolve an application to its file path
		 *
		 * @param string|null $appid
		 * @param string|null $file optional file to resolve
		 * @return string|null
		 */
		public static function resolve(?string $appid, string $file = null): ?string
		{
			static $cache = [];

			if (!$appid) {
				return null;
			}
			if ($appid !== 'login' && $appid !== 'error' && !\Template_Engine::init()->getApplicationFromId($appid)) {
				return null;
			}

			if (!isset($cache[$appid])) {
				$apppath = \Template_Engine::init()->getPathFromApp($appid);
				$cache[$appid] = $apppath;
			}

			$paths = [
				config_path("custom" . $cache[$appid]),
				INCLUDE_PATH . $cache[$appid]
			];

			$file = $file ?? $appid . '.php';
			foreach ($paths as $path) {
				if (file_exists($path . '/' . $file)) {
					return $path . '/' . $file;
				}
			}

			return null;
		}

		protected function isAjax()
		{
			return defined('AJAX') && AJAX == 1;
		}

		public function getApplicationID()
		{
			return $this->app_id;
		}

		protected function loadBlade(string $template = 'index', string $path = null): BladeLite
		{
			if (!$path) {
				$apppath = \Template_Engine::init()->getPathFromApp($this->app_id);

				$path = [
					INCLUDE_PATH . $apppath . '/views'
				];

				// allow overrides in config/custom/apps/<name>
				// @TODO refactor this + resolve()
				$tmp = config_path("custom${apppath}/views");
				if (false === strpos($tmp, $path[0])) {
					array_unshift($path, $tmp);
				}
			} else if (!file_exists("${path}/${template}.blade.php")) {
				fatal("template `%s' does not exist in `%s'", $template, $path);
			}

			self::$view = [
				'instance' => \BladeLite::factory($path),
				'origin'   => $template
			];

			return self::$view['instance'];
		}

		public function addBodyClass($class)
		{
			$this->body_classes[] = $class;
		}

		public static function browser_classes()
		{
			$bc = \Session::get('browser_classes');
			if ($bc) {
				return $bc;
			}
			if (!array_get($_SERVER, 'HTTP_USER_AGENT')) {
				return '';
			}
			$meta = \HTML_Kit::get_browser();
			$css = $browser = '';
			if (!empty($meta->browser)) {
				$tmp = strtolower($meta->browser);

				if ($tmp === 'firefox' || $tmp === 'flock' || $tmp === 'mozilla') {
					$css .= 'ff';
				} else {
					$css .= $tmp;
				}
				if (isset($meta->majorver)) {
					$css .= ' ' . $css . $meta->majorver;
				}
			}
			\Session::set('browser_classes', $css);

			return $css;
		}

		private function _postbackNotifier()
		{
			$this->pb_flushed = true;
		}

		public static function get(): Page_Container
		{
			return self::init();
		}

		/**
		 * Kernel name from app
		 *
		 * @param string $app
		 * @return string
		 */
		public static function kernelFromApp(string $app): string
		{
			return '\\apps\\' . str_replace('-', '_', $app) . '\\Page';
		}

		/**
		 * Initialize application
		 *
		 * @param string $kernel named kernel
		 * @param array  $ctor   constructor arguments
		 * @return Page_Container
		 */
		public static function init(string $kernel = 'Page', array $ctor = []): Page_Container
		{
			if (null !== self::$page && $kernel === 'Page') {
				return self::$page;
			}

			/** @var Page $kernel */
			$page = $kernel::instantiateContexted(\Auth::profile(), $ctor);

			if (null === self::$page) {
				// set first load
				self::$page = $page;
			}

			return $page;
		}

		/**
		 * Get lite instance
		 *
		 * @return Page_Container
		 */
		public static function headless(): Page_Container
		{
			if (null === self::$page) {
				self::$page = new Page();
			}

			return self::$page;
		}

		public function appHelp()
		{
			if (!isset($this->XML)) {
				$this->XML = $this->loadHelpSpec($this->getApplicationID());
				if (\Session::exists('first.' . $this->app_id)) {
					Page_Renderer::show_full_help();
					$key = \Frontend\Tour::KEY . '.' . $this->getApplicationID();
					if (!\Auth::profile()->getImpersonator() &&
						!array_get(Preferences::factory($this->getAuthContext()), $key))
					{
						Page_Renderer::show_tutorial();
					}
				}
				if (!empty($this->XML['title'])) {
					$this->title = $this->XML['title'];
				}
			}

			return $this->XML;
		}

		private function loadHelpSpec($app_id)
		{
			// initialize and set our help.  Should be migrated to its own function
			$c = Cache_Global::spawn();
			$xml = $c->get('spec:' . $app_id);
			if (!is_debug() && $xml) {
				return \Util_PHP::unserialize($xml);
			}

			foreach(['yml', 'spec'] as $ext) {
				$src_xml = self::resolve($app_id, 'application.' . $ext);
				if ($src_xml) {
					break;
				}
			}
			if (!$src_xml) {
				return null;
			}

			$vars = [];

			if ($ext === 'yml') {
				$xml = Yaml::parseFile($src_xml);
				$compiler = function ($values) use (&$compiler) {
					$vars = [];
					foreach ((array)$values as $k => $v) {
						if (is_array($v)) {
							$vars[$k] = $compiler($v);
						} else {
							$vars[$k] = $this->view()->compiler()->compileString($v);
						}
					}

					return is_array($values) ? $vars : array_pop($vars);
				};
				foreach ($xml as $k => $v) {
					$xml[$k] = $compiler($v);
				}
			} else {
				$xml = $this->parse_spec_xml($src_xml);
			}
			$this->pageVars = array_get($xml, 'vars', []);

			$c->add('spec:' . $app_id, serialize($xml), 86400);

			if (!$xml || !isset($xml['help'], $xml['tagline'])) {
				return null;
			}

			return $xml;
		}

		/**
		 * @until 3.3
		 */
		private function parse_spec_xml($spec)
		{
			$spec = file_get_contents($spec);
			$spec = preg_replace('/&([a-z][0-9a-z]*);/', '&amp;\1;', $spec);
			$xml = simplexml_load_string($spec);
			if (!$xml) {
				return null;
			}
			$parsed = array('help' => '', 'tagline' => '', 'tooltips' => '');

			$help = trim(
				preg_replace(
					array(
						'/<help[^>]*>/',
						'/<\/help>/',
						'/&amp;/'
					),
					array('', '', '&'),
					$xml->help->asXML()
				)
			);
			$tooltips = array();
			if ($xml->tagline) {
				$parsed['tagline'] = str_replace('&amp;', '', trim($xml->tagline));
			}

			if (!$help) {
				return $parsed;
			}
			if (isset($xml->help_tooltip)) {
				$i = 0;
				$page = basename(HTML_Kit::page_url());
				foreach ($xml->help_tooltip->children() as $helpKey => $helpItem) {
					$attr = $xml->help_tooltip->$helpKey->attributes();
					$token = $page . '-' . $i;
					if (isset($attr->id)) {
						$token = trim($attr->id);
					}
					$help = str_replace(
						array('<' . $helpKey . '>', '</' . $helpKey . '>'),
						array(
							'<a href="/ajax?engine=tip&amp;app=' . $this->app_id . '&amp;tip=' . $token . '" id="' . $token . '" class="ui-tooltip">',
							'</a>'
						),
						$help
					);
					Tip_Engine::add(
						$token,
						array(
							'title' => trim($attr->title),
							'body'  => trim(preg_replace(array(
								'/<' . $helpKey . '[^>]*>/',
								'!</' . $helpKey . '>!'
							),
								'',
								$xml->help_tooltip->$helpKey->asXML()))
						),
						$this->getApplicationID()
					);
					$tooltips[$helpKey] = $token;
					$i++;
				}

			}
			$parsed['help'] = (string)$help;
			$parsed['tooltips'] = count($tooltips) > 0;

			return $parsed;
		}

		public function __destruct()
		{
			if (class_exists('Stats_Logger')) {
				Stats_Logger::commit();
			}
			if (!$this->pb_flushed) {
				// make sure postback messages are in msg_buffer
				\Session::set('pb-pending', \Error_Reporter::flush_buffer());
			}
		}

		public function getHelpTagline($app = null)
		{
			if (is_null($app) && isset($this->XML['tagline'])) {
				return $this->XML['tagline'];
			} else if ($app) {
				$xml = $this->loadHelpSpec($app);
				if ($xml && isset($xml['tagline'])) {
					return $xml['tagline'];
				}
			}

			return '';
		}

		public function get_pb_icon()
		{
			$severity = $this->get_severity();
			switch ($severity) {
				case Error_Reporter::E_OK:
					return 'ui-pb-msg-success';
				case Error_Reporter::E_ERROR:
					return 'ui-pb-msg-failed';
				case Error_Reporter::E_WARNING:
					return 'ui-pb-msg-warning';
				case Error_Reporter::E_INFO:
					return 'ui-pb-msg-info';
				case Error_Reporter::E_DEBUG:
					return 'ui-pb-msg-debug';
				default:
					warn('unknown pb status `' . $severity . "'");
			}
		}

		public function get_severity()
		{
			return \Error_Reporter::get_severity();
		}

		public function __call($function, $args = array())
		{
			$response = $this->afiCall($function, $args);

			return $this->__bind($response);
		}

		private function __bind(&$status, $hide = false)
		{
			// control backtrace js fade
			static $bt_fade;
			if (!$this->do_bind) {
				return $status;
			}

			if (!isset($this->severity)) {
				$this->severity = Error_Reporter::E_OK;
			}
			if ($status instanceof Exception) {
				error($status->getMessage());
			} else {
				if ($status === false) {
					// to-do check for error messages?
					if (is_debug() && !$this->_checkAJAX()) {
						if (!isset($bt_fade)) {
							$this->add_javascript('$(".backtrace").click(function() { $(this).fadeOut(); });',
								'internal');
							$bt_fade = true;
						}
						print '<code><pre class="backtrace">';
						debug_print_backtrace();
						print '</pre></code> <br />';
					}
				} else {
					if (is_array($status) && isset($status['stderr'])) {
						if ($status['stderr'] instanceof Exception) {
							error($status['stderr']->getMessage());
						} else {
							if ($status['return'] != 0 && $status['stderr']) {
								warn($status['stderr']);
							}
						}
						if (!$hide && trim($status['output'])) {
							info($status['output']);
						}
						$status = $status['output'];
					}
				}
			}
			$this->severity = max($this->severity, Error_Reporter::get_severity());

			return $status;
		}

		public function add_javascript($scripts, $type = 'external', $dom_ready = true, $head = false)
		{
			// don't load html resources for AJAX
			if ($this->isAjax() || $this->subRequest()) {
				return false;
			}
			if ($type === 'internal') {
				return $this->js[$type][$dom_ready][$head][] = $scripts;
			}

			return $this->add_script($scripts, $head);
		}

		// @xxx

		public function subRequest()
		{
			return false;
		}

		public function add_script($scripts, $head = false)
		{
			// don't load html resources for AJAX
			if ($this->isAjax()) {
				return false;
			}
			$scripts = (array)$scripts;
			foreach ($scripts as $script) {
				if ($script[0] !== '/' && 0 !== strpos($script, 'http') && ($script[7] ?? null) !== '/') {
					$script = $this->getAppUrlDirectory() . '/' . $script;
				}
				if (isset($this->js['external-preload'][$script])
					|| isset($this->js['external'][$script])
				) {
					continue;
				}

				$url = $this->try_minified($script, 'js');

				$loc = 'external';
				if ($head) {
					$loc .= '-preload';
				}
				$this->js[$loc][$script] = $url . '?' . self::_scriptCacheMTime($script);
			}
		}

		public function getAppUrlDirectory()
		{
			return Template_Engine::BASE_DIR . '/' . $this->app_id;
		}

		protected function try_minified($file, $ext)
		{
			if (is_debug()) {
				return $file;
			}

			if (substr($file, -5 - strlen($ext)) === '.min.' . $ext) {
				return $file;
			}

			$newurl = substr($file, 0, -1 - strlen($ext)) . '.min.' . $ext;
			if (file_exists(public_path($newurl))) {
				return $newurl;
			}

			return $file;
		}

		private static function _scriptCacheMTime($script)
		{
			if (is_debug()) {
				return '';
			}
			$docroot = INCLUDE_PATH;
			// @todo sideloading problem
			if (file_exists($docroot . '/public/' . $script)) {
				return filemtime($docroot . '/public/' . $script);
			} else if (file_exists($docroot . $script)) {
				return filemtime($docroot . $script);
			}

			return 1;

		}

		public function hide_pb()
		{
			Page_Renderer::hide_postback_notifier();
		}

		/**
		 * Render Laravel application
		 */
		public function render() {
			if ($this->isAjax()) {
				return;
			}
			$blade = BladeLite::factory();

			foreach ([$this->theme, 'layout'] as $layout) {
				if (!$blade->exists($layout)) {
					continue;
				}
				echo $blade->render($layout, $this->getRenderVars());
				break;
			}
		}

		/**
		 * Base rendering variables
		 *
		 * @return array
		 */
		protected function getRenderVars(): array
		{
			return [
				'Page' => $this,
				'vars' => ($this->appHelp()['vars']) ?? []
			];
		}

		public function var(string $name, $default = null)
		{
			return $this->pageVars[$name] ?? $default;
		}

		/**
		 * Display compiled application
		 *
		 * @return \Illuminate\View\View|string|string[]|null
		 */

		public function display_application()
		{
			if (\Page_Renderer::get_options() & \Page_Renderer::HIDE_BUILTIN_RENDERER) {
				return null;
			}
			if ($this->isAjax()) {
				return null;
			} else if (!$this->is_postback &&
				($this->get_severity() & Error_Reporter::E_ERROR) &&
				!Page_Renderer::do_display()
			) {
				return null;
			} else if (null !== self::$view) {
				// Laravel Blade
				return $this->compose(self::$view);
			}

			return $this->getSubPage();
		}

		public function compose(array $view, array $params = []): \Illuminate\View\View
		{
			// @todo update to class
			return $view['instance']->make($view['origin'], array_replace($params, $this->getRenderVars()));
		}

		public function getSubPage()
		{
			return substr_replace($this->src_path, 'tpl', -3);
		}

		/**
		 * Get view instance
		 *
		 * @return \BladeLite
		 */
		public function view(): \BladeLite
		{
			static $view;

			if (!isset($view)) {
				$view = self::$view['instance'] ?? \BladeLite::factory();
			}

			return $view;
		}

		public function getLinks()
		{
			return $this->templateClass->getLinks();
		}

		public function getStatusMessage()
		{
			switch ($this->get_severity()) {
				case Error_Reporter::E_OK:
					return self::SUCCEEDED_MSG;
				case Error_Reporter::E_ERROR:
					return self::FAILED_MSG;
				case Error_Reporter::E_INFO:
					return self::MISC_MSG;
				default:
					return self::WARNING_MSG;

			}
		}

		public function help_exists()
		{
			return isset($this->XML);
		}

		public function purgeMessages()
		{
			$this->msg_buffer = array();
		}

		public function getMessages($class = null)
		{
			return \Error_Reporter::get_buffer($class);
		}

		public function bind(&$status, $hide = false)
		{
			$bind = $this->do_bind;
			$this->do_bind = true;
			$ret = $this->__bind($status, $hide);
			$this->do_bind = $bind;

			return $ret;
		}

		final public function setHelp($mTxt)
		{
			$this->help_expanded = $mTxt;
		}

		final public function getHelp()
		{
			return $this->XML['help'];
		}

		public function printMenu()
		{
			return $this->templateClass->print_quickmenu();
		}

		public function getApplicationTitle()
		{
			return $this->title ?: $this->templateClass->getActivePageName();
		}

		public function get_javascript($type = 'external', $dom_ready = true, $head = true)
		{
			$scripts = array();
			if ($type == 'external-preload' || $type === 'external') {
				return array_values($this->js[$type]);
			}

			return implode('', $this->js['internal'][$dom_ready][$head]);
		}

		public function get_css($type = 'external')
		{
			if ($type == 'internal') {
				return $this->css['internal'];
			}

			return array_values($this->css['external']);
		}

		public function getUser()
		{
			return \Session::get('username');
		}

		public function getDomain()
		{
			return \Session::get('domain');
		}

		public function handle_request()
		{
			if (!isset($this->is_postback)) {
				$this->is_postback = sizeof($_POST) > 0 || sizeof($_GET) > 0;
			}
			if (!isset($this->src_path)) {
				// @TODO add warning if page has constructor
				$this->_init();
			}

			$routes = self::resolve($this->getApplicationID(), 'routes.php');

			if ($routes) {
				// Boot into Laravel router
				// Needs cleanup!
				$app = Lararia\Bootstrapper::minstrap();
				$namespace = (new \ReflectionClass($this))->getNamespaceName();
				$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

				// prevent building additional copies
				$app->instance(static::class, $this);
				class_alias(static::class, 'Page');
				Route::group([
					'prefix'    => $this->getAppUrlDirectory(),
					'namespace' => '\\' . $namespace
				], static function () use ($routes) {
					include($routes);
				});

				$app->bind('view', function($app) {
					$this->_layout();
					if ($this->isAjax()) {
						// shortcircuit additional UI processing on AJAX
						return self::$view['instance'];
					}
					$blade = self::$view['instance']->getContainer()->get('view');

					foreach ([$this->theme, 'layout'] as $layout) {
						if (!$blade->exists("theme::${layout}")) {
							continue;
						}
						$blade->addNamespace('theme::layout', [
							config_path('custom/resources/views'),
							resource_path('views')
						]);
						break;
					}

					$blade->addNamespace('master', [
						config_path('custom/resources/views'),
						resource_path('views')
					]);

					self::$view['instance']->
						getContainer()->get('view')->share($this->getRenderVars());
					return self::$view['instance'];
				});

				$this->templateClass = Template_Engine::init();
				$this->_postbackNotifier();

				$response = $kernel->handle(
					$request = Illuminate\Http\Request::capture()
				);
				$response->send();
				if (method_exists($this, '_render')) {
					$this->_render();
				}
				$kernel->terminate($request, $response);
				return;
			} // end Laravel route boot

			if ($this->isDegraded() || $this->errors_exist() || $this->is_postback) {
				if ($this->is_postback) {
					$this->pb_flushed = false;
					if (method_exists($this, 'on_postback')) {
						$this->_processPostback();
					}
				} else if ($this->isDegraded() && Page_Renderer::do_notifier()) {
					// no postback, but error messages are present
					$this->_postbackNotifier();
				}
			}
			$this->_postbackNotifier();
			if (!$routes) {
				$this->_layout();
			}
			// process layout after postback
			// there should be a better way around this, as even this code would make
			// baby jesus cry
			if (!NO_AUTH) {
				$this->templateClass = Template_Engine::init();
			}
			if (method_exists($this, '_render')) {
				$this->_render();
			}
			$this->render();

		}

		public function isDegraded()
		{
			$severity = $this->get_severity();

			return $severity >= Error_Reporter::E_WARNING;
		}

		public function errors_exist()
		{
			$severity = $this->get_severity();

			return $severity == Error_Reporter::E_ERROR;
		}

		private function _processPostback()
		{
			if (!$this->validateCsrf()) {
				return error('CSRF check failed. Login required.');
			}

			$this->callbacks = array();
			$this->postback_params = array_merge($_GET, $_POST);
			$afi = $this->apnscpFunctionInterceptor;
			if (!NO_AUTH && class_exists('afiProxy', false)) {
				// don't log anonymous stats, look for explicit inclusion of afiProxy
				$this->setApnscpFunctionInterceptor(afiProxy::factory($this->getAuthContext()));
			}

			$this->on_postback($this->postback_params);

			foreach ($this->callbacks as $cb) {
				call_user_func_array($cb['func'], $cb['args']);
			}

			$this->setApnscpFunctionInterceptor($afi);
		}

		/**
		 * Validate CSRF
		 * @return bool
		 */
		protected function validateCsrf(): bool {
			$csrf = $_COOKIE[\Auth_UI::CSRF_COOKIE] ??
				$_POST[\Auth_UI::CSRF_KEY] ?? null;
			if (!$csrf) {
				return false;
			}

			return $csrf === \Session::token();
		}

		/**
		 * Simplified postback dispatcher
		 *
		 * - Any called postback handler may return false to terminate enumeration
		 * - All handleXxx handlers are called if the postback var exists
		 * - Order of process is not guaranteed
		 * - Exceptions derived from \Exception are treated as error that terminates enumeration
		 *
		 * @param array $params postback parameter set
		 * @param mixed $extras parameters shifted before $params
		 * @return bool all items enumerated
		 * @throws ReflectionException
		 */
		protected function dispatchPostback(array $params, ...$extras ): bool
		{
			foreach (array_keys($params) as $name) {
				$fn = 'handle' . studly_case($name);
				if (!method_exists($this, $fn) || (new ReflectionMethod($this, $fn))->getDeclaringClass() === self::class) {
					continue;
				}
				try {
					if ($extras) {
						$params = $extras + append_config([$params]);
					} else {
						$params = [$params];
					}
					if (false === $this->$fn(...$params)) {
						return false;
					}
				} catch (\Exception $e) {
					return error($e->getMessage());
				}
			}
			return true;
		}

		protected function _layout()
		{
			$this->theme = \Auth::authenticated() ?
				\Preferences::get(\Page_Renderer::THEME_KEY, STYLE_THEME) : STYLE_THEME;
			$uripath = '/css/themes/' . $this->theme . '.css';
			if (!file_exists(public_path($uripath))) {
				$this->theme = STYLE_THEME;
			}
			$css = array(
				'/css/themes/' . $this->theme . '.css',
			);
			if ($this->hookCss) {
				$css = array_merge($css, $this->hookCss);
			}

			$this->add_css($css);

			$scripts = [
				'/js/apnscp' . (STYLE_OVERRIDE_JS ? '-custom' : '') . '.js',
			];

			$themejs = '/js/themes/' . basename($this->theme) . '.js';
			$themepath = public_path($themejs);
			if (file_exists($themepath)) {
				$scripts[] = $themejs;
			}

			$this->addTourMarkup();
			$this->add_script($scripts, true);
		}

		protected function addTourMarkup(): void
		{
			$tour = (new \Frontend\Tour($this));
			if (!$tour->exists()) {
				return;
			}

			$alwaysShow = array_get(
				\Preferences::factory($this->getAuthContext()),
				\Frontend\Tour::ALWAYS_SHOW,
				false
			);

			if (!$alwaysShow && !\Page_Renderer::do_tutorial()) {
				return;
			}
			$tour->embed();
		}

		public function add_css($css, $type = 'external')
		{
			// don't load html resources for AJAX
			if ($this->isAjax() || $this->subRequest()) {
				return false;
			}
			$css = (array)$css;
			foreach ($css as $c) {
				if ($type === 'internal') {
					return $this->css['internal'][] = $c;
				}
				$prefix = substr($c, 0, 6);
				$isremote = $prefix === 'http:/' || $prefix === 'https:';
				if ($c[0] != '/' && !$isremote) {
					$c = $this->getAppUrlDirectory() . '/' . $c;
				}
				if (isset($this->css['external'][$c])) {
					return false;
				}
				$url = $this->try_minified($c, 'css');
				$this->css['external'][$c] = $url . (!$isremote ? '?' . self::_scriptCacheMTime($c) : '');
			}
		}

		public function pb_succeeded()
		{
			return $this->getStatus() !== Error_Reporter::E_ERROR;
		}

		public function getStatus()
		{
			return $this->get_severity();
		}

		public function add_head($element)
		{
			$this->head[] = $element;
		}

		public function get_head()
		{
			return join('', $this->head);
		}

		public function getGaugeClass($stats, $type)
		{
			$class = 'ui-gauge';
			if (!$stats || !$stats['total']) {
				return $class . '-unknown';
			}
			$threshold = array(0.45, 0.85);
			$ratio = $stats['used'] / $stats['total'];
			if ($ratio < $threshold[0]) {
				return $class . '-normal';
			} else {
				if ($ratio < $threshold[1]) {
					return $class . '-warn';
				} else {
					return $class . '-crit';
				}
			}
		}

		public function bodyClasses()
		{
			return join(' ', $this->body_classes);
		}

		public function getCard()
		{
			if (!isset($this->card)) {
				$this->card = UCard::init();
			}

			return $this->card;
		}

		/**
		 * Simple way of hiding postback notification on simple mode switch
		 *
		 * @param $modes
		 * @return bool
		 */
		protected function isModeSwitch($modes)
		{

			if (count($_POST) != 0 || $this->postbackPending()) {
				return false;
			}
			if (!is_array($modes) && sizeof($_GET) === 1) {
				return isset($_GET[$modes]);
			}
			$modes = (array)$modes;
			if (count($_GET) != count($modes)) {
				return false;
			}

			foreach ($modes as $m) {
				if (!isset($_GET[$m])) {
					return false;
				}
			}

			return true;

		}

		protected function postbackPending()
		{
			return \Session::exists('pb-pending');
		}

		protected function no_bind()
		{
			return $this->unset_bind();
		}

		protected function unset_bind()
		{
			$this->do_bind = false;
		}

		protected function bindless(): Bindless_Proxy
		{
			static $bindInstances = [];
			$id = $this->getAuthContext()->id;
			if (!isset($bindInstances[$id])) {
				$bindInstances[$id] = Bindless_Proxy::getInstance($this->getAuthContext());
			}
			return $bindInstances[$id];
		}

		protected function do_bind()
		{
			return $this->set_bind();
		}

		protected function set_bind()
		{
			$this->do_bind = true;
		}

		protected function setTitle($title)
		{
			$this->title = $title;
		}

		protected function init_js(...$args)
		{
			for ($i = 0, $n = sizeof($args); $i < $n; $i++) {
				foreach (JS_Template::get_template($args[$i]) as $file => $params) {
					switch ($params['type']) {
						case 'js':
							$this->add_javascript($file);
							break;
						case 'css':
							$this->add_css($file);
							break;
					}
				}
			}
		}

		protected function add_callback(callable $cb, $args = array())
		{
			if (!is_array($this->callbacks)) {
				return error('cannot add callback in non-postback mode');
			}

			$this->callbacks[] = array(
				'func' => $cb,
				'args' => array_slice(func_get_args(), 1)
			);

			return true;
		}

		public function getRoleHelperServiceData(): array
		{
			if (isset($_SESSION['roleCache'])) {
				return $_SESSION['roleCache'];
			}

			$_SESSION['roleCache'] = [
				'dns' => [
					'enabled' => \UCard::init()->hasPrivilege('admin') ?
						$this->dns_configured() : $this->dns_enabled(),
					'driver' => $this->dns_get_provider()
				]
			];

			return $_SESSION['roleCache'];
		}
	}
