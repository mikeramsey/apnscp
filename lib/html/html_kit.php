<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class HTML_Kit
	{
		// Template
		private static $Page;
		// Module handler
		private static $Mod_Caller;

		private function __construct()
		{
		}

		public static function init(Page_Container $page, apnscpFunctionInterceptor $function)
		{
			if (!isset(self::$Page)) {
				self::$Page = $page;
			}
			if (!isset(self::$Mod_Caller)) {
				self::$Mod_Caller = $function;
			}

		}

		public static function print_selected($name, $val)
		{
			if (isset($_POST[$name]) && $_POST[$name] == $val ||
				isset($_GET[$name]) && $_GET[$name] == $val
			) {
				return 'SELECTED';
			}
		}

		public static function get_value($name)
		{
			if (isset($_POST[$name])) {
				return $_POST[$name];
			} else {
				if (isset($_GET[$name])) {
					return $_GET[$name];
				}
			}
		}

		/**
		 * Create a specific-specific absolute URL
		 *
		 * @param       $uri
		 * @param array $args
		 * @param bool  $encode
		 * @return string
		 */
		public static function absolute_url(string $uri, array $args = [], bool $encode = true): string
		{
			return 'https://' . SERVER_NAME . ':' . \Auth_Redirect::CP_SSL_PORT . '/' .
				self::new_page_url_params($uri, $args, $encode);
		}

		public static function new_page_url_params($url = null, array $args = array(), $encode = true)
		{
			$query = $params = array();
			if (is_null($url)) {
				$url = self::page_url();
			}
			foreach ($args as $k => $v) {
				if (array_key_exists($k, $args) && is_null($args[$k])) {
					continue;
				}
				if ($v) {
					$tmp = '';
					if (!is_int($k)) {
						$tmp .= ($encode ? urlencode($k) : $k) . '=';
					}
					$query[] = $tmp . ($encode ? urlencode($v) : $v);

				} else {
					$query[] = ($encode ? urlencode($k) : $k);
				}
			}

			return rtrim($url . '?' . join(($encode ? '&amp;' : '&'), $query), '?');
		}

		public static function page_url()
		{
			return $_SERVER['REDIRECT_URL'] ?? strtok($_SERVER['REQUEST_URI'], '?');
		}

		public static function page_safe_url($show_querystr = true)
		{
			if ($show_querystr) {
				$url = self::page_url_params();
			} else {
				$url = self::page_url();
			}

			return urlencode($url);
		}

		public static function jsonify(): bool
		{
			if (!isset($_SERVER['HTTP_ACCEPT'])) {
				return false;
			}
			$accept = $_SERVER['HTTP_ACCEPT'];

			return false !== strpos($accept, 'application/json');
		}

		public static function accesskey(string $char): array {
			$meta = (array)static::get_browser();
			switch (array_get($meta, 'browser', null)) {
				case 'Firefox':
				case 'IE':
					return ['Alt', 'Shift', strtoupper($char)];
				default:
					return ['Alt', strtoupper($char) ];
			}
		}

		public static function page_url_params(array $args = array(), $encode = true)
		{
			$url = self::page_url();
			parse_str($_SERVER['QUERY_STRING'], $params);

			return self::new_page_url_params($url, array_merge($params, $args), $encode);

		}

		public static function states($cc = 'us')
		{
			switch (strtolower($cc)) {
				case 'ca':
					return array(
						'ab' => 'Alberta',
						'bc' => 'British Columbia',
						'mb' => 'Manitoba',
						'nb' => 'New Brunswick',
						'nl' => 'Newfoundland',
						'ns' => 'Nova Scotia',
						'nt' => 'Northwest Territories',
						'nu' => 'Nunavut',
						'on' => 'Ontario',
						'pe' => 'Prince Edward Islands',
						'qc' => 'Quebec',
						'sk' => 'Saskatchewan',
						'yt' => 'Yukon'
					);
				case 'us':
					return array(
						'al' => 'Alabama',
						'ak' => 'Alaska',
						'as' => 'American Samoa',

						'az' => 'Arizona',
						'ar' => 'Arkansas',
						'aa' => 'Armed Forces America',
						'ae' => 'Armed Forces Other Areas',
						'ap' => 'Armed Forces Pacific',
						'ca' => 'California',

						'co' => 'Colorado',
						'ct' => 'Connecticut',
						'de' => 'Delaware',
						'dc' => 'District of Columbia',
						'fm' => 'Federated States of Micronesia',
						'fl' => 'Florida',

						'ga' => 'Georgia',
						'gu' => 'Guam',
						'hi' => 'Hawaii',
						'id' => 'Idaho',
						'il' => 'Illinois',
						'in' => 'Indiana',

						'ia' => 'Iowa',
						'ks' => 'Kansas',
						'ky' => 'Kentucky',
						'la' => 'Louisiana',
						'me' => 'Maine',
						'mh' => 'Marshall Islands',

						'md' => 'Maryland',
						'ma' => 'Massachusetts',
						'mi' => 'Michigan',
						'mn' => 'Minnesota',
						'ms' => 'Mississippi',
						'mo' => 'Missouri',

						'mt' => 'Montana',
						'ne' => 'Nebraska',
						'nv' => 'Nevada',
						'nh' => 'New Hampshire',
						'nj' => 'New Jersey',
						'nm' => 'New Mexico',

						'ny' => 'New York',
						'nc' => 'North Carolina',
						'nd' => 'North Dakota',
						'mp' => 'Northern Mariana Islands',
						'oh' => 'Ohio',
						'ok' => 'Oklahoma',

						'or' => 'Oregon',
						'pw' => 'Palau',
						'pa' => 'Pennsylvania',
						'pr' => 'Puerto Rico',
						'ri' => 'Rhode Island',
						'sc' => 'South Carolina',

						'sd' => 'South Dakota',
						'tn' => 'Tennessee',
						'tx' => 'Texas',
						'vi' => 'U.S. Virgin Islands',
						'ut' => 'Utah',
						'vt' => 'Vermont',

						'va' => 'Virginia',
						'wa' => 'Washington',
						'wv' => 'West Virginia',
						'wi' => 'Wisconsin',
						'wy' => 'Wyoming'
					);
				case 'au':
					return array(
						'ACT' => 'Australian Capital Territory',
						'NSW' => 'New South Wales',
						'NT'  => 'Northern Territory',
						'Qld' => 'Queensland',
						'SA'  => 'Southern Australia',
						'Tas' => 'Tasmania',
						'Vic' => 'Victoria',
						'WA'  => 'Western Australia'
					);
				case 'mx':
					return array(
						'AG' => 'Aguascalientes',
						'BC' => 'Baja California',
						'BS' => 'Baja California Sur',
						'CM' => 'Campeche',
						'CS' => 'Chiapas',
						'CH' => 'Chihuahua',
						'CO' => 'Coahuila',
						'CL' => 'Colima',
						'DF' => 'Distrito Federal',
						'DG' => 'Durango',
						'GT' => 'Guanajuato',
						'GR' => 'Guerrero',
						'HG' => 'Hidalgo',
						'JA' => 'Jalisco',
						'MX' => 'Mexico',
						'MI' => 'Michoacan',
						'MO' => 'Morelos',
						'NA' => 'Nayarit',
						'NL' => 'Nuevo Leon',
						'OA' => 'Oaxaca',
						'PU' => 'Puebla',
						'QT' => 'Queretaro',
						'QR' => 'Quintana Roo',
						'SL' => 'San Luis Potosi',
						'SI' => 'Sinaloa',
						'SO' => 'Sonora',
						'TB' => 'Tabasco',
						'TM' => 'Tamaulipas',
						'TL' => 'Tlaxcala',
						'VE' => 'Veracruz',
						'YU' => 'Yucatan',
						'ZA' => 'Zacatecas'
					);
				case 'it':
					return array(
						'AG' => 'Agrigento',
						'AL' => 'Alessandria',
						'AN' => 'Ancona',
						'AO' => 'Aosta',
						'AR' => 'Arezzo',
						'AP' => 'Ascoli Piceno',
						'AT' => 'Asti',
						'AV' => 'Avellino',
						'BA' => 'Bari',
						'BL' => 'Belluno',
						'BN' => 'Benevento',
						'BG' => 'Bergamo',
						'BI' => 'Biella',
						'BO' => 'Bologna',
						'BZ' => 'Bolzano-Bozen',
						'BS' => 'Brescia',
						'BR' => 'Brindisi',
						'CA' => 'Cagliari',
						'CL' => 'Caltanissetta',
						'CB' => 'Campobasso',
						'CI' => 'Carbonia-Iglesias',
						'CE' => 'Caserta',
						'CT' => 'Catania',
						'CZ' => 'Catanzaro',
						'CH' => 'Chieti',
						'CO' => 'Como',
						'CS' => 'Cosenza',
						'CR' => 'Cremona',
						'KR' => 'Crotone',
						'CN' => 'Cuneo',
						'EN' => 'Enna',
						'FE' => 'Ferrara',
						'FI' => 'Florence',
						'FG' => 'Foggia',
						'FC' => 'Forli-Cesena',
						'FR' => 'Frosinone',
						'GE' => 'Genoa',
						'GO' => 'Gorizia',
						'GR' => 'Grosseto',
						'IM' => 'Imperia',
						'IS' => 'Isernia',
						'SP' => 'La Spezia',
						'AQ' => "L'Aquila",
						'LT' => 'Latina',
						'LE' => 'Lecce',
						'LC' => 'Lecco',
						'LI' => 'Livorno',
						'LO' => 'Lodi',
						'LU' => 'Lucca',
						'MC' => 'Macerata',
						'MN' => 'Mantua',
						'MS' => 'Massa-Carrara',
						'MT' => 'Matera',
						'VS' => 'Medio Campidano',
						'ME' => 'Messina',
						'MI' => 'Milan',
						'MO' => 'Modena',
						'NA' => 'Naples',
						'NO' => 'Novara',
						'NU' => 'Nuoro',
						'OG' => 'Ogliastra',
						'OT' => 'Olbia-Tempio',
						'OR' => 'Oristano',
						'PD' => 'Padua',
						'PA' => 'Palermo',
						'PR' => 'Parma',
						'PV' => 'Pavia',
						'PG' => 'Perugia',
						'PU' => 'Pesaro e Urbino',
						'PE' => 'Pescara',
						'PC' => 'Piacenza',
						'PI' => 'Pisa',
						'PT' => 'Pistoia',
						'PN' => 'Pordenone',
						'PZ' => 'Potenza',
						'PO' => 'Prato',
						'RG' => 'Ragusa',
						'RA' => 'Ravenna',
						'RC' => 'Reggio Calabria',
						'RE' => 'Reggio Emilia',
						'SM' => 'Republic of San Marino',
						'RI' => 'Rieti',
						'RN' => 'Rimini',
						'RM' => 'Rome',
						'RO' => 'Rovigo',
						'SA' => 'Salerno',
						'SS' => 'Sassari',
						'SV' => 'Savona',
						'SI' => 'Siena',
						'SO' => 'Sondrio',
						'SR' => 'Syracuse',
						'TA' => 'Taranto',
						'TE' => 'Teramo',
						'TR' => 'Terni',
						'TP' => 'Trapani',
						'TN' => 'Trento',
						'TV' => 'Treviso',
						'TS' => 'Trieste',
						'TO' => 'Turin',
						'UD' => 'Udine',
						'VA' => 'Varese',
						'VE' => 'Venice',
						'VB' => 'Verbano-Cusio-Ossola',
						'VC' => 'Vercelli',
						'VR' => 'Verona',
						'VV' => 'Vibo Valentia',
						'VI' => 'Vicenza',
						'VT' => 'Viterbo',
					);
				case 'in':
					return array(
						'AN' => 'Andaman and Nicobar Islands',
						'AP' => 'Andhra Pradesh',
						'AR' => 'Arunachal Pradesh',
						'AS' => 'Assam',
						'BR' => 'Bihar',
						'CH' => 'Chandigarh',
						'CT' => 'Chhattisgarh',
						'DN' => 'Dadra and Nagar Haveli',
						'DD' => 'Daman and Diu',
						'DL' => 'Delhi',
						'GA' => 'Goa',
						'GJ' => 'Gujarat',
						'HR' => 'Haryana',
						'HP' => 'Himachal Pradesh',
						'JK' => 'Jammu and Kashmir',
						'JH' => 'Jharkhand',
						'KA' => 'Karnataka',
						'KL' => 'Kerala',
						'LD' => 'Lakshadweep',
						'MP' => 'Madhya Pradesh',
						'MH' => 'Maharashtra',
						'MN' => 'Manipur',
						'ML' => 'Meghalaya',
						'MZ' => 'Mizoram',
						'NL' => 'Nagaland',
						'OR' => 'Orissa',
						'PY' => 'Puducherry',
						'PB' => 'Punjab',
						'RJ' => 'Rajasthan',
						'SK' => 'Sikkim',
						'TN' => 'Tamil Nadu',
						'TS' => 'Telangana',
						'TR' => 'Tripura',
						'UP' => 'Uttar Pradesh',
						'UL' => 'Uttarakhand',
						'WB' => 'West Bengal',
					);
				case 'br':
					return array(
						'AC' => 'Acre',
						'AL' => 'Alagoas',
						'AP' => 'Amapa',
						'AM' => 'Amazonas',
						'BA' => 'Bahia',
						'CE' => 'Ceara',
						'DF' => 'Distrito Federal',
						'ES' => 'Espirito Santo',
						'GO' => 'Goias',
						'MA' => 'Maranhao',
						'MT' => 'Mato Grosso',
						'MS' => 'Mato Grosso do Sul',
						'MG' => 'Minas Gerais',
						'PA' => 'Para',
						'PB' => 'Paraiba',
						'PR' => 'Parana',
						'PE' => 'Pernambuco',
						'PI' => 'Piaui',
						'RJ' => 'Rio de Janeiro',
						'RN' => 'Rio Grande do Norte',
						'RS' => 'Rio Grande do Sul',
						'RO' => 'Rondonia',
						'RR' => 'Roraima',
						'SC' => 'Santa Catarina',
						'SP' => 'Sao Paulo',
						'SE' => 'Sergipe',
						'TO' => 'Tocantins',
					);
				case 'be':
					return array(
						'VAN' => 'Antwerpen',
						'WBR' => 'Brabant Wallon',
						'BRU' => 'Brussels',
						'WHT' => 'Hainaut',
						'WLG' => 'Liege',
						'VLI' => 'Limburg',
						'WLX' => 'Luxembourg',
						'WNA' => 'Namur',
						'VOV' => 'Oost-Vlaanderen',
						'VBR' => 'Vlaams Brabant',
						'VWV' => 'West-Vlaanderen',
					);
				default:
					return array();
			}

		}

		public static function countries()
		{
			return array(
				'us' => 'United States',
				'uk' => 'United Kingdom',
				'af' => 'Afghanistan',
				'al' => 'Albania',
				'dz' => 'Algeria',
				'as' => 'American Samoa',
				'ad' => 'Andorra',
				'ao' => 'Angola',
				'ai' => 'Anguilla',

				'aq' => 'Antarctica',
				'ag' => 'Antigua and Barbuda',
				'ar' => 'Argentina',
				'am' => 'Armenia',
				'aw' => 'Aruba',
				'ac' => 'Ascension Island',

				'au' => 'Australia',
				'at' => 'Austria',
				'az' => 'Azerbaijan',
				'bs' => 'Bahamas',
				'bh' => 'Bahrain',
				'bd' => 'Bangladesh',

				'bb' => 'Barbados',
				'by' => 'Belarus',
				'be' => 'Belgium',
				'bz' => 'Belize',
				'bj' => 'Benin',
				'bm' => 'Bermuda',

				'bt' => 'Bhutan',
				'bo' => 'Bolivia',
				'ba' => 'Bosnia and Herzegovina',
				'bw' => 'Botswana',
				'bv' => 'Bouvet Island',
				'br' => 'Brazil',

				'io' => 'British Indian Ocean Territory',
				'bn' => 'Brunei Darussalam',
				'bg' => 'Bulgaria',
				'bf' => 'Burkina Faso',
				'bi' => 'Burundi',
				'kh' => 'Cambodia',

				'cm' => 'Cameroon',
				'ca' => 'Canada',
				'cv' => 'Cape Verde Islands',
				'ky' => 'Cayman Islands',
				'cf' => 'Central African Republic',
				'td' => 'Chad',

				'cl' => 'Chile',
				'cn' => 'China',
				'cx' => 'Christmas Island',
				'cc' => 'Cocos (Keeling) Islands',
				'co' => 'Colombia',
				'km' => 'Comoros',

				'cd' => 'Congo, Democratic Republic of',
				'cg' => 'Congo, Republic of',
				'ck' => 'Cook Islands',
				'cr' => 'Costa Rica',
				'ci' => 'Cote d\'Ivoire',
				'hr' => 'Croatia/Hrvatska',

				'cu' => 'Cuba',
				'cy' => 'Cyprus',
				'cz' => 'Czech Republic',
				'dk' => 'Denmark',
				'dj' => 'Djibouti',
				'dm' => 'Dominica',

				'do' => 'Dominican Republic',
				'tp' => 'East Timor',
				'ec' => 'Ecuador',
				'eg' => 'Egypt',
				'sv' => 'El Salvador',
				'gq' => 'Equatorial Guinea',

				'er' => 'Eritrea',
				'ee' => 'Estonia',
				'et' => 'Ethiopia',
				'fk' => 'Falkland Islands',
				'fo' => 'Faroe Islands',
				'fj' => 'Fiji',

				'fi' => 'Finland',
				'fr' => 'France',
				'gf' => 'French Guiana',
				'pf' => 'French Polynesia',
				'tf' => 'French Southern Territories',
				'ga' => 'Gabon',

				'gm' => 'Gambia',
				'ge' => 'Georgia',
				'de' => 'Germany',
				'gh' => 'Ghana',
				'gi' => 'Gibraltar',
				'gr' => 'Greece',

				'gl' => 'Greenland',
				'gd' => 'Grenada',
				'gp' => 'Guadeloupe',
				'gu' => 'Guam',
				'gt' => 'Guatemala',
				'gg' => 'Guernsey',

				'gn' => 'Guinea',
				'gw' => 'Guinea-Bissau',
				'gy' => 'Guyana',
				'ht' => 'Haiti',
				'hm' => 'Heard and McDonald Islands',
				'hn' => 'Honduras',

				'hk' => 'Hong Kong',
				'hu' => 'Hungary',
				'is' => 'Iceland',
				'in' => 'India',
				'id' => 'Indonesia',
				'ir' => 'Iran',

				'iq' => 'Iraq',
				'ie' => 'Ireland',
				'im' => 'Isle of Man',
				'il' => 'Israel',
				'it' => 'Italy',
				'jm' => 'Jamaica',

				'jp' => 'Japan',
				'je' => 'Jersey',
				'jo' => 'Jordan',
				'kz' => 'Kazakhstan',
				'ke' => 'Kenya',
				'ki' => 'Kiribati',

				'kp' => 'Korea, Democratic People\'s Republic of',
				'kr' => 'Korea, Republic of',
				'kw' => 'Kuwait',
				'kg' => 'Kyrgyzstan',
				'la' => 'Laos',
				'lv' => 'Latvia',

				'lb' => 'Lebanon',
				'ls' => 'Lesotho',
				'lr' => 'Liberia',
				'ly' => 'Libya',
				'li' => 'Liechtenstein',
				'lt' => 'Lithuania',

				'lu' => 'Luxembourg',
				'mo' => 'Macau',
				'mk' => 'Macedonia, Former Yugoslav Republic',
				'mg' => 'Madagascar',
				'mw' => 'Malawi',
				'my' => 'Malaysia',

				'mv' => 'Maldives',
				'ml' => 'Mali',
				'mt' => 'Malta',
				'mh' => 'Marshall Islands',
				'mq' => 'Martinique',
				'mr' => 'Mauritania',

				'mu' => 'Mauritius',
				'yt' => 'Mayotte Island',
				'mx' => 'Mexico',
				'fm' => 'Micronesia',
				'md' => 'Moldova',
				'mc' => 'Monaco',

				'mn' => 'Mongolia',
				'me' => 'Montenegro',
				'ms' => 'Montserrat',
				'ma' => 'Morocco',
				'mz' => 'Mozambique',
				'mm' => 'Myanmar',

				'na' => 'Namibia',
				'nr' => 'Nauru',
				'np' => 'Nepal',
				'nl' => 'Netherlands',
				'an' => 'Netherlands Antilles',
				'nc' => 'New Caledonia',

				'nz' => 'New Zealand',
				'ni' => 'Nicaragua',
				'ne' => 'Niger',
				'ng' => 'Nigeria',
				'nu' => 'Niue',
				'nf' => 'Norfolk Island',

				'mp' => 'Northern Mariana Islands',
				'no' => 'Norway',
				'om' => 'Oman',
				'pk' => 'Pakistan',
				'pw' => 'Palau',
				'ps' => 'Palestinian Territories',

				'pa' => 'Panama',
				'pg' => 'Papua New Guinea',
				'py' => 'Paraguay',
				'pe' => 'Peru',
				'ph' => 'Philippines',
				'pn' => 'Pitcairn Island',

				'pl' => 'Poland',
				'pt' => 'Portugal',
				'pr' => 'Puerto Rico',
				'qa' => 'Qatar',
				're' => 'Reunion Island',
				'ro' => 'Romania',

				'ru' => 'Russian Federation',
				'rw' => 'Rwanda',
				'sh' => 'Saint Helena',
				'kn' => 'Saint Kitts and Nevis',
				'lc' => 'Saint Lucia',
				'pm' => 'Saint Pierre and Miquelon',

				'vc' => 'Saint Vincent and the Grenadines',
				'sm' => 'San Marino',
				'st' => 'Sao Tome and Principe',
				'sa' => 'Saudi Arabia',
				'sn' => 'Senegal',
				'rs' => 'Serbia',

				'sc' => 'Seychelles',
				'sl' => 'Sierra Leone',
				'sg' => 'Singapore',
				'sk' => 'Slovak Republic',
				'si' => 'Slovenia',
				'sb' => 'Solomon Islands',

				'so' => 'Somalia',
				'za' => 'South Africa',
				'gs' => 'South Georgia and South Sandwich Islands',
				'es' => 'Spain',
				'lk' => 'Sri Lanka',
				'sd' => 'Sudan',

				'sr' => 'Suriname',
				'sj' => 'Svalbard and Jan Mayen Islands',
				'sz' => 'Swaziland',
				'se' => 'Sweden',
				'ch' => 'Switzerland',
				'sy' => 'Syria',

				'tw' => 'Taiwan',
				'tj' => 'Tajikistan',
				'tz' => 'Tanzania',
				'th' => 'Thailand',
				'tl' => 'Timor-Leste',
				'tg' => 'Togo',

				'tk' => 'Tokelau',
				'to' => 'Tonga Islands',
				'tt' => 'Trinidad and Tobago',
				'tn' => 'Tunisia',
				'tr' => 'Turkey',
				'tm' => 'Turkmenistan',

				'tc' => 'Turks and Caicos Islands',
				'tv' => 'Tuvalu',
				'ug' => 'Uganda',
				'ua' => 'Ukraine',
				'ae' => 'United Arab Emirates',
				'uy' => 'Uruguay',
				'um' => 'US Minor Outlying Islands',
				'uz' => 'Uzbekistan',
				'vu' => 'Vanuatu',
				'va' => 'Vatican City',

				've' => 'Venezuela',
				'vn' => 'Vietnam',
				'vg' => 'Virgin Islands (British)',
				'vi' => 'Virgin Islands (USA)',
				'wf' => 'Wallis and Futuna Islands',
				'eh' => 'Western Sahara',

				'ws' => 'Western Samoa',
				'ye' => 'Yemen',
				'zm' => 'Zambia',
				'zw' => 'Zimbabwe'
			);
		}

		/**
		 * Encodes string to be used in URL
		 *
		 * @param  string $com
		 * @return string encoded string
		 */
		public static function url_encode($com)
		{
			return filter_var($com, FILTER_SANITIZE_ENCODED);
		}

		/**
		 * Decodes string into URL
		 *
		 * @param  $url
		 * @return string
		 */
		public static function url_decode($url)
		{
			return urldecode($url);
		}

		/**
		 * PHP implementation of get_browser()
		 *
		 * @return object get_browser() implementation
		 */
		public static function get_browser()
		{
			if (!\Session::exists('browser-instance')) {
				try {
					$browscap = new Util_Browscap();
					$browser = $browscap->getBrowser();
				} catch (\Exception $e) {
					return null;
				}
				\Session::set('browser-instance', $browser);
			}

			return \Session::get('browser-instance');
		}

		/**
		 * @until 3.3
		 * @deprecated
		 */
		public static function reduce_size($size, $places = 2, $limit = -1)
		{
			deprecated_func('Use Formatter::reduceBytes');
			return Formatter::reduceBytes($size, $places, $limit);
		}

		/**
		 * @until 3.3
		 * @deprecated
		 */
		public static function commafy($text)
		{
			deprecated_func('Use Formatter::commafy');
			return Formatter::commafy($text);
		}

		/**
		 * Get maximum permitted upload in KB
		 *
		 * @return float
		 */
		public static function maxUpload(): float
		{
			$maxUpload = \Formatter::changeBytes(ini_get('upload_max_filesize'), 'kb');
			if (\UCard::is('admin')) {
				$quotas = \UCard::get()->admin_get_storage();
			} else {
				$quotas = \UCard::get()->user_get_quota();
			}

			if (!$quotas['qhard']) {
				return $maxUpload;
			}
			return min(
				$maxUpload,
				$quotas['qhard'] - $quotas['qused']
			);
		}

		/**
		 * Create downloadable stream for file
		 *
		 * @param string|Closure $filespec       filename to read or closure
		 * @param string|null    $attachmentname attachment name required with closure
		 * @param bool           $delete         delete on shutdown
		 */
		public static function makeDownloadable($filespec, string $attachmentname = null, bool $delete = true): void
		{
			$filesize = -1;
			$tmpfile = null;
			if ($filespec instanceof Closure) {
				if (!$attachmentname) {
					fatal('Closure must specify attachment name');
				}
				$tmpfile = fopen('php://tempfile', 'w');
				$filesize = fwrite($tmpfile, $filespec());
				rewind($tmpfile);
			} else {
				if (null === $attachmentname) {
					$attachmentname = basename($filespec);
				}

				if ($delete) {
					register_shutdown_function(static function () use ($filespec) {
						file_exists($filespec) && unlink($filespec);
					});
				}

				// ignore pipe length
				if (filetype($filespec) === 'file') {
					$filesize = filesize($filespec);
				}
			}
			header('Content-disposition: attachment; filename="' . $attachmentname . '"');
			header('Content-Type: application/octet-stream');
			header('Content-Transfer-Encoding: binary');
			header('Pragma: no-cache');
			header('Expires: 0');
			if ($filesize > -1) {
				header('Content-Length: ' . $filesize);
			}
			ob_end_flush();
			is_resource($tmpfile) ? fpassthru($tmpfile) : readfile($filespec);
			exit(0);
		}

		/**
		 * Convert preference key (dot-notation) into HTML name
		 *
		 * @param string $prefkey
		 * @return string
		 */
		public static function prefify(string $prefkey): string
		{
			return '[' . str_replace('.', '][', $prefkey) . ']';
		}
	}