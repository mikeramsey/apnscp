<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class User_Defaults implements ArrayAccess
	{
		use ContextableTrait;
		use apnscpFunctionInterceptorTrait;

		private array $def_options = [
			'disk_quota'        => 500,
			'email_enable'      => 1,
			'email_domains'     => null,
			'email_create'      => 1,
			'email_imap'        => 1,
			'email_smtp'        => 1,
			'ftp_jail'          => 1,
			'ftp_jail_path'     => '',
			'ftp_enable'        => 1,
			'subdomain_enable'  => 0,
			'subdomain_domains' => null,
			'ssh_enable'        => 0,
			'username'          => '',
			'password'          => '',
			'disk_quota_warn'   => 80,
			'disk_quota_crit'   => 95,
			'crontab_enable'    => 0,
			'cp_enable'         => 1,
			'dav_enable'        => 0,
			'shell'             => '/bin/bash'
		];
		private array $options;

		protected function __construct() {
			$this->load_defaults();
		}

		public function get_value($param)
		{
			$values = $this->load_defaults();

			if (!$this->offsetExists($param)) {
				return error('%s: unknown field in user defaults', $param);
			}
			return $this->offsetGet($param);
		}

		public function load_defaults(): array
		{
			if (isset($this->options)) {
				return $this->options;
			}
			$user_settings = array();

			if ($this->file_exists('/etc/usertemplate')) {
				$user_settings = parse_ini_string($this->file_get_file_contents('/etc/usertemplate'), false, INI_SCANNER_TYPED);
				if (isset($user_settings['email_domains']) && false !== strpos($user_settings['email_domains'], ',')) {
					$user_settings['email_domains'] = preg_split('/,\s*/', $user_settings['email_domains']);
				}
				if (empty($user_settings['subdomain_domains'])) {
					$user_settings['subdomain_domains'] = $_SESSION['entry_domain'] ?? $this->getAuthContext()->domain;
				} else if (!empty($user_settings['subdomain_domains']) && false !== strpos($user_settings['subdomain_domains'],
						',')
				) {
					$user_settings['subdomain_domains'] = preg_split('/,\s*/', $user_settings['subdomain_domains']);
				}
			}
			$this->options = array_merge($this->defaults(), $user_settings);

			return $this->options;
		}

		private function defaults()
		{
			return $this->def_options;
		}

		public function set_defaults(array $params)
		{
			$options = $this->merge($params);
			if ($params['disk_quota'] > Util_Conf::call('site_get_account_quota')) {
				$options['disk_quota'] = 0;
			}
			if (is_array($options['email_domains']) && count($options['email_domains']) === count($this->email_list_virtual_transports())) {
				$options['email_domains'] = $email_domains = '*';
			} else if (is_array($options['email_domains'])) {
				$email_domains = implode(',', $options['email_domains']);
			} else {
				$email_domains = '';
			}
			if (is_array($options['subdomain_domains']) &&
				count($options['subdomain_domains']) === count($this->web_list_domains()))
			{
				$options['subdomain_domains'] = $subdomains = '*';
			} else if (is_array($options['subdomain_domains'])) {
				$subdomains = '';
			} else {
				$subdomains = '';
			}
			if (isset($params['disk-unlimited'])) {
				$quota = 0;
			} else {
				$quota = (int)$options['disk_quota'];
			}
			$options['disk_quota'] = $quota;
			$template = '; Default settings for new users created via' . "\n" .
				'; Set User Defaults' . "\n" .
				'[default]' . "\n" .
				'; Default disk quota' . "\n" .
				'disk_quota=' . (int)$quota . "\n" .
				'; Enable IMAP/SMTP access' . "\n" .
				'email_enable=' . (int)$options['email_enable'] . "\n" .
				'; Finer control-- enable IMAP, implies email_enable=true' . "\n" .
				'email_imap=' . (int)$options['email_imap'] . "\n" .
				'; Finer control-- enable SMTP, implies email_enable=true' . "\n" .
				'email_smtp=' . (int)$options['email_smtp'] . "\n" .
				'; Create e-mail addresses for user' . "\n" .
				'email_create=' . (int)$options['email_create'] . "\n" .
				'; Domains to use in address creation, * = all' . "\n" .
				'; Separate multiple entities with comma, e.g.' . "\n" .
				'; foo.com,bar.com,baz.com' . "\n" .
				'email_domains=' . $email_domains . "\n" .
				'; Enable FTP access' . "\n" .
				'ftp_enable=' . (int)$options['ftp_enable'] . "\n" .
				'; Jail FTP users to home' . "\n" .
				'ftp_jail=' . (int)$options['ftp_jail'] . "\n" .
				'; Jail FTP users to this path. %u means present user, ~ /home/%u' . "\n" .
				'ftp_jail_path=' . '"' . $options['ftp_jail_path'] . '"' . "\n" .
				'; Enable subdomain creation' . "\n" .
				'subdomain_enable=' . (int)$options['subdomain_enable'] . "\n" .
				'; Subdomains to bind, * = all' . "\n" .
				'subdomain_domains=' . $subdomains . "\n" .
				'; Enable SSH access' . "\n" .
				'ssh_enable=' . (int)(isset($options['ssh_enable']) && $options['ssh_enable']) . "\n" .
				'; Enable task scheduling via crontab' . "\n" .
				'crontab_enable=' . (int)(isset($options['ssh_enable']) && $options['crontab_enable']) . "\n" .
				'; Enable CP access' . "\n" .
				'cp_enable=' . (int)(isset($options['cp_enable']) && $options['cp_enable']) . "\n" .
				'; Enable DAV access' . "\n" .
				'dav_enable=' . (int)(!empty($options['dav_enable']) && $options['dav_enable']) . "\n" .
				'; Enable API access' . "\n" .
				'api_enable=' . (int)(isset($options['cp_enable']) && $options['cp_enable']) . "\n";
			if (!$this->file_put_file_contents('/etc/usertemplate', $template, true)) {
				return error('failed to set default options');
			}

			return $options;
		}

		private function merge($newopts)
		{
			$oldopts = $this->load_defaults();
			foreach ($oldopts as $key => $val) {
				if (!isset($newopts[$key])) {
					$newopts[$key] = 0;
				}
			}

			return array_merge($oldopts, $newopts);
		}

		private function getEmailDomains(): array
		{
			if (!isset($this->def_options['email_domains'])) {
				$this->def_options['email_domains'] = (array)$this->email_list_virtual_transports();
				asort($this->def_options['email_domains']);
			}

			return $this->def_options['email_domains'];
		}

		public function offsetExists($offset)
		{
			return array_key_exists($offset, $this->def_options);
		}

		public function offsetGet($offset)
		{
			$fn = 'get' . studly_case($offset);
			if (method_exists($this, $fn)) {
				return $this->$fn($offset);
			}
			return $this->options[$offset] ?? $this->def_options;
		}

		public function offsetSet($offset, $value)
		{
			$fn = 'set' . studly_case($offset);
			if (method_exists($this, $fn)) {
				$this->$fn($offset);
			}
			$this->options[$offset] = $value;
		}

		public function offsetUnset($offset)
		{
			unset($this->options[$offset]);
		}
	}