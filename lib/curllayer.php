<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/** establishes a passthru to Ensim */
	class CurlLayer
	{
		public $options;
		public $ch;
		/**
		 * @var false|resource
		 */
		protected $handler;

		public function __destruct()
		{
			if (is_resource($this->ch)) {
				curl_close($this->ch);
			}
		}

		public function reset()
		{
			$this->__construct();
		}

		public function __construct()
		{
			$this->ch = curl_init();
			$this->options = array(
				CURLOPT_SSL_VERIFYHOST => !is_debug() ? 2 : false,
				CURLOPT_SSL_VERIFYPEER => !is_debug(),
				CURLOPT_HEADER         => true,
				CURLOPT_NOBODY         => true,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_RETURNTRANSFER => true
			);
		}

		public function try_request($mUrl)
		{
			curl_setopt($this->ch, CURLOPT_URL, $mUrl);
			curl_setopt_array($this->ch, $this->options);
			//var_export($this->ch);
			$data = curl_exec($this->ch);

			//curl_setopt(CURL_COOKIE,$mCookies);

			if ($err = curl_error($this->ch)) {
				return new apnscpException('cURL error: ' . $err);
			} else {
				return $data;
			}
		}

		public function set_form_method($mMethod)
		{
			if (strtolower($mMethod) == 'post') {
				$this->options[CURLOPT_POST] = true;
				$this->options[CURLOPT_HTTPGET] = false;
			} else {
				if (strtolower($mMethod) == 'get') {
					$this->options[CURLOPT_POST] = false;
					$this->options[CURLOPT_HTTPGET] = true;
				} else {
					throw new apnscpException("Invalid option for form method '" . $mMethod . "'");
				}
			}
		}

		public function toggle_body($mOpt = true)
		{
			$this->options[CURLOPT_NOBODY] = $mOpt ^ 1;
		}

		public function change_option($mOpt, $mValue = null)
		{
			if (is_array($mOpt)) {
				// array_merge renumbers the indexes
				foreach ($mOpt as $k => $v) {
					$this->options[$k] = $v;
				}
			} else {
				$this->options[$mOpt] = $mValue;
			}
		}

		public function trustServerCertificate(): void
		{
			$this->handler = tmpfile();
			fwrite($this->handler, apnscpFunctionInterceptor::init()->ssl_server_certificate());
			fwrite(
				$this->handler,
				file_get_contents(openssl_get_cert_locations()['default_cert_file'])
			);

			$this->change_option(CURLOPT_CAINFO, stream_get_meta_data($this->handler)['uri']);
		}

		public function get_opt($mOpt)
		{
			if (isset($this->options[$mOpt])) {
				return $this->options[$mOpt];
			}

			return null;
		}

		public function collapsePost(array $vars)
		{

		}
	}