<?php

	/**
	 * Util Process
	 * A general process utility
	 *
	 * MIT License
	 *
	 * @author  Matt Saladna <matt@apisnetworks.com>
	 * @license http://opensource.org/licenses/MIT
	 * @version $Rev: 2660 $ $Date: 2016-12-06 14:05:32 -0500 (Tue, 06 Dec 2016) $
	 */
	class Util_Process
	{
		/**
		 * @var array process options
		 */
		protected $opts = array(
			'run'         => 1,
			'pipe'        => null,
			'mute_stderr' => false,
			'mute_stdout' => false,
			'mute_stdin'  => true,
			'tv_sec'      => 5,
			'tv_usec'     => null,
			'binary'      => 0,
			'tail'        => false,
			'timeout'     => null,
			'fd'          => array(),
			// errors automatically generate error()/warn()
			'reporterror' => true,
			'priority'    => 0,
			'suid'        => null,
			'sgid'        => null,
		);
		/**
		 * @var string formatted command
		 */
		private $cmd;
		/**
		 * @var array arguments
		 */
		private $args;
		/**
		 *
		 * @var array|string permitted/regex exit values
		 */
		private $exits = '/^0$/';
		/**
		 * @var string binary name
		 */
		private $prog_name;
		/**
		 * @var object piped peer process
		 */
		private $proc_peer;
		/**
		 * @var string proc_open() instance
		 */
		private $proc_instance;

		// new priority level
		/**
		 * @var array environment variables if null, use $_ENV
		 */
		private $_env = array(
			'PATH' => '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin'
		);

		private $descriptors;

		/**
		 * @var array pipes held by a process
		 */
		private $pipes;

		/**
		 * @var string original command prior to suid/sgid
		 */
		private $originalCommand;

		/**
		 * @var array valid times when a callback may be attached
		 */
		protected $callbacks = [
			'read'  => array(), // read event happened
			'write' => array(), // write event happened
			'open'  => array(), // before proc_open(), no execution
			'exec'  => array(), // after proc_open(), before read
			'close' => array(), // process terminated
		];

		/**
		 * @var int exit code
		 */
		private $exitStatus;
		/**
		 * @var array channel output (stdin, stderr, stdout, etc)
		 */
		private $channelData = array();

		/**
		 * @var array  lookup to translate name => fd (stdin => 0)
		 */
		private $pipeMap = array();
		private $pipeline = array();
		private $revPipeMap = array();

		public function __construct()
		{
			if (null === $this->_env) {
				$this->_env = $_ENV;
			}
			$closestdin = (bool)$this->opts['mute_stdin'];
			$this->setDescriptor(0, 'pipe', 'rb', 'stdin', array('close' => $closestdin))->
			setDescriptor(1, 'pipe', 'wb', 'stdout')->
			setDescriptor(2, 'pipe', 'wb', 'stderr');
		}

		/**
		 * Set file descriptor type
		 *
		 * @param int          $fd    descriptor number
		 * @param string       $type  pipe or file
		 * @param string|array $mode  pipe mode or [file, file mode]
		 * @param string       $alias nickname
		 * @param array        $opts
		 * @return $this|bool
		 */
		public function setDescriptor($fd, $type, $mode, $alias = '', $opts = array())
		{
			if ($type !== 'pipe' && $type !== 'file') {
				return error($type . ': invalid fd type');
			}
			if (!$alias) {
				$alias = $fd;
			}
			if ($this->getOption('binary')) {
				$mode .= 'b';
			}
			if ($type === 'file') {
				if (!is_array($mode)) {
					return error('file descriptors must consist of array [filename, mode], scalar passed');
				}
				$this->descriptors[$fd] = array($type, $mode[0], $mode[1]);
			} else {
				$this->descriptors[$fd] = array($type, $mode);
			}
			$this->channelData[$fd] = array();
			$this->addPipeMap($alias, $fd);
			$this->opts['fd'][$fd] = array();
			foreach ($opts as $opt => $val) {
				$this->setDescriptorOption($fd, $opt, $val);
			}

			return $this;
		}

		public function getOption($name)
		{
			return $this->opts[$name] ?? null;
		}

		/**
		 * Add a lookup for fd alias => fd
		 *
		 * @param string $alias pipe alias name, e.g. "stdin", "stdout", "outfile"
		 * @param int    $fd    fd descriptor assigned by setDescriptor()
		 * @return unknown_type
		 */
		private function addPipeMap($alias, $fd)
		{
			$this->removePipeMap($fd);
			if (isset($this->pipeMap[$alias]) && $this->pipeMap[$alias] !== $fd) {
				warn($alias . ': overwriting alias, previous descriptor ' . $this->pipeMap[$alias]);
			}

			$this->pipeMap[$alias] = $fd;
		}

		private function removePipeMap($fd)
		{
			if (isset($this->pipeMap[$fd])) {
				unset($this->pipeMap[$fd]);

				return true;
			}
			$fd = $this->getPipeNames($fd);
			if (false === ($key = array_search($fd, $this->pipeMap))) {
				unset($this->pipeMap[$key]);
			}

			return $key;
		}

		/**
		 * Get all defined pipe names, alias preferred
		 *
		 * @return array
		 */
		public function getPipeNames()
		{
			return array_keys($this->pipeMap);
		}

		public function setDescriptorOption($fd, $opt, $value)
		{
			$fd = $this->alias2Descriptor($fd);
			$this->opts['fd'][$fd][$opt] = $value;

			return $this;
		}

		/**
		 * Translate alias into descriptor
		 *
		 * @param $pipe
		 * @return unknown_type
		 */
		private function alias2Descriptor($pipe)
		{
			if (is_resource($pipe)) {
				$pipe = $this->resource2Descriptor($pipe);
			} else {
				if (isset($this->pipes[$pipe])) {
					return $pipe;
				} else {
					if (isset($this->descriptors[$pipe])) {
						return $pipe;
					}
				}
			}

			return $this->pipeMap[$pipe];
		}

		/**
		 * Translate pipe resource into descriptor
		 *
		 * @param $pipe
		 * @return unknown_type
		 */
		private function resource2Descriptor($pipe)
		{
			if (is_null($pipe)) {
				return error(__METHOD__ . ': pipe cannot be null');
			}
			if (is_resource($pipe)) {
				$pipe = $this->resource2String($pipe);
			}

			return $this->revPipeMap[$pipe]['fd'];
		}

		/**
		 * Convert resource name to fd int
		 *
		 * Example: Resource #12 -> 12
		 *
		 * @param  resource $res resource created by fopen()
		 * @return int
		 */
		private function resource2String($res)
		{
			if (!is_resource($res)) {
				return error($res . ': not a fd resource');
			}

			return substr(strstr((string)$res, '#'), 1);
		}

		/**
		 * Execute a program
		 *
		 * Optional parameters:
		 *    run (bool) : execute process automatically
		 *  fd  (array): file descriptors consisting of an array of descriptor-specific options
		 *
		 *  fd options:
		 *  -----------
		 *   close (bool): close pipe on creation - useful with stdin
		 *
		 * @param array|string $cmd   command
		 * @param mixed  $args  format arguments
		 * @param array  $exits permitted exit values
		 * @param array  $opts  optional process options
		 * @return array
		 */
		public static function exec($cmd, $args = null, $exits = array(0), $opts = array())
		{
			$c = static::class;
			$proc = new $c;
			return $proc->run(...func_get_args());
		}

		/**
		 * Break a full command down into its command + arguments in bash/sh dialect
		 *
		 * @param $cmd
		 * @return array
		 */
		public static function decompose($cmd)
		{
			$parts = array('cmd' => null, 'args' => array());
			$cmd = trim($cmd);
			if (!$cmd) {
				return $parts;
			}
			$delim = '" \\\'';
			$offset = 0;
			$tmp = array();
			// parse out command
			$mycmd = $cmd;
			$tok = strtok($mycmd, $delim);
			do {
				if (false === $tok) {
					break;
				}
				$offset += strlen($tok);
				$tmp[] = $tok;

				if ($offset >= strlen($mycmd)) {
					break;
				}
				$chr = $mycmd[$offset];

				if ($chr == '\\') {
					// @TODO keep the backslash?
					$tmp[] = $mycmd[++$offset];
					$offset++;
					$mycmd = substr($mycmd, $offset);
					$offset = 0;
					$tok = strtok($mycmd, $delim);
					continue;
				} else {
					if ($chr == '"') {
						$tok = strtok('"');
						continue;
					} else {
						if ($chr == "'") {
							$tok = strtok("'");
							continue;
						} else {
							break;
						}
					}
				}

				$tok = strtok($delim);
			} while (true);
			$parts['cmd'] = implode('', $tmp);
			$myargs = ltrim(substr($mycmd, $offset));
			if (!$myargs) {
				return $parts;
			}

			// second pass, do arguments, which are slightly varied
			$merge = static function ($stack) use (&$self) {
				if (\is_array($stack)) {
					return $stack;
				}
				$str = '';
				if ($stack->isEmpty()) {
					return $str;
				}
				while (!$stack->isEmpty()) {
					$el = $stack->shift();
					if ($el instanceof \SplStack || \is_array($el)) {
						return $self($el);
					}
					$str .= $el;
				}
				// impossible to parse empty string as PHP doesn't recognize "" from CLI
				if (($str[0] === "'" || $str[0] === '"') && $str[-1] === $str[0]) {
					return substr($str, 1, -1);
				}

				return $str;
			};
			$cmdargs = [];
			$myargs = str_split($myargs);
			$stack = new \SplStack();
			$key = null;
			$inquotes = false;
			for (; false !== ($arg = current($myargs)); next($myargs)) {
				if ('' === $arg) {
					continue;
				}
				if ($inquotes) {
					$stack->push($arg);
					if ($inquotes === $arg && $stack->top() !== '\\') {
						// end quoted
						$inquotes = false;
					}
					continue;
				}
				switch ($arg) {
					case '"':
					case "'":
						$stack->push($arg);
						$inquotes = $arg;
						continue 2;
					case ' ':
						if (!$inquotes) {
							if (!$stack->isEmpty()) {
								$cmdargs[] = $merge($stack);
								$stack = new SplStack();
							}
							continue 2;
						}
						break;
					case '\\':
						$next = next($myargs);
						if ($inquotes || $next === $inquotes) {
							$stack->push('\\');
						}
						$stack->push($next);
						continue 2;
				}
				$stack->push($arg);
			}


			$stack = $merge($stack);
			$cmdargs[] = $stack;

			$parts['args'] = $cmdargs;

			return $parts;
		}

		/**
		 * Allocate a fifo pipe
		 *
		 * @param string $path base path
		 * @return string|null
		 */
		public static function mkfifo(string $path = '/tmp'): ?string
		{
			$fifo = tempnam($path, 'cp-fifo');
			unlink($fifo);
			if (!posix_mkfifo($fifo, 0600)) {
				fatal('failed to ready pipe');
				return null;
			}
			return $fifo;
		}

		public function __destruct()
		{
			if (is_resource($this->proc_instance)) {
				proc_close($this->proc_instance);
				// unstable closure
				$this->callback('close', -1);
			}
		}

		public function run($cmd, ...$args)
		{
			$this->_init($cmd, $args);

			if (!$this->opts['run']) {
				return $this;
			}
			return $this->_run();
		}

		protected function _init($cmd, $args)
		{
			$this->cmd = $cmd;
			$this->_setArgs($args);

			$this->prog_name = basename(is_array($cmd) ? $cmd[0] : substr($cmd, 0, strpos($cmd, ' ')));
		}

		protected function _setArgs($args)
		{
			if (!array_key_exists(0, $args)) {
				return true;
			}

			$cmd = $this->cmd;
			$fmt_args = array();
			// cmd commands symbolic format parameters
			// arguments always presented as a hash
			$pos = strpos($cmd, '%(');
			if ($pos !== false) {
				$this->_setArgsNamed($args, $pos);
				$args = $this->args;
			} else {
				$pos = strpos($cmd, '%');
				$cnt = 0;
				while ($pos !== false) {
					$cnt++;
					$pos++;
					if ($cmd[$pos] == '%') {
						$cnt--;
						$pos++;
					} else if (($d = ctype_digit($cmd[$pos]))) {
						$cnt = max($cnt, $d);
					}
					$pos = strpos($cmd, '%', $pos);
				}
				// format specifiers exist
				if ($cnt) {
					if (!is_array($args[0])) {
						$fmt_args = array_slice($args, 0, $cnt);
					} else {
						// args provided as single array
						$fmt_args = $args[0];
						$cnt = 1;
					}
				}
				$this->args = array_slice($args, $cnt);
				$this->cmd = $this->synthesizeCommand($cmd, $fmt_args);
			}

			// all args satisfied
			if (empty($this->args)) {
				return true;
			}
			if ($this->args[0] === null) {
				// special case in which command received no args
				array_shift($this->args);
			}
			for ($i = 0; $i < 2; $i++) {
				$var = array_pop($this->args);
				if ($var === null) {
					break;
				}
				// check if $var is array of exit codes
				// or a regex with matching delimiters
				if (is_array($var) && isset($var[0]) || is_string($var) && $var[0] == $var[strlen($var) - 1]) {
					$this->exits = $var;
				} else if (is_array($var)) {
					// options array
					foreach ($var as $k => $v) {
						$this->setOption($k, $v);
					}
				} else {
					break;
				}
			}

			$szargs = count($this->args);
			if ($szargs > 0) {
				$caller = Error_Reporter::get_caller(1, '/Module_Skeleton|Util_Process/');
				warn($caller . '(): %u additional arguments ignored',
					$szargs);
			}

			return true;
		}

		/**
		 * Format arguments into command
		 *
		 * @param string $cmd
		 * @param array  $args
		 * @return string
		 */
		protected function synthesizeCommand(string $cmd, array $args = []): string
		{
			return vsprintf($cmd, $args);
		}

		/**
		 *  named arguments
		 *  first el in $args will be hash of arguments
		 */
		protected function _setArgsNamed($args, $pos = null)
		{
			// map each format param by its numeric location
			// to substitute later as %n$
			$argtable = array();
			$n = 1;
			foreach ($args[0] as $k => $v) {
				$argtable[$k] = $n;
				$n++;
			}

			$cmd = $this->cmd;

			$newstr = array();
			$start = 0;
			if (is_null($pos)) {
				$pos = strpos($cmd, '%(');
			}
			while (false !== $pos) {
				$len = $pos - $start;
				$newstr[] = substr($cmd, $start, $len);
				$pos += 2 /* %( */
				;
				$n = strpos($cmd, ')', $pos);
				if ($n === false) {
					warn('malformed format var name');
					break;
				}
				$symlen = $n - $pos;
				$sym = substr($cmd, $pos, $symlen);
				$pos += $symlen + 1 /* ) */
				;
				// lookup sym position
				if (isset($argtable[$sym])) {
					$newstr[] = '%' . $argtable[$sym] . '$';
				} else {
					warn("unknown format var `%s'", $sym);
				}
				$start = $pos;
				$pos = strpos($cmd, '%(', $pos);
			}
			$newstr[] = substr($cmd, $start);
			$cmd = implode('', $newstr);
			$this->args = array_slice($args, 1);
			$this->cmd = $this->synthesizeCommand($cmd, $args[0]);

			return true;
		}

		/**
		 * Set process priorrity
		 *
		 * @param int $prio
		 * @return $this|bool|void
		 */
		public function setPriority(int $prio)
		{
			if (!function_exists('pcntl_setpriority')) {
				fatal('pcntl extension not loaded');
			}
			if ($prio < -20 || $prio > 19) {
				return error("invalid priority specified `%d'", $prio);
			}
			$this->opts['priority'] = $prio;
			if (!$prio) {
				$this->deleteCallback('open', 'prio');
				$this->deleteCallback('close', 'prio');
				return true;
			}

			$this->addCallback(static function() use ($prio) {
				if (!pcntl_setpriority($prio, 0, PRIO_PROCESS)) {
					warn('Failed to set priority on process');
				}
			}, 'open.prio');
			// reset after exec
			$oldPriority = pcntl_getpriority();
			$this->addCallback(static function() use ($oldPriority) {
				// reset priority if adjusted
				pcntl_setpriority($oldPriority, 0, PRIO_PROCESS);
			}, 'close.prio');

			return $this;
		}

		/**
		 * Set user id for task
		 *
		 * @param string|int username or user id
		 * @return void
		 */
		public function setSuid($user): void {
			if (0 !== posix_getuid()) {
				fatal('Cannot set effective uid without root');
			}
			if (!is_int($user) && !ctype_digit($user)) {
				$user = posix_getpwnam($user)['uid'] ?? fatal("Failed to lookup user `%s'", $user);
			}
			$olduid = posix_geteuid();
			$this->addCallback(static function() use ($olduid) {
				posix_seteuid($olduid) or fatal('Failed to restore euid!');
			}, 'close.seuid');
			$this->addCallback(function() use ($user) {
				posix_seteuid((int)$user) or fatal('Failed to set euid!');
				$this->setPrivWrapper();
			}, 'open.seuid');
			$this->opts['suid'] = (int)$user;
		}

		/**
		 * Set group id for task
		 *
		 * @param string|int group or group id
		 * @return void
		 */
		public function setSgid($group): void {
			if (0 !== posix_getuid()) {
				fatal('Cannot set effective gid without root');
			}
			if (!is_int($group) && !ctype_digit($group)) {
				$group = posix_getpwnam($group)['uid'] ?? fatal("Failed to lookup group `%s'", $group);
			}
			$oldgid = posix_getegid();
			$this->addCallback(static function () use ($oldgid) {
				posix_setegid($oldgid) or fatal('Failed to restore egid!');
			}, 'close.segid');
			$this->addCallback(function() use ($group) {
				posix_setegid((int)$group) or fatal('Failed to set egid!');
				$this->setPrivWrapper();
			}, 'open.segid');
			$this->opts['sgid'] = (int)$group;
		}

		/**
		 * Wrap command around set-priv
		 */
		private function setPrivWrapper(): void
		{
			if (null === $this->originalCommand) {
				$this->originalCommand = $this->cmd;
			}
			$this->cmd = sprintf(
				'/usr/bin/setpriv --nnp --clear-groups --reuid %d --regid %d /bin/sh -c %s',
				posix_geteuid(),
				posix_getegid(),
				escapeshellarg($this->originalCommand)
			);
		}

		private function _run()
		{
			if (function_exists('pcntl_signal')) {
				$old = pcntl_signal_get_handler(SIGCHLD);
				pcntl_signal(SIGCHLD, SIG_DFL);
				if (is_callable($old)) {
					// proc_close() expects SIGCHLD as default (system) handler
					//
					// Setting a custom handler as in housekeeper worker forces proc_close() (implies wait4() syscall) to return -1
					// as the value is collected before proc_close() captures it.
					//
					// Set default, check if custom handler was in place and, if so, forward event to other SIGCHLD handler.
					// Resolves situation if Horizon dies before process exits and we want to restart it without affecting
					// potential return value of long-running process
					pcntl_signal(SIGCHLD, function(int $signo, $signinfo) use ($old) {
						$procinfo = null;
						if (is_resource($this->proc_instance)) {
							$procinfo = proc_get_status($this->proc_instance);
						}
						// else:
						// proc_get_status calls waitpid, cleanup if proc_open fails with SIGCHLD
						// registered (shouldn't happen) - pcntl_waitpid would collect this zombie.
						// For now let's forward to the next handler

						if (!$this->proc_instance || (!$procinfo || $signinfo['pid'] !== $procinfo['pid'])) {
							return $old($signo, $signinfo);
						}

						$this->setExit($procinfo['exitcode']);
						pcntl_signal(SIGCHLD, $old);
					});
				}
				$this->addCallback(static function () use ($old) {
					// restore default handler
					pcntl_signal(SIGCHLD, $old);
				}, 'close');
			}

			if ($this->callbacks['open']) {
				$this->callback('open', $this->cmd);
			}

			$proc = proc_open($this->cmd,
				$this->descriptors,
				$this->pipes,
				null,
				$this->_env
			);

			if (!is_resource($proc)) {
				$msg = "`%s': unable to open process";
				$format = $this->getOption('format');
				$this->exitStatus = 254;
				$this->channelData[2] = [\ArgumentFormatter::format($msg, [$this->cmd])];
				return $this->format($format);
			}

			$this->proc_instance = &$proc;

			if ($this->callbacks['exec']) {
				$this->callback('exec', $this->cmd);
			}

			$format = $this->getOption('format');

			return $this->process()->close()->format($format);
		}

		/**
		 * Util_Process callback request
		 *
		 * @param string $when
		 * @param mixed  $args,... arguments
		 * @return mixed
		 */
		protected function callback($when, ...$args)
		{
			if (!isset($this->callbacks[$when])) {
				return error("callback `$when' is not registered");
			}
			if (!$this->callbacks[$when]) {
				return false;
			}

			$args[] = $this;

			foreach ($this->callbacks[$when] as $cb) {
				$cb['function'](...(array_merge($args, $cb['args'])));
			}

			return;
		}

		/**
		 * Util_Process a program
		 *
		 * @todo add multiple pipe support (fd > 2)
		 * @todo rewrite pipe algorithm, distribute processing to peers
		 * @param $streams streams
		 * @param $maps    stream lookup table
		 * @param $procs   process list
		 * @return unknown_type
		 */

		public function process()
		{
			$read = $write = $oob = array();
			$procs = array();
			$prevpeer = null;
			$peer = $this;
			$peeridx = 0;

			$tv_sec = $this->getOption('tv_sec');
			$tv_usec = $this->getOption('tv_usec');
			$BLKRD = 8192;       //   8k reads
			$BUFSZ = 4096 * 64;   // 128k buffer
			$T_LOOP = 500;
			$T_READ = 250;
			$T_WRITE = 500;
			$DEBUG = function_exists('is_debug') && is_debug();
			$streambytes = 0;
			/**
			 * @todo drop the "2" functions, replace with asXYZ()
			 * refactor the loop into smaller, manageable methods
			 */
			do {
				foreach ($peer->getDescriptors() as $descriptor) {

					$pipe = $peer->getPipe($descriptor);
					$mode = $peer->getDescriptorMode($descriptor);
					$type = $peer->getDescriptorType($descriptor);
					$opts = $peer->getDescriptorOptions($descriptor);
					$revkey = $this->resource2String($pipe);

					// use res ids as fd names
					if ($peeridx == 0) {
						$key = array_search($descriptor, $this->pipeMap);
						$this->removePipeMap($key);
						$this->addPipeMap($key, $revkey);
					}

					if (isset($opts['close']) && $opts['close']) {
						$peer->addResourceMap($revkey, $descriptor, $peeridx);
						$peer->pipeClose($pipe);
						continue;
					}

					$descriptor = $revkey;
					$this->setDescriptor($descriptor, $type, $mode, $descriptor, $opts);

					//$this->pipes[$descriptor] = $pipe;
					//var_dump($descriptor, $pipe);
					$this->addResourceMap($revkey, $descriptor, $peeridx);

					if ($mode[0] == 'r') {
						$write[] = $pipe;
//	        			stream_set_blocking($pipe, 0);
//						stream_set_write_buffer($pipe,0);
					} else {
						if ($mode[0] == 'w') {
							$read[] = $pipe;
//	        			stream_set_blocking($pipe, 1);

						} else {
							warn($mode . ': unsupported mode for fd ' . $descriptor);
						}
					}
				}
				if ($prevpeer) {
					// connect stdout to stdin
					$this->connectPipeline($prevpeer->getPipe('stdin'),
						$peer->getPipe('stdout'));
				}

				$prevpeer = $peer;
				$procs[] = $peer;
				$peeridx++;
			} while (false != ($peer = $peer->getPipePeer()));
			// quick lookup maps, transforms:
			// resource #12 -> 12 => resource #12
			//
			// resource:  resource created by fopen()
			// streamidx: position of the resource within stream_select() array
			$maps = array();

			ignore_user_abort(false);
			foreach (array('read', 'write', 'oob') as $name) {
				$fdarr = $$name;
				for ($i = 0, $szarr = sizeof($fdarr); $i < $szarr; $i++) {
					$idx = $this->resource2String($fdarr[$i]);
					$res = $fdarr[$i];
					$pljob = $this->lookupPipeline($idx);
					$fd = $this->resource2Descriptor($res);
					$maps[$idx] = array(
						'streamidx' => $i,
						'residx'    => &$fd,
						'type'      => $name,
						'procid'    => $this->revPipeMap[$idx]['procid'],
						'peerid'    => null,
						'pipe'      => $this->getPipe($fd),
						'buffer'    => null,
						'bufpos'    => 0
					);
					$this->pipeline[$peer]['bufferfree'] = $BUFSZ;
					if ($pljob) {
						if ($name == 'read') {
							$peer = $pljob['write'];
						} else {
							$peer = $pljob['read'];
						}
						$maps[$idx] = array_merge($maps[$idx],
							array(
								'peerid'     => $peer,
								'peer'       => $this->getPipe($peer),
								'buffer'     => &$this->pipeline[$peer]['buffer'],
								'wpbuf'      => &$this->pipeline[$peer]['wpbuf'],
								'bufferfree' => &$this->pipeline[$peer]['bufferfree'],
								'rpbuf'      => &$this->pipeline[$peer]['rpbuf'],
							)
						);

					}
				}
			}
			$streams = array(
				'read'  => $read,
				'write' => $write,
				'oob'   => $oob
			);
			$this->maps = &$maps;
			$this->streams = &$streams;

			$stream_names = array_keys($streams);

			$i = 0;
			/** @noinspection CallableInLoopTerminationConditionInspection */
			for (; count($read) || count($write); $read = $streams['read'],
				   $write = $streams['write'], $i++) {
				if (connection_aborted()) {
					$this->_kill();

					return $this;
				}
				$changed = @stream_select($read, $write, $oob, $tv_sec, $tv_usec);
				// signals can interrupt select, treat false (interruption) same as 0
				if (!$changed) {
					continue;
				}


				// enumerate the changed streams
				foreach ($stream_names as $name) {
					$chstreams = ${$name};
					foreach ($chstreams as $piperes) {
						// get fd number for resource
						$pipeid = $this->resource2String($piperes);
						$pipeidx = array_search($piperes, $this->pipes);
						$pipe = $this->pipes[$pipeidx];
						$op = 'read';
						$pipemap = $maps[$pipeid];
						$pipekey = $pipemap['residx'];
						//$pipe      = $pipemap['pipe'];
						//if ($DEBUG && IS_CLI) print "Ready - $piperes - $op\n";
						// do necessary clean-up

						//print "Incoming on $pipeid - $op\n";
						$total = $bytes = 0;
						$eof = true;
						$data = array();
						if ($op == 'read') //&& !$pipebuf || !$pipebuf && sizeof($procs) > 1 && $streambytes > 0)
						{ // read
							//print "YAY";
							//if (isset($procs[1]) && $procs[1]->running()) {
							//print "Not closed... continuing...\n";
							//}
							// direct output, store
							/*while (!feof($piperes)) {
								$data  = fread($piperes,$BLKRD);
								$bytes = strlen($data);
								$eof &= ($bytes > 0);
								$total += $bytes;*/
							stream_set_blocking($piperes, 0);
							$total = 0;
							$buffer = array();
							while (!feof($piperes)) {
								$tmp = stream_get_contents($piperes, $BLKRD);
								$buffer[] = $tmp;
								$bytes = strlen($tmp);
								$total += $bytes;
								if ($bytes < 1) {
									break;
								}
							}
							//print "Read!";
							if ($total < 1) {
								fclose($this->pipes[$pipeidx]);
								unset($this->pipes[$pipeidx]);
								$streams['read'] = $this->pipes;
								//$this->streamClose($piperes, $streams, $maps);
								continue;
							}
							$buffer = join('', $buffer);
							$this->addOutput($pipeid, $buffer);
							if ($this->callbacks['read']) {
								$this->callback('read',
									$buffer,
									$total,
									$pipeid
								);
							}
							continue;
						} else if ($op === 'write') {
							echo 'WTF';
						}
					}
				}
			}
			foreach ($this->pipes as $pipe) {
				fclose($pipe);
			}

			return $this;
		}

		public function getDescriptors()
		{
			return array_keys($this->pipes);
		}

		/**
		 * Fetch pipe resource given a descriptor
		 *
		 * @param string|int $name pipe alias or fd
		 * @return resource
		 */
		private function &getPipe($name)
		{
			if (is_resource($name)) {
				return $name;
			}
			if (isset($this->pipes[$name])) {

				return $this->pipes[$name];
			}
			if (!isset($this->pipeMap[$name])) {
				$name = array_search($name, $this->pipeMap);
			}
			$key = $this->pipeMap[$name];
			if (isset($this->pipes[$key])) {
				$pipe = &$this->pipes[$key];
			} else {
				$pipe = false;
			}

			return $pipe;
		}

		/**
		 * Get mode assigned to descriptor
		 *
		 * @param  int|string $fd fd/alias
		 * @return string         fd mode (r,w,a)
		 */
		public function getDescriptorMode($fd)
		{
			$fd = $this->alias2Descriptor($fd);
			if (!isset($this->descriptors[$fd])) {
				return error($fd . ': descriptor not opened');
			}

			return $this->descriptors[$fd][1];
		}

		/**
		 * Get type assigned to descriptor
		 *
		 * @param  int|string $fd fd/alias
		 * @return string         fd type (file, pipe)
		 */
		public function getDescriptorType($fd)
		{
			$fd = $this->alias2Descriptor($fd);
			if (!isset($this->descriptors[$fd])) {
				return error($fd . ': descriptor not opened');
			}

			return $this->descriptors[$fd][0];
		}

		public function getDescriptorOptions($fd)
		{
			$fd = $this->alias2Descriptor($fd);

			return isset($this->opts['fd'][$fd]) ?
				$this->opts['fd'][$fd] :
				array();

		}

		/**
		 * Map a pipe resource to its descriptor
		 *
		 * @param  string|resource $resid
		 * @param  string          $descriptor
		 * @return bool
		 */
		private function addResourceMap($resid, $descriptor, $peerid = 0)
		{
			if (is_resource($resid)) {
				$resid = $this->resource2String($resid);
			}
			$this->revPipeMap[$resid] = array('fd' => $descriptor, 'procid' => $peerid);

			return true;
		}

		/**
		 * Close pipe
		 *
		 * @param  int|resource $pipe
		 * @return bool
		 */
		public function pipeClose(&$pipe)
		{
			if (!is_resource($pipe)) {
				$pipe = $this->getPipe($pipe);
			}

			$fd = $this->resource2Descriptor($pipe);
			unset($this->pipes[$fd]);
			$alias = array_search($pipe, $this->pipes);
			if (false !== $alias) {
				unset($this->pipes[$alias]);
			}
			for ($i = 0; $i < 5; usleep(500), $i++) {
				fclose($pipe);
				if (!is_resource($pipe)) {
					break;
				}
			}
			if (is_resource($pipe) && !feof($pipe)) {
				print stream_get_contents($pipe);
				error("pipe $pipe not closed yet!");
			}
		}

		/**
		 * Connect in to out
		 *
		 * @param $in  input stream
		 * @param $out output stream
		 * @return unknown_type
		 */
		private function connectPipeline(&$in, &$out)
		{

			$outkey = $this->resource2String($out);
			$inkey = $this->resource2String($in);
			//print "Connecting $out output ($outkey) to $in ($inkey)\n";
			// buffer write position used by output pipe
			$wbufpos = 0;
			// buffer read position used by input
			$rbufpos = 0; // $BUFSZ
			$buffer = $free = null;
			$this->pipeline[$inkey] = array(
				'read'       => $outkey,
				'ready'      => true,
				'buffer'     => &$buffer,
				'wpbuf'      => &$wbufpos,
				'rpbuf'      => &$rbufpos,
				'bufferfree' => &$free
			);
			$this->pipeline[$outkey] = array(
				'ready'      => false,
				'buffer'     => &$buffer,
				'write'      => $inkey,
				'wpbuf'      => &$wbufpos,
				'rpbuf'      => &$rbufpos,
				'bufferfree' => &$free
			);

			return true;
		}

		public function &getPipePeer()
		{
			return $this->proc_peer;
		}

		private function lookupPipeline($lookup)
		{
			return isset($this->pipeline[$lookup]) ?
				$this->pipeline[$lookup] : false;
		}

		private function _kill()
		{
			$pstatus = $this->getProcessStatus();

			return posix_kill($pstatus['pid'], 9);
		}

		/**
		 * Get process status
		 *
		 * @return array|null
		 */
		public function getProcessStatus(): ?array
		{
			if (!$this->proc_instance) {
				return null;
			}

			return proc_get_status($this->proc_instance) ?: null;
		}

		private function addOutput($pipe, $data)
		{
			if (is_resource($pipe)) {
				$pipe = $this->resource2Descriptor($pipe);
			}
			$this->channelData[$pipe][] = $data;
		}

		public function pipe($cmd, ...$args)
		{
			$peer = call_user_func_array(array(static::class, 'create'), $args);
			$this->pipeProcess($peer);

			return $this;
		}

		/**
		 * Attach another program's output to program input
		 *
		 * @param Util_Process $peer processs created by create()
		 * @return object
		 */
		public function pipeProcess(Util_Process &$peer)
		{
			$this->proc_peer = &$peer;
			$this->setEncoding('binary', 'stdin');
			$this->setDescriptorOption('stdin', 'close', 0);

			return $this;
		}

		public function setEncoding($mode, $fd = null)
		{
			$mod = '';
			if ($this->proc_instance) {
				return warn($this->prog_name . ': process has started, encoding change ignored');
			}
			if ($mode == 'binary') {
				$mod = 'b';
			}

			if ($fd) {
				$descriptors = array($fd);
			} else {
				$descriptors = array_keys($this->descriptors);
			}
			foreach ($descriptors as $fd) {
				$fd = $this->alias2Descriptor($fd);
				$fdmode = &$this->descriptors[$fd][1];
				$fdmode = $fdmode[0] . $mod;
			}

			return $this;
		}

		public function pipeDescriptor($read, $write)
		{
			$this->setDescriptorOption($write, 'close', 0);

			return $this->connectPipeline($read, $write);
		}

		/**
		 * Force a delayed start
		 *
		 * Used in conjunction with setOption("run", false)
		 *
		 * @return bool|Util_Process
		 */
		public function forceRun()
		{
			if ($this->getOption('run')) {
				return true;
			}

			return $this->_run();
		}

		public function running()
		{
			$inst = $this->getProcessStatus();

			return is_array($inst) && $inst['running'];
		}

		/**
		 * Transform process data
		 * Format options:
		 * ---------------
		 * esprit: traditional apnscp esprit format
		 *    stderr, stdout, output, error, return, errno fields
		 *
		 * @param  string $fmt data format
		 * @return mixed
		 */
		public function format($fmt = 'apnscp')
		{
			$fmt = $fmt ?: 'apnscp';
			if ($fmt === 'apnscp') {
				return $this->formatDataCallProc();
			}

			if (!is_callable($fmt)) {
				fatal("formatter specified `%s' is not callable", $fmt);
			}

			return $fmt();
		}

		/**
		 * Util_Process output into conventional call_proc() output
		 *
		 * @param  string $exit exit code
		 * @return array
		 */
		private function formatDataCallProc()
		{
			$exit = $this->getExit();
			$success = $this->exitSuccess();
			foreach ($this->getPipeNames() as $pipe) {
				if (is_numeric($pipe)) {
					continue;
				}
				$opts = $this->getDescriptorOptions($pipe);
				if (isset($opts['close'])) {
					continue;
				}
				$output[$pipe] = $this->getOutput($pipe);
			}
			if ($output['stderr'] && $this->getOption('reporterror')) {
				if (!$success) {
					if (!$this->getOption('mute_stderr')) {
						error($this->prog_name . ': ' . $output['stderr']);
					}
				} else if (!$this->getOption('mute_stderr') && \Error_Reporter::is_verbose(\Error_Reporter::E_INFO)) {
					info($this->prog_name . ': ' . $output['stderr']);
				}
			}
			if ($this->getOption('mute_stdout')) {
				$output['stdout'] = null;
			}
			$response = array(
				'output'  => &$output['stdout'],
				'errno'   => $exit,
				'return'  => $exit,
				'error'   => &$output['stderr'],
				'success' => $success
			);
			return array_merge($output, $response);
		}

		/**
		 * Return process exit code
		 *
		 * @return int
		 */
		public function getExit()
		{
			return $this->exitStatus;
		}

		public function exitSuccess()
		{
			if (!$this->exits) {
				$this->exits = '/^0$/';
			}
			if (ctype_digit($this->exits)) {
				$this->exits = (array)$this->exits;
			}
			if (is_array($this->exits)) {
				$exits = '/^' . str_replace('-', '\-', implode('|', $this->exits)) . '$/';
			} else {
				$exits = $this->exits;
			}
			return (bool)preg_match($exits, $this->exitStatus);
		}

		public function getOutput($pipe)
		{
			$pipe = $this->alias2Descriptor($pipe);
			if (!isset($this->channelData[$pipe])) {
				return '';
			}

			return implode('', $this->channelData[$pipe]);
		}

		/*
		 * Assign file descriptor
		 *
		 *
		 * @param  int    $fd    fd channel
		 * @param  string $type  descriptor type (pipe, file)
		 * @param  string $mode  file mode {@link http://php.net/fopen fopen() modes}
		 * @param  string $alias fd name
		 * @return object instance of self
		 */

		/**
		 * Add environment exported to child process via putenv()
		 *
		 * @param $name environment variable name
		 * @param $val  value
		 * @return $this instance
		 */
		public function addEnvironment($name, $val)
		{
			if (isset($this->_env[$name])) {
				error("cannot overwrite environment var `%s'", $name);

				return $this;
			}
			$this->_env[$name] = $val;

			return $this;
		}

		/**
		 * Set resource limits for process
		 *
		 * @param array $limits key => value limits
		 */
		public function setResource($limits): void
		{
			$callbacks = array_column($this->callbacks['open'], 'namespace');
			// ignore duplicate calls
			if (\in_array('resource', $callbacks, true)) {
				return;
			}

			$this->addCallback(static function (string $cmd) use ($limits) {
				foreach ($limits as $res => $value) {
					if (!Util_Ulimit::set($res, $value)) {
						error("Failed to set limit `%s'", $res);
					}
				}
			}, 'open.resource');

		}

		/**
		 * Set environment variable overwriting if set
		 *
		 * @param mixed $name
		 * @param mixed $val
		 * @return $this instance
		 */
		public function setEnvironment($name, $val = null)
		{
			if (is_array($name) && is_null($val)) {
				$this->_env = array_merge($this->_env, $name);
			} else {
				if (is_array($name) && !is_null($val)) {
					// multiple vars, same value
					$this->_env = array_merge($this->_env, array_fill_keys($name, $val));
				} else {
					$this->_env[$name] = $val;
				}
			}

			return $this;
		}

		public function unsetEnvironment($var)
		{
			if (isset($this->_env[$var])) {
				unset($this->_env[$var]);
			}

			return $this;
		}

		public function getEnvironment()
		{
			return $this->_env;
		}

		/**
		 * Terminate process
		 *
		 * @return int exit value
		 */
		public function close()
		{
			$peer = $this->getPipePeer();
			if ($peer) {
				$peer->close();
			}
			$exit = proc_close($this->proc_instance);

			if ($exit > -1) {
				// previously called through SIGCHLD handler
				$this->setExit($exit);
			}
			$this->callback('close', $this->exitStatus);

			return $this;
		}

		private function setExit($code)
		{
			$this->exitStatus = $code;
		}

		/**
		 * Read pipe input
		 *
		 * @param  int|resource $pipe
		 * @param  int          $length
		 * @param  int          $offset
		 * @return string
		 */
		public function pipeRead($pipe, $length = -1, $offset = 0)
		{
			$pipe = $this->getPipe($pipe);

			return stream_get_contents($pipe, $length, $offset);
		}

		/**
		 * Write to pipe
		 *
		 * @param  int|resource $pipe
		 * @param  string       $data
		 * @param  int          $length
		 * @return int                  number of bytes written
		 */
		public function pipeWrite($pipe, $data, $length = null)
		{
			$pipe = $this->getPipe($pipe);

			return fwrite($pipe, $data, $length);
		}

		public function getExits()
		{
			return $this->exits;
		}

		public function setExits($exits = '/./')
		{
			$this->exits = $exits;

			return $this;
		}

		/**
		 * Return process command
		 *
		 * @return string
		 */
		public function getCommand()
		{
			return $this->cmd;
		}

		/**
		 * Set process option
		 *
		 * Create callback methods using camel case
		 * _ is stripped, e.g. tv_usec -> setTvUsec
		 *
		 * @param string $name option name
		 * @param mixed  $val
		 * @return $this
		 */
		public function setOption($name, $val = null)
		{
			$fn = 'set' . str_replace('_', '', ucwords($name, '_'));
			if (method_exists($this, $fn)) {
				$this->$fn($val);
				return $this;
			}

			if (null === $val && isset($this->opts[$name])) {
				unset ($this->opts[$name]);
			} else {
				$this->opts[$name] = $val;
			}
			return $this;
		}

		/**
		 * Add unshare resource to execution context
		 *
		 * @param \Util\Process\Unshare $pool
		 */
		public function setUnshare(\Util\Process\Unshare $pool)
		{
			$callbacks = array_column($this->callbacks['open'], 'namespace');
			if (\in_array('unshare', $callbacks, true)) {
				warn('unshare already set for program - overwriting');
				return;
			}

			$this->addCallback(function (string $cmd) use ($pool) {
				if (is_array($this->cmd)) {
					debug("Collapsing execvp into execl('/bin/sh' ...)");
					$this->cmd = implode(' ', array_map('escapeshellarg', $this->cmd));
				}
				$pool->setCommand($this->cmd);
				$template = (string)$pool;
				$this->cmd = $template;
				if (!posix_seteuid(0)) {
					fatal('Failed to unshare');
				}
			}, 'open.unshare');

			$this->addCallback(function (string $cmd) {
				if (isset($this->opts['unshare']) && is_resource($this->opts['unshare'])) {
					fclose($this->opts['unshare']);
				}
			}, 'close.unshare');
		}

		public function setTee($file)
		{
			if (!file_exists($file)) {
				$file = tempnam(TEMP_DIR, 'tee');
			} else if (filesize($file) > 0) {
				warn("Tee file `%s' has data, appending", $file);
			}
			$fp = fopen($file, 'a');
			flock($fp, LOCK_EX);
			$this->addCallback(static function ($buffer, $total, $pipe) use ($fp) {
				fwrite($fp, $buffer);
			}, 'read');
			$this->addCallback(static function () use ($fp) {
				fclose($fp);
			}, 'close');
		}

		/**
		 *
		 * @param callable $function
		 * @param string   $when callback context {@link $this->callbacks}
		 * @param array    $args additional arguments to pass
		 * @return void
		 */
		public function addCallback(callable $function, $when = 'read', ...$args)
		{
			$namespace = null;
			if (false !== ($pos = strpos($when, '.'))) {
				$namespace = substr($when, ++$pos);
				$when = substr($when, 0, --$pos);
			}

			if (!isset($this->callbacks[$when])) {
				return error($when . ': invalid callback context');
			}

			if (!is_callable($function)) {
				$function = Closure::fromCallable($function);
			}

			$this->callbacks[$when][] = array(
				'function'  => $function,
				'args'      => $args,
				'namespace' => $namespace
			);
		}

		public function getDescriptorOption($fd, $opt)
		{
			$fd = $this->alias2Descriptor($fd);

			return isset($this->opts['fd'][$fd]) &&
			isset($this->opts['fd'][$fd][$opt]) ?
				$this->opts['fd'][$fd][$opt] :
				array();
		}

		protected function setTimeout($maxwait = null)
		{
			static $originalHandler;

			if (!$maxwait) {
				$this->deleteCallback('exec.timeout');
				$this->deleteCallback('close.timeout');

				return true;
			}
			if (null === $originalHandler) {
				// save old alarm handler
				$originalHandler = pcntl_signal_get_handler(SIGALRM);
			}
			// @XXX it may leave children behind much like Evander Holyfield
			pcntl_signal(SIGALRM, static function () {
				posix_kill(posix_getpid(), SIGKILL);
			}, true);
			// @XXX sleep has the potential to break this implementation
			// but this will guard against accidental daemonization
			$this->addCallback(static function () use ($maxwait) {
				// n second timeout
				pcntl_alarm($maxwait);
			}, 'exec.timeout');
			$this->addCallback(static function () use ($originalHandler) {
				pcntl_alarm(0);
				pcntl_signal(SIGALRM, $originalHandler ?? SIG_DFL);
			}, 'close.timeout');

			return true;
		}

		/**
		 * Delete a registered callback
		 *
		 * @param string $when      callback context (read, write, close)
		 * @param string $namespace optional namespace
		 * @return bool|void
		 */
		public function deleteCallback($when, $namespace = '')
		{
			if (false !== ($pos = strpos($when, '.'))) {
				$namespace = substr($when, $pos);
				$when = substr($when, 0, --$pos);
			}
			if (!isset($this->callbacks[$when])) {
				return error("unknown callback context `%s'", $when);
			}
			if (!$namespace) {
				$this->callbacks[$when] = array();

				return true;
			}
			foreach ($this->callbacks[$when] as $key => $cb) {
				if ($cb['namespace'] === $namespace) {
					unset($this->callbacks['exec'][$key]);
				}
			}

			return true;
		}

		/**
		 * Check signal interruption
		 *
		 * @return bool
		 */
		private function isInterruptedSyscall(): bool
		{
			if (!($err = error_get_last())) {
				return false;
			}
			return false !== strpos($err['message'], 'Interrupted system call');
		}

		/**
		 * Close and remove a stream from stream_select()
		 *
		 * @param mixed $fdname     fd name
		 * @param       $streams    all streams
		 * @param       $lookupMap  lookup map
		 * @return bool
		 */
		private function streamClose($fd, &$streams, $lookupMap)
		{
			$fd = $this->getPipe($fd);

			$key = $this->resource2String($fd);
			if (!isset($lookupMap[$key])) {
				return false;
			}

			$streamInfo = $lookupMap[$key];
			$type = $streamInfo['type'];
			$resource = &$fd;

			$idx = array_search($resource, $streams[$type]);

			// Indexes change every time a stream is pruned from $streams
			if ($idx === false) {
				warn($fd . ': unknown stream');
			}
			$this->pipeClose($resource);
			if ($type == 'write' && is_resource($streamInfo['buffer'])) {
				fclose($streamInfo['buffer']);
			}
			unset($this->pipeline[$key]);
			unset($streams[$type][$idx]);
		}

		private function streamOpen($fd, &$streams, $map)
		{
			$pipe = $this->getPipe($fd);
			if (!$pipe) {
				return false;
			}
			$key = $this->resource2String($pipe);
			if (!isset($map[$key])) {
				return false;
			}

			$streamInfo = $map[$key];
			$streamType = $streamInfo['type'];
			$idx = array_search($pipe, $streams[$streamType]);
			if ($idx === false) {
				return false;
			}

			return is_resource($streams[$streamType][$idx]);
		}

		private function flushOutput($pipe)
		{
			$pipe = $this->resource2Descriptor($pipe);
			$output = $this->getOutput($pipe);
			$this->channelData[$pipe] = array();

			return $output;
		}

		private function setPipelineReady($id, $state)
		{
			$this->pipeline[$id]['ready'] = $state;
		}

		private function pipelineReady($id)
		{
			if (!isset($this->pipeline[$id])) {
				return null;
			}

			return $this->pipeline[$id]['ready'];
		}

		private function processPipeline(&$in, &$out)
		{

		}

		private function _debugRing($r, $w, $sz, $buffer)
		{
			$width = 80;
			$posr = floor($r['pos'] / $sz * $width);
			$posw = floor($w['pos'] / $sz * $width);
			$fmt = "|%%%ds%%%ds%%-%ds%%s\n";
			$fw = $width;
			if ($posr == $posw) {
				if ($r['pos'] == $w['pos']) {
					$args = array('B', '', '', '|');
				} else {
					$args = array('*', '', '', '|');
				}
				$fmtargs = array($posr, $fw - $posr, 0);

			} else {
				if ($posr > $posw) {
					$fmtargs = array($posw, $posr - $posw, $fw - $posr);
					$args = array('w', '', 'r', '|');
				} else {
					$fmtargs = array($posr, $posw - $posr - 1, $fw - $posw);
					$args = array('r', '', 'w', '|');
				}
			}

			if (max($posr, $posw) >= 80) {
				$max = array_pop($args);
				$border = array_pop($args);
				$args[] = $max;
				$args[] = $border;
			}
			$fmt = vsprintf($fmt, $fmtargs);
			$fmt = vsprintf($fmt, $args);
			print '+' . str_repeat('-', $width) . '+' . "\n";
			print $fmt;
			print '+' . str_repeat('-', $width) . '+' . "\n";
			$size = $w['pos'] - $r['pos'];
			if ($size < 0) {
				$size += $sz;
			}
			printf("read: %-10d write: %-10d pos: %-10d %12s(%5d/%5d bytes)\n\n", $r['pos'], $w['pos'], ftell($buffer),
				' ', $size, $sz);
		}
	}
