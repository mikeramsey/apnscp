<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use Opcenter\CliParser;

	/**
	 * Wrapper utility to backend *VirtDomain
	 *
	 * @author Matt Saladna <matt@apisnetworks.com>
	 */
	class Util_Account_Editor
	{
		use apnscpFunctionInterceptorTrait;
		use ContextableTrait;

		const EDIT_CMD = '/usr/local/sbin/EditDomain';
		const DELETE_CMD = '/usr/local/sbin/DeleteDomain';
		const ADD_CMD = '/usr/local/sbin/AddDomain';

		private $params = array();
		private $proc;
		private $mode;

		private $flags = [
			'output' => 'json'
		];

		public function __construct(\Auth_Info_Account $accountctx = null, \Auth_Info_User $context = null)
		{

			if ($context) {
				if ($accountctx && $context->site !== $accountctx->site) {
					fatal('Account marker %s does not match authentication context %s', $accountctx->site,
						$context->site);
				}
			} else if ($accountctx) {
				$context = \Auth::context(null, $accountctx->site);
			} else {
				// called via instantiateContextable
				$context = $this->getAuthContext();
			}

			if (!$context->level & (PRIVILEGE_SITE|PRIVILEGE_USER)) {
				fatal("Assertion failed: context is not site");
			}
			$this->setContext($context);
		}

		public function setProcess(Util_Process $proc)
		{
			$this->proc = $proc;

			return $this;
		}

		/**
		 * Import configuration from site
		 *
		 * @return $this
		 */
		public function importConfig(): self
		{
			// always pull the new params which is a composite of new and cur
			$config = Auth_Info_Account::factory(\Auth::context(null, $this->getAuthContext()->site));
			$this->params = array_replace($config->cur, $config->new, $this->params);

			return $this;
		}

		/**
		 * Set operation flags
		 *
		 * @param      $flags
		 * @param null $vars
		 * @return $this
		 */
		public function setFlags($flags, $vars = null): self {
			if (!is_array($flags)) {
				$flags = [$flags => $vars];
			}

			$this->flags = array_replace($this->flags, $flags);
			return $this;
		}

		/**
		 * Set site configuration
		 * @param string|array $service comma
		 * @param string|null $param
		 * @param mixed $val
		 * @return $this
		 */
		public function setConfig($service, $param = null, $val = null)
		{
			if (is_array($service)) {
				foreach ($service as $name => $val) {
					// accept diskquota,quota or diskquota.quota
					[$sc, $sn] = preg_split('/[,.]/', $name, 2);
					$this->setConfig($sc, $sn, $val);
				}
				return $this;
			}
			if (!$param) {
				fatal('Missing service parameter');
			}
			if (!isset($this->params[$service])) {
				$this->params[$service] = array();
			}
			$this->params[$service][$param] = $val;

			return $this;
		}

		public function delete()
		{
			$this->setMode('delete');

			return $this->_exec();
		}

		public function setMode($mode)
		{
			if ($mode === 'delete' || $mode === 'edit' || $mode === 'add') {
				$this->mode = $mode;

				return $this;
			}
			fatal("unknown account editor mode `%s'", $mode);
		}

		/**
		 * Run EditVirtDomain to activate configuration changes
		 *
		 * @return bool
		 */
		private function _exec()
		{
			if ($this->getApnscpFunctionInterceptor()->call('auth_is_inactive')) {
				return error('cannot edit account in inactive state');
			}
			$cmd = $this->getCommand();
			// use a different post-processor
			if (!$this->proc) {
				$proc = new Util_Process();
			} else {
				$proc = $this->proc;
			}
			// make sure editor exports development mode
			if (is_debug()) {
				$proc->setEnvironment('DEBUG', 1);
				$proc->setEnvironment('VERBOSE', DEBUG_BACKTRACE_QUALIFIER);
			}
			$ret = $proc->run(
				$cmd
			);

			return $this->_parseOutput($ret['output']) && $ret['success'];
		}

		public function getCommand()
		{
			return $this->_buildCommand(
				$this->_buildConfig($this->params)
			);
		}

		private function _buildConfig($services)
		{
			ksort($services);
			foreach ($services as &$v) {
				ksort($v);
			}
			unset($v);

			return \Opcenter\CliParser::commandifyConfiguration($services);
		}

		private function _buildCommand($config)
		{
			$cmd = $this->_getCommandFromMode() .
				' ' . CliParser::buildFlags($this->flags) . ' ' . $config;

			if ($this->mode !== 'add') {
				$site = $this->getAuthContext()->site;
				$cmd .= ' ' . $site;
			}

			return $cmd;
		}

		private function _getCommandFromMode()
		{
			$mode = $this->mode;
			if (!$mode) {
				return fatal('no account editor mode set');
			}

			if ($mode !== 'add' && $mode !== 'delete' && $mode !== 'edit') {
				fatal("unknown account editor mode `%s'", $mode);
			}

			return constant(__CLASS__ . '::' . strtoupper($mode) . '_CMD');
		}

		/**
		 * Breakdown Ensim output into meaningful statuses
		 *
		 * @param string $output
		 * @return bool command successful
		 */

		private function _parseOutput(string $output)
		{
			if (!\Error_Reporter::merge_json($output)) {
				// bad JSON decoding?
				return error("Failed to decode JSON - `%s' terminated unexpected? %s",
					$this->_getCommandFromMode(),
					is_debug() ? $output : null
				);
			}

			return !\Error_Reporter::is_error();
		}

		public function edit()
		{
			$this->setMode('edit');

			if (!$this->_exec()) {
				return false;
			}
			// flush cache on successful update
			$this->getAuthContext()->reset();
			return true;
		}

		public function add()
		{
			$this->setMode('add');

			return $this->_exec();
		}
	}
