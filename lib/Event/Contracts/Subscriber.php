<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */


	namespace Event\Contracts;

	interface Subscriber
	{
		/**
		 * Process event
		 *
		 * @param string    $event event name
		 * @param Publisher $caller
		 * @return mixed
		 */
		public function update($event, Publisher $caller);
	}
