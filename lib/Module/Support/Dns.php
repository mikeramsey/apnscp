<?php declare(strict_types=1);

/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
 */

namespace Module\Support;
use Module_Skeleton;
use NamespaceUtilitiesTrait;
use Opcenter\Dns\Record;

abstract class Dns extends Module_Skeleton implements \Opcenter\Contracts\Hookable, \Module\Skeleton\Contracts\Proxied
{
	use NamespaceUtilitiesTrait;

	const DEPENDENCY_MAP = [
		'siteinfo'
	];
	// @var Record
	protected $zoneCache = [];

	/**
	 * Add record to cache
	 *
	 * @param \Opcenter\Dns\Record $r
	 */
	protected function addCache(\Opcenter\Dns\Record $r): void
	{
		$key = $this->getCacheKey($r);
		array_set(
			$this->zoneCache[$r->getZone()],
			$key,
			array_merge(array_get($this->zoneCache[$r->getZone()], $key, []), [$r])
		);
	}

	protected function removeCache(\Opcenter\Dns\Record $r): void
	{
		$key = $this->getCacheKey($r);
		if (!array_has($this->zoneCache[$r->getZone()], $key)) {
			return;
		}
		$records = array_get($this->zoneCache[$r->getZone()], $key);
		if (!$records) {
			// empty cache
			array_forget(
				$this->zoneCache[$r->getZone()],
				$key
			);
			return;
		}
		array_walk_recursive($records, static function (Record $v) use ($r) {
			// soft delete record forcing cache update next modification
			if ($v->is($r)) {
				$v->setMeta('id', null);
			}
		});
		array_set(
			$this->zoneCache[$r->getZone()],
			$key,
			$records
		);
	}

	protected function getCacheKey(\Opcenter\Dns\Record $r): string
	{
		if (!isset($this->zoneCache[$r->getZone()])) {
			$this->zoneAxfr($r->getZone());
		}

		return 'records.' . strtoupper($r['rr']) . '.' . ($r['name'] ?: '@');
	}

	/**
	 * Get internal CF zone ID
	 *
	 * @param string $domain
	 * @return null|string
	 */
	protected function getZoneId(string $domain): ?string
	{
		return $domain;
	}

	protected function getRecordId(\Opcenter\Dns\Record $r): ?string
	{
		// @var \Opcenter\Dns\Record[] $cache
		$idx = $this->getCacheKey($r);
		if (null === ($cache = array_get($this->zoneCache[$r->getZone()], $idx))) {
			return null;
		}

		$id = false;

		foreach ($cache as $record => $c) {
			// @var \Opcenter\Dns\Record $r
			if (\is_array($c)) {
				// nested records e.g. foo.bar.baz.com while examining bar.baz.com
				continue;
			}
			if ($c->is($r)) {
				$id = $c->getMeta('id');
				break;
			}
		}
		if ($id === null) {
			// soft deletion
			unset($this->zoneCache[$r->getZone()]);
			return $this->getRecordId($r);
		}
		return (string)$id ?: null;
	}

	protected function getRecordFromCache(Record $r): ?Record {
		$key = $this->getCacheKey($r);
		if (!isset($this->zoneCache[$r->getZone()])) {
			return null;
		}
		return collect(array_get($this->zoneCache[$r->getZone()], $key, null))->flatten()->first();
	}

	/**
	 * Create a record using appropriate driver
	 *
	 * @param string $zone
	 * @param array  $data
	 * @return mixed
	 */
	public static function createRecord(string $zone, array $data = []): \Opcenter\Dns\Record
	{
		$cls = static::getNamespace() . '\\Record';
		if (!class_exists($cls)) {
			$cls = \Opcenter\Dns\Record::class;
		}

		return new $cls($zone, $data);
	}
}