<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	/**
	 * WebApp Contract
	 *
	 *
	 * @package core
	 */

	namespace Module\Support\Webapps\Contracts;

	/**
	 * Interface Webapp
	 *
	 * @package Module\Support\Webapps\Contracts
	 */
	interface Webapp
	{
		/**
		 * Install application
		 *
		 * @param string $hostname domain or subdomain to install application
		 * @param string $path     optional path under hostname
		 * @param array  $opts     additional install options
		 * @return bool
		 */
		public function install(string $hostname, string $path = '', array $opts = array()): bool;

		/**
		 * Get plugin status
		 *
		 * @param string      $hostname domain or subdomain
		 * @param null|string $path     optional path under hostname
		 * @param null|string $plugin   specific plugin to query
		 * @return array|bool
		 */
		public function plugin_status(string $hostname, string $path = '', string $plugin = null);

		/**
		 * Install and activate plugin
		 *
		 * @param string $hostname domain or subdomain of application
		 * @param string $path     optional path component of application
		 * @param string $plugin   plugin name
		 * @param string $version  optional plugin version
		 * @return bool
		 */
		public function install_plugin(string $hostname, string $path, string $plugin, string $version = ''): bool;

		/**
		 * Uninstall a plugin
		 *
		 * @param string $hostname domain or subdomain of application
		 * @param string $path     optional path component of application
		 * @param string $plugin   plugin name
		 * @param bool   $force    delete even if plugin activated
		 * @return  bool
		 */
		public function uninstall_plugin(
			string $hostname,
			string $path,
			string $plugin,
			bool $force = false
		): bool;

		/**
		 * Recovery mode to disable all plugins
		 *
		 * @param string $hostname subdomain or domain of WP
		 * @param string $path     optional path
		 * @return bool
		 */
		public function disable_all_plugins(string $hostname, string $path = ''): bool;

		/**
		 * Install a theme
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $theme   theme name
		 * @param string $version optional version
		 *
		 * @return bool
		 */
		public function install_theme(string $hostname, string $path, string $theme, string $version = null): bool;

		/**
		 * Get plugin status
		 *
		 * @param string      $hostname domain or subdomain
		 * @param null|string $path     optional path under hostname
		 * @param null|string $theme    specific theme to query
		 * @return array|bool
		 */
		public function theme_status(string $hostname, string $path = '', string $theme = null);

		/**
		 * Uninstall WP from a location
		 *
		 * @param   string $hostname
		 * @param   string $path
		 * @param   string $delete "all", "db", or "files"
		 * @return  bool
		 */
		public function uninstall(string $hostname, string $path = '', string $delete = 'all'): bool;

		/**
		 * Get database configuration for application
		 *
		 * @param string $hostname domain or subdomain of wp blog
		 * @param string $path     optional path
		 * @return bool|array
		 */
		public function db_config(string $hostname, string $path = '');

		/**
		 * Check if version is latest or get latest version
		 *
		 * @param   string|null $version version to query against
		 * @param string|null   $branchcomp
		 * @return int|string
		 */
		public function is_current(string $version = null, string $branchcomp = null);

		/**
		 * Get all available versions sorted in ascending semantic version
		 *
		 * @return array
		 */
		public function get_versions(): array;

		/**
		 * Change admin credentials
		 *
		 * @param string $hostname domain or subdomain of application
		 * @param string $path     optional path component
		 * @param array  $fields   fields to change
		 * @return  bool
		 */
		public function change_admin(string $hostname, string $path, array $fields): bool;

		/**
		 * Get the primary admin for a WP instance
		 *
		 * @param string      $hostname
		 * @param null|string $path
		 * @return string|null admin or null on failure
		 */
		public function get_admin(string $hostname, string $path = ''): ?string;

		/**
		 * Get installed version
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return string|null|bool version number, null if not app or false on failure
		 */
		public function get_version(string $hostname, string $path = ''): ?string;

		/**
		 * Calculate next version in hierarchy
		 *
		 * @param string $version       base version
		 * @param string $maximalbranch max branch to inspect
		 * @return string|null
		 */
		public function next_version(string $version, string $maximalbranch = '99999999.99999999.99999999'): ?string;

		/**
		 * Location is a valid webapp install
		 *
		 * @param string $hostname or $docroot
		 * @param string $path
		 * @return bool
		 */
		public function valid(string $hostname, string $path = ''): bool;

		/**
		 * Update core, plugins, and themes atomically
		 *
		 * @param string $hostname subdomain or domain
		 * @param string $path     optional path under hostname
		 * @param string $version
		 * @return bool
		 */
		public function update_all(string $hostname, string $path = '', string $version = null): bool;

		/**
		 * Update WordPress to latest version
		 *
		 * @param string $hostname domain or subdomain under which WP is installed
		 * @param string $path     optional subdirectory
		 * @param string $version
		 * @return bool
		 */
		public function update(string $hostname, string $path = '', string $version = null): bool;

		/**
		 * Update WordPress plugins
		 *
		 * @param string $hostname domain or subdomain
		 * @param string $path     optional path within host
		 * @param array  $plugins
		 * @return bool
		 */
		public function update_plugins(string $hostname, string $path = '', array $plugins = array()): bool;

		/**
		 * Update themes
		 *
		 * @param string $hostname subdomain or domain
		 * @param string $path     optional path under hostname
		 * @param array  $themes
		 * @return bool
		 */
		public function update_themes(string $hostname, string $path = '', array $themes = array()): bool;

		/**
		 * Remove a theme
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $theme
		 * @param bool   $force deactivate if necessary before removal
		 * @return bool
		 */
		public function uninstall_theme(string $hostname, string $path, string $theme, bool $force = false): bool;

		/**
		 * Web application supports fortification
		 *
		 * @param string      $hostname
		 * @param string      $path
		 * @param string|null $mode optional mode (min, max)
		 * @return bool
		 */
		public function has_fortification(string $hostname, string $path = '', string $mode = null): bool;

		/**
		 * Supported Fortification modes
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return array
		 */
		public function fortification_modes(string $hostname, string $path = ''): array;

		/**
		 * Restrict write-access by the app
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $mode
		 * @param array  $args additional mode settings
		 * @return bool
		 */
		public function fortify(string $hostname, string $path = '', string $mode = 'max', $args = []): bool;

		/**
		 * Relax permissions to allow write-access
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 * @internal param string $mode
		 */
		public function unfortify(string $hostname, string $path = ''): bool;

		/**
		 * @param string       $hostname hostname
		 * @param string       $path     optional path
		 * @param string|array $param    array of reconfigurables or single value
		 * @param mixed        $value    value to set in single-value mode
		 * @return bool
		 */
		public function reconfigure(string $hostname, string $path, $param, $value = null): bool;

		/**
		 * List of reconfigurable parameters
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return array
		 */
		public function reconfigurables(string $hostname, string $path = ''): array;

		/**
		 * Get reconfigurable value
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string|string[] $setting
		 * @return mixed
		 */
		public function get_reconfigurable(string $hostname, string $path, $setting);

		/**
		 * Snapshot web application
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param string $comment
		 * @return bool
		 */
		public function snapshot(string $hostname, string $path = '', string $comment = 'snapshot'): bool;

		/**
		 * Rollback specified snapshot
		 *
		 * @param string      $hostname
		 * @param string      $path
		 * @param string|null $commit optional commit
		 * @return bool
		 */
		public function rollback(string $hostname, string $path = '', string $commit = null): bool;
	}