<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2018
	 */

	declare(strict_types=1);

	namespace Module\Support\Webapps;

	use Lararia\Contracts\ExplicitFlush;
	use Module\Support\Webapps;
	use Module\Support\Webapps\MetaManager\Meta;
	use Symfony\Component\Yaml\Yaml;

	class MetaManager implements \ArrayAccess, ExplicitFlush
	{
		use \ContextableTrait;
		use \apnscpFunctionInterceptorTrait {
			__call as protected callIgnored;
		}

		/**
		 * @var \Preferences preference handler
		 */
		protected $preferenceInstance;
		/*
 		 * @var array preferences map
		 */
		protected $webappMeta;
		public $dirty = false;

		// @var array mappings of [symlink => referent]
		protected $aliases = [];

		/**
		 * @var array
		 */
		protected $preferenceOrigin;

		/**
		 * @var Meta[]
		 */
		private $metaInstances = [];

		private function __construct()
		{
			$this->__wakeup();
		}

		public function __sleep()
		{
			$this->__destruct();
			return array_keys(array_except(get_object_vars($this), ['webappMeta', 'preferenceInstance', 'preferenceOrigin', 'apnscpFunctionInterceptor']));
		}

		public function __wakeup()
		{
			$this->preferenceInstance = \Preferences::factory($this->getAuthContext());
			if (!array_has($this->preferenceInstance, Webapps::APPLICATION_PREF_KEY)) {
				$this->freshenAuthContext();
				$this->preferenceInstance->unlock($this->getApnscpFunctionInterceptor());
				data_set($this->preferenceInstance, Webapps::APPLICATION_PREF_KEY, []);
			}
			$parts = explode('.', Webapps::APPLICATION_PREF_KEY);

			$this->preferenceOrigin = &$this->preferenceInstance->offsetGet(array_shift($parts));
			foreach ($parts as $p) {
				$this->preferenceOrigin = &$this->preferenceOrigin[$p];
			}
			reset ($this->preferenceInstance);
		}

		public function __destruct()
		{
			// Inspect to ensure dirty set, otherwise setOption won't
			// set dirty flag in MetaManager
			if (!$this->dirty) {
				return;
			}
			reset($this->preferenceOrigin);
			$this->sync();
		}

		public function setDirtyFlag(): void
		{
			$this->dirty = true;
		}

		public function sync(): self
		{
			// call explicit sync in case circular references hold up proper destruction
			// see tests/Wordpress
			$this->preferenceInstance->unlock($this->getApnscpFunctionInterceptor());
			unset($this->metaInstances, $this->webappMeta, $this->preferenceOrigin);
			$this->preferenceInstance->sync(true);
			$this->dirty = false;

			return $this;
		}

		public static function factory(\Auth_Info_User $ctx): self {
			return self::instantiateContexted($ctx);
		}

		/**
		 * Forget a docroot
		 *
		 * @param string $docroot
		 * @return self
		 */
		public function forget(string $docroot): self
		{
			$docroot = rtrim($docroot, '/');
			$this->dirty = true;
			unset(
				$this->metaInstances[$docroot],
				$this[$docroot],
				$this->aliases[$docroot]
			);

			if ($docroot !== ($chk = $this->normalizeRoot($docroot))) {
				return $this->forget($chk);
			}
			return $this;
		}

		/**
		 * Clone docroot meta
		 *
		 * @param string $oldroot
		 * @param string $destroot
		 * @return bool
		 */
		public function clone(string $oldroot, string $destroot): bool
		{
			if (!$this->exists($oldroot)) {
				return false;
			}
			$old = $this->get($oldroot);
			$destroot = rtrim($destroot, '/');
			$this->set($destroot, $old->toArray());
			return true;
		}

		/**
		 * Move a docroot
		 *
		 * @param string $oldroot
		 * @param string $newroot
		 * @return bool
		 */
		public function rename(string $oldroot, string $newroot): bool
		{
			if (!$this->exists($oldroot)) {
				return false;
			}

			unset($this->aliases[rtrim($newroot, '/')], $this->aliases[rtrim($oldroot, '/')]);
			$old = $this->get($oldroot)->toArray();
			$this->forget($oldroot);
			$newroot = rtrim($newroot, '/');
			$this->set($newroot, $old);
			return true;
		}

		/**
		 * Docroot is detected
		 *
		 * @param string $docroot
		 * @return bool
		 */
		protected function exists(string $docroot): bool
		{
			$docroot = $this->normalizeRoot($docroot);
			return isset($this[$docroot]);
		}

		public function merge(string $docroot, array $meta): self
		{
			$oldmeta = $this->get($docroot);
			$docroot = rtrim($docroot, '/');
			return $this->set($docroot, $meta + $oldmeta->toArray());
		}

		public function replace(string $docroot, array $meta): self
		{
			$oldmeta = $this->get($docroot);
			$docroot = rtrim($docroot, '/');
			return $this->set($docroot, array_replace($oldmeta->toArray(), $meta))->sync();
		}

		/**
		 * Get app metadata
		 *
		 * @param string $docroot
		 * @return MetaManager\Meta
		 */
		public function get(string $docroot): Meta
		{
			$docroot = $this->normalizeRoot($original = $docroot);

			if (!isset($this[$docroot])) {
				$this[$docroot] = [];
			} else if ($this[$docroot] instanceof Meta) {
				$this[$docroot] = $this[$docroot]->toArray();
			}

			// @XXX storing instances in class results in a reference counting deadlock
			// in which __destruct() is never called at end of scope
			return new Meta(
				$this,
				$original,
				$this->preferenceOrigin[$docroot]
			);
		}

		/**
		 * Normalize meta path taking into account symlinks
		 *
		 * @param string $docroot
		 * @return string
		 */
		private function normalizeRoot(string $docroot): string
		{
			$docroot = rtrim($docroot, '/');
			if (isset($this->aliases[$docroot])) {
				return $this->aliases[$docroot];
			}

			$stat = $this->callIgnored('file_stat', [$docroot]);

			return $this->aliases[$docroot] = ($stat && $stat['referent']) ? $stat['referent'] : $docroot;
		}

		/**
		 * Set app metadata
		 *
		 * @param string $docroot
		 * @param array  $meta
		 * @return self
		 */
		public function set(string $docroot, array $meta): self
		{
			$docroot = $this->normalizeRoot($docroot);
			$this->dirty = true;
			unset($this->metaInstances[$docroot]);
			$this[$docroot] = $meta;
			return $this;
		}

		public function offsetExists($offset)
		{
			return isset($this->preferenceOrigin[$offset]);
		}

		public function offsetGet($offset)
		{
			return $this->preferenceOrigin[$offset];
		}

		public function offsetSet($offset, $value)
		{
			$metaMaps = array_get($this->preferenceInstance, Webapps::APPLICATION_PREF_KEY, []);
			$metaMaps[$offset] = $value;
			array_set($this->preferenceInstance, Webapps::APPLICATION_PREF_KEY, $metaMaps);
		}

		public function offsetUnset($offset)
		{
			$metaMaps = array_get($this->preferenceInstance, Webapps::APPLICATION_PREF_KEY, []);
			unset($metaMaps[$offset]);
			array_set($this->preferenceInstance, Webapps::APPLICATION_PREF_KEY, $metaMaps);
		}
	}