<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support;

	use Crm_Module;
	use File_Module;
	use Letsencrypt_Module;
	use Mail;
	use Module_Skeleton;
	use Opcenter\Account\State;
	use Opcenter\Crypto\Ssl;
	use Opcenter\Filesystem;
	use Opcenter\Http\Apnscp;
	use Util_Account_Hooks;
	use Util_Process;
	use Opcenter\Crypto\Letsencrypt as LEService;

	/**
	 * Auxiliary support components to letsencrypt functions
	 *
	 * Class Module_Support_Letsencrypt
	 *
	 * @see Letsencrypt_Module
	 */
	abstract class Letsencrypt extends Module_Skeleton
	{
		/** @param string docroot for acme validation comprised of ACME_WORKDIR + ACME_URI_PREFIX */
		const ACME_WORKDIR = '/tmp/acme';
		/** @param string HTTP URI prefix for acme validation */
		const ACME_URI_PREFIX = '/.well-known/acme-challenge';

		const ACME_CERTIFICATE_BASE = '/storage/certificates';

		const SKIP_IP_PREFERENCE = LEService\Preferences::VERIFY_IP;

		protected function renewExpiringCertificates()
		{
			// @todo place certificate expirations into a data store
			$certs = (array)$this->_findCertificates();
			// server cert
			if ($this->certificateIssued(LEService::SYSCERT_NAME)) {
				$certs[] = LEService::SYSCERT_NAME;
			}
			foreach ($certs as $c) {
				$path = LEService::acmeSiteStorageDirectory($c) . '/cert.pem';
				if (!file_exists($path)) {
					continue;
				}
				$crt = file_get_contents($path);
				$x509 = $this->ssl_parse_certificate($crt);
				if (!$x509 || !LEService::expiring($x509)) {
					continue;
				}
				if (LETSENCRYPT_DEBUG) {
					warn("%s is eligible for renewal but %s is in Let's Encrypt testing mode. " .
						'Disable with %s then re-run to renew certificate.',
						$c,
						PANEL_BRAND,
						'cpcmd scope:set ssl.debug false'
					);
					continue;
				}

				if (!$this->_renew($c, $x509)) {
					dlog("Failed renewal on $c");
				}
			}
		}

		/**
		 * Enumerate certificate data directory to find profiles
		 *
		 * @return array|null
		 *
		 */
		private function _findCertificates()
		{
			$datadir = LEService::acmeSiteStorageDirectory('');
			if (!file_exists($datadir)) {
				return true;
			}
			$dh = opendir($datadir);
			if (!$dh) {
				return null;
			}
			$certs = array();
			while (false !== ($entry = readdir($dh))) {
				if (0 !== strncmp($entry, 'site', 4)) {
					continue;
				}
				if (!\Auth::get_domain_from_site_id(substr($entry, 4))) {
					warn("Orphaned certificate located for removed site `%s'", $entry);
					continue;
				}

				if (State::disabled($entry)) {
					debug("Site %s disabled - skipping", $entry);
					continue;
				}

				$certs[] = $entry;
			}

			return $certs;
		}

		/**
		 * ACME host-specific storage directory
		 *
		 * @param string $host
		 * @return string
		 */
		protected function acmeSiteStorageDirectory($host): string
		{
			return LEService::acmeDataDirectory() . '/certs/' .
				$this->canonicalizeServer($this->activeServer) . '/' . $host;
		}

		protected function certificateIssued($account = null)
		{
			if (!$account) {
				$account = $this->permission_level & PRIVILEGE_ADMIN ?
					LEService::SYSCERT_NAME : $this->site;
			}
			$prefix = LEService::acmeSiteStorageDirectory($account);

			return file_exists($prefix) && file_exists($prefix . '/cert.pem');
		}



		protected function _renew($site, $x509)
		{
			\Error_Reporter::flush_buffer();
			if ($site !== LEService::SYSCERT_NAME && State::disabled($site)) {
				return info("Site `%s' disabled, bypassing renewal", $site);
			}

			$ctx = substr($site, 0, 4) === 'site' ? \Auth::context(null, $site) : \Auth::context(null);
			$verifyip = data_get(
				\Preferences::factory($ctx),
				self::SKIP_IP_PREFERENCE,
				true
			);

			if ($site === LEService::SYSCERT_NAME) {
				$ret = $this->_renewSystemCertificate($x509);
			}  else {
				$afi = \apnscpFunctionInterceptor::factory($ctx);
				if ($ret = $afi->letsencrypt_renew($verifyip)) {
					$this->checkPhantomCertificate($afi);
				}
			}

			if ($ret || !LETSENCRYPT_NOTIFY_FAILURE || LEService::daysUntilExpiry($x509) > 5) {
				// squelch renewal warnings in early stages
				return $ret;
			}

			$cmd = 'cpcmd ';
			if ($site !== LEService::SYSCERT_NAME) {
				$cmd .= "-d ${site} ";
			}
			$cmd .= 'letsencrypt:renew ' . ($verifyip ? 'true' : 'false');
			Mail::send(
				Crm_Module::COPY_ADMIN,
				'Automated renewal failed on ' . $site,
				"renew manually $site - " . SERVER_NAME . "\n" .
				$cmd . "\n" .
				"\n\nError log:" . var_export(\Error_Reporter::flush_buffer(), true)
			);

			return false;
		}

		protected function checkPhantomCertificate(\apnscpFunctionInterceptor $afi)
		{
			$crt = $afi->ssl_get_certificate();
			$key = $afi->ssl_get_private_key();
			if ($afi->ssl_valid($crt, $key)) {
				return;
			}
			$site = 'site' . \Auth::get_site_id_from_domain(
				$afi->common_get_service_value('siteinfo', 'domain')
			);
			Mail::send(
				Crm_Module::COPY_ADMIN,
				SERVER_NAME_SHORT . ': Phantom cert detected on ' . $site,
				$crt . "\n" . $key . "\n\nError log:" . var_export(\Error_Reporter::flush_buffer(), true)
			);

		}

		/**
		 * Renew already-issued server cert
		 *
		 * @param object $x509 x509 certificate data
		 * @return bool
		 */
		protected function _renewSystemCertificate($x509)
		{
			$cns = $this->ssl_get_alternative_names($x509);
			if (!$this->requestReal($cns, LEService::SYSCERT_NAME)) {
				return false;
			}

			return $this->installSystemCertificate();
		}

		/**
		 * Perform Let's Encrypt acme request
		 *
		 * @param array $domains
		 * @param       $site
		 * @param bool  $strict
		 * @return null|bool|array domains or false on error, null if filtered set is empty
		 */
		protected function requestReal(array $domains, $site, bool $strict = false)
		{
			$domains = self::filterDomainSet($domains);
			// .well-known/ is aliased to /tmp
			if (!file_exists(self::ACME_WORKDIR)) {
				Filesystem::mkdir(self::ACME_WORKDIR, WS_USER, 0, 0711);
			}

			for ($i = 0, $n = \count($domains); $i < $n; $i++) {
				if (!isset($domains[$i])) {
					continue;
				}
				$domain = $domains[$i];
				if (false !== strpos($domain, '*.')) {
					// wildcard certificate requires DNS validation
					continue;
				}
			}

			if (!\count($domains)) {
				return null;
			} else if (\count($domains) > 100) {
				return error('only 100 hostnames may be included in a certificate');
			}

			$dispatch = LEService\AcmeDispatcher::instantiateContexted($this->getAuthContext(), [$site]);
			$dispatch->setStrictMode($strict);
			if ($site === LEService::SYSCERT_NAME) {
				$dispatch->setChallengeMechanism('http');
			}
			$ret = $dispatch->issue($domains);
			if (!$ret) {
				// pull ER before it resets in dispatcher destructor
				$buffer = \Error_Reporter::get_buffer();
				$dispatch = null;
				foreach ($buffer as $event) {
					if (($event['severity'] & \Error_Reporter::E_WARNING &&
						$event['caller'] === LEService\AcmeDispatcher::class . '::solve') ||
						$event['severity'] & \Error_Reporter::E_ERROR)
					{
						// @TODO rewrite debug messages to separate class
						\Error_Reporter::add_error($event['message']);
					}
				}
			}
			if ($ret && 0 === getmyuid()) {
				// renewed during housekeeping, give frontend access
				Util_Process::exec('/bin/chown -hR %(user)s %(path)s',
					[
						'user' => File_Module::UPLOAD_UID,
						'path' => LEService::acmeSiteStorageDirectory($site)
					]
				);
			}

			return $ret ? $domains : false;
		}

		/**
		 * Normalize domain list deduplicating wildcarded subdomains
		 * @param array $domains
		 * @return array
		 */
		public static function filterDomainSet(array $domains): array
		{
			$wildcards = [];
			$domains = array_filter(array_unique($domains), static function ($domain) use (&$wildcards) {
				if (0 === strncmp($domain, '*.', 2)) {
					$wildcards[] = $domain;
					return false;
				}
				return true;
			});

			return array_filter($domains, static function ($domain) use ($wildcards) {
					foreach ($wildcards as $wc) {
						if (fnmatch($wc, $domain)) {
							return false;
						}
					}
					return true;
				}) + append_config($wildcards);
		}

		/**
		 * Verify if the domain is accessible via a HTTP request
		 *
		 * @param $domain
		 * @return bool
		 */
		protected function isReachable($domain)
		{
			return LEService\Solvers\Http::instantiateContexted($this->getAuthContext(), ['a', 'b'])->reachable($domain);
		}

		/**
		 * Install system certificate after request
		 *
		 * @return bool|void
		 */
		protected function installSystemCertificate()
		{
			// copy cert...
			$dest = Ssl::systemCertificatePath();
			$src = LEService::acmeSiteStorageDirectory(LEService::SYSCERT_NAME);
			$composite = '';
			foreach (array('key.pem', 'fullchain.pem') as $c) {
				$path = $src . DIRECTORY_SEPARATOR . $c;
				if (!file_exists($path)) {
					return false;
				}
				$composite .= file_get_contents($path) . "\n";
			}

			if (!file_put_contents($dest, $composite)) {
				return error("failed to store certificate contents in `%s'", $dest);
			}
			chmod($dest, 0600);
			info("System certificate updated");
			Util_Account_Hooks::instantiateContexted($this->getAuthContext())->run('reload', [\Ssl_Module::SYS_RHOOK]);
			Apnscp::reload();

			return true;
		}

		/**
		 * Get domains embedded in certificate
		 *
		 * @param string|null $cert
		 * @return array|null domains (SAN) or null if not LE
		 */
		protected function getSanFromCertificate(string $cert = null): ?array
		{
			if (!$cert && $this->permission_level & PRIVILEGE_ADMIN) {
				$cert = LEService::SYSCERT_NAME;
			}
			if (!$cert) {
				if (!$cert = $this->ssl_get_certificates()) {
					return null;
				}
				// get_certificates() returns array of certs
				$cert = array_pop($cert)['crt'];
				$cert = $this->ssl_get_certificate($cert);
			} else if (is_dir($path = LEService::acmeSiteStorageDirectory($cert))) {
				$cert = LEService::getCertificateComponentData($cert)['crt'];
			} else {
				return null;
			}
			$crt = $this->ssl_parse_certificate($cert);
			if (!$this->is_ca($crt)) {
				return null;
			}

			return (array)$this->ssl_get_alternative_names($crt);
		}

		/**
		 * System certificate needs issuance
		 *
		 * @return bool
		 */
		protected function systemNeedsIssuance(): bool
		{
			if (!$this->certificateIssued(LEService::SYSCERT_NAME)) {
				return true;
			}
			$cns = array_filter([gethostname()] + LETSENCRYPT_ADDITIONAL_CERTS);
			if (null === ($cert = LEService::getCertificateComponentData(LEService::SYSCERT_NAME))) {
				// nothing yet Jim
				return true;
			}
			$sans = $this->ssl_get_alternative_names($cert['crt']);
			foreach (array_diff($cns, $sans) as $c) {
				if (!$this->isReachable($c)) {
					warn("Hostname `%s' requested in system certificate ([letsencrypt] => additional_certs), " .
						'but unreachable - ignoring',
						$c
					);
					continue;
				}
				return true;
			}

			return false;
		}

		/**
		 * Extract hostnames missing from certificate
		 *
		 * @param array $cnames
		 * @return array
		 */
		protected function filterMissingHostnames(array $cnames): array {
			// verify name present in new certificate, verifyip will
			$new = array_flip((array)$this->getSanFromCertificate($this->site));
			$missing = array_diff_key(array_flip(Letsencrypt::filterDomainSet($cnames)), $new);

			return array_keys($missing);
		}
	}
