<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Cache_Account extends Cache_Base
	{
		static $key;

		public static function reset_prefix()
		{
			self::set_key('L-' . static::role_site_id() . ':');
		}

		public static function spawn(\Auth_Info_User $context = null): \Redis
		{
			if (null === $context) {
				$context = \Auth::profile();
			}

			return parent::spawn($context); // TODO: Change the autogenerated stub
		}
	}