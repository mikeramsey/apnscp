<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_CLI extends Auth
	{
		const DB_SYMBOL = 'cli';
		const TV_SEC = 1800;

		public function __construct()
		{
			if (!headers_sent()) {
				apnscpSession::disable_session_header();
			}

			parent::__construct();
		}


		protected static function _log_session()
		{
			return true;
		}

		public function authenticate()
		{
			[$username, $domain] = self::_get_login();
			$password = self::_getParam('password');
			$this->resetID(\apnscpSession::init()->create_sid());
			if (!$this->initializeUser($username, $domain, $password)) {
				fatal("failed to authenticate user `%s'", $username);
			}
			$_SESSION['valid'] = true;

			self::_save_auth_ttl($username, $domain);
			 // CLI has no session support
			// init() unless forced resumes with session_id() that holds the unauthenticated obj
			\apnscpFunctionInterceptor::expire(\session_id());

			return true;
		}

		/**
		 * Grab user and domain from environment
		 *
		 * @return array  username, domain component
		 */
		private static function _get_login()
		{
			$user = self::_getParam('username');
			if (!$user) {
				$uid = posix_getuid();
				$pwd = posix_getpwuid($uid);
				$user = Auth::get_admin_login() ?: $pwd['name'];
			}

			[$user, $domain] = self::_parse_login($user);
			if (!$domain) {
				$domain = self::_getParam('domain');
			}
			if (ctype_digit($user) && !posix_getuid()) {
				if ($domain) {
					$site_id = self::get_site_id_from_domain($domain);
					if (!$site_id) {
						fatal("cannot lookup domain `%s'", $domain);
					}
					$path = self::get_domain_path('site' . $site_id);
				} else {
					$path = '';
				}
				$path .= '/etc/passwd';
				$pwd = file($path);
				if (!$pwd) {
					fatal('cannot open /etc/passwd');
				}
				foreach ($pwd as $entry) {
					if (!preg_match(Regex::PASSWD_USER_ENTRY, $entry, $matches)) {
						continue;
					}
					if ($matches[2] === $user) {
						$user = $matches[1];
						break;
					}
				}
			}
			$_ENV['USERNAME'] = $user;

			return array($user, $domain);
		}

		private static function _getParam($param)
		{
			$param = strtoupper($param);

			return $_ENV[$param] ?? null;
		}

		private static function _save_auth_ttl(string $user, ?string $domain): void
		{
			$site_id = null;
			if ($domain && !($site_id = (int)self::get_site_id_from_domain($domain))) {
				fatal("cannot save auth - unknown domain `%s'", $domain);
			}

			$data = array(
				'id'   => $site_id,
				'stat' => self::_stat_check($site_id),
				'ppid' => self::_get_ppid(),
				'ttl'  => $_SERVER['REQUEST_TIME']
			);
			self::_update_auth_ttl($user, $site_id, $data);
		}

		/**
		 * @param int|null $site_id
		 * @param int|null $ttl
		 * @return bool|int
		 */
		private static function _stat_check(?int $site_id, int $ttl = null)
		{
			$file = '/etc/shadow';
			if ($site_id) {
				$file = FILESYSTEM_VIRTBASE . '/site' .
					$site_id . '/fst/etc/shadow';
			}
			if (!$ttl) {
				return filemtime($file);
			}

			return $ttl === filemtime($file);
		}

		private static function _get_ppid()
		{
			return posix_getppid();
		}

		private static function _update_auth_ttl(string $user, ?int $site_id, $data): bool
		{
			$data['ttl'] = $_SERVER['REQUEST_TIME'];
			Cache_Global::spawn()->set('cli_ttl:' . $site_id . ':' . $user, $data, 3600);

			return true;
		}

		/**
		 * Verify credentials
		 *
		 * @param $username
		 * @param $password
		 * @param $domain
		 * @return bool
		 */
		public function verify($username, $password, $domain)
		{
			if ($this->_isRoot()) {
				return true;
			} // root

			return parent::verify($username, $password, $domain);
		}

		private function _isRoot(): bool
		{
			$uid = posix_getuid();
			return $uid === 0 || $uid === posix_getpwnam(APNSCP_SYSTEM_USER)['uid'] ?? false;
		}

		public function get_user($domain)
		{
			if (!$this->_isRoot()) {
				return false;
			}

			return self::get_admin_from_site_id(self::get_site_id_from_anything($domain));
		}

		public function get_site_id($domain)
		{
			if (!$this->_isRoot()) {
				return false;
			}

			return self::get_site_id_from_domain($domain);
		}

		public function get_domain($site)
		{

			if (!$this->_isRoot()) {
				return false;
			}
			if (0 === strncmp($site, "site", 4) &&
				ctype_digit(substr($site, 4))
			) {
				return self::get_domain_from_site_id(substr($site, 4));
			}

			if (self::domain_exists($site)) {
				return $site;
			}

			return false;
		}

		public function unauthorized()
		{
			fwrite(STDERR, "Unauthorized\n");

			return false;
		}

		public function getInvoker()
		{
			return apnscpFunctionInterceptor::factory($this->getProfile());
		}

		public static function authenticated()
		{
			return !empty($_SESSION['username']);
		}

		/**
		 * Verify session is active
		 */
		public function session_valid()
		{
			list ($user, $domain) = self::_get_login();
			if (!$user) {
				return false;
			}

			$site_id = Auth::get_site_id_from_domain($domain);

			// do a cache fetch routine...
			if (!$this->_check_auth_ttl($user, $site_id) ||
				!$this->_resume_session($user, $site_id))
			{
				$this->invalidate_session();
				return false;
			}

			return true;
		}

		private function _check_auth_ttl($user, ?int $site_id)
		{
			/**
			 * Until resumption can be figured out...
			 */
			// ephemeral program randomizes mmap location
			$data = Cache_Global::spawn()->get('cli_ttl:' . $site_id . ':' . $user);
			if (!isset($data['ttl'])) {
				return false;
			}
			if ($_SERVER['REQUEST_TIME'] - $data['ttl'] > self::TV_SEC || self::_get_ppid() !== $data['ppid']) {
				return false;
			}
			return self::_stat_check($data['id'], $data['stat']) &&
				self::_update_auth_ttl($user, $site_id, $data);
		}

		private function _resume_session(string $user, ?int $site_id)
		{
			// session corruption errors
			return false;

			$db = \apnscpSession::db();
			$site_id = (int)$site_id;
			$q = $db->query("SELECT session_id FROM " . \apnscpSession::TABLE . " WHERE username = " .
				$db->quote($user) . " " .
				' AND site_id ' . ($site_id ? ' = ' . $site_id : 'IS NULL ') .
				' AND last_action >= NOW() - INTERVAL \'' . self::TV_SEC . ' SECONDS\' ' .
				" AND login_method = '" . self::DB_SYMBOL . "'");
			if ($q->rowCount() < 1) {
				return false;
			}
			$rs = $q->fetchObject();

			$session_id = $rs->session_id;

			if (!apnscpSession::restore_from_id($session_id)) {
				return false;
			}

			$this->resetID($session_id);

			if (\Auth::profile()->id !== $session_id) {
				fatal("BUG: Session mismatch. Expected %s got %s", $session_id, \Auth::profile()->id);
			}

			$this->sessionFromContext(\Auth::profile());
			return true;
		}

		public function invalidate_session(): bool
		{
			$auth = $this->authInfo();
			Cache_Global::spawn()->del('cli_ttl:' . $auth->site_id . ':' . $auth->username);
			return parent::invalidate_session();
		}
	}
