<?php

	use Opcenter\Auth\Password;
	use Opcenter\Http\Apnscp;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_UI extends Auth
	{
		public const CSRF_KEY = \Lararia\Http\Kernel::CSRF_KEY;

		/**
		 * @var string separate cookie for CSRF storage
		 * cookie purged after set if "_token"
		 */
		public const CSRF_COOKIE = 'csrf-token';
		public const DB_SYMBOL = 'http';
		protected $username;
		protected $domain;

		public function __construct()
		{
			parent::__construct();
			if (!Session::exists(self::CSRF_KEY)) {
				$this->generateCsrf();
			}
		}

		/**
		 * Generate CSRF token
		 */
		protected function generateCsrf(): void {
			if (!IS_ISAPI) {
				// ignore generating CSRF if it cannot be delivered to browser
				return;
			}

			$csrf = Opcenter\Auth\Password::generate(40);
			$params = [
				self::CSRF_COOKIE     => $csrf,
			];
			foreach ($params as $cookie => $value) {
				setcookie($cookie, $value, 0, '/', null, Util_HTTP::isSecure(), true);
				$_COOKIE[$cookie] = $value;
			}
			Session::set(self::CSRF_KEY, $csrf);
		}

		public function resetID(string $id): ?Auth
		{
			if (null !== ($ret = parent::resetID($id))) {
				$this->generateCsrf();
			}
			return $ret;
		}

		/**
		 * Begin impersonation
		 *
		 * @param Auth_Info_User $ctx
		 * @param string|null    $gate
		 */
		public function beginImpersonation(Auth_Info_User $ctx, string $gate = null): void
		{
			static::_save_pageview(false);
		}

		public function endImpersonation(Auth_Info_User $ctx): void
		{
			static::_restore_pageview();
		}


		/**
		 * Restore session postback
		 *
		 * Called by Page_Container::create()
		 *
		 * @return bool
		 */
		public function restore_postback()
		{
			$payload = \Session::get('last_view.payload', null);
			if (!$payload) {
				return false;
			}
			$tmp = sys_get_temp_dir() . '/';
			if (0 === strpos($payload, $tmp) && file_exists($payload)) {
				$_POST = (array)Util_PHP::unserialize(file_get_contents($payload));
				file_exists($payload) && unlink($payload);
			}
			\Session::forget('last_view');

			return true;
		}

		public function authenticate()
		{
			if (isset($_SERVER['argv'])) {
				return 1;
			}
			if (self::public_page()) {
				return true;
			}
			$username = '';
			$password = '';
			$domain = '';

			if (isset($_GET['username'], $_GET['password'])) {
				$username = $_GET['username'];
				$password = $_GET['password'];
				$domain = isset($_GET['domain']) ?
					$_GET['domain'] : '';
			}
			$logout = isset($_COOKIE['autologout']) && $_COOKIE['autologout'];
			if (!$username || !$password) {
				return false;
			}

			return $this->login($username, $domain, $password, $logout);
		}

		public static function public_page()
		{
			$url = $_SERVER['REDIRECT_URL'] ?? $_SERVER['REQUEST_URI'];

			return NO_AUTH || 0 !== strpos($url, Template_Engine::BASE_DIR);
		}

		public function login(&$username, &$domain, $password, $autologout = true)
		{
			Auth_Anvil::anvil();
			$last_view = \Session::get('last_view');
			\session_regenerate_id(true);
			\Auth::autoload()->setID(\session_id());
			\Auth::autoload()->authInfo(true);

			if ('' === $username) {
				error('missing username');
			} else if (!preg_match(Regex::USERNAME, $username)) {
				error('invalid username');
			}

			if ($domain) {
				/** again, morons... */
				if (0 === strncmp($domain, 'www.', 4)) {
					$domain = substr($domain, 4);
				}
				if (!preg_match(Regex::DOMAIN, $domain)) {
					return error('invalid domain');
				}

				if (!Auth::domain_exists($domain) && !Auth_Redirect::redirect($domain)) {
					return error("Unknown domain - domain `%s' is not hosted on any server.", $domain);
				}
			}

			if (!$password) {
				error('missing password');
			}

			$this->username = $username;
			$this->domain = $domain;
			if (Error_Reporter::is_error()) {
				return false;
			}

			if (!$this->initializeUser($username, $domain, $password, $autologout)) {
				return $this->login_error();
			}
			Auth_Anvil::remove();
			// save password for use with 1-click logins
			$_SESSION['password'] = AUTH_RETAIN_UI_PASSWORD ? (new \Opcenter\Crypto\NaiveCrypt($password)) : null;

			$afi = apnscpFunctionInterceptor::init(true);
			assert($afi->context_matches_id(session_id()));
			$afi->set_session_id(session_id());
			if ($afi->auth_is_inactive()) {
				Auth_Redirect::redirect($domain);
			}

			\Session::set('last_view', $last_view);
			$this->log_login(1);
			return $this->login_success();
		}

		protected function initializeUser(
			string $username,
			?string $domain,
			?string $password,
			?int $timeout = self::TV_SEC
		): ?bool {
			if (!parent::initializeUser($username, $domain, $password,
				$timeout)) {
				return false;
			}

			$this->generateCsrf();
			return true;
		}


		public function login_success()
		{
			$location = Template_Engine::instantiateContexted($this->getProfile())->getEntryLocation();
			header('Location: ' . $location, true, 302);
			$this->setAccessKey();
			return true;
		}

		public function login_error()
		{
			return error('invalid username or password');
		}

		/**
		 * Log login request
		 *
		 * @param int $status status of the request: 0,1
		 * @return bool
		 */
		public function log_login($status): bool
		{
			$username = $this->username;
			$domain = $this->domain;
			$ip = sprintf('%u', ip2long(self::client_ip()));
			$db = self::get_db();
			$db->query('INSERT INTO `login_log`
            (`id`,
            `ip`,
            `login_date`,
            `domain`,
            `username`)
            VALUES
            (NULL,
            ' . $ip . ",
            NOW(),
            '" . $domain . "',
            '" . $username . "'
        );");

			return true;
		}

		private static function _restore_pageview()
		{
			if ( !($last = \Session::get('last_view')) )  {
				return false;
			}
			$last = \Session::get('last_view');
			\Session::forget('last_view');
			if ( !($url = array_get($last, 'url')) ) {
				return false;
			}

			header('Location: ' . $url, true, 303);
			return true;
		}

		public function unauthorized()
		{
			if (defined('AJAX') && AJAX) {
				http_response_code(403);
				\Auth_Anvil::anvil();
				return false;
			}
			self::_need_login() && \Auth_Anvil::anvil() && self::_redirect_login() || $this->login_error();

			return false;
		}

		private static function _need_login()
		{
			return !NO_AUTH && !self::public_page();
		}

		private static function _redirect_login()
		{
			self::_save_pageview();
			apnscpSession::init()->write(session_id(), session_encode());
			header('Location: /apps/login', true, 302);
			exit;
		}

		private static function _save_pageview(bool $saveAction = true)
		{
			// don't overwrite last_view...
			if (\Session::exists('last_view')) {
				//return false;
			}
			$method = strtolower($_SERVER['REQUEST_METHOD']);
			if ($method == 'post' && $saveAction) {
				$payload = tempnam(sys_get_temp_dir(), 'pl');
				file_put_contents($payload, serialize($_POST));
			} else {
				$payload = '';
			}
			$payload = [
				'url'     => $_SERVER['REQUEST_URI'],
				'payload' => $payload,
				'method'  => $method
			];
			\Session::set('last_view', $payload);
			return true;
		}

		/**
		 * Session initialization hook
		 *
		 * @return bool
		 */
		public function init() {
			if (($_COOKIE[Apnscp::SECKEY_COOKIE] ?? null) !== Apnscp::secureAccessKey()) {
				$this->setAccessKey();
			}
			return true;
		}

		public function session_valid()
		{
			if (!\Session::get('valid')) {
				return false;
			}

			$oldts = \Session::get('last_action', 0);
			$newts = $_SERVER['REQUEST_TIME'];
			\Session::set('last_action', $newts);

			return \Session::get('auth_timeout', 1) === -1 ||
				($newts - $oldts) < self::TV_SEC;
		}

		public function verify($username, $password, $domain)
		{
			return parent::verify($username, $password, $domain) &&
				DataStream::get()->setOption(apnscpObject::RESET)->query('auth_user_permitted', $username, 'cp');

		}

		/**
		 * Set access key for static resources
		 */
		private function setAccessKey(): void
		{
			setcookie(Apnscp::SECKEY_COOKIE, Apnscp::secureAccessKey(), 0, '/', null, Util_HTTP::isSecure(), true);
			$_COOKIE[Apnscp::SECKEY_COOKIE] = Apnscp::secureAccessKey();
		}
	}