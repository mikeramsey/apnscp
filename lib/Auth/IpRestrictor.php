<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */

namespace Auth;

use Opcenter\Contracts\VirtualizedContextable;
use Opcenter\Net\IpCommon;
use Opcenter\Service\ServiceValidator;
use Opcenter\Service\Validators\Auth\Iprestrict;

/**
 * Class IpRestrictor
 *
 * Restrict logins to IP ranges
 *
 * @package Auth
 */
class IpRestrictor implements VirtualizedContextable {
	use \AccountInfoTrait;
	use \apnscpFunctionInterceptorTrait;

	// IP-based login restrictions
	const IP_RESTRICTION_PREF = 'auth.ip-limit';

	// @var int maximum number of entries, DoS deterrence
	const MODULE_HARD_LIMIT = 50;

	/**
	 * @var \apnscpFunctionInterceptor
	 */
	private $afi;

	private function __construct()
	{

	}

	/**
	 * Remove IP restriction
	 *
	 * @param string      $ip IPv4, IPv6, or CIDR
	 * @param string|null $gate optional authentication gate
	 * @return bool address removed
	 */
	public function remove(string $ip, string $gate = null): bool
	{
		$authorized = $this->list();
		$ip = $this->normalize($ip);
		if (!\array_key_exists($ip, $authorized)) {
			return false;
		}

		if (\count($authorized) > 1 && IpCommon::is(\Auth::client_ip(), $ip)) {
			// permit removal of last entry
			return error("Cannot deauthorize address/range `%s' which contains active login address `%s'",
				$ip, \Auth::client_ip()
			);
		}
		if ($gate === null) {
			unset($authorized[$ip]);
		} else if (false !== ($pos = array_search($gate, $authorized[$ip], true))) {
			warn("IP `%s' is not authorized for gate `%s'", $ip, $gate);
			return false;
		} else {
			unset($authorized[$ip][$pos]);
		}
		$prefs = \Preferences::factory($this->getAuthContext());
		$prefs->unlock($this->getApnscpFunctionInterceptor());
		array_set($prefs, static::IP_RESTRICTION_PREF, $authorized);
		$prefs->sync();

		return true;
	}

	/**
	 * Add IP restriction
	 *
	 * @param string      $ip IPv4, IPv6, or CIDR
	 * @param string|null $gate authentication gate
	 * @return bool
	 */
	public function add(string $ip, string $gate = null): bool
	{
		if ($gate && !\Auth::driver_exists($gate)) {
			return error("Unknown auth gate `%s'", $gate);
		}
		if (!IpCommon::valid($ip)) {
			return error("Invalid address or CIDR range `%s'", $ip);
		}
		$authorized = $this->list();
		if (!$authorized && !IpCommon::is(\Auth::client_ip(), $ip)) {
			info("Implicit rule to permit access to `%s' added", \Auth::client_ip());
			$authorized[\Auth::client_ip()] = null;
		}
		// normalize
		$ip = $this->normalize($ip);
		$gates = \array_key_exists($ip, $authorized) ? $authorized[$ip] : [];

		if ($gate !== null) {
			if (!$this->gateValid($gate)) {
				return error("Unknown authentication gate `%s'", $gate);
			}
			if ($gates === null) {
				return warn("Ignoring request. Address `%s' already permitted by virtue " .
					'of undefined gate - delete IP restriction then set individual services');
			}
			$authorized[$ip] = array_values(array_unique($gates + append_config([$gate])));
		} else {
			$authorized[$ip] = null;
		}
		$limit = $this->getLimit();
		if (\count($authorized) > $limit) {
			return error('IP restriction exceeds server-configured limit of %d', $limit);
		}
		$prefs = \Preferences::factory($this->getAuthContext());
		$prefs->unlock($this->getApnscpFunctionInterceptor());
		array_set($prefs, static::IP_RESTRICTION_PREF, $authorized);
		$prefs->sync();

		return true;
	}

	/**
	 * Get IP restriction list
	 *
	 * @return array
	 */
	public function list(): array
	{
		// NB: call common:load-preferences - indirect access via Preferences usage
		// will be blocked prior to first login. Full authentication is necessary for \Preferences to boot
		return array_get($this->getApnscpFunctionInterceptor()->common_load_preferences(), static::IP_RESTRICTION_PREF, []);
	}

	/**
	 * Verify if address may access account
	 *
	 * @param string      $loginIp login IP
	 * @param string|null $loginGate gate login request occurred within
	 * @return bool
	 */
	public function authorized(string $loginIp, string $loginGate = null): bool
	{
		$ips = $this->list();
		if (empty($ips)) {
			// no restrictions set
			return true;
		}
		if (!IpCommon::valid($loginIp)) {
			// IP garbled?
			report('Garbled IP in authorization check: %s', $loginIp);
			return true;
		}

		foreach (ANVIL_WHITELIST as $whitelist) {
			if (IpCommon::is($loginIp, $whitelist)) {
				return true;
			}
		}

		foreach ($ips as $ip => $gates) {
			if ($loginGate && null !== $gates && !\in_array($loginGate, $gates, true)) {
				continue;
			}

			if (IpCommon::is($loginIp, $ip)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Authentication gate known
	 *
	 * @param string $gate
	 * @return bool
	 */
	private function gateValid(string $gate): bool
	{
		return \Auth::driver_exists($gate);
	}

	/**
	 * Normalize IPv4/IPv6 address
	 *
	 * @param string $ip
	 * @return string
	 */
	private function normalize(string $ip): string
	{
		return strtolower($ip);
	}

	/**
	 * Get maximum number of restrictions
	 *
	 * @return int
	 */
	public function getLimit(): int
	{
		$svc = explode('.', ServiceValidator::configize(Iprestrict::class));
		$limit = $this->getConfig($svc[0], $svc[1]) ?? static::MODULE_HARD_LIMIT;
		if ($limit === -1) {
			return static::MODULE_HARD_LIMIT;
		}
		return (int)$limit;
	}

}